Opened connection at 6/15/2021 6:16:57 PM -04:00
Started transaction at 6/15/2021 6:16:57 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @0, @1, @2, @3, @4, @5, @6, NULL, NULL, NULL, @7, NULL, NULL, @8, NULL, NULL, NULL, @9, NULL, NULL, NULL, NULL, @10, NULL, NULL, @11, @12, @13, NULL, NULL, NULL, @14)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'True' (Type = Boolean)
-- @1: 'False' (Type = Boolean)
-- @2: '6/15/2021 6:16:57 PM' (Type = DateTime2)
-- @3: '6/15/2021 6:16:57 PM' (Type = DateTime2)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '0' (Type = Int32)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '2291602d-5e22-4b6b-b391-85293a7a7e47' (Type = Guid)
-- @11: 'False' (Type = Boolean)
-- @12: 'John' (Type = String, Size = 50)
-- @13: 'Murrin' (Type = String, Size = 50)
-- @14: 'John  Murrin' (Type = String, Size = 255)
-- Executing at 6/15/2021 6:16:57 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 6:16:57 PM' (Type = DateTime2)
-- @4: '6/15/2021 6:16:57 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116587' (Type = Int32)
-- @7: '116587' (Type = Int32)
-- @8: '525097' (Type = Int64)
-- @9: '3274924' (Type = Int64)
-- @10: 'e3789bba-e66b-43f4-8df0-3b95ee00f0a2' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '137' (Type = Int32)
-- @20: '137 - John  Murrin' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 63 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, @3, @4, @5, @6, @7, @8, @9, @10, NULL, @11, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '7045 Los Santos Dr. Ste 100' (Type = String, Size = 100)
-- @2: 'Long Beach' (Type = String, Size = 50)
-- @3: '90815' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '6/15/2021 6:16:57 PM' (Type = DateTime2)
-- @7: '6/15/2021 6:16:57 PM' (Type = DateTime2)
-- @8: '116587' (Type = Int32)
-- @9: '116587' (Type = Int32)
-- @10: '3615640' (Type = Int64)
-- @11: '525097' (Type = Int64)
-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3615640' (Type = Int64)
-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, NULL, @2, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '69bde49f-fd98-48f7-b666-c526970799e6' (Type = Guid)
-- @1: '169419' (Type = Int64)
-- @2: '137' (Type = Decimal, Precision = 18, Scale = 2)
-- @3: '3274924' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '1f42de4b-a7c8-4bc7-ad3b-c728ad745d4c' (Type = Guid)
-- @1: '169421' (Type = Int64)
-- @2: '(562)342-3011' (Type = String, Size = -1)
-- @3: '3274924' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '4277d479-fd40-4c13-a71f-159ba607aafa' (Type = Guid)
-- @1: '169423' (Type = Int64)
-- @2: '(562)724-7007' (Type = String, Size = -1)
-- @3: '3274924' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '61ea396f-a572-4c45-90c3-af4f93d8b401' (Type = Guid)
-- @1: '169424' (Type = Int64)
-- @2: '(952)994-1066' (Type = String, Size = -1)
-- @3: '3274924' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:16:58 PM -04:00
Closed connection at 6/15/2021 6:16:58 PM -04:00
Opened connection at 6/15/2021 6:16:58 PM -04:00
Started transaction at 6/15/2021 6:16:58 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5040933,
	ShippingAddressId = 5040934
WHERE Id = 3615640;

UPDATE Contacts SET
	AccountId = 3615640
WHERE Id = 3274924;
INSERT INTO TagAccounts VALUES(161920, 3615640);

-- Executing at 6/15/2021 6:16:58 PM -04:00
-- Completed in 56 ms with result: 3

Committed transaction at 6/15/2021 6:16:58 PM -04:00
Closed connection at 6/15/2021 6:16:58 PM -04:00
