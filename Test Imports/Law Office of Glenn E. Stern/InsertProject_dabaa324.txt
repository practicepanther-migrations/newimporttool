Opened connection at 6/15/2021 6:14:27 PM -04:00
Started transaction at 6/15/2021 6:14:27 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, @14, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, NULL, NULL, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '(Appellate) Shawn Wood v Sparks' (Type = String, Size = 800)
-- @1: '6/15/2021 6:14:27 PM' (Type = DateTime2)
-- @2: '6/15/2021 6:14:27 PM' (Type = DateTime2)
-- @3: '3615535' (Type = Int64)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'dabaa324-af38-4033-9197-f36d2f365074' (Type = Guid)
-- @14: 'UC1: Matthew Malczynski 

UC2: Dolores Larrabee' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: '1192' (Type = Int32)
-- @17: '1192 - (Appellate) Shawn Wood v Sparks' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '4/26/2018 12:00:00 AM' (Type = DateTime2)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:27 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'f66727f8-3f99-4731-a94c-2a5e3d4cab19' (Type = Guid)
-- @1: '169433' (Type = Int64)
-- @2: '1553386' (Type = Int64)
-- @3: '4TH APPELLATE DI' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:27 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '3ca93a87-01ff-4ed0-9384-6e68bf871c5d' (Type = Guid)
-- @1: '169434' (Type = Int64)
-- @2: '1553386' (Type = Int64)
-- @3: 'G056181' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:27 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '83c9a06a-cf68-45c6-9b83-d7346846ff76' (Type = Guid)
-- @1: '169435' (Type = Int64)
-- @2: '1553386' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:27 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '8d18ffe0-a377-472c-9e4b-71352149a947' (Type = Guid)
-- @1: '169436' (Type = Int64)
-- @2: '1553386' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:27 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'ecb75a4a-ba88-418d-bcd8-e185636c8f1c' (Type = Guid)
-- @1: 'APP' (Type = String, Size = 50)
-- @2: '525097' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '6/15/2021 6:14:27 PM' (Type = DateTime2)
-- @6: '6/15/2021 6:14:27 PM' (Type = DateTime2)
-- @7: '116587' (Type = Int32)
-- @8: '116587' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 6/15/2021 6:14:27 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1553386' (Type = Int64)
-- @1: '161947' (Type = Int64)
-- Executing at 6/15/2021 6:14:27 PM -04:00
-- Completed in 55 ms with result: 1

Committed transaction at 6/15/2021 6:14:27 PM -04:00
Closed connection at 6/15/2021 6:14:27 PM -04:00
Opened connection at 6/15/2021 6:14:27 PM -04:00
Started transaction at 6/15/2021 6:14:28 PM -04:00
INSERT INTO ProjectUsers VALUES(1553386, 116587);

-- Executing at 6/15/2021 6:14:28 PM -04:00
-- Completed in 55 ms with result: 1

Committed transaction at 6/15/2021 6:14:28 PM -04:00
Closed connection at 6/15/2021 6:14:28 PM -04:00
