Opened connection at 6/15/2021 6:15:08 PM -04:00
Started transaction at 6/15/2021 6:15:09 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @0, @1, @2, @3, @4, @5, @6, NULL, NULL, NULL, @7, NULL, NULL, @8, NULL, NULL, NULL, @9, NULL, NULL, NULL, NULL, @10, NULL, NULL, @11, @12, @13, NULL, NULL, NULL, @14)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'True' (Type = Boolean)
-- @1: 'False' (Type = Boolean)
-- @2: '6/15/2021 6:15:08 PM' (Type = DateTime2)
-- @3: '6/15/2021 6:15:08 PM' (Type = DateTime2)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '0' (Type = Int32)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: 'd4bf61dc-a8b4-4383-ac30-6a3b469f2812' (Type = Guid)
-- @11: 'False' (Type = Boolean)
-- @12: 'Aryel' (Type = String, Size = 50)
-- @13: 'Gilmer' (Type = String, Size = 50)
-- @14: 'Aryel  Gilmer' (Type = String, Size = 255)
-- Executing at 6/15/2021 6:15:09 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 6:15:08 PM' (Type = DateTime2)
-- @4: '6/15/2021 6:15:08 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116587' (Type = Int32)
-- @7: '116587' (Type = Int32)
-- @8: '525097' (Type = Int64)
-- @9: '3274847' (Type = Int64)
-- @10: '62d77e3a-b6c9-4f3c-955b-ae4fd2df3574' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '334' (Type = Int32)
-- @20: '334 - Aryel  Gilmer' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:09 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, NULL, @2, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '50e366b7-9220-4e26-83e8-f02d377a9e63' (Type = Guid)
-- @1: '169419' (Type = Int64)
-- @2: '334' (Type = Decimal, Precision = 18, Scale = 2)
-- @3: '3274847' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:09 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:15:09 PM -04:00
Closed connection at 6/15/2021 6:15:09 PM -04:00
Opened connection at 6/15/2021 6:15:09 PM -04:00
Started transaction at 6/15/2021 6:15:09 PM -04:00
UPDATE Contacts SET
	AccountId = 3615563
WHERE Id = 3274847;
INSERT INTO TagAccounts VALUES(161920, 3615563);
INSERT INTO TagAccounts VALUES(161927, 3615563);

-- Executing at 6/15/2021 6:15:09 PM -04:00
-- Completed in 56 ms with result: 3

Committed transaction at 6/15/2021 6:15:09 PM -04:00
Closed connection at 6/15/2021 6:15:09 PM -04:00
