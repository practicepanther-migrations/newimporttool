Opened connection at 6/15/2021 6:15:23 PM -04:00
Started transaction at 6/15/2021 6:15:23 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, NULL, @24, NULL, @25, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Robert and Ronald Roset' (Type = String, Size = 800)
-- @1: '6/15/2021 6:15:23 PM' (Type = DateTime2)
-- @2: '6/15/2021 6:15:23 PM' (Type = DateTime2)
-- @3: '3615574' (Type = Int64)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '42ae4ef4-eb4a-4ed6-973c-9d7e6c8f49be' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '1146' (Type = Int32)
-- @16: '1146 - Robert and Ronald Roset' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '8/18/2017 12:00:00 AM' (Type = DateTime2)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:23 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '10df63d2-797a-49dd-8a59-a2f0d04df44c' (Type = Guid)
-- @1: '169435' (Type = Int64)
-- @2: '1553428' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:23 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '05059b00-4908-46a8-b3bc-7cfd8b91834e' (Type = Guid)
-- @1: '169436' (Type = Int64)
-- @2: '1553428' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:23 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:15:23 PM -04:00
Closed connection at 6/15/2021 6:15:23 PM -04:00
Opened connection at 6/15/2021 6:15:23 PM -04:00
Started transaction at 6/15/2021 6:15:23 PM -04:00
INSERT INTO ProjectUsers VALUES(1553428, 116587);
INSERT INTO TagProjects VALUES(161955, 1553428);

-- Executing at 6/15/2021 6:15:23 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 6/15/2021 6:15:23 PM -04:00
Closed connection at 6/15/2021 6:15:23 PM -04:00
