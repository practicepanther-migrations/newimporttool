Opened connection at 6/15/2021 6:16:20 PM -04:00
Started transaction at 6/15/2021 6:16:20 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, NULL, @24, NULL, @25, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Kevin Short and Marjorie Short v Pizza Factory, In' (Type = String, Size = 800)
-- @1: '6/15/2021 6:16:20 PM' (Type = DateTime2)
-- @2: '6/15/2021 6:16:20 PM' (Type = DateTime2)
-- @3: '3615614' (Type = Int64)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'cca89e1a-903b-4c3d-af93-98fbabd9d431' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '1043' (Type = Int32)
-- @16: '1043 - Kevin Short and Marjorie Short v Pizza Factory, In' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '3/23/2017 12:00:00 AM' (Type = DateTime2)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:20 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '3ba64978-f127-4abe-a085-8cc635bb775d' (Type = Guid)
-- @1: '169433' (Type = Int64)
-- @2: '1553478' (Type = Int64)
-- @3: 'LA TORRANCE' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:20 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'e6d2605d-b73f-4e89-98a1-2b6f460c63cd' (Type = Guid)
-- @1: '169434' (Type = Int64)
-- @2: '1553478' (Type = Int64)
-- @3: '10874' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:20 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'b0fb8386-eb38-4f67-b288-2bb680fdb1cd' (Type = Guid)
-- @1: '169435' (Type = Int64)
-- @2: '1553478' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:20 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'eff7467c-2cc4-4b6a-b82b-cc0374493bd0' (Type = Guid)
-- @1: '169436' (Type = Int64)
-- @2: '1553478' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:20 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:16:20 PM -04:00
Closed connection at 6/15/2021 6:16:20 PM -04:00
Opened connection at 6/15/2021 6:16:20 PM -04:00
Started transaction at 6/15/2021 6:16:20 PM -04:00
INSERT INTO ProjectUsers VALUES(1553478, 116587);
INSERT INTO TagProjects VALUES(161926, 1553478);

-- Executing at 6/15/2021 6:16:20 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 6/15/2021 6:16:20 PM -04:00
Closed connection at 6/15/2021 6:16:20 PM -04:00
