Opened connection at 6/15/2021 6:14:38 PM -04:00
Started transaction at 6/15/2021 6:14:39 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, @3, @4, @5, @6, @7, NULL, NULL, NULL, @8, NULL, NULL, @9, NULL, NULL, NULL, @10, NULL, NULL, NULL, NULL, @11, NULL, NULL, @12, @13, @14, NULL, @15, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'monicalopez@gmail.com' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 6:14:38 PM' (Type = DateTime2)
-- @4: '6/15/2021 6:14:38 PM' (Type = DateTime2)
-- @5: '116587' (Type = Int32)
-- @6: '116587' (Type = Int32)
-- @7: '525097' (Type = Int64)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '8099d9c2-c1bb-4120-92ba-6046107a2d49' (Type = Guid)
-- @12: 'False' (Type = Boolean)
-- @13: 'Monica' (Type = String, Size = 50)
-- @14: 'Lopez' (Type = String, Size = 50)
-- @15: 'F' (Type = String, Size = 50)
-- @16: 'Monica F Lopez' (Type = String, Size = 255)
-- Executing at 6/15/2021 6:14:39 PM -04:00
-- Completed in 64 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 6:14:38 PM' (Type = DateTime2)
-- @4: '6/15/2021 6:14:38 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116587' (Type = Int32)
-- @7: '116587' (Type = Int32)
-- @8: '525097' (Type = Int64)
-- @9: '3274827' (Type = Int64)
-- @10: '7d886b2f-4db9-4e98-b0f1-fee9c25e0dbc' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '353' (Type = Int32)
-- @20: '353 - Monica F Lopez' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:39 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '638 East Ada Avenue' (Type = String, Size = 100)
-- @2: 'Glendora' (Type = String, Size = 50)
-- @3: 'CA' (Type = String, Size = 50)
-- @4: '91741' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/15/2021 6:14:38 PM' (Type = DateTime2)
-- @8: '6/15/2021 6:14:38 PM' (Type = DateTime2)
-- @9: '116587' (Type = Int32)
-- @10: '116587' (Type = Int32)
-- @11: '3615543' (Type = Int64)
-- @12: '525097' (Type = Int64)
-- Executing at 6/15/2021 6:14:39 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3615543' (Type = Int64)
-- Executing at 6/15/2021 6:14:39 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, NULL, @2, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '39544a5d-8f11-4591-a2cd-30d1cb4b8961' (Type = Guid)
-- @1: '169419' (Type = Int64)
-- @2: '353' (Type = Decimal, Precision = 18, Scale = 2)
-- @3: '3274827' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:39 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '8925c0a4-3b2f-4ec3-b227-32568a6d8db8' (Type = Guid)
-- @1: '169424' (Type = Int64)
-- @2: '(626)250--5141' (Type = String, Size = -1)
-- @3: '3274827' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:39 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:14:39 PM -04:00
Closed connection at 6/15/2021 6:14:39 PM -04:00
Opened connection at 6/15/2021 6:14:39 PM -04:00
Started transaction at 6/15/2021 6:14:39 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5040773,
	ShippingAddressId = 5040774
WHERE Id = 3615543;

UPDATE Contacts SET
	AccountId = 3615543
WHERE Id = 3274827;
INSERT INTO TagAccounts VALUES(161920, 3615543);
INSERT INTO TagAccounts VALUES(161927, 3615543);

-- Executing at 6/15/2021 6:14:39 PM -04:00
-- Completed in 56 ms with result: 4

Committed transaction at 6/15/2021 6:14:39 PM -04:00
Closed connection at 6/15/2021 6:14:39 PM -04:00
