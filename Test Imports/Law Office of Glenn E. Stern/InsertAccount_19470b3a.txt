Opened connection at 6/15/2021 6:16:27 PM -04:00
Started transaction at 6/15/2021 6:16:27 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @0, @1, @2, @3, @4, @5, @6, NULL, NULL, NULL, @7, NULL, NULL, @8, NULL, NULL, NULL, @9, NULL, NULL, NULL, NULL, @10, NULL, NULL, @11, @12, @13, NULL, NULL, NULL, @14)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'True' (Type = Boolean)
-- @1: 'False' (Type = Boolean)
-- @2: '6/15/2021 6:16:27 PM' (Type = DateTime2)
-- @3: '6/15/2021 6:16:27 PM' (Type = DateTime2)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '0' (Type = Int32)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '5900ecec-22b6-4f08-9294-e6533b5c064e' (Type = Guid)
-- @11: 'False' (Type = Boolean)
-- @12: 'Yeros' (Type = String, Size = 50)
-- @13: 'Issagholian' (Type = String, Size = 50)
-- @14: 'Yeros  Issagholian' (Type = String, Size = 255)
-- Executing at 6/15/2021 6:16:27 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 6:16:27 PM' (Type = DateTime2)
-- @4: '6/15/2021 6:16:27 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116587' (Type = Int32)
-- @7: '116587' (Type = Int32)
-- @8: '525097' (Type = Int64)
-- @9: '3274903' (Type = Int64)
-- @10: '19470b3a-85df-4a26-8b51-0e9530d7d39b' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '282' (Type = Int32)
-- @20: '282 - Yeros  Issagholian' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:27 PM -04:00
-- Completed in 63 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, NULL, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '15154 San Jose Street' (Type = String, Size = 100)
-- @2: '2326 San Anita Avenue Apt D' (Type = String, Size = 100)
-- @3: 'Mission Hills' (Type = String, Size = 50)
-- @4: 'CA' (Type = String, Size = 50)
-- @5: '91345' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '6/15/2021 6:16:27 PM' (Type = DateTime2)
-- @9: '6/15/2021 6:16:27 PM' (Type = DateTime2)
-- @10: '116587' (Type = Int32)
-- @11: '116587' (Type = Int32)
-- @12: '3615619' (Type = Int64)
-- @13: '525097' (Type = Int64)
-- Executing at 6/15/2021 6:16:27 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3615619' (Type = Int64)
-- Executing at 6/15/2021 6:16:27 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, NULL, @2, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'b273c63c-2d79-4490-b835-5aed0068ad5b' (Type = Guid)
-- @1: '169419' (Type = Int64)
-- @2: '282' (Type = Decimal, Precision = 18, Scale = 2)
-- @3: '3274903' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:27 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9b7ac34a-de95-40c6-89b9-d2d0a55445ef' (Type = Guid)
-- @1: '169424' (Type = Int64)
-- @2: '(818)340-1927' (Type = String, Size = -1)
-- @3: '3274903' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:27 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:16:27 PM -04:00
Closed connection at 6/15/2021 6:16:27 PM -04:00
Opened connection at 6/15/2021 6:16:27 PM -04:00
Started transaction at 6/15/2021 6:16:27 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5040899,
	ShippingAddressId = 5040900
WHERE Id = 3615619;

UPDATE Contacts SET
	AccountId = 3615619
WHERE Id = 3274903;
INSERT INTO TagAccounts VALUES(161920, 3615619);
INSERT INTO TagAccounts VALUES(161927, 3615619);

-- Executing at 6/15/2021 6:16:27 PM -04:00
-- Completed in 57 ms with result: 4

Committed transaction at 6/15/2021 6:16:27 PM -04:00
Closed connection at 6/15/2021 6:16:27 PM -04:00
