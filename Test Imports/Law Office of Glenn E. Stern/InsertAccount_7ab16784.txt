Opened connection at 6/15/2021 6:15:46 PM -04:00
Started transaction at 6/15/2021 6:15:47 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, @3, @4, @5, @6, @7, NULL, NULL, NULL, @8, NULL, NULL, @9, NULL, NULL, NULL, @10, NULL, NULL, NULL, NULL, @11, NULL, NULL, @12, @13, @14, NULL, NULL, NULL, @15)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'jtaylor@quantumtelcom.com' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 6:15:46 PM' (Type = DateTime2)
-- @4: '6/15/2021 6:15:46 PM' (Type = DateTime2)
-- @5: '116587' (Type = Int32)
-- @6: '116587' (Type = Int32)
-- @7: '525097' (Type = Int64)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '70b9f9b1-4bd4-4419-aeec-34f67883b08c' (Type = Guid)
-- @12: 'False' (Type = Boolean)
-- @13: 'John' (Type = String, Size = 50)
-- @14: 'Taylor' (Type = String, Size = 50)
-- @15: 'John  Taylor' (Type = String, Size = 255)
-- Executing at 6/15/2021 6:15:47 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 6:15:46 PM' (Type = DateTime2)
-- @4: '6/15/2021 6:15:46 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116587' (Type = Int32)
-- @7: '116587' (Type = Int32)
-- @8: '525097' (Type = Int64)
-- @9: '3274875' (Type = Int64)
-- @10: '7ab16784-ca81-46dd-a9c3-a24b921b290d' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '308' (Type = Int32)
-- @20: '308 - John  Taylor' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:47 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '4539 Monogram Avenue' (Type = String, Size = 100)
-- @2: 'Lakewood' (Type = String, Size = 50)
-- @3: 'CA' (Type = String, Size = 50)
-- @4: '90713' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/15/2021 6:15:46 PM' (Type = DateTime2)
-- @8: '6/15/2021 6:15:46 PM' (Type = DateTime2)
-- @9: '116587' (Type = Int32)
-- @10: '116587' (Type = Int32)
-- @11: '3615591' (Type = Int64)
-- @12: '525097' (Type = Int64)
-- Executing at 6/15/2021 6:15:47 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3615591' (Type = Int64)
-- Executing at 6/15/2021 6:15:47 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, NULL, @2, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'b725e232-9d0c-4c21-987b-baf1275bd1ca' (Type = Guid)
-- @1: '169419' (Type = Int64)
-- @2: '308' (Type = Decimal, Precision = 18, Scale = 2)
-- @3: '3274875' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:47 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '786155f8-7f6d-46e8-97b6-e6b2d5b30bf1' (Type = Guid)
-- @1: '169422' (Type = Int64)
-- @2: '(760)403-4065' (Type = String, Size = -1)
-- @3: '3274875' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:15:47 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:15:47 PM -04:00
Closed connection at 6/15/2021 6:15:47 PM -04:00
Opened connection at 6/15/2021 6:15:47 PM -04:00
Started transaction at 6/15/2021 6:15:47 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5040855,
	ShippingAddressId = 5040856
WHERE Id = 3615591;

UPDATE Contacts SET
	AccountId = 3615591
WHERE Id = 3274875;
INSERT INTO TagAccounts VALUES(161920, 3615591);
INSERT INTO TagAccounts VALUES(161927, 3615591);

-- Executing at 6/15/2021 6:15:47 PM -04:00
-- Completed in 57 ms with result: 4

Committed transaction at 6/15/2021 6:15:47 PM -04:00
Closed connection at 6/15/2021 6:15:47 PM -04:00
