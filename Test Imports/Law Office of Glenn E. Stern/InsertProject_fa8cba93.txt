Opened connection at 6/15/2021 6:16:49 PM -04:00
Started transaction at 6/15/2021 6:16:49 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, @14, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, @25, NULL, @26, NULL, @27, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'James I. Nakano v Kamron O. King, et al.' (Type = String, Size = 800)
-- @1: '6/15/2021 6:16:49 PM' (Type = DateTime2)
-- @2: '6/15/2021 6:16:49 PM' (Type = DateTime2)
-- @3: '3615634' (Type = Int64)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'fa8cba93-01e7-4ffd-bb9a-f994ebdb2af2' (Type = Guid)
-- @14: 'UC1: Richard Coberly 

UC2: Dolores Larrabee' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: '1038' (Type = Int32)
-- @17: '1038 - James I. Nakano v Kamron O. King, et al.' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '2/6/2017 12:00:00 AM' (Type = DateTime2)
-- @25: '1/20/2017 12:00:00 AM' (Type = DateTime2)
-- @26: 'False' (Type = Boolean)
-- @27: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:49 PM -04:00
-- Completed in 61 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'a52db91a-a6a1-4137-8bc7-df98616238a0' (Type = Guid)
-- @1: '169433' (Type = Int64)
-- @2: '1553505' (Type = Int64)
-- @3: 'LA-POMONA SOUTH' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:49 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'c9a4c954-82c5-41c1-9602-ffbace9974af' (Type = Guid)
-- @1: '169434' (Type = Int64)
-- @2: '1553505' (Type = Int64)
-- @3: 'KC067340' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:49 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'd94dee27-b96d-44b6-8be1-71df5ff509a4' (Type = Guid)
-- @1: '169435' (Type = Int64)
-- @2: '1553505' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:49 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '5adbc65e-de90-4241-ac0f-f334d8ec2425' (Type = Guid)
-- @1: '169436' (Type = Int64)
-- @2: '1553505' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:16:49 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:16:49 PM -04:00
Closed connection at 6/15/2021 6:16:49 PM -04:00
Opened connection at 6/15/2021 6:16:49 PM -04:00
Started transaction at 6/15/2021 6:16:50 PM -04:00
INSERT INTO TagProjects VALUES(161926, 1553505);

-- Executing at 6/15/2021 6:16:50 PM -04:00
-- Completed in 55 ms with result: 1

Committed transaction at 6/15/2021 6:16:50 PM -04:00
Closed connection at 6/15/2021 6:16:50 PM -04:00
