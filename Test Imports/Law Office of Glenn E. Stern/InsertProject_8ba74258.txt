Opened connection at 6/15/2021 6:14:05 PM -04:00
Started transaction at 6/15/2021 6:14:05 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, @24, NULL, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Linda Thuy Linh Cao v Zimmermans' (Type = String, Size = 800)
-- @1: '6/15/2021 6:14:05 PM' (Type = DateTime2)
-- @2: '6/15/2021 6:14:05 PM' (Type = DateTime2)
-- @3: '3615517' (Type = Int64)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '8ba74258-a2ba-48f4-87ed-d00d7399ee64' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '1211' (Type = Int32)
-- @16: '1211 - Linda Thuy Linh Cao v Zimmermans' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '7/31/2018 12:00:00 AM' (Type = DateTime2)
-- @24: '7/1/2019 12:00:00 AM' (Type = DateTime2)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:05 PM -04:00
-- Completed in 76 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '54d2f909-c4c8-4c86-b1b7-6f64a9df6e7e' (Type = Guid)
-- @1: '169435' (Type = Int64)
-- @2: '1553368' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:05 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '84c48d46-131d-4c67-a7ee-35d97e414845' (Type = Guid)
-- @1: '169436' (Type = Int64)
-- @2: '1553368' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:14:05 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

Committed transaction at 6/15/2021 6:14:05 PM -04:00
Closed connection at 6/15/2021 6:14:05 PM -04:00
Opened connection at 6/15/2021 6:14:05 PM -04:00
Started transaction at 6/15/2021 6:14:05 PM -04:00
INSERT INTO ProjectUsers VALUES(1553368, 116587);
INSERT INTO TagProjects VALUES(161926, 1553368);

-- Executing at 6/15/2021 6:14:05 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 6/15/2021 6:14:05 PM -04:00
Closed connection at 6/15/2021 6:14:05 PM -04:00
