Opened connection at 6/15/2021 6:17:16 PM -04:00
Started transaction at 6/15/2021 6:17:16 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, @24, NULL, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Erica Garcia' (Type = String, Size = 800)
-- @1: '6/15/2021 6:17:16 PM' (Type = DateTime2)
-- @2: '6/15/2021 6:17:16 PM' (Type = DateTime2)
-- @3: '3615653' (Type = Int64)
-- @4: '116587' (Type = Int32)
-- @5: '116587' (Type = Int32)
-- @6: '525097' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '8de454f4-5c99-457f-bc2d-9049a7e86e74' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '1024' (Type = Int32)
-- @16: '1024 - Erica Garcia' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '1/30/2017 12:00:00 AM' (Type = DateTime2)
-- @24: '8/25/2017 12:00:00 AM' (Type = DateTime2)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:17:16 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '72428d7f-d786-4623-bfb3-0ca545cd3bba' (Type = Guid)
-- @1: '169433' (Type = Int64)
-- @2: '1553528' (Type = Int64)
-- @3: 'SBERN-CHILDSUP' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:17:16 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'f6ba822a-db3a-43bd-acae-1f86bc9ac35a' (Type = Guid)
-- @1: '169434' (Type = Int64)
-- @2: '1553528' (Type = Int64)
-- @3: 'J261885-J261888' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:17:16 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '11fe7778-aabe-444a-aa50-5aa9b5b9b5c9' (Type = Guid)
-- @1: '169435' (Type = Int64)
-- @2: '1553528' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:17:16 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '079d1426-d956-4fbd-8a23-a5ac5e765e4f' (Type = Guid)
-- @1: '169436' (Type = Int64)
-- @2: '1553528' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 6:17:16 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9f2324bc-8358-44ca-a469-e297cfa57a59' (Type = Guid)
-- @1: 'JUV' (Type = String, Size = 50)
-- @2: '525097' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '6/15/2021 6:17:16 PM' (Type = DateTime2)
-- @6: '6/15/2021 6:17:16 PM' (Type = DateTime2)
-- @7: '116587' (Type = Int32)
-- @8: '116587' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 6/15/2021 6:17:16 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1553528' (Type = Int64)
-- @1: '161974' (Type = Int64)
-- Executing at 6/15/2021 6:17:16 PM -04:00
-- Completed in 55 ms with result: 1

Committed transaction at 6/15/2021 6:17:17 PM -04:00
Closed connection at 6/15/2021 6:17:17 PM -04:00
