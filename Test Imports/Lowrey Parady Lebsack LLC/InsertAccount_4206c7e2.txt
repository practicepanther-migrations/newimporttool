Opened connection at 6/15/2021 5:40:37 PM -04:00
Started transaction at 6/15/2021 5:40:37 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, NULL, @1, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'mmcpeak@renewcorp.com' (Type = String, Size = 100)
-- @1: '303-777-6717 x117' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @5: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @6: '116584' (Type = Int32)
-- @7: '116584' (Type = Int32)
-- @8: '525096' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: 'd0b721f7-fa8d-4447-9cb0-9d309c2d22ee' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Marc' (Type = String, Size = 50)
-- @15: 'McPeak' (Type = String, Size = 50)
-- @16: 'Marc  McPeak' (Type = String, Size = 255)
-- Executing at 6/15/2021 5:40:37 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @4: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116584' (Type = Int32)
-- @7: '116584' (Type = Int32)
-- @8: '525096' (Type = Int64)
-- @9: '3274236' (Type = Int64)
-- @10: '4206c7e2-c82b-44ef-912d-13b639ca3487' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1009' (Type = Int32)
-- @20: '1009 - Marc  McPeak' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 5:40:37 PM -04:00
-- Completed in 74 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '7070 S. Tucson Way' (Type = String, Size = 100)
-- @2: 'Centennial' (Type = String, Size = 50)
-- @3: 'CO' (Type = String, Size = 50)
-- @4: '80112' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @8: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @9: '116584' (Type = Int32)
-- @10: '116584' (Type = Int32)
-- @11: '3614952' (Type = Int64)
-- @12: '525096' (Type = Int64)
-- Executing at 6/15/2021 5:40:37 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614952' (Type = Int64)
-- Executing at 6/15/2021 5:40:37 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '52e834dd-648f-4cc4-ba12-a17cd6bd0bcf' (Type = Guid)
-- @1: 'Opposing Counsel' (Type = String, Size = 50)
-- @2: '525096' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @6: '6/15/2021 5:40:37 PM' (Type = DateTime2)
-- @7: '116584' (Type = Int32)
-- @8: '116584' (Type = Int32)
-- @9: '0' (Type = Int32)
-- Executing at 6/15/2021 5:40:37 PM -04:00
-- Completed in 61 ms with result: SqlDataReader

INSERT [dbo].[TagAccounts]([Account_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '3614952' (Type = Int64)
-- @1: '161917' (Type = Int64)
-- Executing at 6/15/2021 5:40:37 PM -04:00
-- Completed in 55 ms with result: 1

Committed transaction at 6/15/2021 5:40:37 PM -04:00
Closed connection at 6/15/2021 5:40:37 PM -04:00
Opened connection at 6/15/2021 5:40:37 PM -04:00
Started transaction at 6/15/2021 5:40:37 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5039905,
	ShippingAddressId = 5039906
WHERE Id = 3614952;

UPDATE Contacts SET
	AccountId = 3614952
WHERE Id = 3274236;
INSERT INTO TagAccounts VALUES(161905, 3614952);

-- Executing at 6/15/2021 5:40:37 PM -04:00
-- Completed in 56 ms with result: 3

Committed transaction at 6/15/2021 5:40:37 PM -04:00
Closed connection at 6/15/2021 5:40:37 PM -04:00
