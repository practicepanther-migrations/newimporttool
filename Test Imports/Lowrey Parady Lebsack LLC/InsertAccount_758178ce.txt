Opened connection at 6/15/2021 5:42:22 PM -04:00
Started transaction at 6/15/2021 5:42:22 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, @0, NULL, NULL, NULL, NULL, NULL, @1, @2, @3, @4, @5, @6, @7, NULL, NULL, NULL, @8, NULL, NULL, @9, NULL, NULL, NULL, @10, NULL, NULL, NULL, NULL, @11, NULL, NULL, @12, @13, @14, NULL, NULL, NULL, @15)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '970-485-0149' (Type = String, Size = 50)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 5:42:22 PM' (Type = DateTime2)
-- @4: '6/15/2021 5:42:22 PM' (Type = DateTime2)
-- @5: '116584' (Type = Int32)
-- @6: '116584' (Type = Int32)
-- @7: '525096' (Type = Int64)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '84d043cd-e772-4fbd-9683-9cc45ed12978' (Type = Guid)
-- @12: 'False' (Type = Boolean)
-- @13: 'Connie' (Type = String, Size = 50)
-- @14: 'Hogg' (Type = String, Size = 50)
-- @15: 'Connie  Hogg' (Type = String, Size = 255)
-- Executing at 6/15/2021 5:42:22 PM -04:00
-- Completed in 64 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 5:42:22 PM' (Type = DateTime2)
-- @4: '6/15/2021 5:42:22 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116584' (Type = Int32)
-- @7: '116584' (Type = Int32)
-- @8: '525096' (Type = Int64)
-- @9: '3274419' (Type = Int64)
-- @10: '758178ce-8eea-4019-a8c5-a48416433a2e' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1206' (Type = Int32)
-- @20: '1206 - Connie  Hogg' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 5:42:22 PM -04:00
-- Completed in 63 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '3700 S. Lincoln St. #6' (Type = String, Size = 100)
-- @2: 'Englewood' (Type = String, Size = 50)
-- @3: 'CO' (Type = String, Size = 50)
-- @4: '80113' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/15/2021 5:42:22 PM' (Type = DateTime2)
-- @8: '6/15/2021 5:42:22 PM' (Type = DateTime2)
-- @9: '116584' (Type = Int32)
-- @10: '116584' (Type = Int32)
-- @11: '3615135' (Type = Int64)
-- @12: '525096' (Type = Int64)
-- Executing at 6/15/2021 5:42:22 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3615135' (Type = Int64)
-- Executing at 6/15/2021 5:42:22 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 5:42:22 PM -04:00
Closed connection at 6/15/2021 5:42:22 PM -04:00
Opened connection at 6/15/2021 5:42:22 PM -04:00
Started transaction at 6/15/2021 5:42:22 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5040159,
	ShippingAddressId = 5040160
WHERE Id = 3615135;

UPDATE Contacts SET
	AccountId = 3615135
WHERE Id = 3274419;
INSERT INTO TagAccounts VALUES(161905, 3615135);
INSERT INTO TagAccounts VALUES(161906, 3615135);

-- Executing at 6/15/2021 5:42:22 PM -04:00
-- Completed in 56 ms with result: 4

Committed transaction at 6/15/2021 5:42:22 PM -04:00
Closed connection at 6/15/2021 5:42:22 PM -04:00
