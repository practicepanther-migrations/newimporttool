Opened connection at 6/15/2021 5:44:33 PM -04:00
Started transaction at 6/15/2021 5:44:33 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, @3, @4, @5, @6, @7, NULL, NULL, NULL, @8, NULL, NULL, @9, NULL, NULL, NULL, @10, NULL, NULL, NULL, NULL, @11, NULL, NULL, @12, @13, @14, NULL, NULL, NULL, @15)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'tyson.speer@tysondewolfe.com' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 5:44:33 PM' (Type = DateTime2)
-- @4: '6/15/2021 5:44:33 PM' (Type = DateTime2)
-- @5: '116584' (Type = Int32)
-- @6: '116584' (Type = Int32)
-- @7: '525096' (Type = Int64)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '068c71df-076b-4c7b-b6a0-5b8dce1da4d6' (Type = Guid)
-- @12: 'False' (Type = Boolean)
-- @13: 'Tyson' (Type = String, Size = 50)
-- @14: 'Speer' (Type = String, Size = 50)
-- @15: 'Tyson  Speer' (Type = String, Size = 255)
-- Executing at 6/15/2021 5:44:33 PM -04:00
-- Completed in 69 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 5:44:33 PM' (Type = DateTime2)
-- @4: '6/15/2021 5:44:33 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116584' (Type = Int32)
-- @7: '116584' (Type = Int32)
-- @8: '525096' (Type = Int64)
-- @9: '3274672' (Type = Int64)
-- @10: '0a93214a-656b-4b57-af40-1671d728ebf0' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1478' (Type = Int32)
-- @20: '1478 - Tyson  Speer' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 5:44:33 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

Committed transaction at 6/15/2021 5:44:33 PM -04:00
Closed connection at 6/15/2021 5:44:33 PM -04:00
Opened connection at 6/15/2021 5:44:33 PM -04:00
Started transaction at 6/15/2021 5:44:33 PM -04:00
UPDATE Contacts SET
	AccountId = 3615388
WHERE Id = 3274672;
INSERT INTO TagAccounts VALUES(161905, 3615388);
INSERT INTO TagAccounts VALUES(161906, 3615388);

-- Executing at 6/15/2021 5:44:33 PM -04:00
-- Completed in 55 ms with result: 3

Committed transaction at 6/15/2021 5:44:33 PM -04:00
Closed connection at 6/15/2021 5:44:33 PM -04:00
