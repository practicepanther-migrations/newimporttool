Opened connection at 6/15/2021 4:52:56 PM -04:00
Started transaction at 6/15/2021 4:52:56 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, NULL, NULL, NULL, @23, NULL, @24, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Lombardi, Alexandria DOM' (Type = String, Size = 800)
-- @1: '6/15/2021 4:52:56 PM' (Type = DateTime2)
-- @2: '6/15/2021 4:52:56 PM' (Type = DateTime2)
-- @3: '3614914' (Type = Int64)
-- @4: '116584' (Type = Int32)
-- @5: '116584' (Type = Int32)
-- @6: '525096' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'a09db311-62c6-4c35-81cb-263e0a6de6b8' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10126' (Type = Int32)
-- @16: '10126 - Lombardi, Alexandria DOM' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:52:56 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'a48fd586-74e8-4f77-99ca-e797307ca4c8' (Type = Guid)
-- @1: '169417' (Type = Int64)
-- @2: '1553275' (Type = Int64)
-- @3: 'Not Specified' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:52:56 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'defec1a7-032d-4cb8-9b71-046a58377baf' (Type = Guid)
-- @1: '169418' (Type = Int64)
-- @2: '1553275' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:52:56 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 4:52:57 PM -04:00
Closed connection at 6/15/2021 4:52:57 PM -04:00
Opened connection at 6/15/2021 4:52:57 PM -04:00
Started transaction at 6/15/2021 4:52:57 PM -04:00
INSERT INTO TagProjects VALUES(161912, 1553275);

-- Executing at 6/15/2021 4:52:57 PM -04:00
-- Completed in 54 ms with result: 1

Committed transaction at 6/15/2021 4:52:57 PM -04:00
Closed connection at 6/15/2021 4:52:57 PM -04:00
