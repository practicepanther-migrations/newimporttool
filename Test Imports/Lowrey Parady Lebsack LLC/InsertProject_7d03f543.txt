Opened connection at 6/15/2021 4:51:45 PM -04:00
Started transaction at 6/15/2021 4:51:45 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, NULL, @24, NULL, @25, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Buffalo Exchange COSTS' (Type = String, Size = 800)
-- @1: '6/15/2021 4:51:45 PM' (Type = DateTime2)
-- @2: '6/15/2021 4:51:45 PM' (Type = DateTime2)
-- @3: '3614885' (Type = Int64)
-- @4: '116584' (Type = Int32)
-- @5: '116584' (Type = Int32)
-- @6: '525096' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '7d03f543-5162-4d0f-82d6-70a4c8cf72cc' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10072' (Type = Int32)
-- @16: '10072 - Buffalo Exchange COSTS' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '10/5/2020 12:00:00 AM' (Type = DateTime2)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:51:45 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'bf782829-7407-442c-afec-669154eaae48' (Type = Guid)
-- @1: '169417' (Type = Int64)
-- @2: '1553189' (Type = Int64)
-- @3: 'EMP - PreLit' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:51:45 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'e6a23a84-0a28-4f74-a57e-7f8cc87bddf1' (Type = Guid)
-- @1: '169418' (Type = Int64)
-- @2: '1553189' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:51:45 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 6/15/2021 4:51:45 PM -04:00
Closed connection at 6/15/2021 4:51:45 PM -04:00
Opened connection at 6/15/2021 4:51:45 PM -04:00
Started transaction at 6/15/2021 4:51:45 PM -04:00
INSERT INTO ProjectUsers VALUES(1553189, 116586);
INSERT INTO TagProjects VALUES(161908, 1553189);

-- Executing at 6/15/2021 4:51:45 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 6/15/2021 4:51:46 PM -04:00
Closed connection at 6/15/2021 4:51:46 PM -04:00
