Opened connection at 6/15/2021 4:51:23 PM -04:00
Started transaction at 6/15/2021 4:51:23 PM -04:00
INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @9, NULL, @10, @11, @12, @13, @14, @15, @16, @17, NULL, @18, NULL, NULL, NULL, NULL, @19, @20, @21, NULL, @22, NULL, NULL, NULL, NULL, NULL, @23, NULL, @24)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Denver Transit Operators' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 4:51:22 PM' (Type = DateTime2)
-- @4: '6/15/2021 4:51:22 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116584' (Type = Int32)
-- @7: '116584' (Type = Int32)
-- @8: '525096' (Type = Int64)
-- @9: '2335ca54-2df0-48a4-be5e-9708f7d9ea96' (Type = Guid)
-- @10: 'en-US' (Type = String, Size = 5)
-- @11: 'USD' (Type = String, Size = 3)
-- @12: 'False' (Type = Boolean)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: '1577' (Type = Int32)
-- @19: '1577 - Denver Transit Operators' (Type = String, Size = 255)
-- @20: 'False' (Type = Boolean)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:51:23 PM -04:00
-- Completed in 65 ms with result: SqlDataReader

Committed transaction at 6/15/2021 4:51:23 PM -04:00
Closed connection at 6/15/2021 4:51:23 PM -04:00
