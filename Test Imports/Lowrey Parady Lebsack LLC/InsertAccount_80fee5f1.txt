Opened connection at 6/15/2021 5:43:24 PM -04:00
Started transaction at 6/15/2021 5:43:24 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, @1, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'ndjonson@bjnlaw.com' (Type = String, Size = 100)
-- @1: '303-278-3078' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/15/2021 5:43:24 PM' (Type = DateTime2)
-- @5: '6/15/2021 5:43:24 PM' (Type = DateTime2)
-- @6: '116584' (Type = Int32)
-- @7: '116584' (Type = Int32)
-- @8: '525096' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: 'e4ba7293-c932-412c-ad5e-345658735313' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Nicholas' (Type = String, Size = 50)
-- @15: 'Jonson' (Type = String, Size = 50)
-- @16: 'Nicholas  Jonson' (Type = String, Size = 255)
-- Executing at 6/15/2021 5:43:24 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 5:43:24 PM' (Type = DateTime2)
-- @4: '6/15/2021 5:43:24 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116584' (Type = Int32)
-- @7: '116584' (Type = Int32)
-- @8: '525096' (Type = Int64)
-- @9: '3274539' (Type = Int64)
-- @10: '80fee5f1-ba6e-47d9-9442-000e3a9a3064' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1331' (Type = Int32)
-- @20: '1331 - Nicholas  Jonson' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 5:43:24 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, NULL, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '300 Union Blvd.' (Type = String, Size = 100)
-- @2: 'Suite 300' (Type = String, Size = 100)
-- @3: 'Lakewood' (Type = String, Size = 50)
-- @4: 'CO' (Type = String, Size = 50)
-- @5: '80228' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '6/15/2021 5:43:24 PM' (Type = DateTime2)
-- @9: '6/15/2021 5:43:24 PM' (Type = DateTime2)
-- @10: '116584' (Type = Int32)
-- @11: '116584' (Type = Int32)
-- @12: '3615255' (Type = Int64)
-- @13: '525096' (Type = Int64)
-- Executing at 6/15/2021 5:43:24 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3615255' (Type = Int64)
-- Executing at 6/15/2021 5:43:24 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 5:43:24 PM -04:00
Closed connection at 6/15/2021 5:43:24 PM -04:00
Opened connection at 6/15/2021 5:43:24 PM -04:00
Started transaction at 6/15/2021 5:43:24 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5040329,
	ShippingAddressId = 5040330
WHERE Id = 3615255;

UPDATE Contacts SET
	AccountId = 3615255
WHERE Id = 3274539;
INSERT INTO TagAccounts VALUES(161905, 3615255);
INSERT INTO TagAccounts VALUES(161917, 3615255);

-- Executing at 6/15/2021 5:43:24 PM -04:00
-- Completed in 57 ms with result: 4

Committed transaction at 6/15/2021 5:43:24 PM -04:00
Closed connection at 6/15/2021 5:43:24 PM -04:00
