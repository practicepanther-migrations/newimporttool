Opened connection at 6/15/2021 4:52:22 PM -04:00
Started transaction at 6/15/2021 4:52:22 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, NULL, @24, NULL, @25, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Hernandez v. Venus MedSpa' (Type = String, Size = 800)
-- @1: '6/15/2021 4:52:22 PM' (Type = DateTime2)
-- @2: '6/15/2021 4:52:22 PM' (Type = DateTime2)
-- @3: '3614911' (Type = Int64)
-- @4: '116584' (Type = Int32)
-- @5: '116584' (Type = Int32)
-- @6: '525096' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '849908f6-0bbb-4b0b-b71c-f9f278e1afba' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10024' (Type = Int32)
-- @16: '10024 - Hernandez v. Venus MedSpa' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '11/11/2019 12:00:00 AM' (Type = DateTime2)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:52:22 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '203dbdec-534c-4ff4-b303-5ca8d2b772f4' (Type = Guid)
-- @1: '169417' (Type = Int64)
-- @2: '1553236' (Type = Int64)
-- @3: 'EMP - PreLit' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:52:22 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '2a4e1ac6-2692-4d59-93aa-f8ebf10ac942' (Type = Guid)
-- @1: '169418' (Type = Int64)
-- @2: '1553236' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 6/15/2021 4:52:22 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 4:52:22 PM -04:00
Closed connection at 6/15/2021 4:52:22 PM -04:00
Opened connection at 6/15/2021 4:52:22 PM -04:00
Started transaction at 6/15/2021 4:52:23 PM -04:00
INSERT INTO ProjectUsers VALUES(1553236, 116584);
INSERT INTO TagProjects VALUES(161907, 1553236);

-- Executing at 6/15/2021 4:52:23 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 6/15/2021 4:52:23 PM -04:00
Closed connection at 6/15/2021 4:52:23 PM -04:00
