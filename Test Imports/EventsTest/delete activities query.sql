DELETE FROM ActivityUsers WHERE Activity_Id IN (SELECT Id FROM [dbo].[Activities] WHERE TenantId = 525107);
DELETE FROM [dbo].[Tags] WHERE TenantId = 525107 AND TagFor = 2
DELETE FROM [dbo].[TagActivities] WHERE Activity_Id IN (SELECT Id FROM [dbo].[Activities] WHERE TenantId = 525107)
DELETE FROM [dbo].[Activities]
WHERE TenantId = 525107
AND [Type] = 2;