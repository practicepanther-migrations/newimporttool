Opened connection at 6/17/2021 6:09:24 PM -04:00
Started transaction at 6/17/2021 6:09:24 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, @3, @4, @5, @6, @7, NULL, NULL, NULL, @8, NULL, NULL, @9, NULL, NULL, NULL, @10, NULL, NULL, NULL, NULL, @11, NULL, NULL, @12, @13, @14, NULL, NULL, NULL, @15)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'chall@chicagobooth.edu' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:09:24 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:09:24 PM' (Type = DateTime2)
-- @5: '116591' (Type = Int32)
-- @6: '116591' (Type = Int32)
-- @7: '525100' (Type = Int64)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: 'd85fff76-a691-43c7-b61e-f38ab2c908bc' (Type = Guid)
-- @12: 'False' (Type = Boolean)
-- @13: 'Christopher' (Type = String, Size = 50)
-- @14: 'Hall' (Type = String, Size = 50)
-- @15: 'Christopher  Hall' (Type = String, Size = 255)
-- Executing at 6/17/2021 6:09:24 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:09:24 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:09:24 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '3278150' (Type = Int64)
-- @10: 'd02f8205-1504-4192-bc36-b4dfa7bcd359' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1371' (Type = Int32)
-- @20: '1371 - Christopher  Hall' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:09:24 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '70 E. 18th St. | Unit 4C' (Type = String, Size = 100)
-- @2: 'Chicago' (Type = String, Size = 50)
-- @3: 'IL' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '60616' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '6/17/2021 6:09:24 PM' (Type = DateTime2)
-- @9: '6/17/2021 6:09:24 PM' (Type = DateTime2)
-- @10: '116591' (Type = Int32)
-- @11: '116591' (Type = Int32)
-- @12: '3618866' (Type = Int64)
-- @13: '525100' (Type = Int64)
-- Executing at 6/17/2021 6:09:24 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3618866' (Type = Int64)
-- Executing at 6/17/2021 6:09:25 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 6:09:25 PM -04:00
Closed connection at 6/17/2021 6:09:25 PM -04:00
Opened connection at 6/17/2021 6:09:25 PM -04:00
Started transaction at 6/17/2021 6:09:25 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045386,
	ShippingAddressId = 5045387
WHERE Id = 3618866;

UPDATE Contacts SET
	AccountId = 3618866
WHERE Id = 3278150;

-- Executing at 6/17/2021 6:09:25 PM -04:00
-- Completed in 57 ms with result: 2

Committed transaction at 6/17/2021 6:09:25 PM -04:00
Closed connection at 6/17/2021 6:09:25 PM -04:00
