Opened connection at 6/17/2021 6:05:57 PM -04:00
Started transaction at 6/17/2021 6:05:58 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, @14, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, NULL, NULL, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Corporate IP - Patent Filing Busnel/D'Arcy' (Type = String, Size = 800)
-- @1: '6/17/2021 6:05:57 PM' (Type = DateTime2)
-- @2: '6/17/2021 6:05:57 PM' (Type = DateTime2)
-- @3: '3618718' (Type = Int64)
-- @4: '116591' (Type = Int32)
-- @5: '116591' (Type = Int32)
-- @6: '525100' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0a0be927-d8db-4dd3-946f-fab0875a471d' (Type = Guid)
-- @14: 'Developing a patent application for filing' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: '10140' (Type = Int32)
-- @17: '10140 - Corporate IP - Patent Filing Busnel/D'Arcy' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '11/14/2018 12:00:00 AM' (Type = DateTime2)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:05:58 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '5725153f-d0cb-4540-b057-7c535fece225' (Type = Guid)
-- @1: 'Intellectual Property' (Type = String, Size = 50)
-- @2: '525100' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '6/17/2021 6:05:57 PM' (Type = DateTime2)
-- @6: '6/17/2021 6:05:57 PM' (Type = DateTime2)
-- @7: '116591' (Type = Int32)
-- @8: '116591' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 6/17/2021 6:05:58 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1556569' (Type = Int64)
-- @1: '161997' (Type = Int64)
-- Executing at 6/17/2021 6:05:58 PM -04:00
-- Completed in 56 ms with result: 1

Committed transaction at 6/17/2021 6:05:58 PM -04:00
Closed connection at 6/17/2021 6:05:58 PM -04:00
