Opened connection at 6/17/2021 6:06:15 PM -04:00
Started transaction at 6/17/2021 6:06:15 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, NULL, NULL, NULL, @23, NULL, @24, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Jonathan Barr - Barr Towing Inc' (Type = String, Size = 800)
-- @1: '6/17/2021 6:06:15 PM' (Type = DateTime2)
-- @2: '6/17/2021 6:06:15 PM' (Type = DateTime2)
-- @3: '3618736' (Type = Int64)
-- @4: '116591' (Type = Int32)
-- @5: '116591' (Type = Int32)
-- @6: '525100' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '8c2d5ef1-acea-4594-9300-e0640029e494' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10036' (Type = Int32)
-- @16: '10036 - Jonathan Barr - Barr Towing Inc' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:06:15 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '92f3a5a3-8472-4ee5-b3b0-b0df139583e5' (Type = Guid)
-- @1: 'Towing' (Type = String, Size = 50)
-- @2: '525100' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '6/17/2021 6:06:15 PM' (Type = DateTime2)
-- @6: '6/17/2021 6:06:15 PM' (Type = DateTime2)
-- @7: '116591' (Type = Int32)
-- @8: '116591' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 6/17/2021 6:06:15 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1556594' (Type = Int64)
-- @1: '162003' (Type = Int64)
-- Executing at 6/17/2021 6:06:15 PM -04:00
-- Completed in 56 ms with result: 1

Committed transaction at 6/17/2021 6:06:15 PM -04:00
Closed connection at 6/17/2021 6:06:15 PM -04:00
