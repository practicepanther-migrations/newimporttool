Opened connection at 6/17/2021 6:09:28 PM -04:00
Started transaction at 6/17/2021 6:09:28 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, @3, @4, @5, @6, @7, NULL, NULL, NULL, @8, NULL, NULL, @9, NULL, NULL, NULL, @10, NULL, NULL, NULL, NULL, @11, NULL, NULL, @12, @13, @14, NULL, NULL, NULL, @15)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'drkeyes@keyes2life.org' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:09:28 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:09:28 PM' (Type = DateTime2)
-- @5: '116591' (Type = Int32)
-- @6: '116591' (Type = Int32)
-- @7: '525100' (Type = Int64)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '93ee44b1-f509-489d-9f7c-534a23093266' (Type = Guid)
-- @12: 'False' (Type = Boolean)
-- @13: 'Corpia' (Type = String, Size = 50)
-- @14: 'Keyes' (Type = String, Size = 50)
-- @15: 'Corpia  Keyes' (Type = String, Size = 255)
-- Executing at 6/17/2021 6:09:28 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Keyes 2 Life' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:09:28 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:09:28 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '3278158' (Type = Int64)
-- @10: 'bb5b2261-1c26-4aeb-8786-79903e988788' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1114' (Type = Int32)
-- @20: '1114 - Keyes 2 Life' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:09:28 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, @1, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, @9, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: 'United States' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/17/2021 6:09:28 PM' (Type = DateTime2)
-- @5: '6/17/2021 6:09:28 PM' (Type = DateTime2)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '3618874' (Type = Int64)
-- @9: '525100' (Type = Int64)
-- Executing at 6/17/2021 6:09:28 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3618874' (Type = Int64)
-- Executing at 6/17/2021 6:09:28 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '8218cea0-d0a9-4025-b0fa-9739c90a4364' (Type = Guid)
-- @1: '169451' (Type = Int64)
-- @2: 'CEO' (Type = String, Size = -1)
-- @3: '3278158' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:09:28 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 6:09:28 PM -04:00
Closed connection at 6/17/2021 6:09:28 PM -04:00
Opened connection at 6/17/2021 6:09:28 PM -04:00
Started transaction at 6/17/2021 6:09:29 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045390,
	ShippingAddressId = 5045391
WHERE Id = 3618874;

UPDATE Contacts SET
	AccountId = 3618874
WHERE Id = 3278158;

-- Executing at 6/17/2021 6:09:29 PM -04:00
-- Completed in 56 ms with result: 2

Committed transaction at 6/17/2021 6:09:29 PM -04:00
Closed connection at 6/17/2021 6:09:29 PM -04:00
