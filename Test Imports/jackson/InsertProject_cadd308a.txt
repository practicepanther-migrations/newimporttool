Opened connection at 6/17/2021 6:06:34 PM -04:00
Started transaction at 6/17/2021 6:06:34 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, @14, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, NULL, NULL, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'The Qui Collection LLC Trademark Filing' (Type = String, Size = 800)
-- @1: '6/17/2021 6:06:34 PM' (Type = DateTime2)
-- @2: '6/17/2021 6:06:34 PM' (Type = DateTime2)
-- @3: '3618758' (Type = Int64)
-- @4: '116591' (Type = Int32)
-- @5: '116591' (Type = Int32)
-- @6: '525100' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'cadd308a-5c71-4e0e-b5e4-40c0f705f4e7' (Type = Guid)
-- @14: 'Filing for TM protection of name and logo Kim Jones (05/21/2020): COVID19 update
Mrs. Kendall's trademark application abandoned. The application was re-instated and all of the specimens needed for allegation of use re-submitted with detailed explanation to the examiner.' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: '10104' (Type = Int32)
-- @17: '10104 - The Qui Collection LLC Trademark Filing' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '6/14/2018 12:00:00 AM' (Type = DateTime2)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:06:34 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

Committed transaction at 6/17/2021 6:06:35 PM -04:00
Closed connection at 6/17/2021 6:06:35 PM -04:00
Opened connection at 6/17/2021 6:06:35 PM -04:00
Started transaction at 6/17/2021 6:06:35 PM -04:00
INSERT INTO TagProjects VALUES(161991, 1556620);

-- Executing at 6/17/2021 6:06:35 PM -04:00
-- Completed in 56 ms with result: 1

Committed transaction at 6/17/2021 6:06:35 PM -04:00
Closed connection at 6/17/2021 6:06:35 PM -04:00
