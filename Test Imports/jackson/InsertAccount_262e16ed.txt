Opened connection at 6/17/2021 6:11:06 PM -04:00
Started transaction at 6/17/2021 6:11:06 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, NULL, NULL, @12, NULL, NULL, NULL, NULL, @13, NULL, NULL, @14, @15, @16, NULL, NULL, NULL, @17)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Robert.Kooda@hicargo.co' (Type = String, Size = 100)
-- @1: '(310) 985-0314' (Type = String, Size = 50)
-- @2: '310-230-5611' (Type = String, Size = 50)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '6/17/2021 6:11:06 PM' (Type = DateTime2)
-- @6: '6/17/2021 6:11:06 PM' (Type = DateTime2)
-- @7: '116591' (Type = Int32)
-- @8: '116591' (Type = Int32)
-- @9: '525100' (Type = Int64)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '1ba8e6ad-8fef-4472-83e8-ef27405b2259' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: 'Robert' (Type = String, Size = 50)
-- @16: 'Kooda' (Type = String, Size = 50)
-- @17: 'Robert  Kooda' (Type = String, Size = 255)
-- Executing at 6/17/2021 6:11:06 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Horizon International Cargo' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:11:06 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:11:06 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '3278368' (Type = Int64)
-- @10: 'www.hicargo.com' (Type = String, Size = 200)
-- @11: '262e16ed-baba-42ac-9bd4-9b36d1a2b20f' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '1088' (Type = Int32)
-- @21: '1088 - Horizon International Cargo' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:11:06 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '39f937dd-3814-4dd8-b15b-28aacd6d5af7' (Type = Guid)
-- @1: '169453' (Type = Int64)
-- @2: 'ITAGC' (Type = String, Size = -1)
-- @3: '3278368' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:11:06 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 6:11:06 PM -04:00
Closed connection at 6/17/2021 6:11:06 PM -04:00
Opened connection at 6/17/2021 6:11:06 PM -04:00
Started transaction at 6/17/2021 6:11:06 PM -04:00
UPDATE Contacts SET
	AccountId = 3619084
WHERE Id = 3278368;

-- Executing at 6/17/2021 6:11:06 PM -04:00
-- Completed in 57 ms with result: 1

Committed transaction at 6/17/2021 6:11:06 PM -04:00
Closed connection at 6/17/2021 6:11:06 PM -04:00
