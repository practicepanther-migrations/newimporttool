Opened connection at 6/17/2021 6:10:29 PM -04:00
Started transaction at 6/17/2021 6:10:29 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'llroberson1@yahoo.com' (Type = String, Size = 100)
-- @1: '708-828-1508' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/17/2021 6:10:29 PM' (Type = DateTime2)
-- @5: '6/17/2021 6:10:29 PM' (Type = DateTime2)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: 'e86aae70-d38c-45d6-9c9c-4b97809f0910' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Lisa' (Type = String, Size = 50)
-- @15: 'Roberson' (Type = String, Size = 50)
-- @16: 'Lisa  Roberson' (Type = String, Size = 255)
-- Executing at 6/17/2021 6:10:29 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:10:29 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:10:29 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '3278289' (Type = Int64)
-- @10: 'b8bb9ab3-3def-43fc-aca5-83822a8b48b6' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1500' (Type = Int32)
-- @20: '1500 - Lisa  Roberson' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:10:29 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '1316 Prince Drive' (Type = String, Size = 100)
-- @2: 'South Holland' (Type = String, Size = 50)
-- @3: 'IL' (Type = String, Size = 50)
-- @4: '60473' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/17/2021 6:10:29 PM' (Type = DateTime2)
-- @8: '6/17/2021 6:10:29 PM' (Type = DateTime2)
-- @9: '116591' (Type = Int32)
-- @10: '116591' (Type = Int32)
-- @11: '3619005' (Type = Int64)
-- @12: '525100' (Type = Int64)
-- Executing at 6/17/2021 6:10:29 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3619005' (Type = Int64)
-- Executing at 6/17/2021 6:10:29 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'a2908255-cee5-4e9b-b911-bf45c2fcd015' (Type = Guid)
-- @1: '169451' (Type = Int64)
-- @2: 'Founder' (Type = String, Size = -1)
-- @3: '3278289' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:10:30 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 6:10:30 PM -04:00
Closed connection at 6/17/2021 6:10:30 PM -04:00
Opened connection at 6/17/2021 6:10:30 PM -04:00
Started transaction at 6/17/2021 6:10:30 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045468,
	ShippingAddressId = 5045469
WHERE Id = 3619005;

UPDATE Contacts SET
	AccountId = 3619005
WHERE Id = 3278289;

-- Executing at 6/17/2021 6:10:30 PM -04:00
-- Completed in 57 ms with result: 2

Committed transaction at 6/17/2021 6:10:30 PM -04:00
Closed connection at 6/17/2021 6:10:30 PM -04:00
