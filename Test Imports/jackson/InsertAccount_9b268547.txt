Opened connection at 6/17/2021 6:10:34 PM -04:00
Started transaction at 6/17/2021 6:10:34 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, NULL, NULL, @1, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'mark.chudzinski@nlhpartners.com' (Type = String, Size = 100)
-- @1: '3123005780' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/17/2021 6:10:34 PM' (Type = DateTime2)
-- @5: '6/17/2021 6:10:34 PM' (Type = DateTime2)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '22db1cdd-e5bd-42cc-a8d3-cda6b737addd' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Mark' (Type = String, Size = 50)
-- @15: 'Chudzinski' (Type = String, Size = 50)
-- @16: 'Mark  Chudzinski' (Type = String, Size = 255)
-- Executing at 6/17/2021 6:10:34 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'NextLevel Health Partners, Inc.' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:10:34 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:10:34 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '3278300' (Type = Int64)
-- @10: '9b268547-1cf1-47c9-bf51-2527a555759f' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1157' (Type = Int32)
-- @20: '1157 - NextLevel Health Partners, Inc.' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:10:35 PM -04:00
-- Completed in 60 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '303 West Madison Street' (Type = String, Size = 100)
-- @2: 'Suite 800' (Type = String, Size = 100)
-- @3: 'Chicago' (Type = String, Size = 50)
-- @4: 'IL' (Type = String, Size = 50)
-- @5: 'United States' (Type = String, Size = 50)
-- @6: '60606' (Type = String, Size = 50)
-- @7: 'True' (Type = Boolean)
-- @8: 'False' (Type = Boolean)
-- @9: '6/17/2021 6:10:34 PM' (Type = DateTime2)
-- @10: '6/17/2021 6:10:34 PM' (Type = DateTime2)
-- @11: '116591' (Type = Int32)
-- @12: '116591' (Type = Int32)
-- @13: '3619016' (Type = Int64)
-- @14: '525100' (Type = Int64)
-- Executing at 6/17/2021 6:10:35 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3619016' (Type = Int64)
-- Executing at 6/17/2021 6:10:35 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '3b81510e-4273-4176-bc97-ab9194af3e04' (Type = Guid)
-- @1: '169451' (Type = Int64)
-- @2: 'General Counsel' (Type = String, Size = -1)
-- @3: '3278300' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:10:35 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 6:10:35 PM -04:00
Closed connection at 6/17/2021 6:10:35 PM -04:00
Opened connection at 6/17/2021 6:10:35 PM -04:00
Started transaction at 6/17/2021 6:10:35 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045474,
	ShippingAddressId = 5045475
WHERE Id = 3619016;

UPDATE Contacts SET
	AccountId = 3619016
WHERE Id = 3278300;

-- Executing at 6/17/2021 6:10:35 PM -04:00
-- Completed in 57 ms with result: 2

Committed transaction at 6/17/2021 6:10:35 PM -04:00
Closed connection at 6/17/2021 6:10:35 PM -04:00
