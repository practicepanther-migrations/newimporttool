Opened connection at 6/17/2021 6:06:30 PM -04:00
Started transaction at 6/17/2021 6:06:30 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, NULL, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, NULL, NULL, @11, NULL, NULL, @12, NULL, NULL, NULL, @13, NULL, NULL, NULL, NULL, @14, NULL, NULL, @15, @16, @17, NULL, NULL, NULL, @18)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'melvinr@mielleorganics.com' (Type = String, Size = 100)
-- @1: '8004369165' (Type = String, Size = 50)
-- @2: '8004369165' (Type = String, Size = 50)
-- @3: '8004369165' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '6/17/2021 6:06:30 PM' (Type = DateTime2)
-- @7: '6/17/2021 6:06:30 PM' (Type = DateTime2)
-- @8: '116591' (Type = Int32)
-- @9: '116591' (Type = Int32)
-- @10: '525100' (Type = Int64)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: 'bbf86c8d-6d0e-4b13-8a64-96511806d3fc' (Type = Guid)
-- @15: 'False' (Type = Boolean)
-- @16: 'Melvin' (Type = String, Size = 50)
-- @17: 'Rodriguez' (Type = String, Size = 50)
-- @18: 'Melvin  Rodriguez' (Type = String, Size = 255)
-- Executing at 6/17/2021 6:06:30 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Mielle Organics, LLC' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 6:06:30 PM' (Type = DateTime2)
-- @4: '6/17/2021 6:06:30 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116591' (Type = Int32)
-- @7: '116591' (Type = Int32)
-- @8: '525100' (Type = Int64)
-- @9: '3278037' (Type = Int64)
-- @10: '4a160564-3a89-4ed4-9d1d-0bee2f60be4c' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1142' (Type = Int32)
-- @20: '1142 - Mielle Organics, LLC' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:06:30 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '8707 Louisiana St.' (Type = String, Size = 100)
-- @2: 'Merrillville' (Type = String, Size = 50)
-- @3: 'Indiana' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '46410' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '6/17/2021 6:06:30 PM' (Type = DateTime2)
-- @9: '6/17/2021 6:06:30 PM' (Type = DateTime2)
-- @10: '116591' (Type = Int32)
-- @11: '116591' (Type = Int32)
-- @12: '3618753' (Type = Int64)
-- @13: '525100' (Type = Int64)
-- Executing at 6/17/2021 6:06:30 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3618753' (Type = Int64)
-- Executing at 6/17/2021 6:06:30 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '7ed11bb9-fa69-4b2d-8ea5-b55bf64c581c' (Type = Guid)
-- @1: '169451' (Type = Int64)
-- @2: 'CEO' (Type = String, Size = -1)
-- @3: '3278037' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/17/2021 6:06:30 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 6:06:30 PM -04:00
Closed connection at 6/17/2021 6:06:30 PM -04:00
Opened connection at 6/17/2021 6:06:30 PM -04:00
Started transaction at 6/17/2021 6:06:30 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045314,
	ShippingAddressId = 5045315
WHERE Id = 3618753;

UPDATE Contacts SET
	AccountId = 3618753
WHERE Id = 3278037;

-- Executing at 6/17/2021 6:06:30 PM -04:00
-- Completed in 56 ms with result: 2

Committed transaction at 6/17/2021 6:06:30 PM -04:00
Closed connection at 6/17/2021 6:06:30 PM -04:00
