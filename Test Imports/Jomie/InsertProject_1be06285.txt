Opened connection at 6/17/2021 7:20:34 PM -04:00
Started transaction at 6/17/2021 7:20:34 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, NULL, @24, NULL, @25, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Webby v Webby' (Type = String, Size = 800)
-- @1: '6/17/2021 7:20:34 PM' (Type = DateTime2)
-- @2: '6/17/2021 7:20:34 PM' (Type = DateTime2)
-- @3: '3619279' (Type = Int64)
-- @4: '116592' (Type = Int32)
-- @5: '116592' (Type = Int32)
-- @6: '525101' (Type = Int64)
-- @7: '2' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '1be06285-7c2b-4e76-85e9-1ab55f12dc2d' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10155' (Type = Int32)
-- @16: '10155 - Webby v Webby' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '1/28/2021 12:00:00 AM' (Type = DateTime2)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 7:20:34 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

Committed transaction at 6/17/2021 7:20:34 PM -04:00
Closed connection at 6/17/2021 7:20:34 PM -04:00
