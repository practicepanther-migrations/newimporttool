Opened connection at 6/17/2021 7:20:41 PM -04:00
Started transaction at 6/17/2021 7:20:41 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'jtautotrans@gmail.com' (Type = String, Size = 100)
-- @1: '860-428-3724' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/17/2021 7:20:41 PM' (Type = DateTime2)
-- @5: '6/17/2021 7:20:41 PM' (Type = DateTime2)
-- @6: '116592' (Type = Int32)
-- @7: '116592' (Type = Int32)
-- @8: '525101' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: 'e95328f1-b5ea-4565-be08-67897d9f71b7' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Jay' (Type = String, Size = 50)
-- @15: 'Walker' (Type = String, Size = 50)
-- @16: 'Jay  Walker' (Type = String, Size = 255)
-- Executing at 6/17/2021 7:20:41 PM -04:00
-- Completed in 60 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 7:20:41 PM' (Type = DateTime2)
-- @4: '6/17/2021 7:20:41 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116592' (Type = Int32)
-- @7: '116592' (Type = Int32)
-- @8: '525101' (Type = Int64)
-- @9: '3278572' (Type = Int64)
-- @10: '3d906715-f899-4dfd-a201-d7c28409aed4' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1146' (Type = Int32)
-- @20: '1146 - Jay  Walker' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 7:20:41 PM -04:00
-- Completed in 61 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, NULL, @2, NULL, @3, @4, @5, @6, @7, @8, @9, @10, NULL, @11, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '1087 Thompson Road Thompson' (Type = String, Size = 100)
-- @2: 'CT' (Type = String, Size = 50)
-- @3: '6279' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '6/17/2021 7:20:41 PM' (Type = DateTime2)
-- @7: '6/17/2021 7:20:41 PM' (Type = DateTime2)
-- @8: '116592' (Type = Int32)
-- @9: '116592' (Type = Int32)
-- @10: '3619288' (Type = Int64)
-- @11: '525101' (Type = Int64)
-- Executing at 6/17/2021 7:20:41 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3619288' (Type = Int64)
-- Executing at 6/17/2021 7:20:41 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'ededa6f7-6ad4-4fa4-b8e9-0933568f5af5' (Type = Guid)
-- @1: '169456' (Type = Int64)
-- @2: '18-394' (Type = String, Size = -1)
-- @3: '3278572' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/17/2021 7:20:41 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 7:20:41 PM -04:00
Closed connection at 6/17/2021 7:20:41 PM -04:00
Opened connection at 6/17/2021 7:20:41 PM -04:00
Started transaction at 6/17/2021 7:20:41 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045821,
	ShippingAddressId = 5045822
WHERE Id = 3619288;

UPDATE Contacts SET
	AccountId = 3619288
WHERE Id = 3278572;
INSERT INTO TagAccounts VALUES(162009, 3619288);

-- Executing at 6/17/2021 7:20:41 PM -04:00
-- Completed in 58 ms with result: 3

Committed transaction at 6/17/2021 7:20:41 PM -04:00
Closed connection at 6/17/2021 7:20:41 PM -04:00
