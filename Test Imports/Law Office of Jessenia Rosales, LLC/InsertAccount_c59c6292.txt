Opened connection at 6/17/2021 5:12:26 PM -04:00
Started transaction at 6/17/2021 5:12:26 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, @13, NULL, @14, @15, @16, NULL, NULL, NULL, @17)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'nyasiamaggio2@gmail.com' (Type = String, Size = 100)
-- @1: '813-492-1374' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/17/2021 5:12:26 PM' (Type = DateTime2)
-- @5: '6/17/2021 5:12:26 PM' (Type = DateTime2)
-- @6: '116590' (Type = Int32)
-- @7: '116590' (Type = Int32)
-- @8: '525099' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '5ee8ed25-3f8f-4329-909b-4d20a3c2cd40' (Type = Guid)
-- @13: 'Hillsborough County- Thirteenth Judicial Circuit
Case Number: 20- DR-007389

**NEW JUDGE: The Honorable Wendy J. DePaul
800 E. Twiggs St., Room #421
Tampa, Florida 33602
Courtroom #403
DIVISION: C

Judicial Assistant: TBA
phone:(813) 272-5777

OP: Mark T. Hutchinson III
OC: Alex Reed, Stavrou.P.A.
Address: 300 S. Hyde Park, Ave Ste 180
Tampa, FL 33606

Mom Info: Nancy Maggio:
Email: nancy.maggio888@gmail.com
Phone Number: 813-458-3320' (Type = String, Size = -1)
-- @14: 'False' (Type = Boolean)
-- @15: 'Nyasia' (Type = String, Size = 50)
-- @16: 'Maggio' (Type = String, Size = 50)
-- @17: 'Nyasia  Maggio' (Type = String, Size = 255)
-- Executing at 6/17/2021 5:12:27 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 5:12:26 PM' (Type = DateTime2)
-- @4: '6/17/2021 5:12:26 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116590' (Type = Int32)
-- @7: '116590' (Type = Int32)
-- @8: '525099' (Type = Int64)
-- @9: '3277133' (Type = Int64)
-- @10: 'c59c6292-df1f-431d-93ac-e97a1a4c65a2' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '16' (Type = Int32)
-- @20: '16 - Nyasia  Maggio' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 5:12:27 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, @11, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '7101 S. Manhattan Ave' (Type = String, Size = 100)
-- @2: 'Tampa' (Type = String, Size = 50)
-- @3: 'FL' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '6/17/2021 5:12:26 PM' (Type = DateTime2)
-- @7: '6/17/2021 5:12:26 PM' (Type = DateTime2)
-- @8: '116590' (Type = Int32)
-- @9: '116590' (Type = Int32)
-- @10: '3617849' (Type = Int64)
-- @11: '525099' (Type = Int64)
-- Executing at 6/17/2021 5:12:27 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3617849' (Type = Int64)
-- Executing at 6/17/2021 5:12:27 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/17/2021 5:12:27 PM -04:00
Closed connection at 6/17/2021 5:12:27 PM -04:00
Opened connection at 6/17/2021 5:12:27 PM -04:00
Started transaction at 6/17/2021 5:12:27 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045091,
	ShippingAddressId = 5045092
WHERE Id = 3617849;

UPDATE Contacts SET
	AccountId = 3617849
WHERE Id = 3277133;

-- Executing at 6/17/2021 5:12:27 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 6/17/2021 5:12:27 PM -04:00
Closed connection at 6/17/2021 5:12:27 PM -04:00
