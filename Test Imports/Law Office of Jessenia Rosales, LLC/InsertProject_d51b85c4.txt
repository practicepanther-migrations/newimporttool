Opened connection at 6/17/2021 5:12:32 PM -04:00
Started transaction at 6/17/2021 5:12:32 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, NULL, @24, NULL, @25, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Answer to Supplemental Petition for Modification' (Type = String, Size = 800)
-- @1: '6/17/2021 5:12:32 PM' (Type = DateTime2)
-- @2: '6/17/2021 5:12:32 PM' (Type = DateTime2)
-- @3: '3617856' (Type = Int64)
-- @4: '116590' (Type = Int32)
-- @5: '116590' (Type = Int32)
-- @6: '525099' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'd51b85c4-1011-4343-a106-c8295775e49a' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10009' (Type = Int32)
-- @16: '10009 - Answer to Supplemental Petition for Modification' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '12/9/2020 12:00:00 AM' (Type = DateTime2)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 5:12:32 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

Committed transaction at 6/17/2021 5:12:32 PM -04:00
Closed connection at 6/17/2021 5:12:32 PM -04:00
