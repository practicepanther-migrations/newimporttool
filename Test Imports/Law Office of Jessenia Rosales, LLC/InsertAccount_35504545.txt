Opened connection at 6/17/2021 5:15:32 PM -04:00
Started transaction at 6/17/2021 5:15:32 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, @0, NULL, NULL, NULL, NULL, NULL, @1, @2, @3, @4, @5, @6, @7, NULL, NULL, NULL, @8, NULL, NULL, @9, NULL, NULL, NULL, @10, NULL, NULL, NULL, NULL, @11, NULL, NULL, @12, @13, NULL, NULL, NULL, NULL, @14)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '14074552041' (Type = String, Size = 50)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 5:15:32 PM' (Type = DateTime2)
-- @4: '6/17/2021 5:15:32 PM' (Type = DateTime2)
-- @5: '116590' (Type = Int32)
-- @6: '116590' (Type = Int32)
-- @7: '525099' (Type = Int64)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '16fc8ab3-cef1-4deb-8c72-57faa666e23e' (Type = Guid)
-- @12: 'False' (Type = Boolean)
-- @13: 'Iamrenee' (Type = String, Size = 50)
-- @14: 'Iamrenee' (Type = String, Size = 255)
-- Executing at 6/17/2021 5:15:32 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 5:15:32 PM' (Type = DateTime2)
-- @4: '6/17/2021 5:15:32 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116590' (Type = Int32)
-- @7: '116590' (Type = Int32)
-- @8: '525099' (Type = Int64)
-- @9: '3277498' (Type = Int64)
-- @10: '35504545-ebdb-4bc6-beed-73399e4285d7' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '296' (Type = Int32)
-- @20: '296 - Iamrenee' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 5:15:32 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

Committed transaction at 6/17/2021 5:15:32 PM -04:00
Closed connection at 6/17/2021 5:15:32 PM -04:00
Opened connection at 6/17/2021 5:15:32 PM -04:00
Started transaction at 6/17/2021 5:15:32 PM -04:00
UPDATE Contacts SET
	AccountId = 3618214
WHERE Id = 3277498;

-- Executing at 6/17/2021 5:15:32 PM -04:00
-- Completed in 55 ms with result: 1

Committed transaction at 6/17/2021 5:15:32 PM -04:00
Closed connection at 6/17/2021 5:15:32 PM -04:00
