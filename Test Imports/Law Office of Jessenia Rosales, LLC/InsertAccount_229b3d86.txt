Opened connection at 6/17/2021 5:14:00 PM -04:00
Started transaction at 6/17/2021 5:14:00 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @0, @1, @2, @3, @4, @5, @6, NULL, NULL, NULL, @7, NULL, NULL, @8, NULL, NULL, NULL, @9, NULL, NULL, NULL, NULL, @10, NULL, NULL, @11, @12, @13, NULL, NULL, NULL, @14)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'True' (Type = Boolean)
-- @1: 'False' (Type = Boolean)
-- @2: '6/17/2021 5:14:00 PM' (Type = DateTime2)
-- @3: '6/17/2021 5:14:00 PM' (Type = DateTime2)
-- @4: '116590' (Type = Int32)
-- @5: '116590' (Type = Int32)
-- @6: '525099' (Type = Int64)
-- @7: '0' (Type = Int32)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: 'b781a6b7-176b-45f1-9173-2efc57cf5899' (Type = Guid)
-- @11: 'False' (Type = Boolean)
-- @12: 'Anthony' (Type = String, Size = 50)
-- @13: 'Bridges' (Type = String, Size = 50)
-- @14: 'Anthony  Bridges' (Type = String, Size = 255)
-- Executing at 6/17/2021 5:14:00 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 5:14:00 PM' (Type = DateTime2)
-- @4: '6/17/2021 5:14:00 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116590' (Type = Int32)
-- @7: '116590' (Type = Int32)
-- @8: '525099' (Type = Int64)
-- @9: '3277281' (Type = Int64)
-- @10: '229b3d86-4912-4be5-a55e-4531d45c31cc' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '59' (Type = Int32)
-- @20: '59 - Anthony  Bridges' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 5:14:00 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

Committed transaction at 6/17/2021 5:14:00 PM -04:00
Closed connection at 6/17/2021 5:14:00 PM -04:00
Opened connection at 6/17/2021 5:14:00 PM -04:00
Started transaction at 6/17/2021 5:14:00 PM -04:00
UPDATE Contacts SET
	AccountId = 3617997
WHERE Id = 3277281;

-- Executing at 6/17/2021 5:14:00 PM -04:00
-- Completed in 56 ms with result: 1

Committed transaction at 6/17/2021 5:14:00 PM -04:00
Closed connection at 6/17/2021 5:14:00 PM -04:00
