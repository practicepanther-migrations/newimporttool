Opened connection at 6/17/2021 5:12:35 PM -04:00
Started transaction at 6/17/2021 5:12:35 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, @13, NULL, @14, @15, @16, NULL, NULL, NULL, @17)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'emerald.rivera92@gmail.com' (Type = String, Size = 100)
-- @1: '347-820-3874' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/17/2021 5:12:35 PM' (Type = DateTime2)
-- @5: '6/17/2021 5:12:35 PM' (Type = DateTime2)
-- @6: '116590' (Type = Int32)
-- @7: '116590' (Type = Int32)
-- @8: '525099' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '5640a0cb-f37f-4bc3-8ae8-48d947f8329f' (Type = Guid)
-- @13: 'Hillsborough County - Thirteenth Judicial Circuit
Case Number: 20-DR-009304

Honorable: Jared Smith
800 E. Twiggs St., Room #427- Edgecomb Courthouse
Tampa, Florida 33602
Courtroom #411
 
Judicial Assistant: Kortina Jones
phone: (813) 272-6992
email:famlawdiva@fljud13.org

Opposing Counsel:
Law Office of Maurine A. Thomas, P.A.
15313 Lazy Lake Pl
Tampa, FL 33624-2130
Office: 813-857-2183
Fax: 813-343-6241
Email:maurine.thomas@yahoo.com

Opposing Party:
Joey J. Cooper
6636 New Town Circle
Unit B3
Tampa, FL 33625' (Type = String, Size = -1)
-- @14: 'False' (Type = Boolean)
-- @15: 'Emerald' (Type = String, Size = 50)
-- @16: 'Rivera' (Type = String, Size = 50)
-- @17: 'Emerald  Rivera' (Type = String, Size = 255)
-- Executing at 6/17/2021 5:12:35 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 5:12:35 PM' (Type = DateTime2)
-- @4: '6/17/2021 5:12:35 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116590' (Type = Int32)
-- @7: '116590' (Type = Int32)
-- @8: '525099' (Type = Int64)
-- @9: '3277146' (Type = Int64)
-- @10: 'a274c651-833f-4b89-8b8b-0ecb1951d61e' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '225' (Type = Int32)
-- @20: '225 - Emerald  Rivera' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 5:12:36 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, NULL, NULL, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '230 E.123 St.' (Type = String, Size = 100)
-- @2: 'Apt.1701' (Type = String, Size = 100)
-- @3: 'New York' (Type = String, Size = 50)
-- @4: 'NY' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/17/2021 5:12:35 PM' (Type = DateTime2)
-- @8: '6/17/2021 5:12:35 PM' (Type = DateTime2)
-- @9: '116590' (Type = Int32)
-- @10: '116590' (Type = Int32)
-- @11: '3617862' (Type = Int64)
-- @12: '525099' (Type = Int64)
-- Executing at 6/17/2021 5:12:36 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3617862' (Type = Int64)
-- Executing at 6/17/2021 5:12:36 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/17/2021 5:12:36 PM -04:00
Closed connection at 6/17/2021 5:12:36 PM -04:00
Opened connection at 6/17/2021 5:12:36 PM -04:00
Started transaction at 6/17/2021 5:12:36 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045103,
	ShippingAddressId = 5045104
WHERE Id = 3617862;

UPDATE Contacts SET
	AccountId = 3617862
WHERE Id = 3277146;

-- Executing at 6/17/2021 5:12:36 PM -04:00
-- Completed in 57 ms with result: 2

Committed transaction at 6/17/2021 5:12:36 PM -04:00
Closed connection at 6/17/2021 5:12:36 PM -04:00
