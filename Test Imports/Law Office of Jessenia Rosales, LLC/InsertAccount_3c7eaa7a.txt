Opened connection at 6/17/2021 5:13:37 PM -04:00
Started transaction at 6/17/2021 5:13:37 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @0, @1, @2, @3, @4, @5, @6, NULL, NULL, NULL, @7, NULL, NULL, @8, NULL, NULL, NULL, @9, NULL, NULL, NULL, NULL, @10, @11, NULL, @12, @13, @14, NULL, NULL, NULL, @15)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'True' (Type = Boolean)
-- @1: 'False' (Type = Boolean)
-- @2: '6/17/2021 5:13:37 PM' (Type = DateTime2)
-- @3: '6/17/2021 5:13:37 PM' (Type = DateTime2)
-- @4: '116590' (Type = Int32)
-- @5: '116590' (Type = Int32)
-- @6: '525099' (Type = Int64)
-- @7: '0' (Type = Int32)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '6ce01add-beeb-4cb0-8bf2-13479241684f' (Type = Guid)
-- @11: 'Hillsborough County
Case: 16-CF-017807
Judge: Michelle Sisco
Trial Division 1

STATE ATTORNEYS OFFICE 
419 N PIERCE ST
TAMPA, FL 33602
Telephone: 813-272-5400' (Type = String, Size = -1)
-- @12: 'False' (Type = Boolean)
-- @13: 'Jessie' (Type = String, Size = 50)
-- @14: 'Harris Jr.' (Type = String, Size = 50)
-- @15: 'Jessie  Harris Jr.' (Type = String, Size = 255)
-- Executing at 6/17/2021 5:13:37 PM -04:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/17/2021 5:13:37 PM' (Type = DateTime2)
-- @4: '6/17/2021 5:13:37 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116590' (Type = Int32)
-- @7: '116590' (Type = Int32)
-- @8: '525099' (Type = Int64)
-- @9: '3277238' (Type = Int64)
-- @10: '3c7eaa7a-0bfe-4be5-9eca-04d0e396f565' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '343' (Type = Int32)
-- @20: '343 - Jessie  Harris Jr.' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/17/2021 5:13:37 PM -04:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, NULL, NULL, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '116 Lake Emerald Dr.' (Type = String, Size = 100)
-- @2: 'Apt. 103' (Type = String, Size = 100)
-- @3: 'Oakland Park,' (Type = String, Size = 50)
-- @4: 'FL' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/17/2021 5:13:37 PM' (Type = DateTime2)
-- @8: '6/17/2021 5:13:37 PM' (Type = DateTime2)
-- @9: '116590' (Type = Int32)
-- @10: '116590' (Type = Int32)
-- @11: '3617954' (Type = Int64)
-- @12: '525099' (Type = Int64)
-- Executing at 6/17/2021 5:13:37 PM -04:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3617954' (Type = Int64)
-- Executing at 6/17/2021 5:13:38 PM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/17/2021 5:13:38 PM -04:00
Closed connection at 6/17/2021 5:13:38 PM -04:00
Opened connection at 6/17/2021 5:13:38 PM -04:00
Started transaction at 6/17/2021 5:13:38 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5045181,
	ShippingAddressId = 5045182
WHERE Id = 3617954;

UPDATE Contacts SET
	AccountId = 3617954
WHERE Id = 3277238;

-- Executing at 6/17/2021 5:13:38 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 6/17/2021 5:13:38 PM -04:00
Closed connection at 6/17/2021 5:13:38 PM -04:00
