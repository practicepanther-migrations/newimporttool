﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;

namespace CsvParser
{
    public class AccountFields
    {
        [Name("Contact: CompanyName")]
        public string Name { get; set; }
        [Name("Contact: Website")]
        public string Website { get; set; }
        [Name("Contact: CompanyNotes")]
        public string Notes { get; set; }
        [Name("Contact: Number")]
        public int? Number { get; set; }
    }

    public class ContactFields
    {
        [Name("Contact: FirstName")]
        public string FirstName { get; set; }
        [Name("Contact: MiddleName")]
        public string MiddleName { get; set; }
        [Name("Contact: LastName")]
        public string LastName { get; set; }
        [Name("Contact: Email")]
        public string Email { get; set; }
        [Name("Contact: MobileNumber")]
        public string Mobile { get; set; }
        [Name("Contact: HomeNumber")]
        public string Home { get; set; }
        [Name("Contact: OfficeNumber")]
        public string Office { get; set; }
        [Name("Contact: FaxNumber")]
        public string Fax { get; set; }
        [Name("Contact: ContactNotes")]
        public string Notes { get; set; }
    }

    public class AddressFields
    {
        [Name("Contact: Street1")]
        public string Address1 { get; set; }
        [Name("Contact: Street2")]
        public string Address2 { get; set; }
        [Name("Contact: City")]
        public string City { get; set; }
        [Name("Contact: ProvinceState")]
        public string State { get; set; }
        [Name("Contact: ZipPostalCode")]
        public string PostalCode { get; set; }
        [Name("Contact: Country")]
        public string Country { get; set; }

    }

    public class EventFields
    {
        /// <summary>
        /// Converts dynamic row from CSV to EventFields object
        /// </summary>
        /// <param name="row"></param>
        public EventFields(dynamic row)
        {
            foreach (var field in row)
            {
                if (!String.IsNullOrWhiteSpace(field.Value))
                {
                    switch (field.Key)
                    {
                        case "ContactNumber":
                            ContactNumber = int.Parse(field.Value);
                            break;
                        case "MatterNumber":
                            MatterNumber = int.Parse(field.Value);
                            break;
                        case "StartDateTime":
                            StartDateTime = DateTime.Parse(field.Value);
                            break;
                        case "EndDateTime":
                            EndDateTime = DateTime.Parse(field.Value);
                            break;
                        case "IsAllDay (Yes/No)":
                            IsActivityAllDay = field.Value;
                            break;
                        case "Subject":
                            Name = field.Value;
                            break;
                        case "Description":
                            Description = field.Value;
                            break;
                        case "Assigned To (Email Address)":
                            AssignedUserEmails = field.Value;
                            break;
                        case "Tags":
                            Tags = field.Value;
                            break;
                        case "Location":
                            Location = field.Value;
                            break;
                    }
                }
            }
        }

        [Name("ContactNumber")]
        public int? ContactNumber { get; set; }
        [Name("MatterNumber")]
        public int? MatterNumber { get; set; }
        [Name("StartDateTime")]
        public DateTime StartDateTime { get; set; }
        [Name("EndDateTime")]
        public DateTime EndDateTime { get; set; }
        [Name("IsAllDay (Yes/No)")]
        public string IsActivityAllDay { get; set; }
        [Name("Subject")]
        public string Name { get; set; }
        [Name("Description")]
        public string Description { get; set; }
        [Name("Assigned To (Email Address)")]
        public string AssignedUserEmails { get; set; }
        [Name("Location")]
        public string Tags { get; set; }
        public string Location { get; set; }
    }

    public class NoteFields
    {
        public NoteFields(dynamic row)
        {
            foreach (var field in row)
            {
                if (!String.IsNullOrWhiteSpace(field.Value))
                {
                    switch (field.Key)
                    {
                        case "ContactNumber":
                            ContactNumber = int.Parse(field.Value);
                            break;
                        case "MatterNumber":
                            MatterNumber = int.Parse(field.Value);
                            break;
                        case "Date":
                            Date = DateTime.Parse(field.Value);
                            break;
                        case "Subject":
                            Name = field.Value;
                            break;
                        case "Description":
                            Description = field.Value;
                            break;
                        case "User (Email Address)":
                            AssignedUserEmails = field.Value;
                            break;
                        case "Tags":
                            Tags = field.Value;
                            break;
                    }
                }
            }
        }

        public int? ContactNumber { get; set; }
        public int? MatterNumber { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public string AssignedUserEmails { get; set; }
    }

    public class ProjectFields
    {
        [Name("Matter: Number")]
        public int? Number { get; set; }
        [Name("Matter: Name")]
        public string Name { get; set; }
        [Name("Matter: Status")]
        public string Status { get; set; }
        [Name("Matter: HourlyRateType")]
        public int? HourlyRateType { get; set; }
        [Name("Matter: Notes")]
        public string Notes { get; set; }
        [Name("Matter: OriginatedBy")]
        public string OriginatedByEmail { get; set; }
        [Name("Matter: AssignedTo")]
        public string AssignedUserEmails { get; set; }
        [Name("Matter: HourlyRate")]
        public decimal? HourlyRate { get; set; }
        [Name("Matter: ContingencyRatePercent")]
        public decimal? ContingencyRatePercent { get; set; }
        [Name("Matter: OpenDate")]
        public DateTime? OpenDate { get; set; }
        [Name("Matter: CloseDate")]
        public DateTime? CloseDate { get; set; }
        [Name("Matter: DueDate")]
        public DateTime? DueDate { get; set; }
        [Name("Matter: FlatRateAmount")]
        public decimal? FlatRate { get; set; }
        /* TODO Maybe use the Enum instead of the int.
     * 
     *     public enum ProjectHourlyRate
    {
        [Display(Name = "Item Rate")]
        ItemRate = 0,
        [Display(Name = "User Rate")]
        UserRate = 1,
        [Display(Name = "Project Rate")]
        ProjectRate = 2,
        [Display(Name = "Flat Rate")]
        FlatRate = 3,
        [Display(Name = "Contingency")]
        Contingency = 4,
        [Display(Name = "Custom Hourly Rate")]
        PresetProjectRate = 5
    }

     public enum ProjectStatus
    {
        //Active = 0,
        //Inactive = 1,
        Closed = 2,
        //Cancelled = 3
        Pending = 3,
        Open = 4
    }
    */
    }

    public class TagAccountsField
    {
        public TagAccountsField(long account_Id, string tagsColumn)
        {
            Account_Id = account_Id;
            TagsColumn = tagsColumn.Split(',').ToList();
        }
        public long Account_Id { get; set; }
        public List<string> TagsColumn { get; set; }
    }

    public class TaskFields
    {
        public TaskFields(dynamic row)
        {
            foreach (var field in row)
            {
                if (!String.IsNullOrWhiteSpace(field.Value))
                {
                    switch (field.Key)
                    {
                        case "ContactNumber":
                            ContactNumber = int.Parse(field.Value);
                            break;
                        case "MatterNumber":
                            MatterNumber = int.Parse(field.Value);
                            break;
                        case "DueDate":
                            DueDate = DateTime.Parse(field.Value);
                            break;
                        case "Subject":
                            Name = field.Value;
                            break;
                        case "Description":
                            Description = field.Value;
                            break;
                        case "Status (Completed/NotCompleted/InProgress)":
                            Status = field.Value;
                            break;
                        case "Assigned To (Email Address)":
                            AssignedUserEmails = field.Value;
                            break;
                        case "Tags":
                            Tags = field.Value;
                            break;
                    }
                }
            }
        }

        public int? ContactNumber { get; set; }
        public int? MatterNumber { get; set; }
        public DateTime DueDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string AssignedUserEmails { get; set; }
        public string Tags { get; set; }
    }

    public class TimeEntryFields
    {
        //static DatabaseFunctions dbF { get; set; }

        public TimeEntryFields(dynamic row)
        {
            foreach (var field in row)
            {
                if (!String.IsNullOrWhiteSpace(field.Value))
                {
                    switch (field.Key)
                    {
                        case "MatterNumber":
                            MatterNumber = int.Parse(field.Value);
                            break;
                        case "Date":
                            try
                            {
                                Date = DateTime.Parse(field.Value);
                                Error = null;
                            }
                            catch(Exception)
                            {
                                Error = "Invalid Date";
                            }
                            //Date = DateTime.Parse(field.Value);
                            break;
                        case "Hours":
                            Hours = decimal.Parse(field.Value);
                            break;
                        case "Rate":
                            HourlyRate = decimal.Parse(field.Value);
                            break;
                        case "Description":
                            Description = field.Value;
                            break;
                        case "Status (Billable/Billed/NotBillable)":
                            Status = field.Value;
                            break;
                        case "Billed By (Email Address)":
                            User = field.Value;
                            break;
                        case "ItemCode":
                            ItemCode = field.Value;
                            break;
                    }
                }
            }
        }

        public string Error { get; set; }

        [Name("MatterNumber")]
        public int? MatterNumber { get; set; }
        [Name("Date")]
        public DateTime? Date { get; set; }
        [Name("Hours")]
        public decimal Hours { get; set; }
        [Name("Rate")]
        public decimal HourlyRate { get; set; }
        [Name("Description")]
        public string Description { get; set; }
        [Name("Status (Billable/Billed/NotBillable)")]
        public string Status { get; set; }
        [Name("Billed By (Email Address)")]
        public string User { get; set; }
        [Name("ItemCode")]
        public string ItemCode { get; set; }

        /*public enum TimeEntryAndExpenseStatus
        {
            Billable = 1,
            Billed = 2,
            Paid = 3,
            NotBillable = 4
        }*/

    }

    public class ExpenseFields
    {
        public ExpenseFields(dynamic row)
        {
            foreach (var field in row)
            {
                if (!String.IsNullOrWhiteSpace(field.Value))
                {
                    switch (field.Key)
                    {
                        case "MatterNumber":
                            MatterNumber = int.Parse(field.Value);
                            break;
                        case "Date":
                            try
                            {
                                Date = DateTime.Parse(field.Value);
                                Error = null;
                            }
                            catch (Exception)
                            {
                                Error = "Invalid Date";
                            }
                            //Date = DateTime.Parse(field.Value);
                            break;
                        case "Quantity":
                            Qty = decimal.Parse(field.Value);
                            break;
                        case "Price":
                            Amount = decimal.Parse(field.Value);
                            break;
                        case "Description":
                            Description = field.Value;
                            break;
                        case "Status (Billable/Billed/NotBillable)":
                            Status = field.Value;
                            break;
                        case "Billed By (Email Address)":
                            User = field.Value;
                            break;
                    }
                }
            }
        }

        public string Error { get; set; }

        [Name("MatterNumber")]
        public int? MatterNumber { get; set; }
        [Name("Date")]
        public DateTime? Date { get; set; }
        [Name("Quantity")]
        public decimal Qty { get; set; }
        [Name("Price")]
        public decimal Amount { get; set; }
        [Name("Description")]
        public string Description { get; set; }
        [Name("Status (Billable/Billed/NotBillable)")]
        public string Status { get; set; }
        [Name("Billed By (Email Address)")]
        public string User { get; set; }
    }

    public class FlatFeeFields
    {
        public FlatFeeFields(dynamic row)
        {
            foreach (var field in row)
            {
                if (!String.IsNullOrWhiteSpace(field.Value))
                {
                    switch (field.Key)
                    {
                        case "MatterNumber":
                            MatterNumber = int.Parse(field.Value);
                            break;
                        case "Date":
                            try
                            {
                                Date = DateTime.Parse(field.Value);
                                Error = null;
                            }
                            catch (Exception)
                            {
                                Error = "Invalid Date";
                            }
                            break;
                        case "Quantity":
                            Qty = decimal.Parse(field.Value);
                            break;
                        case "Price":
                            Price = decimal.Parse(field.Value);
                            break;
                        case "Description":
                            Description = field.Value;
                            break;
                        case "Status (Billable/Billed/NotBillable)":
                            Status = field.Value;
                            break;
                        case "Billed By (Email Address)":
                            User = field.Value;
                            break;
                        case "ItemCode":
                            ItemCode = field.Value;
                            break;
                    }
                }
            }
        }

        public string Error { get; set; }

        [Name("MatterNumber")]
        public int? MatterNumber { get; set; }
        [Name("Date")]
        public DateTime? Date { get; set; }
        [Name("Quantity")]
        public decimal Qty { get; set; }
        [Name("Price")]
        public decimal Price { get; set; }
        [Name("Description")]
        public string Description { get; set; }
        [Name("Status (Billable/Billed/NotBillable)")]
        public string Status { get; set; }
        [Name("Billed By (Email Address)")]
        public string User { get; set; }
        [Name("ItemCode")]
        public string ItemCode { get; set; }
    }

    public class RelationshipFields
    {
        public RelationshipFields(dynamic row)
        {
            foreach (var field in row)
            {
                if (!String.IsNullOrWhiteSpace(field.Value))
                {
                    switch (field.Key)
                    {
                        case "ContactNumber":
                            ContactNumber = int.Parse(field.Value);
                            break;
                        case "MatterNumber":
                            MatterNumber = int.Parse(field.Value);
                            break;
                        case "RelationshipName":
                            RelationshipName = field.Value;
                            break;
                        case "RelationshipNotes":
                            Notes = field.Value;
                            break; 
                    }
                }
            }
        }
        [Name("ContactNumber")]
        public int? ContactNumber { get; set; }
        [Name("MatterNumber")]
        public int? MatterNumber { get; set; }
        [Name("RelationshipName")]
        public string RelationshipName { get; set; }
        [Name("RelationshipNotes")]
        public string Notes { get; set; }
    }

}
