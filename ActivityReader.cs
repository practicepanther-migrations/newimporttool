﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using CsvParser.Models;
using System.Data;

namespace CsvParser
{
    public class ActivityReader
    {
        public ActivityReader (string userEmail, string fileName, string firmDirectory)
        {
            DbFunctions = new DatabaseFunctions(userEmail);
            FirmDirectory = firmDirectory;
            FileName = fileName;
        }

        public static string FirmDirectory { get; set; }
        public static string FileName { get; set; }
        static DatabaseFunctions DbFunctions { get; set; }

        /// <summary>
        /// Parses provided CSV and inserts into database
        /// </summary>
        /// <param name="filepath">Filepath of the CSV to import</param>
        /// <param name="rowsPerInsert">Number of rows to include in each SqlBulkInsert</param>
        /// <param name="importType">Can pass Events, Tasks, Notes</param>
        /// <returns></returns>
        public async Task<int> UploadActivityCsv (string filepath, int rowsPerInsert, ImportType importType)
        {
            List<Activity> activitiesToInsert = new List<Activity>(); // store activities here for bulk insert
            using (var reader = new StreamReader(filepath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Read();
                csv.ReadHeader();
                List<string> headers = csv.Context.HeaderRecord.ToList();
                var rows = csv.GetRecords<dynamic>();
                UploadResults uploadResults = null;
                switch (importType)
                {
                    case ImportType.Events:
                        HelperFunctions.ValidateHeaders(headers, importType);
                        uploadResults = await DbFunctions.ImportEventCsv(rows.ToList(), rowsPerInsert);
                        break;
                    case ImportType.Tasks:
                        HelperFunctions.ValidateHeaders(headers, importType);
                        uploadResults = await DbFunctions.ImportTaskCsv(rows.ToList(), rowsPerInsert);
                        break;
                    case ImportType.Notes:
                        HelperFunctions.ValidateHeaders(headers, importType);
                        uploadResults = await DbFunctions.ImportNoteCsv(rows.ToList(), rowsPerInsert);
                        break;
                    case ImportType.Relationships:
                        HelperFunctions.ValidateHeaders(headers, importType);
                        uploadResults = await DbFunctions.ImportRelationshipsCsv(rows.ToList(), rowsPerInsert);
                        break;
                }

                var successPath = FileName.Replace(".csv", "") + " - Success File.csv";
                using (var successWriter = new StreamWriter(successPath))
                using (var successCsv = new CsvWriter(successWriter, CultureInfo.InvariantCulture))
                {
                    successCsv.WriteRecords(uploadResults.SuccessRows);
                }

                var errorPath = FileName.Replace(".csv", "") + " - Error File.csv";
                using (var errorWriter = new StreamWriter(errorPath))
                using (var errorCsv = new CsvWriter(errorWriter, CultureInfo.InvariantCulture))
                {
                    errorCsv.WriteRecords(uploadResults.ErrorRows);
                }
                var sqlLogOutputPath = Path.Combine(FirmDirectory, "Queries.txt");
                File.WriteAllLines(sqlLogOutputPath, DbFunctions.SqlLogFile);
                DbFunctions.CloseConnection();

                var uploadSummary = uploadResults.GetUploadSummary(rowsPerInsert);
                var splitDateTime = uploadResults.StartTime.ToString().Split(' ');
                var uploadSummaryFileName = "Upload Summary " + splitDateTime[0] + " " + splitDateTime[1] + ".txt";
                var uploadSummaryPath = Path.Combine(FirmDirectory, uploadSummaryFileName.Replace("/", "-").Replace(":", ""));
                File.WriteAllLines(uploadSummaryPath, uploadSummary);

                return uploadResults.SuccessRows.Count;
            }
        }
    }
}
