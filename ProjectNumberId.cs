﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvParser
{
    public class ProjectNumberId
    {
        public int Number { get; set; }
        public long Id { get; set; }
        public long AccountId { get; set; }
    }
}
