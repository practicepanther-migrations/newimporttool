Opened connection at 11/6/2020 5:31:02 PM -05:00
Started transaction at 11/6/2020 5:31:02 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, NULL, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, NULL, NULL, @11, NULL, NULL, @12, NULL, NULL, NULL, @13, NULL, NULL, NULL, NULL, @14, @15, NULL, @16, @17, @18, NULL, NULL, NULL, @19)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'kbennie3g1@mysql.com' (Type = String, Size = 100)
-- @1: '727-741-4007' (Type = String, Size = 50)
-- @2: '651-811-0178' (Type = String, Size = 50)
-- @3: '304-482-1406' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '11/6/2020 5:31:02 PM' (Type = DateTime2)
-- @7: '11/6/2020 5:31:02 PM' (Type = DateTime2)
-- @8: '116087' (Type = Int32)
-- @9: '116087' (Type = Int32)
-- @10: '524647' (Type = Int64)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: '511e7632-999c-43da-ab45-e2327d0f3803' (Type = Guid)
-- @15: 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: 'Kristan' (Type = String, Size = 50)
-- @18: 'Bennie' (Type = String, Size = 50)
-- @19: 'Kristan  Bennie' (Type = String, Size = 255)
-- Executing at 11/6/2020 5:31:02 PM -05:00
-- Completed in 199 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 5:31:02 PM' (Type = DateTime2)
-- @4: '11/6/2020 5:31:02 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3276736' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '0105f2af-2e8d-4c80-9333-2f831787c46a' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '4466' (Type = Int32)
-- @21: '4466 - Kristan  Bennie' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:31:02 PM -05:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '7 Bartillon Drive' (Type = String, Size = 100)
-- @2: 'Saint Paul' (Type = String, Size = 50)
-- @3: 'MN' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '55127' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '11/6/2020 5:31:02 PM' (Type = DateTime2)
-- @9: '11/6/2020 5:31:02 PM' (Type = DateTime2)
-- @10: '116087' (Type = Int32)
-- @11: '116087' (Type = Int32)
-- @12: '3618705' (Type = Int64)
-- @13: '524647' (Type = Int64)
-- Executing at 11/6/2020 5:31:02 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3618705' (Type = Int64)
-- Executing at 11/6/2020 5:31:02 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9a29e078-1b5c-41fa-b5b7-bd207049bec6' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Switchable intangible database' (Type = String, Size = -1)
-- @3: '3276736' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:31:02 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 5:31:02 PM -05:00
Closed connection at 11/6/2020 5:31:02 PM -05:00
Opened connection at 11/6/2020 5:31:02 PM -05:00
Started transaction at 11/6/2020 5:31:02 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5046701,
	ShippingAddressId = 5046702
WHERE Id = 3618705;

UPDATE Contacts SET
	AccountId = 3618705
WHERE Id = 3276736;
INSERT INTO AccountUsers VALUES(116087, 3618705);

-- Executing at 11/6/2020 5:31:02 PM -05:00
-- Completed in 66 ms with result: 3

Committed transaction at 11/6/2020 5:31:02 PM -05:00
Closed connection at 11/6/2020 5:31:02 PM -05:00
