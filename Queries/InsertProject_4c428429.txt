Opened connection at 11/6/2020 5:01:50 PM -05:00
Started transaction at 11/6/2020 5:01:50 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Flowdesk' (Type = String, Size = 800)
-- @1: '11/6/2020 5:01:50 PM' (Type = DateTime2)
-- @2: '11/6/2020 5:01:50 PM' (Type = DateTime2)
-- @3: '3617213' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '7/10/2014 12:00:00 AM' (Type = DateTime2)
-- @8: '3' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '4c428429-40db-4515-8b65-60f4748dae23' (Type = Guid)
-- @15: 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '1370' (Type = Int32)
-- @18: '1370 - Flowdesk' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '7/28/2015 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:01:50 PM -05:00
-- Completed in 92 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'fb0c90b6-ff18-4169-b161-a708de61ae3b' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1555674' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:01:51 PM -05:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '3dafd67b-9ebc-4eb4-83b0-7bf7e1aee481' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1555674' (Type = Int64)
-- @3: 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:01:51 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 5:01:51 PM -05:00
Closed connection at 11/6/2020 5:01:51 PM -05:00
Opened connection at 11/6/2020 5:01:51 PM -05:00
Started transaction at 11/6/2020 5:01:51 PM -05:00
INSERT INTO ProjectUsers VALUES(1555674, 116087);
INSERT INTO TagProjects VALUES(161947, 1555674);

-- Executing at 11/6/2020 5:01:51 PM -05:00
-- Completed in 54 ms with result: 2

Committed transaction at 11/6/2020 5:01:51 PM -05:00
Closed connection at 11/6/2020 5:01:51 PM -05:00
