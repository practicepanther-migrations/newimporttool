Opened connection at 10/23/2020 2:36:55 PM -04:00
Started transaction at 10/23/2020 2:36:56 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, @24, @25, @26, NULL, @27, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '151.006 Leckie, Janice' (Type = String, Size = 800)
-- @1: '10/23/2020 2:36:41 PM' (Type = DateTime2)
-- @2: '10/23/2020 2:36:41 PM' (Type = DateTime2)
-- @3: '3613942' (Type = Int64)
-- @4: '116076' (Type = Int32)
-- @5: '116076' (Type = Int32)
-- @6: '524646' (Type = Int64)
-- @7: '2' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '150d1f88-e72b-4a3f-be14-840deb27cecc' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10059' (Type = Int32)
-- @16: '10059 - 151.006 Leckie, Janice' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '3/15/2019 5:00:00 PM' (Type = DateTime2)
-- @24: '6/21/2019 5:00:00 PM' (Type = DateTime2)
-- @25: '116076' (Type = Int32)
-- @26: 'False' (Type = Boolean)
-- @27: 'False' (Type = Boolean)
-- Executing at 10/23/2020 2:36:56 PM -04:00
-- Completed in 64 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '46440497-3af6-4708-98f3-e1eb4142f084' (Type = Guid)
-- @1: '169058' (Type = Int64)
-- @2: '1552403' (Type = Int64)
-- @3: '151.006' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 10/23/2020 2:36:56 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '4535c831-3ed5-40fe-9220-df1b6a6bbcc2' (Type = Guid)
-- @1: 'Collections' (Type = String, Size = 50)
-- @2: '524646' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '10/23/2020 2:36:41 PM' (Type = DateTime2)
-- @6: '10/23/2020 2:36:41 PM' (Type = DateTime2)
-- @7: '116076' (Type = Int32)
-- @8: '116076' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 10/23/2020 2:36:56 PM -04:00
-- Completed in 60 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1552403' (Type = Int64)
-- @1: '161862' (Type = Int64)
-- Executing at 10/23/2020 2:36:56 PM -04:00
-- Completed in 55 ms with result: 1

Committed transaction at 10/23/2020 2:36:56 PM -04:00
Closed connection at 10/23/2020 2:36:56 PM -04:00
Opened connection at 10/23/2020 2:36:58 PM -04:00
Started transaction at 10/23/2020 2:36:58 PM -04:00
INSERT INTO ProjectUsers VALUES(1552403, 116076);
INSERT INTO TagProjects VALUES(161860, 1552403);

-- Executing at 10/23/2020 2:36:58 PM -04:00
-- Completed in 55 ms with result: 2

Committed transaction at 10/23/2020 2:36:58 PM -04:00
Closed connection at 10/23/2020 2:36:58 PM -04:00
