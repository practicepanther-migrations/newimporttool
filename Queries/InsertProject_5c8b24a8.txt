Opened connection at 11/6/2020 5:24:29 PM -05:00
Started transaction at 11/6/2020 5:24:30 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Duobam' (Type = String, Size = 800)
-- @1: '11/6/2020 5:24:29 PM' (Type = DateTime2)
-- @2: '11/6/2020 5:24:29 PM' (Type = DateTime2)
-- @3: '3618368' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '3/24/2012 12:00:00 AM' (Type = DateTime2)
-- @8: '3' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '5c8b24a8-3275-40b4-b049-df397af951cf' (Type = Guid)
-- @15: 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '505' (Type = Int32)
-- @18: '505 - Duobam' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '73' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '1/31/2020 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:24:30 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'b634c468-bb5c-4cd1-a366-882c28d09c92' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1556829' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 5:24:30 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '10ac6a24-5293-49f7-bd28-5f49417e928a' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1556829' (Type = Int64)
-- @3: 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:24:30 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 5:24:30 PM -05:00
Closed connection at 11/6/2020 5:24:30 PM -05:00
Opened connection at 11/6/2020 5:24:30 PM -05:00
Started transaction at 11/6/2020 5:24:30 PM -05:00
INSERT INTO ProjectUsers VALUES(1556829, 116087);

-- Executing at 11/6/2020 5:24:30 PM -05:00
-- Completed in 53 ms with result: 1

Committed transaction at 11/6/2020 5:24:30 PM -05:00
Closed connection at 11/6/2020 5:24:30 PM -05:00
