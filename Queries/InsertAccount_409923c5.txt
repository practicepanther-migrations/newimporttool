Opened connection at 10/23/2020 2:25:52 PM -04:00
Started transaction at 10/23/2020 2:25:52 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'joshua.pruegert@gmail.com' (Type = String, Size = 100)
-- @1: '405-274-2367' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '10/23/2020 2:25:52 PM' (Type = DateTime2)
-- @5: '10/23/2020 2:25:52 PM' (Type = DateTime2)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: 'fbd45348-05ff-4fb6-9873-be5e7bdedf8e' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Joshua' (Type = String, Size = 50)
-- @15: 'Pruegert' (Type = String, Size = 50)
-- @16: 'Joshua  Pruegert' (Type = String, Size = 255)
-- Executing at 10/23/2020 2:25:52 PM -04:00
-- Completed in 70 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '10/23/2020 2:25:52 PM' (Type = DateTime2)
-- @4: '10/23/2020 2:25:52 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '3271969' (Type = Int64)
-- @10: '409923c5-0252-4058-bdd0-fbdc0f942dbf' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1055' (Type = Int32)
-- @20: '1055 - Joshua  Pruegert' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 10/23/2020 2:25:52 PM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '705 N. Cherokee Way' (Type = String, Size = 100)
-- @2: 'Mustang' (Type = String, Size = 50)
-- @3: 'OK' (Type = String, Size = 50)
-- @4: '73064' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '10/23/2020 2:25:52 PM' (Type = DateTime2)
-- @8: '10/23/2020 2:25:52 PM' (Type = DateTime2)
-- @9: '116076' (Type = Int32)
-- @10: '116076' (Type = Int32)
-- @11: '3613940' (Type = Int64)
-- @12: '524646' (Type = Int64)
-- Executing at 10/23/2020 2:25:52 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3613940' (Type = Int64)
-- Executing at 10/23/2020 2:25:52 PM -04:00
-- Completed in 53 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, @2, NULL, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9427be68-cb00-4fae-8815-1236c86b34cf' (Type = Guid)
-- @1: '169044' (Type = Int64)
-- @2: '10/4/1990 12:00:00 AM' (Type = DateTime2)
-- @3: '3271969' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 10/23/2020 2:25:52 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '1610c0cd-de14-4ece-a190-e93857571bc8' (Type = Guid)
-- @1: '169045' (Type = Int64)
-- @2: '444-98-6370' (Type = String, Size = -1)
-- @3: '3271969' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 10/23/2020 2:25:52 PM -04:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 10/23/2020 2:25:52 PM -04:00
Closed connection at 10/23/2020 2:25:52 PM -04:00
Opened connection at 10/23/2020 2:25:52 PM -04:00
Started transaction at 10/23/2020 2:25:52 PM -04:00
UPDATE Accounts SET
	BillingAddressId = 5037224,
	ShippingAddressId = 5037225
WHERE Id = 3613940;

UPDATE Contacts SET
	AccountId = 3613940
WHERE Id = 3271969;
INSERT INTO TagAccounts VALUES(161853, 3613940);
INSERT INTO AccountUsers VALUES(116076, 3613940);

-- Executing at 10/23/2020 2:25:52 PM -04:00
-- Completed in 66 ms with result: 4

Committed transaction at 10/23/2020 2:25:53 PM -04:00
Closed connection at 10/23/2020 2:25:53 PM -04:00
