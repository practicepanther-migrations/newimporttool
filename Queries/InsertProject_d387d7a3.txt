Opened connection at 6/15/2021 10:15:24 AM -04:00
Started transaction at 6/15/2021 10:15:24 AM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, NULL, NULL, NULL, @23, NULL, @24, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Legal Matters' (Type = String, Size = 800)
-- @1: '6/15/2021 10:15:24 AM' (Type = DateTime2)
-- @2: '6/15/2021 10:15:24 AM' (Type = DateTime2)
-- @3: '3614630' (Type = Int64)
-- @4: '116582' (Type = Int32)
-- @5: '116582' (Type = Int32)
-- @6: '525095' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'd387d7a3-b89c-494b-845e-cb35a95599c1' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '207' (Type = Int32)
-- @16: '207 - Legal Matters' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 6/15/2021 10:15:24 AM -04:00
-- Completed in 61 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'a726161a-15fc-432f-84f3-ef48f992a585' (Type = Guid)
-- @1: '169413' (Type = Int64)
-- @2: '1552895' (Type = Int64)
-- @3: '7042' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 10:15:24 AM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 10:15:24 AM -04:00
Closed connection at 6/15/2021 10:15:24 AM -04:00
Opened connection at 6/15/2021 10:15:24 AM -04:00
Started transaction at 6/15/2021 10:15:24 AM -04:00
INSERT INTO ProjectUsers VALUES(1552895, 116582);

-- Executing at 6/15/2021 10:15:24 AM -04:00
-- Completed in 54 ms with result: 1

Committed transaction at 6/15/2021 10:15:24 AM -04:00
Closed connection at 6/15/2021 10:15:24 AM -04:00
