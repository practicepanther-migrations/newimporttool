Opened connection at 11/6/2020 4:01:48 PM -05:00
Started transaction at 11/6/2020 4:01:48 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, @4, NULL, NULL, @5, @6, @7, @8, @9, @10, @11, NULL, NULL, NULL, @12, NULL, NULL, @13, NULL, NULL, NULL, @14, NULL, NULL, NULL, NULL, @15, @16, NULL, @17, @18, @19, NULL, NULL, NULL, @20)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'aslaney3te@berkeley.edu' (Type = String, Size = 100)
-- @1: '734-793-1740' (Type = String, Size = 50)
-- @2: '901-338-9892' (Type = String, Size = 50)
-- @3: '505-557-5519' (Type = String, Size = 50)
-- @4: '763-508-1903' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '11/6/2020 4:01:48 PM' (Type = DateTime2)
-- @8: '11/6/2020 4:01:48 PM' (Type = DateTime2)
-- @9: '116087' (Type = Int32)
-- @10: '116087' (Type = Int32)
-- @11: '524647' (Type = Int64)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: '0' (Type = Int32)
-- @15: '20cbd342-3c72-470c-896e-8d5efcd88543' (Type = Guid)
-- @16: 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' (Type = String, Size = -1)
-- @17: 'False' (Type = Boolean)
-- @18: 'Ambros' (Type = String, Size = 50)
-- @19: 'Slaney' (Type = String, Size = 50)
-- @20: 'Ambros  Slaney' (Type = String, Size = 255)
-- Executing at 11/6/2020 4:01:48 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 4:01:48 PM' (Type = DateTime2)
-- @4: '11/6/2020 4:01:48 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3272471' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '965f1f99-0a2f-479d-9c7f-b99d1317bf67' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '4947' (Type = Int32)
-- @21: '4947 - Ambros  Slaney' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:01:48 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '18634 Eastlawn Road' (Type = String, Size = 100)
-- @2: 'Memphis' (Type = String, Size = 50)
-- @3: 'TN' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '38197' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '11/6/2020 4:01:48 PM' (Type = DateTime2)
-- @9: '11/6/2020 4:01:48 PM' (Type = DateTime2)
-- @10: '116087' (Type = Int32)
-- @11: '116087' (Type = Int32)
-- @12: '3614440' (Type = Int64)
-- @13: '524647' (Type = Int64)
-- Executing at 11/6/2020 4:01:48 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614440' (Type = Int64)
-- Executing at 11/6/2020 4:01:48 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'd26ab844-5f40-4acf-a008-d4de7b799d3c' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Optional 6th generation framework' (Type = String, Size = -1)
-- @3: '3272471' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:01:48 PM -05:00
-- Completed in 77 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:01:48 PM -05:00
Closed connection at 11/6/2020 4:01:48 PM -05:00
Opened connection at 11/6/2020 4:01:48 PM -05:00
Started transaction at 11/6/2020 4:01:48 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5038171,
	ShippingAddressId = 5038172
WHERE Id = 3614440;

UPDATE Contacts SET
	AccountId = 3614440
WHERE Id = 3272471;
INSERT INTO TagAccounts VALUES(161949, 3614440);
INSERT INTO AccountUsers VALUES(116089, 3614440);

-- Executing at 11/6/2020 4:01:48 PM -05:00
-- Completed in 66 ms with result: 4

Committed transaction at 11/6/2020 4:01:48 PM -05:00
Closed connection at 11/6/2020 4:01:48 PM -05:00
