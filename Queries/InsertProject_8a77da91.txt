Opened connection at 11/6/2020 4:27:03 PM -05:00
Started transaction at 11/6/2020 4:27:03 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Konklab' (Type = String, Size = 800)
-- @1: '11/6/2020 4:27:03 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:27:03 PM' (Type = DateTime2)
-- @3: '3615665' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '7/22/2017 12:00:00 AM' (Type = DateTime2)
-- @8: '4' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '8a77da91-c963-4dbb-aed0-c42f7703eec4' (Type = Guid)
-- @15: 'Fusce consequat. Nulla nisl. Nunc nisl.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '4386' (Type = Int32)
-- @18: '4386 - Konklab' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '9/17/2013 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:27:03 PM -05:00
-- Completed in 282 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '1bfbff2a-5ace-4e48-8a28-7cf5afa071dc' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554126' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:27:03 PM -05:00
-- Completed in 203 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'f9b85cf1-c353-4a1e-a3c9-c1ced9563f5a' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554126' (Type = Int64)
-- @3: 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:27:03 PM -05:00
-- Completed in 201 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:27:04 PM -05:00
Closed connection at 11/6/2020 4:27:04 PM -05:00
Opened connection at 11/6/2020 4:27:04 PM -05:00
Started transaction at 11/6/2020 4:27:04 PM -05:00
INSERT INTO ProjectUsers VALUES(1554126, 116087);

-- Executing at 11/6/2020 4:27:04 PM -05:00
-- Completed in 204 ms with result: 1

Committed transaction at 11/6/2020 4:27:04 PM -05:00
Closed connection at 11/6/2020 4:27:04 PM -05:00
