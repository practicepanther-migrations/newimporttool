Opened connection at 11/6/2020 4:13:54 PM -05:00
Started transaction at 11/6/2020 4:13:54 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Tres-Zap' (Type = String, Size = 800)
-- @1: '11/6/2020 4:13:54 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:13:54 PM' (Type = DateTime2)
-- @3: '3615068' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '10/25/2018 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '9845c157-f4ab-4080-aa37-b395a031c02d' (Type = Guid)
-- @15: 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '2975' (Type = Int32)
-- @18: '2975 - Tres-Zap' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '11/12/2011 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:13:54 PM -05:00
-- Completed in 81 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'ddb70cff-4e9a-45ce-b751-a4104fedbc3f' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1553529' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 4:13:54 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '6c51de3e-a158-46c1-873f-e92f4292e8d9' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1553529' (Type = Int64)
-- @3: 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:13:55 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:13:55 PM -05:00
Closed connection at 11/6/2020 4:13:55 PM -05:00
Opened connection at 11/6/2020 4:13:55 PM -05:00
Started transaction at 11/6/2020 4:13:55 PM -05:00
INSERT INTO ProjectUsers VALUES(1553529, 116087);
INSERT INTO TagProjects VALUES(161954, 1553529);

-- Executing at 11/6/2020 4:13:55 PM -05:00
-- Completed in 54 ms with result: 2

Committed transaction at 11/6/2020 4:13:55 PM -05:00
Closed connection at 11/6/2020 4:13:55 PM -05:00
