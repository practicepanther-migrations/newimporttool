Opened connection at 6/15/2021 10:13:21 AM -04:00
Started transaction at 6/15/2021 10:13:21 AM -04:00
INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @9, NULL, @10, @11, @12, @13, @14, @15, @16, @17, NULL, @18, NULL, NULL, NULL, NULL, @19, @20, @21, NULL, @22, NULL, NULL, NULL, NULL, NULL, @23, NULL, @24)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'BancFirst Sand Springs' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 10:13:21 AM' (Type = DateTime2)
-- @4: '6/15/2021 10:13:21 AM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116582' (Type = Int32)
-- @7: '116582' (Type = Int32)
-- @8: '525095' (Type = Int64)
-- @9: '91c911fe-992e-4486-bfe8-cbea7ae6890a' (Type = Guid)
-- @10: 'en-US' (Type = String, Size = 5)
-- @11: 'USD' (Type = String, Size = 3)
-- @12: 'False' (Type = Boolean)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: '7204' (Type = Int32)
-- @19: '7204 - BancFirst Sand Springs' (Type = String, Size = 255)
-- @20: 'False' (Type = Boolean)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 6/15/2021 10:13:21 AM -04:00
-- Completed in 73 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: 'Kaye Davis P O Box 1010 ' (Type = String, Size = 100)
-- @2: 'Sand Springs' (Type = String, Size = 50)
-- @3: 'OK' (Type = String, Size = 50)
-- @4: '74063' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/15/2021 10:13:21 AM' (Type = DateTime2)
-- @8: '6/15/2021 10:13:21 AM' (Type = DateTime2)
-- @9: '116582' (Type = Int32)
-- @10: '116582' (Type = Int32)
-- @11: '3614506' (Type = Int64)
-- @12: '525095' (Type = Int64)
-- Executing at 6/15/2021 10:13:21 AM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614506' (Type = Int64)
-- Executing at 6/15/2021 10:13:21 AM -04:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 6/15/2021 10:13:21 AM -04:00
Closed connection at 6/15/2021 10:13:21 AM -04:00
Opened connection at 6/15/2021 10:13:21 AM -04:00
Started transaction at 6/15/2021 10:13:21 AM -04:00
UPDATE Accounts SET
	BillingAddressId = 5039102,
	ShippingAddressId = 5039103
WHERE Id = 3614506;

INSERT INTO TagAccounts VALUES(161898, 3614506);
INSERT INTO AccountUsers VALUES(116582, 3614506);

-- Executing at 6/15/2021 10:13:21 AM -04:00
-- Completed in 55 ms with result: 3

Committed transaction at 6/15/2021 10:13:21 AM -04:00
Closed connection at 6/15/2021 10:13:21 AM -04:00
