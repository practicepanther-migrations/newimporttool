Opened connection at 11/6/2020 4:47:56 PM -05:00
Started transaction at 11/6/2020 4:47:56 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Redhold' (Type = String, Size = 800)
-- @1: '11/6/2020 4:47:56 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:47:56 PM' (Type = DateTime2)
-- @3: '3616538' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '11/12/2015 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: 'f45a633b-5e37-4b59-8a3d-46f1beffdf65' (Type = Guid)
-- @15: 'Sed ante. Vivamus tortor. Duis mattis egestas metus.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '3499' (Type = Int32)
-- @18: '3499 - Redhold' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '29' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '9/23/2015 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:47:56 PM -05:00
-- Completed in 251 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'e52abc4d-9db3-430d-982d-e95a0cd5449c' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554999' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 4:47:56 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'fb99a565-f3a8-4cb2-861f-c73b3077887e' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554999' (Type = Int64)
-- @3: 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:47:56 PM -05:00
-- Completed in 58 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:47:57 PM -05:00
Closed connection at 11/6/2020 4:47:57 PM -05:00
Opened connection at 11/6/2020 4:47:57 PM -05:00
Started transaction at 11/6/2020 4:47:57 PM -05:00
INSERT INTO ProjectUsers VALUES(1554999, 116087);

-- Executing at 11/6/2020 4:47:57 PM -05:00
-- Completed in 54 ms with result: 1

Committed transaction at 11/6/2020 4:47:57 PM -05:00
Closed connection at 11/6/2020 4:47:57 PM -05:00
