Opened connection at 11/6/2020 4:03:19 PM -05:00
Started transaction at 11/6/2020 4:03:19 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Cardguard' (Type = String, Size = 800)
-- @1: '11/6/2020 4:03:19 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:03:19 PM' (Type = DateTime2)
-- @3: '3614519' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '11/24/2019 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '470b1df8-35cf-48d3-9a2d-d6c8a6c46bfe' (Type = Guid)
-- @15: 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '499' (Type = Int32)
-- @18: '499 - Cardguard' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '8/15/2014 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:03:19 PM -05:00
-- Completed in 138 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '81e60933-9293-4535-9714-70395272c1a1' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1552980' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 4:03:19 PM -05:00
-- Completed in 164 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '764da3e2-1214-49d8-8e5a-c9330078f74b' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1552980' (Type = Int64)
-- @3: 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:03:19 PM -05:00
-- Completed in 160 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:03:19 PM -05:00
Closed connection at 11/6/2020 4:03:19 PM -05:00
Opened connection at 11/6/2020 4:03:19 PM -05:00
Started transaction at 11/6/2020 4:03:19 PM -05:00
INSERT INTO ProjectUsers VALUES(1552980, 116087);

-- Executing at 11/6/2020 4:03:19 PM -05:00
-- Completed in 87 ms with result: 1

Committed transaction at 11/6/2020 4:03:20 PM -05:00
Closed connection at 11/6/2020 4:03:20 PM -05:00
