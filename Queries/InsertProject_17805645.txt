Opened connection at 11/6/2020 3:56:08 PM -05:00
Started transaction at 11/6/2020 3:56:08 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, NULL, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, NULL, @25, @26, NULL, @27, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Regrant' (Type = String, Size = 800)
-- @1: '11/6/2020 3:56:08 PM' (Type = DateTime2)
-- @2: '11/6/2020 3:56:08 PM' (Type = DateTime2)
-- @3: '3614141' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '8/22/2020 12:00:00 AM' (Type = DateTime2)
-- @8: '3' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '17805645-37a8-49d1-ae90-3bdbf02425b7' (Type = Guid)
-- @15: 'False' (Type = Boolean)
-- @16: '723' (Type = Int32)
-- @17: '723 - Regrant' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '9/8/2012 12:00:00 AM' (Type = DateTime2)
-- @25: '116087' (Type = Int32)
-- @26: 'False' (Type = Boolean)
-- @27: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:56:08 PM -05:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9ea49bcb-ea17-4a14-9fc3-5a2f40a4a4a2' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1552602' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 3:56:08 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '80585fc9-2352-4deb-8baf-97866fbd97a7' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1552602' (Type = Int64)
-- @3: 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:56:08 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 3:56:08 PM -05:00
Closed connection at 11/6/2020 3:56:08 PM -05:00
Opened connection at 11/6/2020 3:56:08 PM -05:00
Started transaction at 11/6/2020 3:56:08 PM -05:00
INSERT INTO ProjectUsers VALUES(1552602, 116087);
INSERT INTO TagProjects VALUES(161921, 1552602);

-- Executing at 11/6/2020 3:56:08 PM -05:00
-- Completed in 54 ms with result: 2

Committed transaction at 11/6/2020 3:56:08 PM -05:00
Closed connection at 11/6/2020 3:56:08 PM -05:00
