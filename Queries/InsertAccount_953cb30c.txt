Opened connection at 11/6/2020 4:35:22 PM -05:00
Started transaction at 11/6/2020 4:35:22 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, NULL, NULL, NULL, NULL, NULL, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, NULL, NULL, @12, NULL, NULL, NULL, NULL, @13, @14, NULL, @15, @16, @17, NULL, NULL, NULL, @18)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'rpennicott3h6@amazonaws.com' (Type = String, Size = 100)
-- @1: '213-353-1587' (Type = String, Size = 50)
-- @2: '209-286-1753' (Type = String, Size = 50)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 4:35:22 PM' (Type = DateTime2)
-- @6: '11/6/2020 4:35:22 PM' (Type = DateTime2)
-- @7: '116087' (Type = Int32)
-- @8: '116087' (Type = Int32)
-- @9: '524647' (Type = Int64)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '64aff510-40e5-4595-b69c-e07b7026e531' (Type = Guid)
-- @14: 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: 'Rosa' (Type = String, Size = 50)
-- @17: 'Pennicott' (Type = String, Size = 50)
-- @18: 'Rosa  Pennicott' (Type = String, Size = 255)
-- Executing at 11/6/2020 4:35:22 PM -05:00
-- Completed in 59 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 4:35:22 PM' (Type = DateTime2)
-- @4: '11/6/2020 4:35:22 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3273973' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '953cb30c-2938-4d82-8e8d-143933505991' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '4507' (Type = Int32)
-- @21: '4507 - Rosa  Pennicott' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:35:22 PM -05:00
-- Completed in 224 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '8 Westend Crossing' (Type = String, Size = 100)
-- @2: '8533' (Type = String, Size = 100)
-- @3: 'Fresno' (Type = String, Size = 50)
-- @4: 'CA' (Type = String, Size = 50)
-- @5: 'United States' (Type = String, Size = 50)
-- @6: '93721' (Type = String, Size = 50)
-- @7: 'True' (Type = Boolean)
-- @8: 'False' (Type = Boolean)
-- @9: '11/6/2020 4:35:22 PM' (Type = DateTime2)
-- @10: '11/6/2020 4:35:22 PM' (Type = DateTime2)
-- @11: '116087' (Type = Int32)
-- @12: '116087' (Type = Int32)
-- @13: '3615942' (Type = Int64)
-- @14: '524647' (Type = Int64)
-- Executing at 11/6/2020 4:35:22 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3615942' (Type = Int64)
-- Executing at 11/6/2020 4:35:22 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '33394ad5-0949-4c39-b5c9-51a43b4cc944' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Reactive directional neural-net' (Type = String, Size = -1)
-- @3: '3273973' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:35:22 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:35:22 PM -05:00
Closed connection at 11/6/2020 4:35:22 PM -05:00
Opened connection at 11/6/2020 4:35:22 PM -05:00
Started transaction at 11/6/2020 4:35:22 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5041175,
	ShippingAddressId = 5041176
WHERE Id = 3615942;

UPDATE Contacts SET
	AccountId = 3615942
WHERE Id = 3273973;
INSERT INTO AccountUsers VALUES(116087, 3615942);

-- Executing at 11/6/2020 4:35:22 PM -05:00
-- Completed in 68 ms with result: 3

Committed transaction at 11/6/2020 4:35:22 PM -05:00
Closed connection at 11/6/2020 4:35:22 PM -05:00
