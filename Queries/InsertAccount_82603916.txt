Opened connection at 10/23/2020 11:48:52 AM -04:00
Started transaction at 10/23/2020 11:48:52 AM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, NULL, NULL, @12, NULL, NULL, NULL, NULL, @13, @14, NULL, @15, @16, @17, NULL, NULL, NULL, @18)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'krbuilderssean@gmail.com' (Type = String, Size = 100)
-- @1: '405-831-2622' (Type = String, Size = 50)
-- @2: '405-745-4558 x: 103' (Type = String, Size = 50)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '10/23/2020 11:48:52 AM' (Type = DateTime2)
-- @6: '10/23/2020 11:48:52 AM' (Type = DateTime2)
-- @7: '116076' (Type = Int32)
-- @8: '116076' (Type = Int32)
-- @9: '524646' (Type = Int64)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '71a4b276-8bde-489a-a4a7-b735d2c26bf8' (Type = Guid)
-- @14: 'General Manager of K&R' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: 'Sean' (Type = String, Size = 50)
-- @17: 'Bailey' (Type = String, Size = 50)
-- @18: 'Sean  Bailey' (Type = String, Size = 255)
-- Executing at 10/23/2020 11:48:52 AM -04:00
-- Completed in 96 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, NULL, @20, @21, NULL, @22, NULL, NULL, NULL, NULL, NULL, @23, NULL, @24)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'K&R Builders, Inc.' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '10/23/2020 11:48:52 AM' (Type = DateTime2)
-- @4: '10/23/2020 11:48:52 AM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '3271960' (Type = Int64)
-- @10: '82603916-bf10-4619-999f-97dcd3510d20' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1051' (Type = Int32)
-- @20: 'False' (Type = Boolean)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 10/23/2020 11:48:52 AM -04:00
-- Completed in 95 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: 'PO Box 656' (Type = String, Size = 100)
-- @2: 'Wheatland' (Type = String, Size = 50)
-- @3: 'OK' (Type = String, Size = 50)
-- @4: '73097' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '10/23/2020 11:48:52 AM' (Type = DateTime2)
-- @8: '10/23/2020 11:48:52 AM' (Type = DateTime2)
-- @9: '116076' (Type = Int32)
-- @10: '116076' (Type = Int32)
-- @11: '3613931' (Type = Int64)
-- @12: '524646' (Type = Int64)
-- Executing at 10/23/2020 11:48:53 AM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3613931' (Type = Int64)
-- Executing at 10/23/2020 11:48:53 AM -04:00
-- Completed in 53 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '6ff087fa-2a74-49b9-a294-24ab07225a85' (Type = Guid)
-- @1: '169047' (Type = Int64)
-- @2: 'PO Box 656, Wheatland, OK  73097' (Type = String, Size = -1)
-- @3: '3271960' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 10/23/2020 11:48:53 AM -04:00
-- Completed in 61 ms with result: SqlDataReader

Committed transaction at 10/23/2020 11:48:53 AM -04:00
Closed connection at 10/23/2020 11:48:53 AM -04:00
Opened connection at 10/23/2020 11:48:53 AM -04:00
Started transaction at 10/23/2020 11:48:53 AM -04:00
UPDATE Accounts SET
	BillingAddressId = 5037210,
	ShippingAddressId = 5037211
WHERE Id = 3613931;

UPDATE Contacts SET
	AccountId = 3613931
WHERE Id = 3271960;
INSERT INTO TagAccounts VALUES(161853, 3613931);
INSERT INTO AccountUsers VALUES(116076, 3613931);

-- Executing at 10/23/2020 11:48:53 AM -04:00
-- Completed in 71 ms with result: 4

Committed transaction at 10/23/2020 11:48:53 AM -04:00
Closed connection at 10/23/2020 11:48:53 AM -04:00
