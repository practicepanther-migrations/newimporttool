Opened connection at 6/15/2021 10:18:34 AM -04:00
Started transaction at 6/15/2021 10:18:34 AM -04:00
INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @9, NULL, @10, @11, @12, @13, @14, @15, @16, @17, NULL, @18, NULL, NULL, NULL, NULL, @19, @20, @21, NULL, @22, NULL, NULL, NULL, NULL, NULL, @23, NULL, @24)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Brown Children' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 10:18:34 AM' (Type = DateTime2)
-- @4: '6/15/2021 10:18:34 AM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116582' (Type = Int32)
-- @7: '116582' (Type = Int32)
-- @8: '525095' (Type = Int64)
-- @9: '404c7ecb-7add-4c44-9f13-028c74e3e3d8' (Type = Guid)
-- @10: 'en-US' (Type = String, Size = 5)
-- @11: 'USD' (Type = String, Size = 3)
-- @12: 'False' (Type = Boolean)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: '661' (Type = Int32)
-- @19: '661 - Brown Children' (Type = String, Size = 255)
-- @20: 'False' (Type = Boolean)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 6/15/2021 10:18:34 AM -04:00
-- Completed in 70 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, @9, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: 'Court Fund' (Type = String, Size = 100)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '6/15/2021 10:18:34 AM' (Type = DateTime2)
-- @5: '6/15/2021 10:18:34 AM' (Type = DateTime2)
-- @6: '116582' (Type = Int32)
-- @7: '116582' (Type = Int32)
-- @8: '3614820' (Type = Int64)
-- @9: '525095' (Type = Int64)
-- Executing at 6/15/2021 10:18:34 AM -04:00
-- Completed in 65 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614820' (Type = Int64)
-- Executing at 6/15/2021 10:18:34 AM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 10:18:34 AM -04:00
Closed connection at 6/15/2021 10:18:34 AM -04:00
Opened connection at 6/15/2021 10:18:34 AM -04:00
Started transaction at 6/15/2021 10:18:34 AM -04:00
UPDATE Accounts SET
	BillingAddressId = 5039722,
	ShippingAddressId = 5039723
WHERE Id = 3614820;

INSERT INTO TagAccounts VALUES(161898, 3614820);
INSERT INTO AccountUsers VALUES(116582, 3614820);

-- Executing at 6/15/2021 10:18:34 AM -04:00
-- Completed in 56 ms with result: 3

Committed transaction at 6/15/2021 10:18:34 AM -04:00
Closed connection at 6/15/2021 10:18:34 AM -04:00
