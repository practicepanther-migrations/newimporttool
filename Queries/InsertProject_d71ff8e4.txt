Opened connection at 11/6/2020 5:34:42 PM -05:00
Started transaction at 11/6/2020 5:34:42 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Lotlux' (Type = String, Size = 800)
-- @1: '11/6/2020 5:34:42 PM' (Type = DateTime2)
-- @2: '11/6/2020 5:34:42 PM' (Type = DateTime2)
-- @3: '3618890' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '2/26/2011 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: 'd71ff8e4-fb9f-48c4-9bdb-66d72148c4c2' (Type = Guid)
-- @15: 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '512' (Type = Int32)
-- @18: '512 - Lotlux' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '17' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '2/28/2014 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:34:42 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'fc521508-facb-4ab1-bd9d-5aaf4d94f908' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1557351' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:34:42 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'b8bf81a1-40d7-4961-a610-1fcfaf1ac1f7' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1557351' (Type = Int64)
-- @3: 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:34:42 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 5:34:42 PM -05:00
Closed connection at 11/6/2020 5:34:42 PM -05:00
Opened connection at 11/6/2020 5:34:42 PM -05:00
Started transaction at 11/6/2020 5:34:42 PM -05:00
INSERT INTO ProjectUsers VALUES(1557351, 116087);
INSERT INTO TagProjects VALUES(161940, 1557351);

-- Executing at 11/6/2020 5:34:42 PM -05:00
-- Completed in 61 ms with result: 2

Committed transaction at 11/6/2020 5:34:42 PM -05:00
Closed connection at 11/6/2020 5:34:42 PM -05:00
