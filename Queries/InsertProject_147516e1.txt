Opened connection at 11/6/2020 4:33:14 PM -05:00
Started transaction at 11/6/2020 4:33:14 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Domainer' (Type = String, Size = 800)
-- @1: '11/6/2020 4:33:14 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:33:14 PM' (Type = DateTime2)
-- @3: '3615842' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '3/14/2017 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '147516e1-7bff-41f8-bfe2-26b0009eff55' (Type = Guid)
-- @15: 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '4478' (Type = Int32)
-- @18: '4478 - Domainer' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '6/17/2013 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:33:14 PM -05:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'faf7a7d5-cc97-4b12-b626-218452ce11ea' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554303' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:33:14 PM -05:00
-- Completed in 65 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'c09c262a-b332-48d8-947d-f38be8711a1d' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554303' (Type = Int64)
-- @3: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:33:14 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:33:14 PM -05:00
Closed connection at 11/6/2020 4:33:14 PM -05:00
Opened connection at 11/6/2020 4:33:14 PM -05:00
Started transaction at 11/6/2020 4:33:14 PM -05:00
INSERT INTO ProjectUsers VALUES(1554303, 116087);
INSERT INTO TagProjects VALUES(161940, 1554303);

-- Executing at 11/6/2020 4:33:14 PM -05:00
-- Completed in 163 ms with result: 2

Committed transaction at 11/6/2020 4:33:15 PM -05:00
Closed connection at 11/6/2020 4:33:15 PM -05:00
