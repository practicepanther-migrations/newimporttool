Opened connection at 10/22/2020 5:02:45 PM -04:00
Started transaction at 10/22/2020 5:02:45 PM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'angelaspringer007@yahoo.com' (Type = String, Size = 100)
-- @1: '405-921-0145' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '10/22/2020 5:02:44 PM' (Type = DateTime2)
-- @5: '10/22/2020 5:02:44 PM' (Type = DateTime2)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '5753cf30-4f22-4447-ab0e-cb3720fd72eb' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Angela' (Type = String, Size = 50)
-- @15: 'Canary' (Type = String, Size = 50)
-- @16: 'Angela  Canary' (Type = String, Size = 255)
-- Executing at 10/22/2020 5:02:45 PM -04:00
-- Completed in 96 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '10/22/2020 5:02:44 PM' (Type = DateTime2)
-- @4: '10/22/2020 5:02:44 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '3271956' (Type = Int64)
-- @10: '2c7b7c25-6d42-4db1-aa3c-85de9abe01e5' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1046' (Type = Int32)
-- @20: '1046 - Angela  Canary' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 10/22/2020 5:02:45 PM -04:00
-- Completed in 66 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '586 NE 22nd Street' (Type = String, Size = 100)
-- @2: 'Newcastle' (Type = String, Size = 50)
-- @3: 'Oklahoma' (Type = String, Size = 50)
-- @4: '73065' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '10/22/2020 5:02:44 PM' (Type = DateTime2)
-- @8: '10/22/2020 5:02:44 PM' (Type = DateTime2)
-- @9: '116076' (Type = Int32)
-- @10: '116076' (Type = Int32)
-- @11: '3613927' (Type = Int64)
-- @12: '524646' (Type = Int64)
-- Executing at 10/22/2020 5:02:45 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3613927' (Type = Int64)
-- Executing at 10/22/2020 5:02:45 PM -04:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, @2, NULL, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '31cddb35-e396-4998-8b54-eccc9f909076' (Type = Guid)
-- @1: '169044' (Type = Int64)
-- @2: '3/13/1972 12:00:00 AM' (Type = DateTime2)
-- @3: '3271956' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 10/22/2020 5:02:45 PM -04:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '00b87104-f69d-4ead-93ef-eef6d3ab86fc' (Type = Guid)
-- @1: '169045' (Type = Int64)
-- @2: '441-86-7455' (Type = String, Size = -1)
-- @3: '3271956' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 10/22/2020 5:02:45 PM -04:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 10/22/2020 5:02:45 PM -04:00
Closed connection at 10/22/2020 5:02:45 PM -04:00
Opened connection at 10/22/2020 5:03:07 PM -04:00
Started transaction at 10/22/2020 5:03:07 PM -04:00
UPDATE Accounts SET BillingAddressId = 5037202, ShippingAddressId = 5037203 WHERE Id = 3613927;
UPDATE Contacts SET AccountId = 3613927 WHERE Id = 3271956;
INSERT INTO TagAccounts VALUES(161853, 3613927);
INSERT INTO AccountUsers VALUES(116076, 3613927);

-- Executing at 10/22/2020 5:03:07 PM -04:00
-- Completed in 68 ms with result: 4

Committed transaction at 10/22/2020 5:03:07 PM -04:00
Closed connection at 10/22/2020 5:03:07 PM -04:00
