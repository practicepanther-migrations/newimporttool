Opened connection at 11/6/2020 4:07:46 PM -05:00
Started transaction at 11/6/2020 4:07:46 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, NULL, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, NULL, NULL, @11, NULL, NULL, @12, NULL, NULL, NULL, @13, NULL, NULL, NULL, NULL, @14, @15, NULL, @16, @17, @18, NULL, NULL, NULL, @19)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'creichhardt30l@timesonline.co.uk' (Type = String, Size = 100)
-- @1: '260-985-2912' (Type = String, Size = 50)
-- @2: '909-184-7662' (Type = String, Size = 50)
-- @3: '602-735-0368' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '11/6/2020 4:07:46 PM' (Type = DateTime2)
-- @7: '11/6/2020 4:07:46 PM' (Type = DateTime2)
-- @8: '116087' (Type = Int32)
-- @9: '116087' (Type = Int32)
-- @10: '524647' (Type = Int64)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: 'c067c2ee-ac8e-43ef-8824-7dd691062339' (Type = Guid)
-- @15: 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: 'Carena' (Type = String, Size = 50)
-- @18: 'Reichhardt' (Type = String, Size = 50)
-- @19: 'Carena  Reichhardt' (Type = String, Size = 255)
-- Executing at 11/6/2020 4:07:46 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 4:07:46 PM' (Type = DateTime2)
-- @4: '11/6/2020 4:07:46 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3272792' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '9b925b31-465f-42b8-8da3-b28ca5c94e38' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '3910' (Type = Int32)
-- @21: '3910 - Carena  Reichhardt' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:07:46 PM -05:00
-- Completed in 118 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '1968 Clove Circle' (Type = String, Size = 100)
-- @2: '91' (Type = String, Size = 100)
-- @3: 'San Bernardino' (Type = String, Size = 50)
-- @4: 'CA' (Type = String, Size = 50)
-- @5: 'United States' (Type = String, Size = 50)
-- @6: '92405' (Type = String, Size = 50)
-- @7: 'True' (Type = Boolean)
-- @8: 'False' (Type = Boolean)
-- @9: '11/6/2020 4:07:46 PM' (Type = DateTime2)
-- @10: '11/6/2020 4:07:46 PM' (Type = DateTime2)
-- @11: '116087' (Type = Int32)
-- @12: '116087' (Type = Int32)
-- @13: '3614761' (Type = Int64)
-- @14: '524647' (Type = Int64)
-- Executing at 11/6/2020 4:07:46 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614761' (Type = Int64)
-- Executing at 11/6/2020 4:07:46 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'adb5c93a-672c-4593-b2a2-f428573b0851' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Cloned multi-state data-warehouse' (Type = String, Size = -1)
-- @3: '3272792' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:07:46 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:07:46 PM -05:00
Closed connection at 11/6/2020 4:07:46 PM -05:00
Opened connection at 11/6/2020 4:07:46 PM -05:00
Started transaction at 11/6/2020 4:07:46 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5038813,
	ShippingAddressId = 5038814
WHERE Id = 3614761;

UPDATE Contacts SET
	AccountId = 3614761
WHERE Id = 3272792;
INSERT INTO TagAccounts VALUES(161925, 3614761);
INSERT INTO AccountUsers VALUES(116089, 3614761);

-- Executing at 11/6/2020 4:07:46 PM -05:00
-- Completed in 103 ms with result: 4

Committed transaction at 11/6/2020 4:07:47 PM -05:00
Closed connection at 11/6/2020 4:07:47 PM -05:00
