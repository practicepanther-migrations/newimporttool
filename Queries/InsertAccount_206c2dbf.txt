Opened connection at 11/6/2020 3:01:48 PM -05:00
Started transaction at 11/6/2020 3:01:48 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, NULL, NULL, NULL, NULL, NULL, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, NULL, NULL, @12, NULL, NULL, NULL, NULL, @13, NULL, NULL, @14, @15, @16, NULL, @17, NULL, @18)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'vgertek@cox.net' (Type = String, Size = 100)
-- @1: '405-830-6339' (Type = String, Size = 50)
-- @2: '405-285-0036' (Type = String, Size = 50)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 3:01:48 PM' (Type = DateTime2)
-- @6: '11/6/2020 3:01:48 PM' (Type = DateTime2)
-- @7: '116076' (Type = Int32)
-- @8: '116076' (Type = Int32)
-- @9: '524646' (Type = Int64)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: 'b00992ae-0a90-4f50-9b92-9dddfd90ed15' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: 'Michael' (Type = String, Size = 50)
-- @16: 'Brown' (Type = String, Size = 50)
-- @17: 'D.' (Type = String, Size = 50)
-- @18: 'Michael D. Brown' (Type = String, Size = 255)
-- Executing at 11/6/2020 3:01:48 PM -05:00
-- Completed in 80 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 3:01:48 PM' (Type = DateTime2)
-- @4: '11/6/2020 3:01:48 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '3272043' (Type = Int64)
-- @10: '206c2dbf-814d-4145-bca4-bb0b4d5b2fdb' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1131' (Type = Int32)
-- @20: '1131 - Michael D. Brown' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:01:48 PM -05:00
-- Completed in 76 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '2606 Rambling Road' (Type = String, Size = 100)
-- @2: 'Edmond' (Type = String, Size = 50)
-- @3: 'OK' (Type = String, Size = 50)
-- @4: '73025' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '11/6/2020 3:01:48 PM' (Type = DateTime2)
-- @8: '11/6/2020 3:01:48 PM' (Type = DateTime2)
-- @9: '116076' (Type = Int32)
-- @10: '116076' (Type = Int32)
-- @11: '3614012' (Type = Int64)
-- @12: '524646' (Type = Int64)
-- Executing at 11/6/2020 3:01:48 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614012' (Type = Int64)
-- Executing at 11/6/2020 3:01:49 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 3:01:49 PM -05:00
Closed connection at 11/6/2020 3:01:49 PM -05:00
Opened connection at 11/6/2020 3:01:49 PM -05:00
Started transaction at 11/6/2020 3:01:49 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5037337,
	ShippingAddressId = 5037338
WHERE Id = 3614012;

UPDATE Contacts SET
	AccountId = 3614012
WHERE Id = 3272043;
INSERT INTO TagAccounts VALUES(161853, 3614012);
INSERT INTO AccountUsers VALUES(116076, 3614012);

-- Executing at 11/6/2020 3:01:49 PM -05:00
-- Completed in 70 ms with result: 4

Committed transaction at 11/6/2020 3:01:49 PM -05:00
Closed connection at 11/6/2020 3:01:49 PM -05:00
