Opened connection at 10/23/2020 12:28:02 PM -04:00
Started transaction at 10/23/2020 12:28:02 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '180.252 K&R Builders, Inc.' (Type = String, Size = 800)
-- @1: '10/23/2020 12:28:02 PM' (Type = DateTime2)
-- @2: '10/23/2020 12:28:02 PM' (Type = DateTime2)
-- @3: '3613931' (Type = Int64)
-- @4: '116076' (Type = Int32)
-- @5: '116076' (Type = Int32)
-- @6: '524646' (Type = Int64)
-- @7: '5/24/2019 5:00:00 PM' (Type = DateTime2)
-- @8: '4' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '4adff388-02a4-4b11-ab62-3cb797559a87' (Type = Guid)
-- @15: 'Sean Bailey is GM.  They were referred to us by Marvin Laws and Bridgette Kennedy.Mike McNeill is the principal of Insurance Agency of MidAmerica.  His son-in-law, Kyle Reser, works for him as well.  They are going to send up more work, I hope.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '10050' (Type = Int32)
-- @18: '10050 - 180.252 K&R Builders, Inc.' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '12/1/2018 6:00:00 PM' (Type = DateTime2)
-- @26: '116076' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 10/23/2020 12:28:02 PM -04:00
-- Completed in 81 ms with result: SqlDataReader

Committed transaction at 10/23/2020 12:28:02 PM -04:00
Closed connection at 10/23/2020 12:28:02 PM -04:00
Opened connection at 10/23/2020 12:28:02 PM -04:00
Started transaction at 10/23/2020 12:28:02 PM -04:00
INSERT INTO ProjectUsers VALUES(1552393, 116076)
-- Executing at 10/23/2020 12:28:02 PM -04:00
-- Completed in 54 ms with result: 1

Committed transaction at 10/23/2020 12:28:02 PM -04:00
Closed connection at 10/23/2020 12:28:02 PM -04:00
