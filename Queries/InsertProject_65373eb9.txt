Opened connection at 11/6/2020 3:01:30 PM -05:00
Started transaction at 11/6/2020 3:01:30 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, NULL, @23, @24, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '180.230 J Hill Homes, Inc.' (Type = String, Size = 800)
-- @1: '11/6/2020 3:01:30 PM' (Type = DateTime2)
-- @2: '11/6/2020 3:01:30 PM' (Type = DateTime2)
-- @3: '3613996' (Type = Int64)
-- @4: '116076' (Type = Int32)
-- @5: '116076' (Type = Int32)
-- @6: '524646' (Type = Int64)
-- @7: '2' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '65373eb9-6d00-4962-a0fa-90468ed34b1e' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10114' (Type = Int32)
-- @16: '10114 - 180.230 J Hill Homes, Inc.' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '2/20/2019 6:00:00 PM' (Type = DateTime2)
-- @24: '116076' (Type = Int32)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:01:30 PM -05:00
-- Completed in 80 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '061f5417-b5a0-43ad-afa2-5eb2e8c4822e' (Type = Guid)
-- @1: '169058' (Type = Int64)
-- @2: '1552458' (Type = Int64)
-- @3: '180.23' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:01:30 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 3:01:30 PM -05:00
Closed connection at 11/6/2020 3:01:30 PM -05:00
Opened connection at 11/6/2020 3:01:30 PM -05:00
Started transaction at 11/6/2020 3:01:30 PM -05:00
INSERT INTO TagProjects VALUES(161908, 1552458);

-- Executing at 11/6/2020 3:01:30 PM -05:00
-- Completed in 70 ms with result: 1

Committed transaction at 11/6/2020 3:01:30 PM -05:00
Closed connection at 11/6/2020 3:01:30 PM -05:00
