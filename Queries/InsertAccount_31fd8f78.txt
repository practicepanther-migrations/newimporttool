Opened connection at 11/6/2020 5:36:15 PM -05:00
Started transaction at 11/6/2020 5:36:15 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, NULL, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, NULL, NULL, @11, NULL, NULL, @12, NULL, NULL, NULL, @13, NULL, NULL, NULL, NULL, @14, @15, NULL, @16, @17, @18, NULL, NULL, NULL, @19)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'rhallan6y@ucoz.com' (Type = String, Size = 100)
-- @1: '951-838-7951' (Type = String, Size = 50)
-- @2: '404-653-1841' (Type = String, Size = 50)
-- @3: '262-421-9603' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '11/6/2020 5:36:15 PM' (Type = DateTime2)
-- @7: '11/6/2020 5:36:15 PM' (Type = DateTime2)
-- @8: '116087' (Type = Int32)
-- @9: '116087' (Type = Int32)
-- @10: '524647' (Type = Int64)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: '634f7f37-de0a-46dc-b141-9b5ea8e886c2' (Type = Guid)
-- @15: 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: 'Roderigo' (Type = String, Size = 50)
-- @18: 'Hallan' (Type = String, Size = 50)
-- @19: 'Roderigo  Hallan' (Type = String, Size = 255)
-- Executing at 11/6/2020 5:36:15 PM -05:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 5:36:15 PM' (Type = DateTime2)
-- @4: '11/6/2020 5:36:15 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3277003' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '31fd8f78-8350-44af-a689-de6657ce368a' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '251' (Type = Int32)
-- @21: '251 - Roderigo  Hallan' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:36:15 PM -05:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '06357 Anhalt Parkway' (Type = String, Size = 100)
-- @2: 'Atlanta' (Type = String, Size = 50)
-- @3: 'GA' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '30336' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '11/6/2020 5:36:15 PM' (Type = DateTime2)
-- @9: '11/6/2020 5:36:15 PM' (Type = DateTime2)
-- @10: '116087' (Type = Int32)
-- @11: '116087' (Type = Int32)
-- @12: '3618972' (Type = Int64)
-- @13: '524647' (Type = Int64)
-- Executing at 11/6/2020 5:36:15 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3618972' (Type = Int64)
-- Executing at 11/6/2020 5:36:15 PM -05:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '44717def-4a89-442d-930d-845b35d49d39' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Cloned real-time neural-net' (Type = String, Size = -1)
-- @3: '3277003' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:36:15 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 5:36:15 PM -05:00
Closed connection at 11/6/2020 5:36:15 PM -05:00
Opened connection at 11/6/2020 5:36:15 PM -05:00
Started transaction at 11/6/2020 5:36:15 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5047235,
	ShippingAddressId = 5047236
WHERE Id = 3618972;

UPDATE Contacts SET
	AccountId = 3618972
WHERE Id = 3277003;
INSERT INTO TagAccounts VALUES(161926, 3618972);
INSERT INTO AccountUsers VALUES(116087, 3618972);

-- Executing at 11/6/2020 5:36:15 PM -05:00
-- Completed in 68 ms with result: 4

Committed transaction at 11/6/2020 5:36:15 PM -05:00
Closed connection at 11/6/2020 5:36:15 PM -05:00
