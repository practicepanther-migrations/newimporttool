Opened connection at 11/6/2020 5:16:15 PM -05:00
Started transaction at 11/6/2020 5:16:15 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, NULL, NULL, NULL, NULL, NULL, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, NULL, NULL, @12, NULL, NULL, NULL, NULL, @13, @14, NULL, @15, @16, @17, NULL, NULL, NULL, @18)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'bstobart3nw@google.com' (Type = String, Size = 100)
-- @1: '320-488-3902' (Type = String, Size = 50)
-- @2: '918-991-6833' (Type = String, Size = 50)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 5:16:15 PM' (Type = DateTime2)
-- @6: '11/6/2020 5:16:15 PM' (Type = DateTime2)
-- @7: '116087' (Type = Int32)
-- @8: '116087' (Type = Int32)
-- @9: '524647' (Type = Int64)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '4369f298-e32c-4a32-9ca5-b59461b12b36' (Type = Guid)
-- @14: 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: 'Blakelee' (Type = String, Size = 50)
-- @17: 'Stobart' (Type = String, Size = 50)
-- @18: 'Blakelee  Stobart' (Type = String, Size = 255)
-- Executing at 11/6/2020 5:16:15 PM -05:00
-- Completed in 135 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 5:16:15 PM' (Type = DateTime2)
-- @4: '11/6/2020 5:16:15 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3275979' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: 'db8942e1-af2e-476b-ba4c-a836b0fdc164' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '4749' (Type = Int32)
-- @21: '4749 - Blakelee  Stobart' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:16:15 PM -05:00
-- Completed in 168 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '557 Bayside Plaza' (Type = String, Size = 100)
-- @2: 'Tulsa' (Type = String, Size = 50)
-- @3: 'OK' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '74116' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '11/6/2020 5:16:15 PM' (Type = DateTime2)
-- @9: '11/6/2020 5:16:15 PM' (Type = DateTime2)
-- @10: '116087' (Type = Int32)
-- @11: '116087' (Type = Int32)
-- @12: '3617948' (Type = Int64)
-- @13: '524647' (Type = Int64)
-- Executing at 11/6/2020 5:16:15 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3617948' (Type = Int64)
-- Executing at 11/6/2020 5:16:15 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'd2fac25d-4589-4242-a177-f3a34f99c292' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Open-architected exuding interface' (Type = String, Size = -1)
-- @3: '3275979' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:16:15 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 11/6/2020 5:16:15 PM -05:00
Closed connection at 11/6/2020 5:16:15 PM -05:00
Opened connection at 11/6/2020 5:16:15 PM -05:00
Started transaction at 11/6/2020 5:16:16 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5045187,
	ShippingAddressId = 5045188
WHERE Id = 3617948;

UPDATE Contacts SET
	AccountId = 3617948
WHERE Id = 3275979;
INSERT INTO AccountUsers VALUES(116088, 3617948);

-- Executing at 11/6/2020 5:16:16 PM -05:00
-- Completed in 68 ms with result: 3

Committed transaction at 11/6/2020 5:16:16 PM -05:00
Closed connection at 11/6/2020 5:16:16 PM -05:00
