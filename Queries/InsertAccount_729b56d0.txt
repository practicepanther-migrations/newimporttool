Opened connection at 11/6/2020 3:00:48 PM -05:00
Started transaction at 11/6/2020 3:00:48 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, NULL, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, NULL, NULL, @11, NULL, NULL, @12, NULL, NULL, NULL, @13, NULL, NULL, NULL, NULL, @14, NULL, NULL, @15, @16, @17, NULL, NULL, NULL, @18)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'viola.malloy@gmail.com' (Type = String, Size = 100)
-- @1: '(405) 226-4258' (Type = String, Size = 50)
-- @2: '432-202-1794 (Work Number)' (Type = String, Size = 50)
-- @3: '(405) 376-3732' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '11/6/2020 3:00:48 PM' (Type = DateTime2)
-- @7: '11/6/2020 3:00:48 PM' (Type = DateTime2)
-- @8: '116076' (Type = Int32)
-- @9: '116076' (Type = Int32)
-- @10: '524646' (Type = Int64)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: 'cb295622-a104-459c-bdcc-0454df71be24' (Type = Guid)
-- @15: 'False' (Type = Boolean)
-- @16: 'Jerry' (Type = String, Size = 50)
-- @17: 'Malloy' (Type = String, Size = 50)
-- @18: 'Jerry  Malloy' (Type = String, Size = 255)
-- Executing at 11/6/2020 3:00:48 PM -05:00
-- Completed in 284 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 3:00:48 PM' (Type = DateTime2)
-- @4: '11/6/2020 3:00:48 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '3271996' (Type = Int64)
-- @10: '729b56d0-71b2-472d-8846-b535a8f25d5e' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1084' (Type = Int32)
-- @20: '1084 - Jerry  Malloy' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:00:48 PM -05:00
-- Completed in 148 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '709 N. Pine Branch Way' (Type = String, Size = 100)
-- @2: 'Mustang' (Type = String, Size = 50)
-- @3: 'OK' (Type = String, Size = 50)
-- @4: '73064' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '11/6/2020 3:00:48 PM' (Type = DateTime2)
-- @8: '11/6/2020 3:00:48 PM' (Type = DateTime2)
-- @9: '116076' (Type = Int32)
-- @10: '116076' (Type = Int32)
-- @11: '3613965' (Type = Int64)
-- @12: '524646' (Type = Int64)
-- Executing at 11/6/2020 3:00:48 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3613965' (Type = Int64)
-- Executing at 11/6/2020 3:00:48 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 11/6/2020 3:00:48 PM -05:00
Closed connection at 11/6/2020 3:00:48 PM -05:00
Opened connection at 11/6/2020 3:00:48 PM -05:00
Started transaction at 11/6/2020 3:00:48 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5037265,
	ShippingAddressId = 5037266
WHERE Id = 3613965;

UPDATE Contacts SET
	AccountId = 3613965
WHERE Id = 3271996;
INSERT INTO TagAccounts VALUES(161853, 3613965);
INSERT INTO AccountUsers VALUES(116076, 3613965);

-- Executing at 11/6/2020 3:00:48 PM -05:00
-- Completed in 66 ms with result: 4

Committed transaction at 11/6/2020 3:00:49 PM -05:00
Closed connection at 11/6/2020 3:00:49 PM -05:00
