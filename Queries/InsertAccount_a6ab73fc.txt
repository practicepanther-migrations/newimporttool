Opened connection at 11/6/2020 4:00:02 PM -05:00
Started transaction at 11/6/2020 4:00:02 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, NULL, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, NULL, NULL, @11, NULL, NULL, @12, NULL, NULL, NULL, @13, NULL, NULL, NULL, NULL, @14, @15, NULL, @16, @17, @18, NULL, NULL, NULL, @19)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'kstigger1ve@cafepress.com' (Type = String, Size = 100)
-- @1: '816-195-4248' (Type = String, Size = 50)
-- @2: '718-669-0579' (Type = String, Size = 50)
-- @3: '951-581-0524' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '11/6/2020 4:00:02 PM' (Type = DateTime2)
-- @7: '11/6/2020 4:00:02 PM' (Type = DateTime2)
-- @8: '116087' (Type = Int32)
-- @9: '116087' (Type = Int32)
-- @10: '524647' (Type = Int64)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: '0b7c3b0a-7b43-4ca1-95df-a84ae382f03c' (Type = Guid)
-- @15: 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: 'Kalindi' (Type = String, Size = 50)
-- @18: 'Stigger' (Type = String, Size = 50)
-- @19: 'Kalindi  Stigger' (Type = String, Size = 255)
-- Executing at 11/6/2020 4:00:02 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 4:00:02 PM' (Type = DateTime2)
-- @4: '11/6/2020 4:00:02 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3272376' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: 'a6ab73fc-3251-48a3-ac5f-a46ebdb4dba7' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '2427' (Type = Int32)
-- @21: '2427 - Kalindi  Stigger' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:00:02 PM -05:00
-- Completed in 131 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '23 Sycamore Court' (Type = String, Size = 100)
-- @2: '6' (Type = String, Size = 100)
-- @3: 'Bronx' (Type = String, Size = 50)
-- @4: 'NY' (Type = String, Size = 50)
-- @5: 'United States' (Type = String, Size = 50)
-- @6: '10464' (Type = String, Size = 50)
-- @7: 'True' (Type = Boolean)
-- @8: 'False' (Type = Boolean)
-- @9: '11/6/2020 4:00:02 PM' (Type = DateTime2)
-- @10: '11/6/2020 4:00:02 PM' (Type = DateTime2)
-- @11: '116087' (Type = Int32)
-- @12: '116087' (Type = Int32)
-- @13: '3614345' (Type = Int64)
-- @14: '524647' (Type = Int64)
-- Executing at 11/6/2020 4:00:02 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614345' (Type = Int64)
-- Executing at 11/6/2020 4:00:02 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '8dfc34c0-7c30-446c-8bc4-764b89d45789' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Universal optimal encoding' (Type = String, Size = -1)
-- @3: '3272376' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:00:02 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:00:02 PM -05:00
Closed connection at 11/6/2020 4:00:02 PM -05:00
Opened connection at 11/6/2020 4:00:02 PM -05:00
Started transaction at 11/6/2020 4:00:02 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5037981,
	ShippingAddressId = 5037982
WHERE Id = 3614345;

UPDATE Contacts SET
	AccountId = 3614345
WHERE Id = 3272376;
INSERT INTO TagAccounts VALUES(161923, 3614345);
INSERT INTO AccountUsers VALUES(116088, 3614345);

-- Executing at 11/6/2020 4:00:02 PM -05:00
-- Completed in 66 ms with result: 4

Committed transaction at 11/6/2020 4:00:02 PM -05:00
Closed connection at 11/6/2020 4:00:02 PM -05:00
