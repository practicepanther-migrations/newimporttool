Opened connection at 11/6/2020 4:53:04 PM -05:00
Started transaction at 11/6/2020 4:53:04 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Voltsillam' (Type = String, Size = 800)
-- @1: '11/6/2020 4:53:04 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:53:04 PM' (Type = DateTime2)
-- @3: '3616781' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '5/25/2015 12:00:00 AM' (Type = DateTime2)
-- @8: '3' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '9fd8e4e0-dcb1-41df-b5eb-a3467c5db967' (Type = Guid)
-- @15: 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '2987' (Type = Int32)
-- @18: '2987 - Voltsillam' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '12/20/2011 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:53:04 PM -05:00
-- Completed in 78 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'a74a705c-6246-43ce-bd87-b514e9846c14' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1555242' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:53:04 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '18eaccb7-44b1-45b1-af8e-0cd3530f682e' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1555242' (Type = Int64)
-- @3: 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:53:04 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:53:04 PM -05:00
Closed connection at 11/6/2020 4:53:04 PM -05:00
Opened connection at 11/6/2020 4:53:04 PM -05:00
Started transaction at 11/6/2020 4:53:04 PM -05:00
INSERT INTO ProjectUsers VALUES(1555242, 116087);
INSERT INTO TagProjects VALUES(161929, 1555242);

-- Executing at 11/6/2020 4:53:04 PM -05:00
-- Completed in 54 ms with result: 2

Committed transaction at 11/6/2020 4:53:04 PM -05:00
Closed connection at 11/6/2020 4:53:04 PM -05:00
