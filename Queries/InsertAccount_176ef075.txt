Opened connection at 11/6/2020 3:54:40 PM -05:00
Started transaction at 11/6/2020 3:54:40 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, @4, NULL, NULL, @5, @6, @7, @8, @9, @10, @11, NULL, NULL, NULL, @12, NULL, NULL, @13, NULL, NULL, NULL, @14, NULL, NULL, NULL, NULL, @15, @16, NULL, @17, @18, @19, NULL, NULL, NULL, @20)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'hgreenslade1ai@microsoft.com' (Type = String, Size = 100)
-- @1: '321-717-0329' (Type = String, Size = 50)
-- @2: '817-753-8562' (Type = String, Size = 50)
-- @3: '585-314-1025' (Type = String, Size = 50)
-- @4: '713-945-7769' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @8: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @9: '116087' (Type = Int32)
-- @10: '116087' (Type = Int32)
-- @11: '524647' (Type = Int64)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: '0' (Type = Int32)
-- @15: 'fb2d2a4b-53c4-4797-bf14-92ecab2fd60b' (Type = Guid)
-- @16: 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.' (Type = String, Size = -1)
-- @17: 'False' (Type = Boolean)
-- @18: 'Harv' (Type = String, Size = 50)
-- @19: 'Greenslade' (Type = String, Size = 50)
-- @20: 'Harv  Greenslade' (Type = String, Size = 255)
-- Executing at 11/6/2020 3:54:40 PM -05:00
-- Completed in 534 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @4: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3272103' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '176ef075-2e89-4fae-bdf2-a6f20665c7bc' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '1675' (Type = Int32)
-- @21: '1675 - Harv  Greenslade' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:54:41 PM -05:00
-- Completed in 168 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '33990 Lindbergh Road' (Type = String, Size = 100)
-- @2: '951' (Type = String, Size = 100)
-- @3: 'Fort Worth' (Type = String, Size = 50)
-- @4: 'TX' (Type = String, Size = 50)
-- @5: 'United States' (Type = String, Size = 50)
-- @6: '76147' (Type = String, Size = 50)
-- @7: 'True' (Type = Boolean)
-- @8: 'False' (Type = Boolean)
-- @9: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @10: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @11: '116087' (Type = Int32)
-- @12: '116087' (Type = Int32)
-- @13: '3614072' (Type = Int64)
-- @14: '524647' (Type = Int64)
-- Executing at 11/6/2020 3:54:41 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614072' (Type = Int64)
-- Executing at 11/6/2020 3:54:41 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'e89bb6cd-0487-4f07-8916-e570e3be84be' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Implemented composite moderator' (Type = String, Size = -1)
-- @3: '3272103' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:54:41 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'a68d164a-d3ad-4c05-bc69-aeec25217746' (Type = Guid)
-- @1: 'Green' (Type = String, Size = 50)
-- @2: '524647' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @6: '11/6/2020 3:54:40 PM' (Type = DateTime2)
-- @7: '116087' (Type = Int32)
-- @8: '116087' (Type = Int32)
-- @9: '0' (Type = Int32)
-- Executing at 11/6/2020 3:54:41 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[TagAccounts]([Account_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '3614072' (Type = Int64)
-- @1: '161944' (Type = Int64)
-- Executing at 11/6/2020 3:54:41 PM -05:00
-- Completed in 54 ms with result: 1

Committed transaction at 11/6/2020 3:54:41 PM -05:00
Closed connection at 11/6/2020 3:54:41 PM -05:00
Opened connection at 11/6/2020 3:54:41 PM -05:00
Started transaction at 11/6/2020 3:54:41 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5037435,
	ShippingAddressId = 5037436
WHERE Id = 3614072;

UPDATE Contacts SET
	AccountId = 3614072
WHERE Id = 3272103;
INSERT INTO AccountUsers VALUES(116088, 3614072);

-- Executing at 11/6/2020 3:54:41 PM -05:00
-- Completed in 71 ms with result: 3

Committed transaction at 11/6/2020 3:54:41 PM -05:00
Closed connection at 11/6/2020 3:54:41 PM -05:00
