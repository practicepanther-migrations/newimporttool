Opened connection at 11/6/2020 4:47:17 PM -05:00
Started transaction at 11/6/2020 4:47:17 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, NULL, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, NULL, @25, @26, NULL, @27, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Daltfresh' (Type = String, Size = 800)
-- @1: '11/6/2020 4:47:17 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:47:17 PM' (Type = DateTime2)
-- @3: '3616506' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '12/1/2015 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '6750933a-de9b-47b1-ac2f-a95e0c3d4c8b' (Type = Guid)
-- @15: 'False' (Type = Boolean)
-- @16: '3649' (Type = Int32)
-- @17: '3649 - Daltfresh' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '6/21/2012 12:00:00 AM' (Type = DateTime2)
-- @25: '116087' (Type = Int32)
-- @26: 'False' (Type = Boolean)
-- @27: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:47:17 PM -05:00
-- Completed in 118 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '3b44d8de-4fbb-4571-a963-a4a2076f8e70' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554967' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 4:47:18 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'f632f2f1-3da5-418b-92d3-56bf44a3362e' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554967' (Type = Int64)
-- @3: 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:47:18 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:47:18 PM -05:00
Closed connection at 11/6/2020 4:47:18 PM -05:00
