Opened connection at 11/6/2020 4:26:53 PM -05:00
Started transaction at 11/6/2020 4:26:53 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Alpha' (Type = String, Size = 800)
-- @1: '11/6/2020 4:26:53 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:26:53 PM' (Type = DateTime2)
-- @3: '3615663' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '7/22/2017 12:00:00 AM' (Type = DateTime2)
-- @8: '3' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: 'da7a2584-e20d-4ab0-ab4b-a1c8ebc6c9bd' (Type = Guid)
-- @15: 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '3214' (Type = Int32)
-- @18: '3214 - Alpha' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '8/28/2016 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:26:53 PM -05:00
-- Completed in 227 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'fcc659f8-eea4-4bb8-a268-630859f29aef' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554124' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:26:53 PM -05:00
-- Completed in 222 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'a2e41183-d8b2-424e-b441-b6a4fa613e00' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554124' (Type = Int64)
-- @3: 'In congue. Etiam justo. Etiam pretium iaculis justo.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:26:53 PM -05:00
-- Completed in 212 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:26:54 PM -05:00
Closed connection at 11/6/2020 4:26:54 PM -05:00
Opened connection at 11/6/2020 4:26:54 PM -05:00
Started transaction at 11/6/2020 4:26:54 PM -05:00
INSERT INTO TagProjects VALUES(161929, 1554124);

-- Executing at 11/6/2020 4:26:54 PM -05:00
-- Completed in 489 ms with result: 1

Committed transaction at 11/6/2020 4:26:55 PM -05:00
Closed connection at 11/6/2020 4:26:55 PM -05:00
