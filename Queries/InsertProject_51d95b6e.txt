Opened connection at 11/6/2020 3:56:04 PM -05:00
Started transaction at 11/6/2020 3:56:04 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Tin' (Type = String, Size = 800)
-- @1: '11/6/2020 3:56:04 PM' (Type = DateTime2)
-- @2: '11/6/2020 3:56:04 PM' (Type = DateTime2)
-- @3: '3614138' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '8/23/2020 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '51d95b6e-7f22-4fad-95a2-0c640d8d5679' (Type = Guid)
-- @15: 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '4236' (Type = Int32)
-- @18: '4236 - Tin' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '7/30/2020 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:56:04 PM -05:00
-- Completed in 84 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'b62968f7-27b1-4cb2-a65b-dab398bbb299' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1552599' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:56:05 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '51aa6824-0af6-4b4c-beb5-8cfe442d3882' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1552599' (Type = Int64)
-- @3: 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:56:05 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '14d0b88f-3fe6-44dc-bbba-c470b6df0cdc' (Type = Guid)
-- @1: 'Teal' (Type = String, Size = 50)
-- @2: '524647' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 3:56:04 PM' (Type = DateTime2)
-- @6: '11/6/2020 3:56:04 PM' (Type = DateTime2)
-- @7: '116087' (Type = Int32)
-- @8: '116087' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 11/6/2020 3:56:05 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1552599' (Type = Int64)
-- @1: '161956' (Type = Int64)
-- Executing at 11/6/2020 3:56:05 PM -05:00
-- Completed in 54 ms with result: 1

Committed transaction at 11/6/2020 3:56:05 PM -05:00
Closed connection at 11/6/2020 3:56:05 PM -05:00
Opened connection at 11/6/2020 3:56:05 PM -05:00
Started transaction at 11/6/2020 3:56:05 PM -05:00
INSERT INTO ProjectUsers VALUES(1552599, 116087);

-- Executing at 11/6/2020 3:56:05 PM -05:00
-- Completed in 54 ms with result: 1

Committed transaction at 11/6/2020 3:56:05 PM -05:00
Closed connection at 11/6/2020 3:56:05 PM -05:00
