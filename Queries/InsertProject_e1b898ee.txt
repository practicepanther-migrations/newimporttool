Opened connection at 10/23/2020 2:27:31 PM -04:00
Started transaction at 10/23/2020 2:27:31 PM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, @24, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '430.801 Pruegert, Joshua' (Type = String, Size = 800)
-- @1: '10/23/2020 2:25:53 PM' (Type = DateTime2)
-- @2: '10/23/2020 2:25:53 PM' (Type = DateTime2)
-- @3: '3613940' (Type = Int64)
-- @4: '116076' (Type = Int32)
-- @5: '116076' (Type = Int32)
-- @6: '524646' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'e1b898ee-2f34-409f-b438-3e250b7b0c2c' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10054' (Type = Int32)
-- @16: '10054 - 430.801 Pruegert, Joshua' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '1/30/2019 6:00:00 PM' (Type = DateTime2)
-- @24: '116076' (Type = Int32)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 10/23/2020 2:27:31 PM -04:00
-- Completed in 63 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'c865ceaf-41cf-4668-ace9-bbc0279bf689' (Type = Guid)
-- @1: '169058' (Type = Int64)
-- @2: '1552401' (Type = Int64)
-- @3: '430.801' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 10/23/2020 2:27:31 PM -04:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'd1c3fa40-bff8-4ab2-97de-2e2e8f975193' (Type = Guid)
-- @1: 'Child Custody-Support' (Type = String, Size = 50)
-- @2: '524646' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '10/23/2020 2:25:53 PM' (Type = DateTime2)
-- @6: '10/23/2020 2:25:53 PM' (Type = DateTime2)
-- @7: '116076' (Type = Int32)
-- @8: '116076' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 10/23/2020 2:27:31 PM -04:00
-- Completed in 69 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1552401' (Type = Int64)
-- @1: '161861' (Type = Int64)
-- Executing at 10/23/2020 2:27:31 PM -04:00
-- Completed in 53 ms with result: 1

Committed transaction at 10/23/2020 2:27:31 PM -04:00
Closed connection at 10/23/2020 2:27:31 PM -04:00
Opened connection at 10/23/2020 2:27:31 PM -04:00
Started transaction at 10/23/2020 2:27:31 PM -04:00
INSERT INTO ProjectUsers VALUES(1552401, 116076);

-- Executing at 10/23/2020 2:27:31 PM -04:00
-- Completed in 54 ms with result: 1

Committed transaction at 10/23/2020 2:27:31 PM -04:00
Closed connection at 10/23/2020 2:27:31 PM -04:00
