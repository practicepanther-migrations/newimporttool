Opened connection at 6/15/2021 10:16:34 AM -04:00
Started transaction at 6/15/2021 10:16:34 AM -04:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, NULL, NULL, NULL, @23, NULL, @24, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Legal Matters' (Type = String, Size = 800)
-- @1: '6/15/2021 10:16:34 AM' (Type = DateTime2)
-- @2: '6/15/2021 10:16:34 AM' (Type = DateTime2)
-- @3: '3614699' (Type = Int64)
-- @4: '116582' (Type = Int32)
-- @5: '116582' (Type = Int32)
-- @6: '525095' (Type = Int64)
-- @7: '4' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: 'a429fb6d-c60c-4572-8aae-6f64cfd73884' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '305' (Type = Int32)
-- @16: '305 - Legal Matters' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- Executing at 6/15/2021 10:16:34 AM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'b3ecbe98-fdb3-46c8-b581-b4a030fa1e2e' (Type = Guid)
-- @1: '169413' (Type = Int64)
-- @2: '1552970' (Type = Int64)
-- @3: '5864' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 6/15/2021 10:16:34 AM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '429cd611-a464-4279-b559-1fa5d8a70a2f' (Type = Guid)
-- @1: 'Delivery' (Type = String, Size = 50)
-- @2: '525095' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '6/15/2021 10:16:34 AM' (Type = DateTime2)
-- @6: '6/15/2021 10:16:34 AM' (Type = DateTime2)
-- @7: '116582' (Type = Int32)
-- @8: '116582' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 6/15/2021 10:16:34 AM -04:00
-- Completed in 65 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1552970' (Type = Int64)
-- @1: '161903' (Type = Int64)
-- Executing at 6/15/2021 10:16:34 AM -04:00
-- Completed in 56 ms with result: 1

Committed transaction at 6/15/2021 10:16:34 AM -04:00
Closed connection at 6/15/2021 10:16:34 AM -04:00
Opened connection at 6/15/2021 10:16:34 AM -04:00
Started transaction at 6/15/2021 10:16:34 AM -04:00
INSERT INTO ProjectUsers VALUES(1552970, 116582);

-- Executing at 6/15/2021 10:16:34 AM -04:00
-- Completed in 54 ms with result: 1

Committed transaction at 6/15/2021 10:16:34 AM -04:00
Closed connection at 6/15/2021 10:16:34 AM -04:00
