Opened connection at 11/6/2020 4:42:11 PM -05:00
Started transaction at 11/6/2020 4:42:11 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Aerified' (Type = String, Size = 800)
-- @1: '11/6/2020 4:42:11 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:42:11 PM' (Type = DateTime2)
-- @3: '3616265' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '5/23/2016 12:00:00 AM' (Type = DateTime2)
-- @8: '4' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: 'ca2d733b-bc39-40bf-933b-8e6fef1f0ec5' (Type = Guid)
-- @15: 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '3799' (Type = Int32)
-- @18: '3799 - Aerified' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '8/3/2017 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:42:11 PM -05:00
-- Completed in 93 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '4a786085-06c2-4bc0-8343-8c4f28937bb8' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554726' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:42:11 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9f30b8fd-ddcb-48c4-97f1-a3d3d9438bc1' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554726' (Type = Int64)
-- @3: 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:42:11 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:42:11 PM -05:00
Closed connection at 11/6/2020 4:42:11 PM -05:00
Opened connection at 11/6/2020 4:42:11 PM -05:00
Started transaction at 11/6/2020 4:42:11 PM -05:00
INSERT INTO ProjectUsers VALUES(1554726, 116087);

-- Executing at 11/6/2020 4:42:11 PM -05:00
-- Completed in 53 ms with result: 1

Committed transaction at 11/6/2020 4:42:11 PM -05:00
Closed connection at 11/6/2020 4:42:11 PM -05:00
