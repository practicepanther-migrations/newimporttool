Opened connection at 6/15/2021 10:13:34 AM -04:00
Started transaction at 6/15/2021 10:13:34 AM -04:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @0, @1, @2, @3, @4, @5, @6, NULL, NULL, NULL, @7, NULL, NULL, @8, NULL, NULL, NULL, @9, NULL, NULL, NULL, NULL, @10, NULL, NULL, @11, @12, @13, NULL, NULL, NULL, @14)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'True' (Type = Boolean)
-- @1: 'False' (Type = Boolean)
-- @2: '6/15/2021 10:13:34 AM' (Type = DateTime2)
-- @3: '6/15/2021 10:13:34 AM' (Type = DateTime2)
-- @4: '116582' (Type = Int32)
-- @5: '116582' (Type = Int32)
-- @6: '525095' (Type = Int64)
-- @7: '0' (Type = Int32)
-- @8: '0' (Type = Int32)
-- @9: '0' (Type = Int32)
-- @10: '19c3d31e-5fd9-4635-8690-bfad3ac79bbc' (Type = Guid)
-- @11: 'False' (Type = Boolean)
-- @12: 'Scott' (Type = String, Size = 50)
-- @13: 'Brengi' (Type = String, Size = 50)
-- @14: 'Scott  Brengi' (Type = String, Size = 255)
-- Executing at 6/15/2021 10:13:34 AM -04:00
-- Completed in 62 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '6/15/2021 10:13:34 AM' (Type = DateTime2)
-- @4: '6/15/2021 10:13:34 AM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116582' (Type = Int32)
-- @7: '116582' (Type = Int32)
-- @8: '525095' (Type = Int64)
-- @9: '3273839' (Type = Int64)
-- @10: 'be3d6921-409a-4b8b-8086-b8bf8f7de1eb' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '7277' (Type = Int32)
-- @20: '7277 - Scott  Brengi' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 6/15/2021 10:13:34 AM -04:00
-- Completed in 75 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, NULL, @4, @5, @6, @7, @8, @9, @10, @11, NULL, @12, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '19707 East 126th Street ' (Type = String, Size = 100)
-- @2: 'North Collinsville' (Type = String, Size = 50)
-- @3: 'OK' (Type = String, Size = 50)
-- @4: '74021' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '6/15/2021 10:13:34 AM' (Type = DateTime2)
-- @8: '6/15/2021 10:13:34 AM' (Type = DateTime2)
-- @9: '116582' (Type = Int32)
-- @10: '116582' (Type = Int32)
-- @11: '3614520' (Type = Int64)
-- @12: '525095' (Type = Int64)
-- Executing at 6/15/2021 10:13:34 AM -04:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3614520' (Type = Int64)
-- Executing at 6/15/2021 10:13:34 AM -04:00
-- Completed in 56 ms with result: SqlDataReader

Committed transaction at 6/15/2021 10:13:34 AM -04:00
Closed connection at 6/15/2021 10:13:34 AM -04:00
Opened connection at 6/15/2021 10:13:34 AM -04:00
Started transaction at 6/15/2021 10:13:35 AM -04:00
UPDATE Accounts SET
	BillingAddressId = 5039130,
	ShippingAddressId = 5039131
WHERE Id = 3614520;

UPDATE Contacts SET
	AccountId = 3614520
WHERE Id = 3273839;
INSERT INTO TagAccounts VALUES(161898, 3614520);
INSERT INTO AccountUsers VALUES(116582, 3614520);

-- Executing at 6/15/2021 10:13:35 AM -04:00
-- Completed in 58 ms with result: 4

Committed transaction at 6/15/2021 10:13:35 AM -04:00
Closed connection at 6/15/2021 10:13:35 AM -04:00
