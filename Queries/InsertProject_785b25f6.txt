Opened connection at 11/6/2020 4:41:33 PM -05:00
Started transaction at 11/6/2020 4:41:33 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Sub-Ex' (Type = String, Size = 800)
-- @1: '11/6/2020 4:41:33 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:41:33 PM' (Type = DateTime2)
-- @3: '3616235' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '6/15/2016 12:00:00 AM' (Type = DateTime2)
-- @8: '3' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '785b25f6-c1f6-4d06-8240-bbe5610f4905' (Type = Guid)
-- @15: 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '842' (Type = Int32)
-- @18: '842 - Sub-Ex' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '12/14/2011 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:41:33 PM -05:00
-- Completed in 143 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'ea38bd49-dde1-4221-ba80-9a5bb8ec5c42' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554696' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:41:33 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '2535bed5-df07-4213-8081-6709d8bcb144' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554696' (Type = Int64)
-- @3: 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:41:33 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:41:33 PM -05:00
Closed connection at 11/6/2020 4:41:33 PM -05:00
Opened connection at 11/6/2020 4:41:33 PM -05:00
Started transaction at 11/6/2020 4:41:33 PM -05:00
INSERT INTO ProjectUsers VALUES(1554696, 116087);
INSERT INTO TagProjects VALUES(161929, 1554696);

-- Executing at 11/6/2020 4:41:33 PM -05:00
-- Completed in 59 ms with result: 2

Committed transaction at 11/6/2020 4:41:34 PM -05:00
Closed connection at 11/6/2020 4:41:34 PM -05:00
