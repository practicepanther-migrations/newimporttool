Opened connection at 11/6/2020 3:01:38 PM -05:00
Started transaction at 11/6/2020 3:01:38 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, NULL, NULL, NULL, NULL, NULL, NULL, @2, @3, @4, @5, @6, @7, @8, NULL, NULL, NULL, @9, NULL, NULL, @10, NULL, NULL, NULL, @11, NULL, NULL, NULL, NULL, @12, NULL, NULL, @13, @14, @15, NULL, NULL, NULL, @16)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'raychel.schatzer@yahoo.com' (Type = String, Size = 100)
-- @1: '(405) 255-7379' (Type = String, Size = 50)
-- @2: 'True' (Type = Boolean)
-- @3: 'False' (Type = Boolean)
-- @4: '11/6/2020 3:01:38 PM' (Type = DateTime2)
-- @5: '11/6/2020 3:01:38 PM' (Type = DateTime2)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '0' (Type = Int32)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: 'e49cc7a3-7956-402d-a6b4-749c96653a09' (Type = Guid)
-- @13: 'False' (Type = Boolean)
-- @14: 'Raychel' (Type = String, Size = 50)
-- @15: 'Warren' (Type = String, Size = 50)
-- @16: 'Raychel  Warren' (Type = String, Size = 255)
-- Executing at 11/6/2020 3:01:38 PM -05:00
-- Completed in 97 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, NULL, NULL, NULL, @10, NULL, @11, @12, @13, @14, @15, @16, @17, @18, NULL, @19, NULL, NULL, NULL, NULL, @20, @21, @22, NULL, @23, NULL, NULL, NULL, NULL, NULL, @24, NULL, @25)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 3:01:38 PM' (Type = DateTime2)
-- @4: '11/6/2020 3:01:38 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116076' (Type = Int32)
-- @7: '116076' (Type = Int32)
-- @8: '524646' (Type = Int64)
-- @9: '3272034' (Type = Int64)
-- @10: '7bf7437e-b404-46ef-ad26-b9fd041d58d3' (Type = Guid)
-- @11: 'en-US' (Type = String, Size = 5)
-- @12: 'USD' (Type = String, Size = 3)
-- @13: 'False' (Type = Boolean)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: '1122' (Type = Int32)
-- @20: '1122 - Raychel  Warren' (Type = String, Size = 255)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:01:38 PM -05:00
-- Completed in 106 ms with result: SqlDataReader

Committed transaction at 11/6/2020 3:01:39 PM -05:00
Closed connection at 11/6/2020 3:01:39 PM -05:00
Opened connection at 11/6/2020 3:01:39 PM -05:00
Started transaction at 11/6/2020 3:01:39 PM -05:00
UPDATE Contacts SET
	AccountId = 3614003
WHERE Id = 3272034;
INSERT INTO TagAccounts VALUES(161853, 3614003);
INSERT INTO AccountUsers VALUES(116076, 3614003);

-- Executing at 11/6/2020 3:01:39 PM -05:00
-- Completed in 68 ms with result: 3

Committed transaction at 11/6/2020 3:01:39 PM -05:00
Closed connection at 11/6/2020 3:01:39 PM -05:00
