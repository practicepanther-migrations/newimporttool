Opened connection at 11/6/2020 4:57:55 PM -05:00
Started transaction at 11/6/2020 4:57:55 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Y-Solowarm' (Type = String, Size = 800)
-- @1: '11/6/2020 4:57:55 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:57:55 PM' (Type = DateTime2)
-- @3: '3617018' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '11/25/2014 12:00:00 AM' (Type = DateTime2)
-- @8: '4' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '5eaa5a8b-a452-4824-b4cb-d816a17b5fe9' (Type = Guid)
-- @15: 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '2488' (Type = Int32)
-- @18: '2488 - Y-Solowarm' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '8/16/2014 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:57:55 PM -05:00
-- Completed in 77 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '7dfd17a2-9f3f-46f7-941f-e3c98b8bbfa7' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1555479' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 4:57:55 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '99831ec5-53d3-421b-a59f-fd343dfb73f8' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1555479' (Type = Int64)
-- @3: 'Sed ante. Vivamus tortor. Duis mattis egestas metus.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:57:55 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:57:55 PM -05:00
Closed connection at 11/6/2020 4:57:55 PM -05:00
Opened connection at 11/6/2020 4:57:55 PM -05:00
Started transaction at 11/6/2020 4:57:56 PM -05:00
INSERT INTO ProjectUsers VALUES(1555479, 116087);

-- Executing at 11/6/2020 4:57:56 PM -05:00
-- Completed in 53 ms with result: 1

Committed transaction at 11/6/2020 4:57:56 PM -05:00
Closed connection at 11/6/2020 4:57:56 PM -05:00
