Opened connection at 11/6/2020 4:58:32 PM -05:00
Started transaction at 11/6/2020 4:58:32 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, @4, NULL, NULL, @5, @6, @7, @8, @9, @10, @11, NULL, NULL, NULL, @12, NULL, NULL, @13, NULL, NULL, NULL, @14, NULL, NULL, NULL, NULL, @15, @16, NULL, @17, @18, @19, NULL, NULL, NULL, @20)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'smoorerud3mb@over-blog.com' (Type = String, Size = 100)
-- @1: '505-706-6378' (Type = String, Size = 50)
-- @2: '253-640-5246' (Type = String, Size = 50)
-- @3: '810-596-0408' (Type = String, Size = 50)
-- @4: '404-721-6601' (Type = String, Size = 50)
-- @5: 'True' (Type = Boolean)
-- @6: 'False' (Type = Boolean)
-- @7: '11/6/2020 4:58:32 PM' (Type = DateTime2)
-- @8: '11/6/2020 4:58:32 PM' (Type = DateTime2)
-- @9: '116087' (Type = Int32)
-- @10: '116087' (Type = Int32)
-- @11: '524647' (Type = Int64)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: '0' (Type = Int32)
-- @15: 'b053c789-c546-4c1a-aeff-c13318175955' (Type = Guid)
-- @16: 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.' (Type = String, Size = -1)
-- @17: 'False' (Type = Boolean)
-- @18: 'Sid' (Type = String, Size = 50)
-- @19: 'Moorerud' (Type = String, Size = 50)
-- @20: 'Sid  Moorerud' (Type = String, Size = 255)
-- Executing at 11/6/2020 4:58:32 PM -05:00
-- Completed in 150 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 4:58:32 PM' (Type = DateTime2)
-- @4: '11/6/2020 4:58:32 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3275080' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: 'cdbd0ddf-fc22-4799-8ce0-43fee4762566' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '4692' (Type = Int32)
-- @21: '4692 - Sid  Moorerud' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:58:32 PM -05:00
-- Completed in 118 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '49227 Center Trail' (Type = String, Size = 100)
-- @2: 'Lakewood' (Type = String, Size = 50)
-- @3: 'WA' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '98498' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '11/6/2020 4:58:32 PM' (Type = DateTime2)
-- @9: '11/6/2020 4:58:32 PM' (Type = DateTime2)
-- @10: '116087' (Type = Int32)
-- @11: '116087' (Type = Int32)
-- @12: '3617049' (Type = Int64)
-- @13: '524647' (Type = Int64)
-- Executing at 11/6/2020 4:58:32 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3617049' (Type = Int64)
-- Executing at 11/6/2020 4:58:32 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '6c3c7b34-0fbb-4100-8afd-8658411492a5' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Public-key regional analyzer' (Type = String, Size = -1)
-- @3: '3275080' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:58:32 PM -05:00
-- Completed in 62 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:58:33 PM -05:00
Closed connection at 11/6/2020 4:58:33 PM -05:00
Opened connection at 11/6/2020 4:58:33 PM -05:00
Started transaction at 11/6/2020 4:58:33 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5043389,
	ShippingAddressId = 5043390
WHERE Id = 3617049;

UPDATE Contacts SET
	AccountId = 3617049
WHERE Id = 3275080;
INSERT INTO TagAccounts VALUES(161946, 3617049);
INSERT INTO AccountUsers VALUES(116087, 3617049);

-- Executing at 11/6/2020 4:58:33 PM -05:00
-- Completed in 65 ms with result: 4

Committed transaction at 11/6/2020 4:58:33 PM -05:00
Closed connection at 11/6/2020 4:58:33 PM -05:00
