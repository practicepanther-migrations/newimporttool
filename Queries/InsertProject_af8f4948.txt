Opened connection at 11/6/2020 3:56:17 PM -05:00
Started transaction at 11/6/2020 3:56:17 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Latlux' (Type = String, Size = 800)
-- @1: '11/6/2020 3:56:17 PM' (Type = DateTime2)
-- @2: '11/6/2020 3:56:17 PM' (Type = DateTime2)
-- @3: '3614149' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '8/17/2020 12:00:00 AM' (Type = DateTime2)
-- @8: '4' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: 'af8f4948-0aca-4049-b344-9bfc15fc8237' (Type = Guid)
-- @15: 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '2920' (Type = Int32)
-- @18: '2920 - Latlux' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '1' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '4/5/2012 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:56:17 PM -05:00
-- Completed in 57 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'd2f9dedf-5a00-4860-942c-5ffa9bb7d1ba' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1552610' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 3:56:17 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'c34e4282-ee27-432e-b931-f60bdec5d379' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1552610' (Type = Int64)
-- @3: 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:56:17 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'af1f2bbb-99fb-40d6-8b26-7487a1d56cc9' (Type = Guid)
-- @1: 'Blue' (Type = String, Size = 50)
-- @2: '524647' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 3:56:17 PM' (Type = DateTime2)
-- @6: '11/6/2020 3:56:17 PM' (Type = DateTime2)
-- @7: '116087' (Type = Int32)
-- @8: '116087' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 11/6/2020 3:56:17 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1552610' (Type = Int64)
-- @1: '161957' (Type = Int64)
-- Executing at 11/6/2020 3:56:17 PM -05:00
-- Completed in 53 ms with result: 1

Committed transaction at 11/6/2020 3:56:18 PM -05:00
Closed connection at 11/6/2020 3:56:18 PM -05:00
