Opened connection at 11/6/2020 4:48:51 PM -05:00
Started transaction at 11/6/2020 4:48:51 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, NULL, NULL, NULL, NULL, NULL, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, NULL, NULL, @12, NULL, NULL, NULL, NULL, @13, @14, NULL, @15, @16, @17, NULL, NULL, NULL, @18)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'gshubotham29n@omniture.com' (Type = String, Size = 100)
-- @1: '404-441-5101' (Type = String, Size = 50)
-- @2: '907-616-0137' (Type = String, Size = 50)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 4:48:51 PM' (Type = DateTime2)
-- @6: '11/6/2020 4:48:51 PM' (Type = DateTime2)
-- @7: '116087' (Type = Int32)
-- @8: '116087' (Type = Int32)
-- @9: '524647' (Type = Int64)
-- @10: '0' (Type = Int32)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: 'bee7e155-1db3-4ba7-803a-1e1a842e1b33' (Type = Guid)
-- @14: 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.' (Type = String, Size = -1)
-- @15: 'False' (Type = Boolean)
-- @16: 'Georgeanne' (Type = String, Size = 50)
-- @17: 'Shubotham' (Type = String, Size = 50)
-- @18: 'Georgeanne  Shubotham' (Type = String, Size = 255)
-- Executing at 11/6/2020 4:48:51 PM -05:00
-- Completed in 182 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 4:48:51 PM' (Type = DateTime2)
-- @4: '11/6/2020 4:48:51 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3274613' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '5444969e-3abe-43ad-893d-c3f810811547' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '2940' (Type = Int32)
-- @21: '2940 - Georgeanne  Shubotham' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:48:52 PM -05:00
-- Completed in 194 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '8856 Anthes Circle' (Type = String, Size = 100)
-- @2: 'Anchorage' (Type = String, Size = 50)
-- @3: 'AK' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '99517' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '11/6/2020 4:48:51 PM' (Type = DateTime2)
-- @9: '11/6/2020 4:48:51 PM' (Type = DateTime2)
-- @10: '116087' (Type = Int32)
-- @11: '116087' (Type = Int32)
-- @12: '3616582' (Type = Int64)
-- @13: '524647' (Type = Int64)
-- Executing at 11/6/2020 4:48:52 PM -05:00
-- Completed in 71 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3616582' (Type = Int64)
-- Executing at 11/6/2020 4:48:52 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '18b077aa-b11c-436f-a37d-a5eb2f516a10' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Multi-lateral upward-trending encoding' (Type = String, Size = -1)
-- @3: '3274613' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:48:52 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:48:52 PM -05:00
Closed connection at 11/6/2020 4:48:52 PM -05:00
Opened connection at 11/6/2020 4:48:52 PM -05:00
Started transaction at 11/6/2020 4:48:52 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5042455,
	ShippingAddressId = 5042456
WHERE Id = 3616582;

UPDATE Contacts SET
	AccountId = 3616582
WHERE Id = 3274613;
INSERT INTO TagAccounts VALUES(161942, 3616582);
INSERT INTO AccountUsers VALUES(116089, 3616582);

-- Executing at 11/6/2020 4:48:52 PM -05:00
-- Completed in 68 ms with result: 4

Committed transaction at 11/6/2020 4:48:52 PM -05:00
Closed connection at 11/6/2020 4:48:52 PM -05:00
