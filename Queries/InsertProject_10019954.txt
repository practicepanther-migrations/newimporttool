Opened connection at 11/6/2020 3:00:56 PM -05:00
Started transaction at 11/6/2020 3:00:56 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, NULL, @7, @8, @9, @10, @11, @12, @13, NULL, @14, NULL, @15, @16, @17, @18, @19, NULL, @20, @21, @22, NULL, @23, NULL, @24, @25, NULL, @26, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '430.835 Davis, Kim' (Type = String, Size = 800)
-- @1: '11/6/2020 3:00:56 PM' (Type = DateTime2)
-- @2: '11/6/2020 3:00:56 PM' (Type = DateTime2)
-- @3: '3613970' (Type = Int64)
-- @4: '116076' (Type = Int32)
-- @5: '116076' (Type = Int32)
-- @6: '524646' (Type = Int64)
-- @7: '3' (Type = Int32)
-- @8: '1' (Type = Int32)
-- @9: 'False' (Type = Boolean)
-- @10: 'True' (Type = Boolean)
-- @11: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '10019954-42c9-44d4-a3ab-b19d05482837' (Type = Guid)
-- @14: 'False' (Type = Boolean)
-- @15: '10088' (Type = Int32)
-- @16: '10088 - 430.835 Davis, Kim' (Type = String, Size = 850)
-- @17: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @18: 'False' (Type = Boolean)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: 'False' (Type = Boolean)
-- @22: 'False' (Type = Boolean)
-- @23: '12/10/2019 6:00:00 PM' (Type = DateTime2)
-- @24: '116076' (Type = Int32)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 3:00:56 PM -05:00
-- Completed in 180 ms with result: SqlDataReader

INSERT [dbo].[Tags]([Guid], [Name], [TenantId], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedById], [LastModifiedById], [TagFor])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)
SELECT [Id]
FROM [dbo].[Tags]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '1e3e5b78-47a8-46dd-b5c9-2932279f70ee' (Type = Guid)
-- @1: 'Protective Orders' (Type = String, Size = 50)
-- @2: '524646' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- @4: 'False' (Type = Boolean)
-- @5: '11/6/2020 3:00:56 PM' (Type = DateTime2)
-- @6: '11/6/2020 3:00:56 PM' (Type = DateTime2)
-- @7: '116076' (Type = Int32)
-- @8: '116076' (Type = Int32)
-- @9: '1' (Type = Int32)
-- Executing at 11/6/2020 3:00:56 PM -05:00
-- Completed in 154 ms with result: SqlDataReader

INSERT [dbo].[TagProjects]([Project_Id], [Tag_Id])
VALUES (@0, @1)
-- @0: '1552432' (Type = Int64)
-- @1: '161911' (Type = Int64)
-- Executing at 11/6/2020 3:00:56 PM -05:00
-- Completed in 54 ms with result: 1

Committed transaction at 11/6/2020 3:00:56 PM -05:00
Closed connection at 11/6/2020 3:00:56 PM -05:00
