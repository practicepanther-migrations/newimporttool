Opened connection at 11/6/2020 4:09:18 PM -05:00
Started transaction at 11/6/2020 4:09:18 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, NULL, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, NULL, @25, @26, NULL, @27, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Bitwolf' (Type = String, Size = 800)
-- @1: '11/6/2020 4:09:18 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:09:18 PM' (Type = DateTime2)
-- @3: '3614842' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '3/31/2019 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '9136ad23-442c-47b9-b245-dbfb0079b72f' (Type = Guid)
-- @15: 'False' (Type = Boolean)
-- @16: '2232' (Type = Int32)
-- @17: '2232 - Bitwolf' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '8/17/2019 12:00:00 AM' (Type = DateTime2)
-- @25: '116087' (Type = Int32)
-- @26: 'False' (Type = Boolean)
-- @27: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:09:18 PM -05:00
-- Completed in 77 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'e08138cc-084e-4ffb-8e0d-997e3a378977' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1553303' (Type = Int64)
-- @3: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:09:18 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9ddf904f-8afa-41a2-94e5-ac3caeba21b8' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1553303' (Type = Int64)
-- @3: 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:09:18 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:09:18 PM -05:00
Closed connection at 11/6/2020 4:09:18 PM -05:00
