Opened connection at 11/6/2020 5:28:19 PM -05:00
Started transaction at 11/6/2020 5:28:19 PM -05:00
INSERT [dbo].[Contacts]([Email], [Mobile], [Home], [Office], [Ext], [Fax], [Salutation], [Position], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DateOfBirth], [MailingAddressId], [OtherAddressId], [Gender], [Anniversary], [AccountId], [Type], [ConversionDate], [SkypeId], [Twitter], [LeadStatus], [LeadCompanyName], [OwnerId], [CampaignId], [Photo_Id], [Guid], [Notes], [JobTitle], [IsEmailOptOut], [FirstName], [LastName], [Prefix], [MiddleName], [Portal_Id], [DisplayName])
VALUES (@0, @1, @2, @3, NULL, NULL, NULL, NULL, @4, @5, @6, @7, @8, @9, @10, NULL, NULL, NULL, @11, NULL, NULL, @12, NULL, NULL, NULL, @13, NULL, NULL, NULL, NULL, @14, @15, NULL, @16, @17, @18, NULL, NULL, NULL, @19)
SELECT [Id], [FullName]
FROM [dbo].[Contacts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'mhamlynen@cnn.com' (Type = String, Size = 100)
-- @1: '516-175-4414' (Type = String, Size = 50)
-- @2: '954-637-3399' (Type = String, Size = 50)
-- @3: '209-510-7982' (Type = String, Size = 50)
-- @4: 'True' (Type = Boolean)
-- @5: 'False' (Type = Boolean)
-- @6: '11/6/2020 5:28:19 PM' (Type = DateTime2)
-- @7: '11/6/2020 5:28:19 PM' (Type = DateTime2)
-- @8: '116087' (Type = Int32)
-- @9: '116087' (Type = Int32)
-- @10: '524647' (Type = Int64)
-- @11: '0' (Type = Int32)
-- @12: '0' (Type = Int32)
-- @13: '0' (Type = Int32)
-- @14: 'ff89d4ec-7bf3-42c3-aa1a-217fc0d18f5d' (Type = Guid)
-- @15: 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: 'Mickie' (Type = String, Size = 50)
-- @18: 'Hamlyn' (Type = String, Size = 50)
-- @19: 'Mickie  Hamlyn' (Type = String, Size = 255)
-- Executing at 11/6/2020 5:28:19 PM -05:00
-- Completed in 172 ms with result: SqlDataReader

INSERT [dbo].[Accounts]([Name], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [IsCustomPriceList], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [PrimaryContactId], [PriceListId], [BillingAddressId], [ShippingAddressId], [Website], [CampaignId], [Logo_Id], [Guid], [Notes], [CultureName], [CurrencyCode], [IsAllowClientToViewAllOutstandingInvoices], [IsAllowClientToViewAllPayments], [IsAllowClientToViewAccountBalances], [IsSendInvoicePaymentReminders], [IsSendQuoteApprovalReminders], [IsAllowClientToViewAllPaidInvoices], [BoxFolderId], [Number], [QuickbooksId], [LastQuickbooksSyncDate], [XeroId], [LastXeroSyncDate], [NameAndNumber], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [IsContactSync], [DefaultPaymentSourceGuid], [LedesClientId], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [UtbmsIsEnabled], [HeadNoteClientId], [IsDisableEcheck])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, NULL, NULL, NULL, @10, NULL, NULL, @11, NULL, @12, @13, @14, @15, @16, @17, @18, @19, NULL, @20, NULL, NULL, NULL, NULL, @21, @22, @23, NULL, @24, NULL, NULL, NULL, NULL, NULL, @25, NULL, @26)
SELECT [Id]
FROM [dbo].[Accounts]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '' (Type = String, Size = 100)
-- @1: 'True' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '11/6/2020 5:28:19 PM' (Type = DateTime2)
-- @4: '11/6/2020 5:28:19 PM' (Type = DateTime2)
-- @5: 'False' (Type = Boolean)
-- @6: '116087' (Type = Int32)
-- @7: '116087' (Type = Int32)
-- @8: '524647' (Type = Int64)
-- @9: '3276597' (Type = Int64)
-- @10: 'www.google.com' (Type = String, Size = 200)
-- @11: '185faca4-d8d9-42af-ae5b-52d4ea598612' (Type = Guid)
-- @12: 'en-US' (Type = String, Size = 5)
-- @13: 'USD' (Type = String, Size = 3)
-- @14: 'False' (Type = Boolean)
-- @15: 'False' (Type = Boolean)
-- @16: 'False' (Type = Boolean)
-- @17: 'False' (Type = Boolean)
-- @18: 'False' (Type = Boolean)
-- @19: 'False' (Type = Boolean)
-- @20: '528' (Type = Int32)
-- @21: '528 - Mickie  Hamlyn' (Type = String, Size = 255)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: 'False' (Type = Boolean)
-- @26: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:28:19 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, @1, NULL, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, NULL, @13, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Billing' (Type = String, Size = 50)
-- @1: '4071 Vernon Street' (Type = String, Size = 100)
-- @2: 'Boca Raton' (Type = String, Size = 50)
-- @3: 'FL' (Type = String, Size = 50)
-- @4: 'United States' (Type = String, Size = 50)
-- @5: '33432' (Type = String, Size = 50)
-- @6: 'True' (Type = Boolean)
-- @7: 'False' (Type = Boolean)
-- @8: '11/6/2020 5:28:19 PM' (Type = DateTime2)
-- @9: '11/6/2020 5:28:19 PM' (Type = DateTime2)
-- @10: '116087' (Type = Int32)
-- @11: '116087' (Type = Int32)
-- @12: '3618566' (Type = Int64)
-- @13: '524647' (Type = Int64)
-- Executing at 11/6/2020 5:28:20 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[Addresses]([Name], [Address1], [Address2], [City], [State], [Country], [PostalCode], [IsEnabled], [IsDeleted], [CreatedDate], [LastModifiedDate], [CreatedBy_Id], [LastModifiedBy_Id], [AccountId], [Contact_Id], [TenantId], [ContactId])
VALUES (@0, NULL, NULL, NULL, NULL, NULL, NULL, @1, @2, NULL, NULL, NULL, NULL, @3, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Addresses]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Shipping' (Type = String, Size = 50)
-- @1: 'False' (Type = Boolean)
-- @2: 'False' (Type = Boolean)
-- @3: '3618566' (Type = Int64)
-- Executing at 11/6/2020 5:28:20 PM -05:00
-- Completed in 56 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, NULL, NULL, @2, NULL, @3, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '5a32baef-324a-4888-b5d8-56b5ecc26f1b' (Type = Guid)
-- @1: '169059' (Type = Int64)
-- @2: 'Persevering stable hierarchy' (Type = String, Size = -1)
-- @3: '3276597' (Type = Int64)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 5:28:20 PM -05:00
-- Completed in 55 ms with result: SqlDataReader

Committed transaction at 11/6/2020 5:28:20 PM -05:00
Closed connection at 11/6/2020 5:28:20 PM -05:00
Opened connection at 11/6/2020 5:28:20 PM -05:00
Started transaction at 11/6/2020 5:28:20 PM -05:00
UPDATE Accounts SET
	BillingAddressId = 5046423,
	ShippingAddressId = 5046424
WHERE Id = 3618566;

UPDATE Contacts SET
	AccountId = 3618566
WHERE Id = 3276597;
INSERT INTO TagAccounts VALUES(161941, 3618566);
INSERT INTO AccountUsers VALUES(116088, 3618566);

-- Executing at 11/6/2020 5:28:20 PM -05:00
-- Completed in 69 ms with result: 4

Committed transaction at 11/6/2020 5:28:20 PM -05:00
Closed connection at 11/6/2020 5:28:20 PM -05:00
