Opened connection at 11/6/2020 4:29:05 PM -05:00
Started transaction at 11/6/2020 4:29:06 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, NULL, @17, @18, @19, @20, @21, NULL, @22, @23, @24, NULL, @25, NULL, @26, @27, NULL, @28, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Job' (Type = String, Size = 800)
-- @1: '11/6/2020 4:29:05 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:29:05 PM' (Type = DateTime2)
-- @3: '3615703' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '6/26/2017 12:00:00 AM' (Type = DateTime2)
-- @8: '4' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '0faf706e-7aaf-4948-b022-44f83adac5eb' (Type = Guid)
-- @15: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' (Type = String, Size = -1)
-- @16: 'False' (Type = Boolean)
-- @17: '2228' (Type = Int32)
-- @18: '2228 - Job' (Type = String, Size = 850)
-- @19: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @20: 'False' (Type = Boolean)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @23: 'False' (Type = Boolean)
-- @24: 'False' (Type = Boolean)
-- @25: '8/25/2016 12:00:00 AM' (Type = DateTime2)
-- @26: '116087' (Type = Int32)
-- @27: 'False' (Type = Boolean)
-- @28: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:29:06 PM -05:00
-- Completed in 177 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '9d650353-686c-4258-9b08-65dc0e44bf12' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1554164' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 4:29:06 PM -05:00
-- Completed in 308 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'fdbda45b-3ca4-42c6-870c-0afc00d030e5' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1554164' (Type = Int64)
-- @3: 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:29:06 PM -05:00
-- Completed in 157 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:29:06 PM -05:00
Closed connection at 11/6/2020 4:29:06 PM -05:00
Opened connection at 11/6/2020 4:29:06 PM -05:00
Started transaction at 11/6/2020 4:29:07 PM -05:00
INSERT INTO TagProjects VALUES(161929, 1554164);

-- Executing at 11/6/2020 4:29:07 PM -05:00
-- Completed in 149 ms with result: 1

Committed transaction at 11/6/2020 4:29:07 PM -05:00
Closed connection at 11/6/2020 4:29:07 PM -05:00
