Opened connection at 11/6/2020 4:10:47 PM -05:00
Started transaction at 11/6/2020 4:10:47 PM -05:00
INSERT [dbo].[Projects]([Name], [CreatedDate], [LastModifiedDate], [AccountId], [CreatedBy_Id], [LastModifiedBy_Id], [TenantId], [DueDate], [Status], [HourlyRateType], [IsDeleted], [IsEnabled], [HourlyRate], [FlatRate], [Guid], [Notes], [IsFlatRateBilled], [BoxFolderId], [Number], [NameAndNumber], [BillableFlatRate], [EvergreenIsOn], [EvergreenAmount], [LedesClientMatterId], [ContingencyRatePercent], [IsEmailSync], [IsFilesSync], [DropboxFolderId], [OpenDate], [CloseDate], [OriginatedBy_Id], [IsContactSync], [DefaultPaymentSourceGuid], [IsAddEvergreenToInvoice], [BoxSharedFolderUrl], [GoogleDriveFolderId], [OneDriveFolderId], [PresetProjectRate_Id])
VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, NULL, @15, NULL, @16, @17, @18, @19, @20, NULL, @21, @22, @23, NULL, @24, NULL, @25, @26, NULL, @27, NULL, NULL, NULL, NULL)
SELECT [Id]
FROM [dbo].[Projects]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: 'Greenlam' (Type = String, Size = 800)
-- @1: '11/6/2020 4:10:47 PM' (Type = DateTime2)
-- @2: '11/6/2020 4:10:47 PM' (Type = DateTime2)
-- @3: '3614918' (Type = Int64)
-- @4: '116087' (Type = Int32)
-- @5: '116087' (Type = Int32)
-- @6: '524647' (Type = Int64)
-- @7: '2/5/2019 12:00:00 AM' (Type = DateTime2)
-- @8: '2' (Type = Int32)
-- @9: '1' (Type = Int32)
-- @10: 'False' (Type = Boolean)
-- @11: 'True' (Type = Boolean)
-- @12: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @13: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @14: '3dcd67b2-368e-42bb-a1f2-f924154d5bab' (Type = Guid)
-- @15: 'False' (Type = Boolean)
-- @16: '1266' (Type = Int32)
-- @17: '1266 - Greenlam' (Type = String, Size = 850)
-- @18: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @19: 'False' (Type = Boolean)
-- @20: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @21: '0' (Type = Decimal, Precision = 18, Scale = 2)
-- @22: 'False' (Type = Boolean)
-- @23: 'False' (Type = Boolean)
-- @24: '7/18/2018 12:00:00 AM' (Type = DateTime2)
-- @25: '116087' (Type = Int32)
-- @26: 'False' (Type = Boolean)
-- @27: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:10:47 PM -05:00
-- Completed in 58 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, NULL, NULL, NULL, @3, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '76cc35d4-c410-4225-bb1f-2d3fd8170725' (Type = Guid)
-- @1: '169060' (Type = Int64)
-- @2: '1553379' (Type = Int64)
-- @3: 'True' (Type = Boolean)
-- Executing at 11/6/2020 4:10:47 PM -05:00
-- Completed in 54 ms with result: SqlDataReader

INSERT [dbo].[CustomFieldValues]([Guid], [CustomFieldId], [AccountId], [ProjectId], [ValueDateTime], [ValueString], [ValueNumber], [ContactId], [ValueBoolean], [ValueContactId])
VALUES (@0, @1, NULL, @2, NULL, @3, NULL, NULL, @4, NULL)
SELECT [Id]
FROM [dbo].[CustomFieldValues]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
-- @0: '66973261-98cc-4a68-93ef-038392ede15b' (Type = Guid)
-- @1: '169061' (Type = Int64)
-- @2: '1553379' (Type = Int64)
-- @3: 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.' (Type = String, Size = -1)
-- @4: 'False' (Type = Boolean)
-- Executing at 11/6/2020 4:10:47 PM -05:00
-- Completed in 83 ms with result: SqlDataReader

Committed transaction at 11/6/2020 4:10:47 PM -05:00
Closed connection at 11/6/2020 4:10:47 PM -05:00
Opened connection at 11/6/2020 4:10:48 PM -05:00
Started transaction at 11/6/2020 4:10:48 PM -05:00
INSERT INTO ProjectUsers VALUES(1553379, 116087);
INSERT INTO TagProjects VALUES(161952, 1553379);

-- Executing at 11/6/2020 4:10:48 PM -05:00
-- Completed in 54 ms with result: 2

Committed transaction at 11/6/2020 4:10:48 PM -05:00
Closed connection at 11/6/2020 4:10:48 PM -05:00
