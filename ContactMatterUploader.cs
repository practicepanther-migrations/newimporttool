﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using CsvHelper;
using System.Globalization;
using System.Collections;
using System.Data;
using CsvHelper.Expressions;
using System.Dynamic;
using CsvParser.Models;

namespace CsvParser
{

    //TODO We should look into creating a portalId since it seems that it will be needed

    public class ContactMatterUploader
    {

        //public ContactMatterUploader(string userEmail, string firmName, string fileName)
        public ContactMatterUploader(string userEmail, string firmName, string fileName)
        {
            //FirmDirectory = Path.Combine(System.IO.Directory.GetParent(System.IO.Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString(), "Test Imports", firmName);
            DbFunctions = new DatabaseFunctions(userEmail);
            FileName = fileName;
            FirmDirectory = firmName;
            //var readPath = Path.Combine(FirmDirectory, fileName);
            UploadContactMatterCsv(fileName);
        }

        // these properties are just for testing purposes, will not be included in deployment
        static string FirmDirectory { get; set; }
        static string FileName { get; set; }
        static DatabaseFunctions DbFunctions { get; set; }

        public static void UploadContactMatterCsv(string filepath)
        {
            var successRows = new List<dynamic>();
            var errorRows = new List<dynamic>();
            using (var reader = new StreamReader(filepath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                // goes through each row in the CSV
                while (csv.Read())
                {
                    // allows rows to not contain all fields of an object for mapping
                    csv.Configuration.MissingFieldFound = null;
                    csv.Configuration.HeaderValidated = null; ;

                    AccountFields accountFields = null;
                    ContactFields contactFields = null;
                    ProjectFields projectFields = null;
                    AddressFields addressFields = null;



                    string errorMessage = null;
                    // split row into objects that represent columns in tables
                    try
                    {
                        var _acountFields = csv.GetRecord<AccountFields>();
                        accountFields = (AccountFields)HelperFunctions.Clean(csv.GetRecord<AccountFields>());
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.InnerException.ToString().Split('\n')[0];
                        // accountFields stays null
                    }
                    try
                    {
                        contactFields = (ContactFields)HelperFunctions.Clean(csv.GetRecord<ContactFields>());
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.InnerException.ToString().Split('\n')[0];
                        // contactFields stays null
                    }

                    try
                    {
                        var _projectFields = csv.GetRecord<ProjectFields>();
                        projectFields = (ProjectFields)HelperFunctions.Clean(_projectFields);
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.InnerException.ToString().Split('\n')[0];
                        //projectFields stays null
                    }
                    try
                    {
                        addressFields = (AddressFields)HelperFunctions.Clean(csv.GetRecord<AddressFields>());
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.InnerException.ToString().Split('\n')[0];
                        // addressFields stays null
                    }


                    var row = csv.GetRecord<dynamic>();

                    // for success/error files
                    dynamic successFields = new ExpandoObject();
                    var successDictionary = (IDictionary<string, object>)successFields;
                    successDictionary.Add("AccountGuid", null);  // update this field if an account is created
                    successDictionary.Add("ProjectGuid", null);  // update this field if a project is created
                    foreach (var field in row)
                    {
                        successDictionary.Add(field.Key, field.Value);
                    }

                    dynamic errorFields = new ExpandoObject();
                    var errorDictionary = (IDictionary<string, object>)errorFields;
                    errorDictionary.Add("Error", null);
                    foreach (var field in row)
                    {
                        errorDictionary.Add(field.Key, field.Value);
                    }

                    if (!(errorMessage is null))
                    {
                        errorFields.Error = errorMessage;
                        errorRows.Add(errorFields);
                        continue;
                    }

                    
                    // Check if account already exists in database
                    //var _accountId = DbFunctions.CheckForAccountNumber(accountFields.Number.Value);
                    var accountNumberId = DbFunctions.AccountNumberIds.FirstOrDefault(a => a.Number == accountFields.Number.Value);
                    Account account = null;
                    Project project = null;
                    if (accountNumberId is null)
                    {
                        // TODO add error handling

                        try
                        {
                            // store all of the CustomFieldValues for this row here
                            var rowCustomFieldValues = HelperFunctions.GetCustomFieldValues(row, DbFunctions.CustomFields);
                            var accountTagsColumn = csv.GetField("Contact: Tags");
                            var accountUsersColumn = csv.GetField("Contact: AssignedTo");
                            var projectUsersColuumn = csv.GetField("Matter: AssignedTo");
                            var matterTagsColumn = csv.GetField("Matter: Tags");
                            // Insert rows into tables
                            account = DbFunctions.InsertAccount(accountFields, contactFields, addressFields, rowCustomFieldValues, accountTagsColumn, accountUsersColumn);
                            //account = DbFunctions.InsertAccount(accountFields, contactFields, addressFields, rowCustomFieldValues, accountTagsColumn, accountUsersColumn);
                            //accountId = DbFunctions.InsertAccount(accountFields, contactFields, addressFields, rowCustomFieldValues, accountTagsColumn, accountUsersColumn);
                            Console.WriteLine("Inserted account with Id " + account.Id);
                            successFields.AccountGuid = account.Guid;
                            if (projectFields is null)
                            {
                                successRows.Add(successFields);
                                continue;
                            }
                            project = DbFunctions.InsertProject(projectFields, account.Id, rowCustomFieldValues, matterTagsColumn, projectUsersColuumn);
                            //projectId = DbFunctions.InsertProject(projectFields, account.Id, rowCustomFieldValues, matterTagsColumn, projectUsersColuumn);
                            Console.WriteLine("Inserted project with Id " + project.Id);
                            successFields.ProjectGuid = project.Guid;
                            successRows.Add(successFields);
                            continue;
                        }


                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                        {
                            errorFields.Error = dbEx.EntityValidationErrors.ToList()[0].ValidationErrors.ToList()[0].ErrorMessage;
                            errorRows.Add(errorFields);

                            if (!(project is null))
                            {
                                DbFunctions.DeleteEntry(project);
                            }

                            DbFunctions.RefreshContext();
                            continue;
                        }


                        catch (Exception ex)
                        {
                            errorFields.Error = ex.Message;
                            errorRows.Add(errorFields);

                            if (!(account is null))
                            {
                                DbFunctions.DeleteEntry(account);
                            }

                            if (!(project is null))
                            {
                                DbFunctions.DeleteEntry(project);
                            }
                            DbFunctions.RefreshContext();
                            continue;
                        }
                    }
                    
                    else
                    {
                        if (projectFields is null)
                        {
                            continue;
                        }
                        try
                        {
                            var rowCustomFieldValues = HelperFunctions.GetCustomFieldValues(row, DbFunctions.CustomFields);
                            var matterTagsColumn = csv.GetField("Matter: Tags");
                            var projectUsersColumn = csv.GetField("Matter: AssignedTo");
                            // no need to create account, contact, address. Just create the project
                            project = DbFunctions.InsertProject(projectFields, accountNumberId.Id, rowCustomFieldValues, matterTagsColumn, projectUsersColumn);

                            successFields.ProjectGuid = project.Guid;
                            successRows.Add(successFields);
                            continue;
                        }

                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                        {
                            errorFields.Error = dbEx.EntityValidationErrors.ToList()[0].ValidationErrors.ToList()[0].ErrorMessage;
                            errorRows.Add(errorFields);

                            if (!(project is null))
                            {
                                DbFunctions.DeleteEntry(project);
                            }
                            continue;
                        }


                        catch (Exception ex)
                        {
                            errorFields.Error = ex.Message;
                            errorRows.Add(errorFields);

                            if (!(project is null))
                            {
                                DbFunctions.DeleteEntry(project);
                            }
                            continue;
                        }
                    }
                }
                Console.WriteLine("File processed. Press any key to output success and error files.");
                Console.ReadKey();

                var successPath = FileName.Replace(".csv", "") + " - Success File.csv";
                using (var successWriter = new StreamWriter(successPath))
                using (var successCsv = new CsvWriter(successWriter, CultureInfo.InvariantCulture))
                {
                    successCsv.WriteRecords(successRows);
                }

                var errorPath = FileName.Replace(".csv", "") + "- Error File.csv";
                using (var errorWriter = new StreamWriter(errorPath))
                using (var errorCsv = new CsvWriter(errorWriter, CultureInfo.InvariantCulture))
                {
                    errorCsv.WriteRecords(errorRows);
                }

                string fileDir = Path.GetDirectoryName(FirmDirectory);
                var sqlLogOutputPath = Path.Combine(fileDir, "Queries.txt");
                File.WriteAllLines(sqlLogOutputPath, DbFunctions.SqlLogFile);
                DbFunctions.CloseConnection();
            }
        }
    }
}
