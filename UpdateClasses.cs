﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvParser
{
    public class ActivityGuidUserTag
    {
        public ActivityGuidUserTag()
        {
            UserIds = new List<long>();
            TagIds = new List<long>();
        }
        public Guid ActivityGuid { get; set; }
        public long? ActivityId { get; set; }
        public List<long> UserIds { get; set; }
        public List<long> TagIds { get; set; }
    }

    // copied from the old import tool
    /// <summary>
    /// Can set to ContactsAndMatters, Notes, Tasks, Events, TimeEntries, Expenses, Relationships
    /// </summary>
    public enum ImportType : int
    {
        ContactsAndMatters = 0,
        Notes = 1,
        Tasks = 2,
        Events = 3,
        TimeEntries = 4,
        Expenses = 5,
        Relationships = 6,
        FlatFee = 7
    }
}