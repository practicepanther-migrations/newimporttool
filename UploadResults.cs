﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvParser
{
    public class UploadResults
    {
        public UploadResults()
        {
            SuccessRows = new List<dynamic>();
            ErrorRows = new List<dynamic>();
        }
        public List<dynamic> OriginalRows { get; set; }
        public List<dynamic> SuccessRows { get; set; }
        public List<dynamic> ErrorRows { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public TimeSpan? Duration { get; set; }
        public int NumberOfTransactions { get; set; }

        public List<string> GetUploadSummary(int rowsPerInsert)
        {
            var uploadSummary = new List<string>();
            uploadSummary.Add("Rows in original file:   \t" + OriginalRows.Count.ToString());
            uploadSummary.Add("Rows in success file:   \t" + SuccessRows.Count);
            uploadSummary.Add("Rows in error file:   \t\t" + ErrorRows.Count);
            uploadSummary.Add("\nRows per transaction: \t\t" + rowsPerInsert.ToString());
            uploadSummary.Add("# of transactions:   \t\t" + NumberOfTransactions);
            uploadSummary.Add("\nStart Time:\t\t\t" + StartTime);
            uploadSummary.Add("End Time:\t\t\t" + EndTime);
            uploadSummary.Add("\nDuration:\t\t\t" + Duration);

            return uploadSummary;

        }
    }
}
