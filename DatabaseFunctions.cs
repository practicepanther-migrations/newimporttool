﻿using AutoMapper;
using CsvParser.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CsvParser
{
    public class DatabaseFunctions
    {

        //TODO for the love of God, let's clean up everything that is commented out that we don't use, LMAO

        public DatabaseFunctions(string userEmail)
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            ContextStartTime = DateTime.Now;
            Context = new PPContext();
            Context.Database.Connection.Open(); // this should keep the database connection open even after save changes
                                                // https://docs.microsoft.com/en-us/ef/ef6/fundamentals/connection-management#databaseconnectionopen
            SqlLogFile = new List<string>();
            Context.Database.Log = SqlLogFile.Add;
            FirmTenant = GetTenant(userEmail);
            FirmTenant.TimeZoneId = "Eastern Standard Time";
            //FirmTenant.TimeZoneId = GetTenantTimeZoneId(FirmTenant.Id);
            TimeZoneInfo = GetTenantTimeZoneInfo(FirmTenant.Id);
            AccountNumberIds = GetAccountNumberIds();
            ProjectNumberIds = GetProjectNumberIds();
            CustomFields = GetCustomFields();
            PpTags = GetTags();
            Users = GetUsers();
            MapperConfig = new MapperConfiguration(cfg =>
            {
                // Property names and types are the same, so no need to explicitly map
                cfg.CreateMap<AccountFields, Account>();
                cfg.CreateMap<ContactFields, Contact>();
                cfg.CreateMap<ProjectFields, Project>();
                cfg.CreateMap<AddressFields, Address>();
                cfg.CreateMap<CustomFieldValueField, CustomFieldValue>();
            }
                );
        }

        static PPContext Context { get; set; }
        public static List<Tag> PpTags { get; set; }
        public List<CustomField> CustomFields { get; set; }
        public static Tenant FirmTenant { get; set; }
        public List<AccountNumberId> AccountNumberIds { get; set; }
        public List<ProjectNumberId> ProjectNumberIds { get; set; }
        public static List<User> Users { get; set; }

        public static MapperConfiguration MapperConfig { get; set; }
        public static string QueryOutputPath { get; set; }
        static string ConnectionString { get; set; }
        public List<string> SqlLogFile { get; set; }
        public DateTime ContextStartTime { get; set; }
        public static TimeZoneInfo TimeZoneInfo { get; set; }
        
        /// <summary>
        /// This function returns the TenantId from the DB by passing the user email address.
        /// </summary>
        /// <param name="email">Billing email for the tenant</param>
        /// <returns>TenantId from the DB</returns>
        public static Tenant GetTenant(string email)
        {
            //string queryString = "SELECT Id from Tenants WHERE BillingEmailAddress = " + "'" + email + "'";
            //using (Context)
            //{
            var tenant = Context.Tenants.OrderByDescending(tid => tid.Id).First(t => t.BillingEmailAddress == email && t.IsEnabled == true && t.IsDeleted == false);
            tenant.User = Context.Users.FirstOrDefault(u => u.Id == tenant.PrimaryUserId);
            return tenant;

        }

        public void RefreshContext()
        {
            Context = new PPContext();
            Context.Database.Connection.Open(); // this should keep the database connection open even after save changes
                                                // https://docs.microsoft.com/en-us/ef/ef6/fundamentals/connection-management#databaseconnectionopen
        }

        public static TimeZoneInfo GetTenantTimeZoneInfo(long tenantId)
        {
            TimeZoneInfo timeZone = null;
            string query = "SELECT TimeZoneId from Tenants WHERE Id = " + "'" + tenantId + "'";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(query, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var timeZoneId = reader[0].ToString();
                        if (timeZoneId == "")
                        {
                            var newTimeZoneId = "Eastern Standard Time";
                            timeZone = TimeZoneInfo.FindSystemTimeZoneById(newTimeZoneId);
                        }
                        else
                        {
                            timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                        }
                        
                    }
                }
                connection.Close();
            }
            return timeZone;

            //optional
            //var tenantTimeZoneId = Context.Tenant.FirstOrDefault(t => t.Id == tenantId).TimeZoneId;
            //var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            //return timeZoneInfo;
        }

        /// <summary>
        /// Store the account numbers and account Ids for the tenant in local memory,
        /// to avoid constantly querying the database
        /// </summary>
        /// <returns>List of all account numbers and account Ids for the tenant</returns>
        public  List<AccountNumberId> GetAccountNumberIds()
        {
            List<AccountNumberId> _accountNumberIds = new List<AccountNumberId>();
            string queryString = "SELECT Number, Id, PrimaryContactId from Accounts WHERE IsDeleted = 0 and TenantId = " + Convert.ToString(FirmTenant.Id);
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        AccountNumberId accountNumberId = new AccountNumberId();
                        var accountNumber = reader[0].ToString();
                        if (String.IsNullOrWhiteSpace(accountNumber))
                        {
                            continue;
                        }

                        var accountId = reader[1].ToString();
                        var accountPrimryContactId = reader[2].ToString();

                        accountNumberId.Number = int.Parse(accountNumber);
                        accountNumberId.Id = long.Parse(accountId);
                        accountNumberId.PrimaryContactId = long.Parse(accountPrimryContactId);
                        _accountNumberIds.Add(accountNumberId);
                    }
                }
                connection.Close();
            }
            return _accountNumberIds;
        }

        /// <summary>
        /// Store the project numbers and project Ids for the tenant in local memory,
        /// to avoid constantly querying the database
        /// </summary>
        /// <returns>List of all project numbers and project Ids for the tenant</returns>
        public List<ProjectNumberId> GetProjectNumberIds()
        {
            List<ProjectNumberId> _projectNumberIds = new List<ProjectNumberId>();
            string queryString = "SELECT Number, Id, AccountId from Projects WHERE IsDeleted = 0 and TenantId = " + Convert.ToString(FirmTenant.Id);
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ProjectNumberId projectNumberId = new ProjectNumberId();
                        var projectNumber = reader[0].ToString();
                        if (String.IsNullOrWhiteSpace(projectNumber))
                        {
                            continue;
                        }

                        var projectId = reader[1].ToString();
                        var projectAccountId = reader[2].ToString();

                        projectNumberId.Number = int.Parse(projectNumber);
                        projectNumberId.Id = long.Parse(projectId);
                        projectNumberId.AccountId = long.Parse(projectAccountId);
                        _projectNumberIds.Add(projectNumberId);
                    }
                }
                connection.Close();
            }
            return _projectNumberIds;
        }
        public List<User> GetUsers()
        {
            //using (context)
            //{
            //   users = context.Users.Where(u => u.Tenants.ToList()[0].Id == tenant.Id).ToList();

            //}

            List<long> userIds = new List<long>();
            string queryString = "SELECT [User_Id], [Tenant_Id] FROM TenantUsers WHERE Tenant_Id = " + Convert.ToString(FirmTenant.Id);
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        userIds.Add(long.Parse(reader[0].ToString()));
                    }
                }
                connection.Close();
            }
            var PPUsers = new List<User>();
            foreach (var userId in userIds)
            {
                var user = Context.Users.FirstOrDefault(u => u.Id == userId);
                Context.Users.Attach(user);
                PPUsers.Add(user);
            }
            return PPUsers;
        }

        public IEnumerable<UtbmsCode> GetUtbmsCodes()
        {
            //UtbmsCode codeToAdd = new UtbmsCode();
            //List<UtbmsCode> codes = new List<UtbmsCode>();
            string queryString = " SELECT [Id], [Code], [Name] FROM[dbo].[UtbmsCodes]";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        yield return new UtbmsCode()
                        {
                            Id = long.Parse(reader["Id"].ToString()),
                            Code = reader["Code"].ToString(),
                            Name = reader["Name"].ToString()

                        };
                        //codeToAdd.Id = long.Parse(reader[0].ToString());
                        //codeToAdd.Code = reader[1].ToString();
                    }
                    //codes.Add(codeToAdd);
                }
                connection.Close();
            }
        }

        /// <summary>
        /// Gets all rows of the CustomField table with a matching tenant
        /// </summary>
        /// <returns>All custom fields for tenant</returns>
        public static List<CustomField> GetCustomFields()
        {
            var _customFields = Context.CustomFields.Where(cf => cf.TenantId == FirmTenant.Id && cf.IsDeleted == false);
            return _customFields.ToList();
        }

        public List<Tag> GetTags()
        {
            var ppTags = Context.Tags.Where(t => t.IsDeleted == false && t.IsEnabled == true && t.TenantId == FirmTenant.Id);
            foreach(var tag in ppTags)
            {
                Context.Tags.Attach(tag);
            }
            return ppTags.ToList();
        }

        /// <summary>
        /// Finds the ContactId using a TenantId and account number
        /// </summary>
        /// <param name="accountNumber">Value of "Number" column in Accounts table</param>
        /// <returns>int</returns>
        public static long GetContactId(string accountNumber)
        {
            // TODO write method to lookup ContactId by account number
            return 0;
        }

        public Account InsertAccount(AccountFields account, ContactFields contact, AddressFields addressFields, List<CustomFieldValueField> customFieldValueFields, string tagsColumn, string assignedUsersColumn)
        {
            var mapper = new Mapper(MapperConfig);
            var ppAccount = mapper.Map<AccountFields, Account>(account);
            ppAccount.TenantId = FirmTenant.Id;
            
            ppAccount.CreatedDate = DateTime.Now;
            ppAccount.LastModifiedDate = DateTime.Now;
            ppAccount.Guid = Guid.NewGuid();
            ppAccount.IsDeleted = false;
            ppAccount.IsEnabled = true;
            ppAccount.CultureName = "en-US";
            ppAccount.CurrencyCode = "USD";
            ppAccount.CreatedBy_Id = FirmTenant.PrimaryUserId;
            ppAccount.LastModifiedBy_Id = FirmTenant.PrimaryUserId;
            if (ppAccount.Name is null)
            {
                ppAccount.Name = "";
            }

            if (!(contact is null))
                {
                var ppContact = mapper.Map<ContactFields, Contact>(contact);
                ppContact.LastModifiedBy_Id = FirmTenant.PrimaryUserId;
                ppContact.CreatedBy_Id = FirmTenant.PrimaryUserId;
                ppContact.FullName = (ppContact.FirstName + " " + ppContact.LastName).Trim().Replace("  "," ");
                ppContact.DisplayName = HelperFunctions.GetDisplayName(ppContact, FirmTenant.AccountDisplayNameFormat);
                if (String.IsNullOrWhiteSpace(ppContact.FullName))
                {
                    //ppContact.FirstName = null;
                    //ppContact.LastName = null;
                    ppContact.FullName = " ";
                    ppContact.DisplayName = null;
                }
                
                ppContact.Guid = Guid.NewGuid();
                ppContact.TenantId = FirmTenant.Id;
                ppContact.CreatedDate = DateTime.Now;
                ppContact.LastModifiedDate = DateTime.Now;
                ppContact.IsEnabled = true;
                ppAccount.Contact = ppContact;
            }

            if (contact is null)
            {
                ppAccount.Contact = new Contact()
                {
                    DisplayName = null,
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedBy_Id = FirmTenant.PrimaryUserId,
                    Guid = Guid.NewGuid(),
                    TenantId = FirmTenant.Id,
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    IsEnabled = true,
                    //FirstName = "",
                    //LastName = ""
                    FullName = " "
                };

            }


            ppAccount.NameAndNumber = HelperFunctions.GetDisplayName(ppAccount, FirmTenant.AccountDisplayNameFormat);

            if (addressFields != null)
            {
                var ppAddress = mapper.Map<AddressFields, Address>(addressFields);
                ppAddress.TenantId = FirmTenant.Id;
                ppAddress.Name = "Billing";
                ppAddress.IsEnabled = true;
                ppAddress.CreatedDate = DateTime.Now;
                ppAddress.LastModifiedDate = DateTime.Now;
                Address shippingAddress = new Address();
                shippingAddress.Name = "Shipping";
                ppAddress.LastModifiedBy_Id = FirmTenant.PrimaryUserId;
                ppAddress.CreatedBy_Id = FirmTenant.PrimaryUserId;
                ppAccount.Addresses.Add(ppAddress);
                ppAccount.Addresses.Add(shippingAddress);
            }


            var tagIds = new List<long>();
            if (!String.IsNullOrWhiteSpace(tagsColumn))
            {
                var tagsList = tagsColumn.Split(',');
                /*
                Copied from Tag model in PP
                public enum TagFor : int
                {
                    Account = 0,
                    Project = 1,
                    Activity = 2
                }
            */
                foreach (var tagName in tagsList)
                {
                    var ppTag = PpTags.FirstOrDefault(t => t.Name.ToLower() == tagName.ToLower() && t.TagFor == 0);
                    if (ppTag is null)
                    {
                        var newTag = new Tag();
                        newTag.Name = tagName;
                        newTag.Guid = Guid.NewGuid();
                        newTag.TagFor = 0;
                        newTag.TenantId = FirmTenant.Id;
                        newTag.CreatedDate = DateTime.Now;
                        newTag.CreatedById = FirmTenant.PrimaryUserId;
                        newTag.LastModifiedById = FirmTenant.PrimaryUserId;
                        newTag.LastModifiedDate = DateTime.Now;
                        newTag.IsEnabled = true;
                        newTag.IsDeleted = false;
                        // add tag to account. This should create the tag in the Tags table as well.
                        ppAccount.Tags.Add(newTag);
                        // update in memory tags
                        PpTags.Add(newTag);
                    }
                    else
                    {
                        // store the Id to update the AccountTags table once the context is open
                        tagIds.Add(ppTag.Id);
                    }
                }
            }

            if (customFieldValueFields.Count > 0)
            {
                // go through each CustomFieldValueField and add it to respective account or contact
                foreach (var cfv in customFieldValueFields)
                {
                    // map from local CustomFieldValue to db model for CustomFieldValue
                    var customFieldValue = mapper.Map<CustomFieldValueField, CustomFieldValue>(cfv);
                    customFieldValue.Guid = Guid.NewGuid();

                    switch (customFieldValue.CustomField.ObjectType)
                    {
                        case (1):
                            customFieldValue.CustomFieldId = customFieldValue.CustomField.Id;
                            customFieldValue.CustomField = null;
                            ppAccount.CustomFieldValues.Add(customFieldValue);
                            continue;
                        case (2):
                            continue;
                        case (3):
                            customFieldValue.CustomFieldId = customFieldValue.CustomField.Id;
                            customFieldValue.CustomField = null;
                            customFieldValue.Contact = null;
                            ppAccount.Contact.CustomFieldValues.Add(customFieldValue);
                            continue;
                    }
                }
            }

            var userIds = new List<long>();
            if (!String.IsNullOrWhiteSpace(assignedUsersColumn))
            {
                // split the emails by comma and look up Ids
                foreach(var userEmail in assignedUsersColumn.Split())
                {
                    var user = Users.FirstOrDefault(u => u.Email == userEmail);
                    if(user != null)
                    {
                        userIds.Add(user.Id);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            Context.Accounts.Add(ppAccount);
            try
            {
                Context.SaveChanges(); // make sure to actually update the Database, and not just the in memory context
            }
            catch (Exception dbEx)
            {
                
                //DeleteEntry(ppAccount);
                throw dbEx;
            }
                    
            // update AccountNumberIds to include new account, if a number was provided
            if (ppAccount.Number.HasValue)
            {
                AccountNumberId accountNumberId = new AccountNumberId()
                {
                    Id = ppAccount.Id,
                    Number = ppAccount.Number.Value
                };
                AccountNumberIds.Add(accountNumberId);
            }
                    
            // build query to update everything with foreign keys
            string updateAddressQuery = "";
            string updateContactQuery = "";
            string insertTagsQuery = "";
            string assignUsersQuery = "";
            if (ppAccount.Addresses.Count > 0)
            {
                long billingAddressId = ppAccount.Addresses.ToList()[0].Id;
                long shippingAddressId = ppAccount.Addresses.ToList()[1].Id;
                updateAddressQuery = "UPDATE Accounts SET\n\tBillingAddressId = " + billingAddressId + ",\n\tShippingAddressId = " + shippingAddressId + "\nWHERE Id = " + ppAccount.Id + ";\n\n";
            }
            if (ppAccount.Contact != null)
            {
                updateContactQuery = "UPDATE Contacts SET\n\tAccountId = " + ppAccount.Id + "\nWHERE Id = " + ppAccount.Contact.Id + ";\n";
            }

            foreach (var tagId in tagIds)
            {
                insertTagsQuery += "INSERT INTO TagAccounts VALUES(" + tagId + ", " + ppAccount.Id + ");\n";                        
            }

            foreach(var userId in userIds)
            {
                assignUsersQuery += "INSERT INTO AccountUsers VALUES(" + userId + ", " + ppAccount.Id + ");\n";
            }

            var updateQuery = updateAddressQuery + updateContactQuery + insertTagsQuery + assignUsersQuery;
            if (!String.IsNullOrWhiteSpace(updateQuery))
            {
                Context.Database.ExecuteSqlCommand(updateQuery);
            }
        return ppAccount;
        }

        /// <summary>
        /// Inserts project fields into Projects table in database
        /// </summary>
        /// <param name="project">ProjectFields object mapped from CSV record</param>
        /// <param name="accountId"> Id of linked row in Accounts table</param>
        /// <returns>Id of newly created row in Projects table</returns>
        public Project InsertProject(ProjectFields project, long accountId, List<CustomFieldValueField> customFieldValueFields, string tagsColumn, string assignedUsersColumn)
        {
            var mapper = new Mapper(MapperConfig);
            //var ppProject = mapper.Map<ProjectFields, Project>(project);
            var ppProject = new Project
            {
                Name = project.Name,
                Notes = project.Notes,
                Guid = Guid.NewGuid(),
                TenantId = FirmTenant.Id,
                AccountId = accountId,
                CreatedBy_Id = FirmTenant.PrimaryUserId,
                LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now,
                OpenDate = project.OpenDate,
                CloseDate = project.CloseDate,
                NameAndNumber = project.Number + " - " + project.Name,
                DueDate = project.DueDate
            };

            // check if a matter with this number already exists for the tenant, throw an exception if so
            var numberId = ProjectNumberIds.FirstOrDefault(p => p.Number == project.Number.Value);
            if (numberId != null)
            {
                var exceptionString = "Matter Number " + project.Number.ToString() + " already exists.";
                throw new ArgumentException(exceptionString);
            }
            else
            {
                ppProject.Number = project.Number.Value;
            }

            
            switch (project.Status)
            {
                /*
                    From PracticePanther Project model:
                    public enum ProjectStatus
                    {                    
                        Closed = 2,
                        Pending = 3,
                        Open = 4
                    }
                Convert status string to int and add to model. Default to open if
                no status is provided.
                    */
                case "Open":
                    ppProject.Status = 4;
                    break;
                case "Pending":
                    ppProject.Status = 3;
                    break;
                case "Closed":
                    ppProject.Status = 2;
                    break;
                // if invalid status is provided, default to open
                default:
                    ppProject.Status = 4;
                    break;
            }


            /*
            from the PracticePanther Project model:
            public enum ProjectHourlyRate
                {
                    [Display(Name = "Item Price")]
                    ItemRate = 0,
                    [Display(Name = "User Hourly Rate")]
                    UserRate = 1,
                    [Display(Name = "Matter Hourly Rate")]
                    ProjectRate = 2,
                    [Display(Name = "Flat Rate")]
                    FlatRate = 3,
                    [Display(Name = "Contingency")]
                    Contingency = 4,
                    [Display(Name = "Custom Hourly Rate")]
                    PresetProjectRate = 5
                }
                */
            if (project.HourlyRateType != null)
            {
                ppProject.HourlyRateType = project.HourlyRateType.Value;
            }
            else
            {
                ppProject.HourlyRateType = 1;
            }

            ppProject.IsDeleted = false;
            ppProject.IsEnabled = true;
            // turns out you do have to check if it is null, my bad
            if (project.HourlyRate != null)
            {
                ppProject.HourlyRate = project.HourlyRate.Value;
            }
            if (project.FlatRate != null)
            {
                ppProject.FlatRate = project.FlatRate.Value;
            }

            


            if (customFieldValueFields.Count > 0)
            {
                // filters custom field values to only include project custom fields
                foreach (var cfv in customFieldValueFields.Where(c => c.CustomField.ObjectType == 2))
                {
                    // map from local CustomFieldValue to db model for CustomFieldValue
                    var customFieldValue = mapper.Map<CustomFieldValueField, CustomFieldValue>(cfv);
                    customFieldValue.Guid = Guid.NewGuid();
                    customFieldValue.CustomFieldId = customFieldValue.CustomField.Id;
                    customFieldValue.CustomField = null;
                    ppProject.CustomFieldValues.Add(customFieldValue);
                }
            }


            var tagIds = new List<long>();
            if (!string.IsNullOrWhiteSpace(tagsColumn))
            {
                var tagsList = tagsColumn.Split(',');

                foreach (var tagName in tagsList)
                {
                    var ppTag = PpTags.FirstOrDefault(t => t.Name.ToLower() == tagName.ToLower() && t.TagFor == 1);
                    if (ppTag is null)
                    {
                        var newTag = new Tag();
                        newTag.Name = tagName;
                        newTag.Guid = Guid.NewGuid();
                        newTag.TagFor = 1;
                        newTag.TenantId = FirmTenant.Id;
                        newTag.CreatedDate = DateTime.Now;
                        newTag.CreatedById = FirmTenant.PrimaryUserId;
                        newTag.LastModifiedById = FirmTenant.PrimaryUserId;
                        newTag.LastModifiedDate = DateTime.Now;
                        newTag.IsEnabled = true;
                        newTag.IsDeleted = false;
                        // add tag to account. This should create the tag in the Tags table as well.
                        ppProject.Tags.Add(newTag);
                        // update in memory tags
                        PpTags.Add(newTag);
                    }
                    else
                    {
                        // store the Id to update the AccountTags table once the context is open
                        tagIds.Add(ppTag.Id);
                    }
                }
            }

            var userIds = new List<long>();
            if (!String.IsNullOrWhiteSpace(assignedUsersColumn))
            {
                // split the emails by comma and look up Ids
                foreach (var userEmail in assignedUsersColumn.Split())
                {
                    var user = Users.FirstOrDefault(u => u.Email == userEmail);
                    if (user != null)
                    {
                        userIds.Add(user.Id);
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            if (!String.IsNullOrWhiteSpace(project.OriginatedByEmail))
            {
                var originatedByUser = Users.FirstOrDefault(u => u.Email == project.OriginatedByEmail);
                if (originatedByUser != null)
                {
                    ppProject.OriginatedBy_Id = originatedByUser.Id;
                }
            }

            var guidString = ppProject.Guid.ToString();
            Context.Projects.Add(ppProject);
            Context.SaveChanges(); // make sure to actually update the Database, and not just the in memory context
            // check if a matter number was provided, update projectNumberIds if so
            if (ppProject.Number.HasValue)
            {
                ProjectNumberIds.Add(new ProjectNumberId { Number = ppProject.Number.Value, Id = ppProject.Id });
            }
            string insertTagsQuery = "";
            var assignUsersQuery = "";
            foreach(var userId in userIds)
            {
                assignUsersQuery += "INSERT INTO ProjectUsers VALUES(" + ppProject.Id + ", " + userId + ");\n";
            }
                    
            foreach (var tagId in tagIds)
            {
                insertTagsQuery += "INSERT INTO TagProjects VALUES(" + tagId + ", " + ppProject.Id + ");\n";
            }

            var updateQuery = assignUsersQuery + insertTagsQuery;
            if (!String.IsNullOrWhiteSpace(updateQuery))
            {
                Context.Database.ExecuteSqlCommand(updateQuery);
            }
        return ppProject;
        }

        /// <summary>
        /// Deletes row of entry and associated entries in other tables
        /// </summary>
        /// <param name="entryTable">The table in which to delete the row for given Id</param>
        /// <param name="id">Id of entry to remove</param>
        public void DeleteEntry (string entryTable, long id)
        {
            //string deleteQuery;
            switch (entryTable)
            {
                case "Accounts":
                    var accountToDelete = Context.Accounts.FirstOrDefault(a => a.Id == id);
                    var accountNumberIdToDelete = AccountNumberIds.FirstOrDefault(a => a.Id == id);
                    AccountNumberIds.Remove(accountNumberIdToDelete);
                    Context.Accounts.Remove(accountToDelete);
                    Context.SaveChanges();
                    break;

                case "Projects":
                    var projectToDelete = Context.Projects.FirstOrDefault(p => p.Id == id);
                    var projectNumberIdToDelete = ProjectNumberIds.FirstOrDefault(p => p.Id == id);
                    ProjectNumberIds.Remove(projectNumberIdToDelete);
                    Context.Projects.Remove(projectToDelete);
                    Context.SaveChanges();
                    break;
            }      
        }

        public void DeleteEntry(Account account)
        {
            Context.Accounts.Remove(account);
            //Context.SaveChanges();
            var accountNumberIdToDelete = AccountNumberIds.FirstOrDefault(a => a.Id == account.Id);
            AccountNumberIds.Remove(accountNumberIdToDelete);
        }

        public void DeleteEntry(Project project)
        {
            Context.Projects.Remove(project);
            Context.SaveChanges();
            var projectNumberIdToDelete = ProjectNumberIds.FirstOrDefault(a => a.Id == project.Id);
            ProjectNumberIds.Remove(projectNumberIdToDelete);
        }

        public void CloseConnection()
        {
            Context.Database.Connection.Close();
        }

        //TODO consider changing the way TenantTimeZoneInfo is implemented. I added a property that is set in the constructor by invoking a method that gets the TZID from DB

        /// <summary>
        /// Validates the provided events and then calls InsertEventsAsync
        /// to asynchronously insert the events to the database
        /// </summary>
        /// <param name="csvRows">Rows that were read from CSV file</param>
        /// <param name="rowsPerInsert">Number of rows to include in each SqlBulkInsert</param>
        /// <returns>UploadResults with statistics and
        /// a list of the rows which were successfully uploaded with associated Guid,
        /// as well as a list of rows which failed validation with the validation error</returns>
        public async Task<UploadResults> ImportEventCsv(List<dynamic> csvRows, int rowsPerInsert)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(FirmTenant.TimeZoneId);
            UploadResults uploadResults = new UploadResults
            {
                OriginalRows = csvRows
            };
            var counter = 0;
            List<Task> taskInserts = new List<Task>();  // add task from InsertEventsAsync() to this list to await it before calling InsertEventsAsync again
            List<Activity> activitiesToAdd = new List<Activity>();  // add validated activities here, insert with SqlBulkInsert
            List<ActivityGuidUserTag> activityGuidUserTags = new List<ActivityGuidUserTag>();   // store each Event with its Guid and list of UserIds and TagIds. Use this to insert rows into
                                                                                                // ActivityUsers and TagActivities

            foreach (var row in csvRows)
            {
                // for success/error files
                dynamic successFields = new ExpandoObject();
                var successDictionary = (IDictionary<string, object>)successFields;
                successDictionary.Add("EventGuid", null);  // update this field if an Event is created
                foreach (var field in row)
                {
                    successDictionary.Add(field.Key, field.Value);
                }

                dynamic errorFields = new ExpandoObject();
                var errorDictionary = (IDictionary<string, object>)errorFields;
                errorDictionary.Add("Error", null);
                foreach (var field in row)
                {
                    errorDictionary.Add(field.Key, field.Value);
                }

                EventFields eventFields = null;
                var eventToInsert = new Activity()
                {
                    CreatedById = FirmTenant.PrimaryUserId,
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    LastModifiedDate = DateTime.Now.ToUniversalTime(),
                    IsDeleted = false,
                    IsEnabled = true,
                    //  copied from PP Activity model
                    //  public enum Activity_Type
                    //  {
                    //      Call = 0,
                    //      Task = 1,
                    //      Event = 2,
                    //      Email = 3,
                    //      Note = 4
                    //  }
                    EmailProperties_From = "null",
                    EmailProperties_To = "[]",
                    EmailProperties_Cc = "[]",
                    EventColor = "#368ee0",
                    Priority = 2,
                    IsPrivate = false,
                    Type = 2,
                    Guid = Guid.NewGuid(),
                    TenantId = FirmTenant.Id,
                };

                var activityGuidUserTag = new ActivityGuidUserTag() { ActivityGuid = eventToInsert.Guid };

                try
                {
                    eventFields = new EventFields(row);
                    if (eventFields is null)
                    {
                        continue;
                    }
                    if (String.IsNullOrWhiteSpace(eventFields.IsActivityAllDay))
                    {
                        errorFields.Error = "Must specify if event is all day";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    if (String.IsNullOrWhiteSpace(eventFields.Name))
                    {
                        errorFields.Error = "Must provide subject";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    if (String.IsNullOrWhiteSpace(eventFields.AssignedUserEmails) | !eventFields.AssignedUserEmails.Contains("@"))
                    {
                        errorFields.Error = "Must provide at least one user email address";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                }

                catch (Exception ex)
                {
                    errorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                if (!(eventFields.ContactNumber is null))
                {
                    var accountNumberId = AccountNumberIds.FirstOrDefault(a => a.Number == eventFields.ContactNumber);

                    if (accountNumberId is null)
                    {
                        errorFields.Error = "No contact found with number " + eventFields.ContactNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        eventToInsert.AccountId = accountNumberId.Id;
                    }
                }

                if (!(eventFields.MatterNumber is null))
                {
                    var projectNumberId = ProjectNumberIds.FirstOrDefault(a => a.Number == eventFields.MatterNumber);

                    if (projectNumberId is null)
                    {
                        errorFields.Error = "No matter found with number " + eventFields.MatterNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        eventToInsert.ProjectId = projectNumberId.Id;
                        eventToInsert.AccountId = projectNumberId.AccountId;
                    }
                }

                //If start date time is greater than end date time, output and error.
                if(eventFields.StartDateTime > eventFields.EndDateTime)
                {
                    errorFields.Error = "End date time must be greater than Start Date Time";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }
                else
                {
                    eventToInsert.StartDateTime = TimeZoneInfo.ConvertTimeToUtc(eventFields.StartDateTime, timeZoneInfo);
                    eventToInsert.Date = eventToInsert.StartDateTime;
                    eventToInsert.EndDateTime = TimeZoneInfo.ConvertTimeToUtc(eventFields.EndDateTime, timeZoneInfo);
                }

                // if EndDateTime is greater than the current DateTime, mark as completed.
                // if event has started but not completed, mark as In Progress
                // otherwise mark as NotCompleted
                if (eventToInsert.EndDateTime > DateTime.UtcNow)
                {
                    eventToInsert.Status = 2;
                }
                else
                {
                    if (eventToInsert.StartDateTime > DateTime.UtcNow)
                    {
                        eventToInsert.Status = 1;
                    }
                    else
                    {
                        eventToInsert.Status = 0;
                    }
                }


                // copied from Activty model in PracticePanther
                //public enum Activity_Status
                //{
                //    NotCompleted = 0,
                //    InProgress = 1,
                //    Completed = 2,//    Conditional = 4 // Since status =3 is being used by one record, i'll just use the number 4 to avoid any conflict. I don't want to manually delete the record.
                //}


                if (String.IsNullOrWhiteSpace(eventFields.IsActivityAllDay))
                {
                    errorFields.Error = "Must specify if event is all day";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                if (!String.IsNullOrWhiteSpace(eventFields.IsActivityAllDay))
                {
                    switch (eventFields.IsActivityAllDay.ToLower())
                    {
                        case "yes":
                            eventToInsert.IsAllDayActivity = true;
                            break;
                        case "no":
                            eventToInsert.IsAllDayActivity = false;
                            break;
                        case "true":
                            eventToInsert.IsAllDayActivity = true;
                            break;
                        case "false":
                            eventToInsert.IsAllDayActivity = false;
                            break;
                        default:
                            errorFields.Error = "Must specify Yes or No for IsAllDay";
                            uploadResults.ErrorRows.Add(errorFields);
                            continue;
                    }
                }

                //Why not add this validation inside of the same if statement?
                if (String.IsNullOrWhiteSpace(eventFields.Name))
                {
                    errorFields.Error = "Must provide subject";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                if (!String.IsNullOrWhiteSpace(eventFields.Name))
                {
                    if(eventFields.Name.Length>1000)
                    {
                        errorFields.Error = "The subject must not be greater than 1000 characters";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        eventToInsert.Name = eventFields.Name;
                    }
                }

                if (!String.IsNullOrWhiteSpace(eventFields.Description))
                {
                    eventToInsert.Description = eventFields.Description;
                }

                if (String.IsNullOrWhiteSpace(eventFields.AssignedUserEmails) | !eventFields.AssignedUserEmails.Contains("@"))
                {
                    errorFields.Error = "Must provide at least one user email address";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                List<string> userEmails;
                if (!String.IsNullOrWhiteSpace(eventFields.AssignedUserEmails))
                {
                    userEmails = eventFields.AssignedUserEmails.Split(',').ToList();
                    foreach (var userEmail in userEmails.Distinct())
                    {
                        try
                        {
                            var assignedUser = Users.FirstOrDefault(u => u.Email == userEmail);
                            if (assignedUser is null)
                            {
                                errorFields.Error = "Could not find user for email " + userEmail;
                                uploadResults.ErrorRows.Add(errorFields);
                                continue;
                            }
                            activityGuidUserTag.UserIds.Add(assignedUser.Id);
                        }
                        catch (Exception)
                        {
                            errorFields.Error = "Could not find user for email " + userEmail;
                            uploadResults.ErrorRows.Add(errorFields);
                            continue;
                        }
                    }
                }

                List<string> tags;
                if (!String.IsNullOrWhiteSpace(eventFields.Tags))
                {
                    tags = eventFields.Tags.Split(',').ToList();
                    foreach (var tag in tags.Distinct())
                    {
                        var eventTag = PpTags.FirstOrDefault(t => t.Name == tag & t.TagFor == 2);
                        if (eventTag is null)
                        {
                            Console.WriteLine("Creating new activity tag with name '" + tag + "'\n");
                            var newTag = new Tag()
                            {
                                /*
                                    Copied from Tag model in PP
                                    public enum TagFor : int
                                    {
                                        Account = 0,
                                        Project = 1,
                                        Activity = 2
                                    }
                                */
                                Name = tag.Trim(),
                                Guid = Guid.NewGuid(),
                                IsDeleted = false,
                                IsEnabled = true,
                                CreatedById = FirmTenant.PrimaryUserId,
                                LastModifiedById = FirmTenant.LastModifiedById,
                                CreatedDate = DateTime.Now,
                                LastModifiedDate = DateTime.Now,
                                TenantId = FirmTenant.Id,
                                TagFor = 2
                            };

                            Context.Tags.Add(newTag);
                            Context.SaveChanges();
                            Console.WriteLine("Successfully created new activity tag with Id " + newTag.Id);

                            activityGuidUserTag.TagIds.Add(newTag.Id);

                            //var activityGuidTag = new ActivityGuidTag()
                            //{
                            //    ActivityGuid = eventToInsert.Guid,
                            //    Tag_Id = newTag.Id
                            //};
                            PpTags = GetTags();
                            //activityGuidTags.Add(activityGuidTag);
                        }

                        else
                        {
                            activityGuidUserTag.TagIds.Add(eventTag.Id);
                            //var activityGuidTag = new ActivityGuidTag()
                            //{
                            //    ActivityGuid = eventToInsert.Guid,
                            //    Tag_Id = eventTag.Id
                            //};
                            //activityGuidTags.Add(activityGuidTag);
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(eventFields.Location))
                {
                    if(eventFields.Location.Length>100)
                    {
                        errorFields.Error = "Location must be a string of 100 characters or less";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        eventToInsert.Location = eventFields.Location;
                    }
                }

                if (!(errorFields.Error is null))
                {
                    continue;
                }

                successFields.EventGuid = eventToInsert.Guid;
                uploadResults.SuccessRows.Add(successFields);
                activityGuidUserTags.Add(activityGuidUserTag);
                activitiesToAdd.Add(eventToInsert);
                counter++;
                //Console.WriteLine("Row #" + counter.ToString() + "    \t\tValidated event " + eventToInsert.Guid + "\n");

                if (counter % rowsPerInsert == 0)
                {
                    if (taskInserts.Count > 10)  // this prevents too many Tasks from running at once
                    {
                        var ongoingInserts = taskInserts.GetRange(0, taskInserts.Count - 10).ToArray();  // isolate the previous tasks
                        await Task.WhenAll(ongoingInserts);  // wait for all of the previous tasks to complete
                        taskInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                        activitiesToAdd = new List<Activity>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }

                    else
                    {
                        taskInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                        activitiesToAdd = new List<Activity>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }
                }
            }

            if (activitiesToAdd.Count > 0)
            {
                //eventInserts.Add(InsertEventsAsync(activitiesToAdd, activityGuidUsers, activityGuidTags));
                taskInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                await Task.WhenAll(taskInserts.ToArray());
                activitiesToAdd = new List<Activity>();
                activityGuidUserTags = new List<ActivityGuidUserTag>();
            }
            var contextEndTime = DateTime.Now;
            var totalElapsedTime = ContextStartTime - contextEndTime;
            uploadResults.StartTime = ContextStartTime;
            uploadResults.EndTime = contextEndTime;
            uploadResults.Duration = totalElapsedTime;
            uploadResults.NumberOfTransactions = taskInserts.Count * 3;
            return uploadResults;
        }

        public async Task<UploadResults> ImportTaskCsv(List<dynamic> csvRows, int rowsPerInsert)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(FirmTenant.TimeZoneId);
            UploadResults uploadResults = new UploadResults();
            uploadResults.OriginalRows = csvRows;
            var counter = 0;
            List<Task> taskInserts = new List<Task>();  // add task from InsertEventsAsync() to this list to await it before calling InsertEventsAsync again
            List<Activity> activitiesToAdd = new List<Activity>();  // add validated activities here, insert with SqlBulkInsert
            List<ActivityGuidUserTag> activityGuidUserTags = new List<ActivityGuidUserTag>();   // store each Event with its Guid and list of UserIds and TagIds. Use this to insert rows into
                                                                                                // ActivityUsers and TagActivities

            foreach (var row in csvRows)
            {
                // for success/error files
                dynamic successFields = new ExpandoObject();
                var successDictionary = (IDictionary<string, object>)successFields;
                successDictionary.Add("TaskGuid", null);  // update this field if an Event is created
                foreach (var field in row)
                {
                    successDictionary.Add(field.Key, field.Value);
                }

                dynamic errorFields = new ExpandoObject();
                var errorDictionary = (IDictionary<string, object>)errorFields;
                errorDictionary.Add("Error", null);
                foreach (var field in row)
                {
                    errorDictionary.Add(field.Key, field.Value);
                }

                TaskFields taskFields = null;
                var taskToInsert = new Activity()
                {
                    CreatedById = FirmTenant.PrimaryUserId,
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    LastModifiedDate = DateTime.Now.ToUniversalTime(),
                    IsDeleted = false,
                    IsEnabled = true,
                    //  copied from PP Activity model
                    //  public enum Activity_Type
                    //  {
                    //      Call = 0,
                    //      Task = 1,
                    //      Event = 2,
                    //      Email = 3,
                    //      Note = 4
                    //  }
                    EmailProperties_From = "null",
                    EmailProperties_To = "[]",
                    EmailProperties_Cc = "[]",
                    EventColor = "#368ee0",
                    Priority = 2,
                    IsPrivate = false,
                    Type = 1,
                    Guid = Guid.NewGuid(),
                    TenantId = FirmTenant.Id,
                };
                
                var activityGuidUserTag = new ActivityGuidUserTag() { ActivityGuid = taskToInsert.Guid };
                
                try
                {
                    taskFields = new TaskFields(row);
                    if (String.IsNullOrWhiteSpace(taskFields.Name))
                    {
                        errorFields.Error = "Must provide subject";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    if (String.IsNullOrWhiteSpace(taskFields.AssignedUserEmails) | !taskFields.AssignedUserEmails.Contains("@"))
                    {
                        errorFields.Error = "Must provide at least one user email address";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    if (String.IsNullOrWhiteSpace(taskFields.Status))
                    {
                        errorFields.Error = "Must provide task status";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                }

                catch (Exception ex)
                {
                    errorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                if (!(taskFields.ContactNumber is null))
                {
                    var accountNumberId = AccountNumberIds.FirstOrDefault(a => a.Number == taskFields.ContactNumber);

                    if (accountNumberId is null)
                    {
                        errorFields.Error = "No contact found with number " + taskFields.ContactNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        taskToInsert.AccountId = accountNumberId.Id;
                    }
                }

                if (!(taskFields.MatterNumber is null))
                {
                    var projectNumberId = ProjectNumberIds.FirstOrDefault(a => a.Number == taskFields.MatterNumber);

                    if (projectNumberId is null)
                    {
                        errorFields.Error = "No matter found with number " + taskFields.MatterNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        taskToInsert.ProjectId = projectNumberId.Id;
                        taskToInsert.AccountId = projectNumberId.AccountId;
                    }
                }

                taskToInsert.DueDate = TimeZoneInfo.ConvertTimeToUtc(taskFields.DueDate, timeZoneInfo);

                if (!String.IsNullOrWhiteSpace(taskFields.Name))
                {
                    if (taskFields.Name.Length > 1000)
                    {
                        errorFields.Error = "The subject must not be greater than 1000 characters";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        taskToInsert.Name = taskFields.Name;
                    }
                }

                if (!String.IsNullOrWhiteSpace(taskFields.Description))
                {
                    taskToInsert.Description = taskFields.Description;
                }

                if (String.IsNullOrWhiteSpace(taskFields.Status))
                {
                    errorFields.Error = "Must provide status";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                if (!String.IsNullOrWhiteSpace(taskFields.Status))
                {
                    // copied from Activty model in PracticePanther
                    //public enum Activity_Status
                    //{
                    //    NotCompleted = 0,
                    //    InProgress = 1,
                    //    Completed = 2,//    Conditional = 4 // Since status =3 is being used by one record, i'll just use the number 4 to avoid any conflict. I don't want to manually delete the record.
                    //}

                    switch (taskFields.Status.ToLower())
                    {
                        case "notcompleted":
                            taskToInsert.Status = 0;
                            break;
                        case "inprogress":
                            taskToInsert.Status = 1;
                            break;
                        case "completed":
                            taskToInsert.Status = 2;
                            break;
                        default:
                            errorFields.Error = "Invalid task status";
                            uploadResults.ErrorRows.Add(errorFields);
                            continue;
                    }
                }

                if (String.IsNullOrWhiteSpace(taskFields.AssignedUserEmails) | !taskFields.AssignedUserEmails.Contains("@"))
                {
                    errorFields.Error = "Must provide at least one user email address";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                List<string> userEmails;
                if (!String.IsNullOrWhiteSpace(taskFields.AssignedUserEmails))
                {
                    userEmails = taskFields.AssignedUserEmails.Split(',').ToList();
                    foreach (var userEmail in userEmails.Distinct())
                    {
                        try
                        {
                            var assignedUser = Users.FirstOrDefault(u => u.Email == userEmail);
                            if (assignedUser is null)
                            {
                                errorFields.Error = "Could not find user for email " + userEmail;
                                uploadResults.ErrorRows.Add(errorFields);
                                continue;
                            }
                            activityGuidUserTag.UserIds.Add(assignedUser.Id);
                        }
                        catch (Exception)
                        {
                            errorFields.Error = "Could not find user for email " + userEmail;
                            uploadResults.ErrorRows.Add(errorFields);
                            continue;
                        }
                    }
                }

                List<string> tags;
                if (!String.IsNullOrWhiteSpace(taskFields.Tags))
                {
                    tags = taskFields.Tags.Split(',').ToList();
                    foreach (var tag in tags.Distinct())
                    {
                        var taskTag = PpTags.FirstOrDefault(t => t.Name == tag & t.TagFor == 2);
                        if (taskTag is null)
                        {
                            Console.WriteLine("Creating new activity tag with name '" + tag + "'\n");
                            var newTag = new Tag()
                            {
                                /*
                                    Copied from Tag model in PP
                                    public enum TagFor : int
                                    {
                                        Account = 0,
                                        Project = 1,
                                        Activity = 2
                                    }
                                */
                                Name = tag.Trim(),
                                Guid = Guid.NewGuid(),
                                IsDeleted = false,
                                IsEnabled = true,
                                CreatedById = FirmTenant.PrimaryUserId,
                                LastModifiedById = FirmTenant.LastModifiedById,
                                CreatedDate = DateTime.Now,
                                LastModifiedDate = DateTime.Now,
                                TenantId = FirmTenant.Id,
                                TagFor = 2
                            };

                            Context.Tags.Add(newTag);
                            Context.SaveChanges();
                            Console.WriteLine("Successfully created new activity tag with Id " + newTag.Id);
                            activityGuidUserTag.TagIds.Add(newTag.Id);
                            PpTags = GetTags();
                        }

                        else
                        {
                            activityGuidUserTag.TagIds.Add(taskTag.Id);
                        }
                    }
                }

                if (!(errorFields.Error is null))
                {
                    continue;
                }

                successFields.TaskGuid = taskToInsert.Guid;
                uploadResults.SuccessRows.Add(successFields);
                activityGuidUserTags.Add(activityGuidUserTag);
                activitiesToAdd.Add(taskToInsert);
                counter++;

                if (counter % rowsPerInsert == 0)
                {
                    if (taskInserts.Count > 10)  // this prevents too many Tasks from running at once
                    {
                        var ongoingInserts = taskInserts.GetRange(0, taskInserts.Count - 10).ToArray();  // isolate the previous tasks
                        await Task.WhenAll(ongoingInserts);  // wait for all of the previous tasks to complete
                        taskInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                        activitiesToAdd = new List<Activity>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }

                    else
                    {
                        taskInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                        activitiesToAdd = new List<Activity>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }
                }
            }
            if (activitiesToAdd.Count > 0)
            {
                taskInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                await Task.WhenAll(taskInserts.ToArray());
                activitiesToAdd = new List<Activity>();
                activityGuidUserTags = new List<ActivityGuidUserTag>();
            }

            var contextEndTime = DateTime.Now;
            var totalElapsedTime = ContextStartTime - contextEndTime;
            uploadResults.StartTime = ContextStartTime;
            uploadResults.EndTime = contextEndTime;
            uploadResults.Duration = totalElapsedTime;
            uploadResults.NumberOfTransactions = taskInserts.Count * 3;
            return uploadResults;
        }

        public async Task<UploadResults> ImportNoteCsv (List<dynamic> csvRows, int rowsPerInsert)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(FirmTenant.TimeZoneId);
            UploadResults uploadResults = new UploadResults();
            uploadResults.OriginalRows = csvRows;
            var counter = 0;
            List<Task> noteInserts = new List<Task>();  // add task from InsertEventsAsync() to this list to await it before calling InsertEventsAsync again
            List<Activity> activitiesToAdd = new List<Activity>();  // add validated activities here, insert with SqlBulkInsert
            List<ActivityGuidUserTag> activityGuidUserTags = new List<ActivityGuidUserTag>();   // store each Event with its Guid and list of UserIds and TagIds. Use this to insert rows into
                                                                                                // ActivityUsers and TagActivities

            foreach (var row in csvRows)
            {
                // for success/error files
                dynamic successFields = new ExpandoObject();
                var successDictionary = (IDictionary<string, object>)successFields;
                successDictionary.Add("NoteGuid", null);  // update this field if an Event is created
                foreach (var field in row)
                {
                    successDictionary.Add(field.Key, field.Value);
                }

                dynamic errorFields = new ExpandoObject();
                var errorDictionary = (IDictionary<string, object>)errorFields;
                errorDictionary.Add("Error", null);
                foreach (var field in row)
                {
                    errorDictionary.Add(field.Key, field.Value);
                }

                NoteFields noteFields = null;
                var noteToInsert = new Activity()
                {
                    CreatedById = FirmTenant.PrimaryUserId,
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    LastModifiedDate = DateTime.Now.ToUniversalTime(),
                    IsDeleted = false,
                    IsEnabled = true,
                    //  copied from PP Activity model
                    //  public enum Activity_Type
                    //  {
                    //      Call = 0,
                    //      Task = 1,
                    //      Event = 2,
                    //      Email = 3,
                    //      Note = 4
                    //  }
                    EmailProperties_From = "null",
                    EmailProperties_To = "[]",
                    EmailProperties_Cc = "[]",
                    EventColor = "#368ee0",
                    Priority = 2,
                    IsPrivate = false,
                    Type = 4,
                    Guid = Guid.NewGuid(),
                    TenantId = FirmTenant.Id,
                };

                var activityGuidUserTag = new ActivityGuidUserTag() { ActivityGuid = noteToInsert.Guid };

                try
                {
                    noteFields = new NoteFields(row);
                    if (String.IsNullOrWhiteSpace(noteFields.Name))
                    {
                        errorFields.Error = "Must provide subject";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                }

                catch (Exception ex)
                {
                    errorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                if (!(noteFields.ContactNumber is null))
                {
                    var accountNumberId = AccountNumberIds.FirstOrDefault(a => a.Number == noteFields.ContactNumber);

                    if (accountNumberId is null)
                    {
                        errorFields.Error = "No contact found with number " + noteFields.ContactNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        noteToInsert.AccountId = accountNumberId.Id;
                    }
                }

                if (!(noteFields.MatterNumber is null))
                {
                    var projectNumberId = ProjectNumberIds.FirstOrDefault(a => a.Number == noteFields.MatterNumber);

                    if (projectNumberId is null)
                    {
                        errorFields.Error = "No matter found with number " + noteFields.MatterNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        noteToInsert.ProjectId = projectNumberId.Id;
                        noteToInsert.AccountId = projectNumberId.AccountId;
                    }
                }

                noteToInsert.Date = TimeZoneInfo.ConvertTimeToUtc(noteFields.Date, timeZoneInfo);

                if (!String.IsNullOrWhiteSpace(noteFields.Name))
                {
                    if (noteFields.Name.Length > 1000)
                    {
                        errorFields.Error = "The subject must not be greater than 1000 characters";
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        noteToInsert.Name = noteFields.Name;
                    }
                }

                if (!String.IsNullOrWhiteSpace(noteFields.Description))
                {
                    noteToInsert.Description = noteFields.Description;
                }

                if (String.IsNullOrWhiteSpace(noteFields.AssignedUserEmails) | !noteFields.AssignedUserEmails.Contains("@"))
                {
                    errorFields.Error = "Must provide at least one user email address";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                List<string> userEmails;
                if (!String.IsNullOrWhiteSpace(noteFields.AssignedUserEmails))
                {
                    userEmails = noteFields.AssignedUserEmails.Split(',').ToList();
                    foreach (var userEmail in userEmails.Distinct())
                    {
                        try
                        {
                            var assignedUser = Users.FirstOrDefault(u => u.Email == userEmail);
                            if (assignedUser is null)
                            {
                                errorFields.Error = "Could not find user for email " + userEmail;
                                uploadResults.ErrorRows.Add(errorFields);
                                continue;
                            }
                            activityGuidUserTag.UserIds.Add(assignedUser.Id);
                        }
                        catch (Exception)
                        {
                            errorFields.Error = "Could not find user for email " + userEmail;
                            uploadResults.ErrorRows.Add(errorFields);
                            continue;
                        }
                    }
                }

                List<string> tags;
                if (!String.IsNullOrWhiteSpace(noteFields.Tags))
                {
                    tags = noteFields.Tags.Split(',').ToList();
                    foreach (var tag in tags.Distinct())
                    {
                        var taskTag = PpTags.FirstOrDefault(t => t.Name == tag & t.TagFor == 2);
                        if (taskTag is null)
                        {
                            Console.WriteLine("Creating new activity tag with name '" + tag + "'\n");
                            var newTag = new Tag()
                            {
                                /*
                                    Copied from Tag model in PP
                                    public enum TagFor : int
                                    {
                                        Account = 0,
                                        Project = 1,
                                        Activity = 2
                                    }
                                */
                                Name = tag.Trim(),
                                Guid = Guid.NewGuid(),
                                IsDeleted = false,
                                IsEnabled = true,
                                CreatedById = FirmTenant.PrimaryUserId,
                                LastModifiedById = FirmTenant.LastModifiedById,
                                CreatedDate = DateTime.Now,
                                LastModifiedDate = DateTime.Now,
                                TenantId = FirmTenant.Id,
                                TagFor = 2
                            };

                            Context.Tags.Add(newTag);
                            Context.SaveChanges();
                            Console.WriteLine("Successfully created new activity tag with Id " + newTag.Id);
                            activityGuidUserTag.TagIds.Add(newTag.Id);
                            PpTags = GetTags();
                        }

                        else
                        {
                            activityGuidUserTag.TagIds.Add(taskTag.Id);
                        }
                    }
                }

                if (!(errorFields.Error is null))
                {
                    continue;
                }

                successFields.NoteGuid = noteToInsert.Guid;
                uploadResults.SuccessRows.Add(successFields);
                activityGuidUserTags.Add(activityGuidUserTag);
                activitiesToAdd.Add(noteToInsert);
                counter++;

                if (counter % rowsPerInsert == 0)
                {
                    if (noteInserts.Count > 10)  // this prevents too many Tasks from running at once
                    {
                        var ongoingInserts = noteInserts.GetRange(0, noteInserts.Count - 10).ToArray();  // isolate the previous tasks
                        await Task.WhenAll(ongoingInserts);  // wait for all of the previous tasks to complete
                        noteInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                        activitiesToAdd = new List<Activity>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }

                    else
                    {
                        noteInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                        activitiesToAdd = new List<Activity>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }
                }
            }
            if (activitiesToAdd.Count > 0)
            {
                noteInserts.Add(InsertActivitiesAsync(activitiesToAdd, activityGuidUserTags));
                await Task.WhenAll(noteInserts.ToArray());
                activitiesToAdd = new List<Activity>();
                activityGuidUserTags = new List<ActivityGuidUserTag>();
            }

            var contextEndTime = DateTime.Now;
            var totalElapsedTime = ContextStartTime - contextEndTime;
            uploadResults.StartTime = ContextStartTime;
            uploadResults.EndTime = contextEndTime;
            uploadResults.Duration = totalElapsedTime;
            uploadResults.NumberOfTransactions = noteInserts.Count * 3;
            return uploadResults;
        }

        public async Task<UploadResults> ImportRelationshipsCsv(List<dynamic> csvRows, int rowsPerInsert)
        {
            UploadResults uploadResults = new UploadResults();
            uploadResults.OriginalRows = csvRows;
            var counter = 0;
            List<Task> relInserts = new List<Task>();
            List<AccountProjectLink> relsToAdd = new List<AccountProjectLink>();
            List<ActivityGuidUserTag> activityGuidUserTags = new List<ActivityGuidUserTag>();

            foreach (var row in csvRows)
            {
                // for success/error files
                dynamic successFields = new ExpandoObject();
                var successDictionary = (IDictionary<string, object>)successFields;
                successDictionary.Add("RelationshipGuid", null);  // update this field if an Event is created
                foreach (var field in row)
                {
                    successDictionary.Add(field.Key, field.Value);
                }

                dynamic errorFields = new ExpandoObject();
                var errorDictionary = (IDictionary<string, object>)errorFields;
                errorDictionary.Add("Error", null);
                foreach (var field in row)
                {
                    errorDictionary.Add(field.Key, field.Value);
                }

                RelationshipFields relFields = null;
                var relToInsert = new AccountProjectLink()
                {
                    CreatedBy_Id = FirmTenant.PrimaryUserId,
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    LastModifiedDate = DateTime.Now.ToUniversalTime(),
                    IsDeleted = false,
                    IsEnabled = true,
                };

                var activityGuidUserTag = new ActivityGuidUserTag() { ActivityGuid = relToInsert.Guid };

                try
                {
                    relFields = new RelationshipFields(row);
                }
                catch (Exception ex)
                {
                    errorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }

                if (!(relFields.ContactNumber is null))
                {
                    var accountNumberId = AccountNumberIds.FirstOrDefault(a => a.Number == relFields.ContactNumber);

                    if (accountNumberId is null)
                    {
                        errorFields.Error = "No contact found with number " + relFields.ContactNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        relToInsert.AccountId = accountNumberId.Id;
                        relToInsert.ContactId = accountNumberId.PrimaryContactId;
                    }
                }

                if (!(relFields.MatterNumber is null))
                {
                    var projectNumberId = ProjectNumberIds.FirstOrDefault(a => a.Number == relFields.MatterNumber);

                    if (projectNumberId is null)
                    {
                        errorFields.Error = "No matter found with number " + relFields.MatterNumber;
                        uploadResults.ErrorRows.Add(errorFields);
                        continue;
                    }
                    else
                    {
                        relToInsert.ProjectId = projectNumberId.Id;
                    }
                }
                
                if (String.IsNullOrWhiteSpace(relFields.RelationshipName))
                {
                    errorFields.Error = "Must provide RelationshipName";
                    uploadResults.ErrorRows.Add(errorFields);
                    continue;
                }
                else 
                {
                    relToInsert.RelationshipName = relFields.RelationshipName;
                }


                if (!String.IsNullOrWhiteSpace(relFields.Notes))
                {
                    relToInsert.Notes = relFields.Notes;
                }

                if (!(errorFields.Error is null))
                {
                    continue;
                }

                successFields.NoteGuid = relToInsert.Guid;
                uploadResults.SuccessRows.Add(successFields);
                activityGuidUserTags.Add(activityGuidUserTag);
                relsToAdd.Add(relToInsert);
                counter++;

                if (counter % rowsPerInsert == 0)
                {
                    if (relInserts.Count > 10)  // this prevents too many Tasks from running at once
                    {
                        var ongoingInserts = relInserts.GetRange(0, relInserts.Count - 10).ToArray();  // isolate the previous tasks
                        await Task.WhenAll(ongoingInserts);  // wait for all of the previous tasks to complete
                        relInserts.Add(InsertRelationshipsAsync(relsToAdd));
                        relsToAdd = new List<AccountProjectLink>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }

                    else
                    {
                        relInserts.Add(InsertRelationshipsAsync(relsToAdd));
                        relsToAdd = new List<AccountProjectLink>();
                        activityGuidUserTags = new List<ActivityGuidUserTag>();
                        continue;
                    }
                }
            }
            if (relsToAdd.Count > 0)
            {
                relInserts.Add(InsertRelationshipsAsync(relsToAdd));
                await Task.WhenAll(relInserts.ToArray());
                relsToAdd = new List<AccountProjectLink>();
                activityGuidUserTags = new List<ActivityGuidUserTag>();
            }

            var contextEndTime = DateTime.Now;
            var totalElapsedTime = ContextStartTime - contextEndTime;
            uploadResults.StartTime = ContextStartTime;
            uploadResults.EndTime = contextEndTime;
            uploadResults.Duration = totalElapsedTime;
            uploadResults.NumberOfTransactions = relInserts.Count * 3;
            return uploadResults;

        }

        public DataTable ConvertListToDataTable<T>(List<T> items)
        {
            string tableName = null;
            switch (typeof(T).Name)
            {
                case "Activity":
                    tableName = "Activities";
                    break;
                case "TimeEntry":
                    tableName = "TimeEntries";
                    break;
                case "Expens":
                    tableName = "Expenses";
                    break;
                case "AccountProjectLink":
                    tableName = "AccountProjectLinks";
                    break;
            }
            List<string> activityCols = new List<string>();
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                String[] tableRestrictions = new String[4];
                tableRestrictions[2] = tableName;
                var activitySchema = connection.GetSchema("Columns", tableRestrictions);

                foreach (DataRow schemaRow in activitySchema.Rows)
                {
                    activityCols.Add((schemaRow["Column_Name"]).ToString());
                }
                connection.Close();
            }

            DataTable dataTable = new DataTable();
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                if (activityCols.Contains(prop.Name))
                {
                    //Setting column names as Property names
                    DataColumn column = new DataColumn();
                    column.ColumnName = prop.Name;
                    var dataType = System.Type.GetType(prop.PropertyType.FullName);
                    if (dataType.Name.Contains("Nullable"))
                    {
                        column.DataType = Nullable.GetUnderlyingType(System.Type.GetType(prop.PropertyType.FullName));
                    }
                    else
                    {
                        column.DataType = dataType;
                    }
                    dataTable.Columns.Add(column);
                }
                else
                {
                    continue;
                }
            }
            DataRow row;
            foreach (T item in items)
            {
                row = dataTable.NewRow();
                foreach (var col in dataTable.Columns)
                {
                    var columnProp = Props.FirstOrDefault(cp => cp.Name == col.ToString());
                    var colValue = columnProp.GetValue(item, null);
                    if (colValue is null)
                    {
                        colValue = DBNull.Value;
                    }
                    if (columnProp.PropertyType.Name is "Boolean")
                    {
                        var tempBool = Convert.ToBoolean(colValue.ToString());
                        row[columnProp.Name] = tempBool;
                    }

                    else
                    {
                        row[columnProp.Name] = colValue;
                    }

                }
                if (row.HasErrors)
                {
                    var rowErrors = row.GetColumnsInError();
                }

                else
                {

                    dataTable.Rows.Add(row);
                }
            }
            var tableErrors = dataTable.GetErrors();
            return dataTable;
        }

        public async Task<UploadResults> ImportTimeCsv(List<dynamic> csvRows, int rowsPerInsert)
        {
            UploadResults uploadResults = new UploadResults();
            uploadResults.OriginalRows = csvRows;
            var counter = 0;
            List<Task> timeInserts = new List<Task>();  // add task from InsertEventsAsync() to this list to await it before calling InsertEventsAsync again
            List<TimeEntry> timeEntriesToAdd = new List<TimeEntry>();  // add validated activities here, insert with SqlBulkInsert

            foreach (var row in csvRows)
            {
                bool errorFlag = false;
                // for success/error files
                dynamic successFields = new ExpandoObject();
                var successDictionary = (IDictionary<string, object>)successFields;
                successDictionary.Add("TimeEntryGuid", null);  // update this field if a Time Entry is created
                foreach (var field in row)
                {
                    successDictionary.Add(field.Key, field.Value);
                }

                dynamic timeErrorFields = new ExpandoObject();
                var errorDictionary = (IDictionary<string, object>)timeErrorFields;
                errorDictionary.Add("Error", null);
                foreach (var field in row)
                {
                    errorDictionary.Add(field.Key, field.Value);
                }

                TimeEntryFields timeFields = null;
                var timeToInsert = new TimeEntry()
                {
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    LastModifiedDate = DateTime.Now.ToUniversalTime(),
                    IsDeleted = false,
                    IsEnabled = true,
                    Guid = Guid.NewGuid(),
                    Tenant_Id = FirmTenant.Id,
                };

                
                timeFields = new TimeEntryFields(row);

                if (!String.IsNullOrWhiteSpace(timeFields.Error))
                {
                    timeErrorFields.Error = timeFields.Error;
                    uploadResults.ErrorRows.Add(timeErrorFields);
                    errorFlag = true;
                }
                
                if (!(timeFields.MatterNumber is null))
                {
                    var projectNumberId = ProjectNumberIds.FirstOrDefault(a => a.Number == timeFields.MatterNumber);

                    if (projectNumberId is null)
                    {
                        timeErrorFields.Error = "No matter found with number " + timeFields.MatterNumber;
                        uploadResults.ErrorRows.Add(timeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        timeToInsert.ProjectId = projectNumberId.Id;
                        timeToInsert.AccountId = projectNumberId.AccountId;
                    }
                }

                try
                {
                    if (timeFields.Date.HasValue)
                    {
                        timeToInsert.Date = TimeZoneInfo.ConvertTimeToUtc(timeFields.Date.Value, TimeZoneInfo);
                    }
                    else
                    {
                        timeErrorFields.Error = "Date is required";
                        uploadResults.ErrorRows.Add(timeErrorFields);
                        errorFlag = true;
                    }
                }
                catch (Exception ex)
                {
                    timeErrorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(timeErrorFields);
                    errorFlag = true;
                }

                try
                {
                    if (timeFields.Hours <= 0)
                    {
                        timeErrorFields.Error = "Hours must be greater than zero";
                        uploadResults.ErrorRows.Add(timeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        timeToInsert.Hours = timeFields.Hours;
                    }
                    if(timeFields.HourlyRate < 0)
                    {
                        timeErrorFields.Error = "Rate cannot be a negative number";
                        uploadResults.ErrorRows.Add(timeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        timeToInsert.HourlyRate = timeFields.HourlyRate;
                    }
                    if(!(String.IsNullOrWhiteSpace(timeFields.Description)))
                    {
                        timeToInsert.Description = timeFields.Description;
                    }
                    if (String.IsNullOrWhiteSpace(timeFields.User))
                    {
                        timeErrorFields.Error = "Must provide at least one user email address";
                        uploadResults.ErrorRows.Add(timeErrorFields);
                        errorFlag = true;
                    }
                    else if(!timeFields.User.Contains("@"))
                    {
                        timeErrorFields.Error = "Invalid email address.";
                        uploadResults.ErrorRows.Add(timeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        var userId = Users.FirstOrDefault(u => u.Email == timeFields.User).Id;
                        timeToInsert.UserId = userId;
                        timeToInsert.CreatedBy_Id = userId;
                    }
                }
                catch (Exception ex)
                {
                    timeErrorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(timeErrorFields);
                    errorFlag = true;
                }

                //TODO move to csvfields (re-consider since it would require a lot of changes and too little abstraction for it to be worth it)
                //Handles the time entry status by reading the status string from csv and setting the number according to the status enum on PP model.

                switch (timeFields.Status)
                {
                    case "Billable":
                        timeToInsert.Status = 1;
                        timeToInsert.IsBillable = true;
                        timeToInsert.Total = timeToInsert.HourlyRate * timeToInsert.Hours;
                        break;
                    case "Billed":
                        timeToInsert.Status = 2;
                        timeToInsert.IsBillable = true;
                        timeToInsert.IsBilled = true;
                        timeToInsert.Total = timeToInsert.HourlyRate * timeToInsert.Hours;
                        break;
                    case "NotBillable":
                        timeToInsert.Status = 4;
                        timeToInsert.IsBillable = false;
                        timeToInsert.Total = 0;
                        break;
                    default:
                        timeErrorFields.Error = "Invalid status";
                        errorFlag = true;
                        break;
                }

                if (!String.IsNullOrWhiteSpace(timeFields.ItemCode))
                {
                    try
                    {
                        var codes = GetUtbmsCodes();
                        timeToInsert.UtbmsCodeId = codes.FirstOrDefault(c => c.Code == timeFields.ItemCode).Id;
                        timeToInsert.Name = codes.FirstOrDefault(c => c.Code == timeFields.ItemCode).Name;
                    }
                    catch (Exception ex)
                    {
                        //TODO add method to create custom products.
                        timeErrorFields.Error = ex.InnerException;
                        uploadResults.ErrorRows.Add(timeErrorFields);
                        errorFlag = true;
                    }
                }
                else
                {
                    timeToInsert.Name = "Time";
                }

                if (errorFlag == true)
                {
                    continue;
                }

                successFields.TimeEntryGuid = timeToInsert.Guid;
                
                uploadResults.SuccessRows.Add(successFields);
                timeEntriesToAdd.Add(timeToInsert);
                counter++;

                if (counter % rowsPerInsert == 0)
                {
                    if (timeInserts.Count > 10)  // this prevents too many Tasks from running at once
                    {
                        var ongoingInserts = timeInserts.GetRange(0, timeInserts.Count - 10).ToArray();  // isolate the previous tasks
                        await Task.WhenAll(ongoingInserts);  // wait for all of the previous tasks to complete
                        timeInserts.Add(InsertTimeAsync(timeEntriesToAdd));
                        timeEntriesToAdd = new List<TimeEntry>();
                        continue;
                    }

                    else
                    {
                        timeInserts.Add(InsertTimeAsync(timeEntriesToAdd));
                        timeEntriesToAdd = new List<TimeEntry>();
                        continue;
                    }
                }
            }
            if (timeEntriesToAdd.Count > 0)
            {
                timeInserts.Add(InsertTimeAsync(timeEntriesToAdd));
                await Task.WhenAll(timeInserts.ToArray());
                timeEntriesToAdd = new List<TimeEntry>();
            }

            var contextEndTime = DateTime.Now;
            var totalElapsedTime = ContextStartTime - contextEndTime;
            uploadResults.StartTime = ContextStartTime;
            uploadResults.EndTime = contextEndTime;
            uploadResults.Duration = totalElapsedTime;
            uploadResults.NumberOfTransactions = timeInserts.Count * 3;
            return uploadResults;
        }

        public async Task<UploadResults> ImportExpenseCsv(List<dynamic> csvRows, int rowsPerInsert)
        {
            UploadResults uploadResults = new UploadResults();
            uploadResults.OriginalRows = csvRows;
            var counter = 0;
            List<Task> expenseInserts = new List<Task>();  // add task from InsertEventsAsync() to this list to await it before calling InsertEventsAsync again
            List<Expens> expensesToAdd = new List<Expens>();  // add validated activities here, insert with SqlBulkInsert

            foreach (var row in csvRows)
            {
                bool errorFlag = false;
                // for success/error files
                dynamic successFields = new ExpandoObject();
                var successDictionary = (IDictionary<string, object>)successFields;
                successDictionary.Add("ExpenseGuid", null);  // update this field if a Time Entry is created
                foreach (var field in row)
                {
                    successDictionary.Add(field.Key, field.Value);
                }

                dynamic expenseErrorFields = new ExpandoObject();
                var errorDictionary = (IDictionary<string, object>)expenseErrorFields;
                errorDictionary.Add("Error", null);
                foreach (var field in row)
                {
                    errorDictionary.Add(field.Key, field.Value);
                }

                ExpenseFields expenseFields = null;
                var expenseToInsert = new Expens()
                {
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    LastModifiedDate = DateTime.Now.ToUniversalTime(),
                    IsDeleted = false,
                    IsEnabled = true,
                    Guid = Guid.NewGuid(),
                    TenantId = FirmTenant.Id,
                };


                expenseFields = new ExpenseFields(row);

                if (!String.IsNullOrWhiteSpace(expenseFields.Error))
                {
                    expenseErrorFields.Error = expenseFields.Error;
                    uploadResults.ErrorRows.Add(expenseErrorFields);
                    errorFlag = true;
                }

                if (!(expenseFields.MatterNumber is null))
                {
                    var projectNumberId = ProjectNumberIds.FirstOrDefault(a => a.Number == expenseFields.MatterNumber);

                    if (projectNumberId is null)
                    {
                        expenseErrorFields.Error = "No matter found with number " + expenseFields.MatterNumber;
                        uploadResults.ErrorRows.Add(expenseErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        expenseToInsert.ProjectId = projectNumberId.Id;
                        expenseToInsert.AccountId = projectNumberId.AccountId;
                    }
                }

                expenseToInsert.AccountingAccountId = 42;

                try
                {
                    if (expenseFields.Date.HasValue)
                    {
                        expenseToInsert.Date = TimeZoneInfo.ConvertTimeToUtc(expenseFields.Date.Value, TimeZoneInfo);
                    }
                    else
                    {
                        expenseErrorFields.Error = "Date is required";
                        uploadResults.ErrorRows.Add(expenseErrorFields);
                        errorFlag = true;
                    }
                }
                catch (Exception ex)
                {
                    expenseErrorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(expenseErrorFields);
                    errorFlag = true;
                }

                try
                {
                    if (expenseFields.Qty <= 0)
                    {
                        expenseErrorFields.Error = "Quantity must be greater than zero";
                        uploadResults.ErrorRows.Add(expenseErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        expenseToInsert.Qty = expenseFields.Qty;
                    }
                    if (expenseFields.Amount < 0)
                    {
                        expenseErrorFields.Error = "Amount cannot be a negative number";
                        uploadResults.ErrorRows.Add(expenseErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        expenseToInsert.Amount = expenseFields.Amount;
                    }
                    if (!(String.IsNullOrWhiteSpace(expenseFields.Description)))
                    {
                        expenseToInsert.Description = expenseFields.Description;
                    }
                    if (String.IsNullOrWhiteSpace(expenseFields.User))
                    {
                        expenseErrorFields.Error = "Must provide at least one user email address";
                        uploadResults.ErrorRows.Add(expenseErrorFields);
                        errorFlag = true;
                    }
                    else if (!expenseFields.User.Contains("@"))
                    {
                        expenseErrorFields.Error = "Invalid email address.";
                        uploadResults.ErrorRows.Add(expenseErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        var userId = Users.FirstOrDefault(u => u.Email == expenseFields.User).Id;
                        expenseToInsert.UserId = userId;
                        expenseToInsert.CreatedBy_Id = userId;
                    }
                }
                catch (Exception ex)
                {
                    expenseErrorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(expenseErrorFields);
                    errorFlag = true;
                }

                //Handles the Expense status by reading the status string from csv and setting the number according to the status enum on PP model.

                switch (expenseFields.Status)
                {
                    case "Billable":
                        expenseToInsert.Status = 1;
                        expenseToInsert.IsBillable = true;
                        expenseToInsert.TotalAmount = expenseToInsert.Amount * expenseToInsert.Qty;
                        break;
                    case "Billed":
                        expenseToInsert.Status = 2;
                        expenseToInsert.IsBillable = true;
                        expenseToInsert.IsBilled = true;
                        expenseToInsert.TotalAmount = expenseToInsert.Amount * expenseToInsert.Qty;
                        break;
                    case "NotBillable":
                        expenseToInsert.Status = 4;
                        expenseToInsert.IsBillable = false;
                        expenseToInsert.Amount = 0;
                        break;
                    default:
                        expenseErrorFields.Error = "Invalid status";
                        errorFlag = true;
                        break;
                }

                if (errorFlag == true)
                {
                    continue;
                }

                successFields.ExpenseGuid = expenseToInsert.Guid;

                uploadResults.SuccessRows.Add(successFields);
                expensesToAdd.Add(expenseToInsert);
                counter++;

                if (counter % rowsPerInsert == 0)
                {
                    if (expenseInserts.Count > 10)  // this prevents too many Tasks from running at once
                    {
                        var ongoingInserts = expenseInserts.GetRange(0, expenseInserts.Count - 10).ToArray();  // isolate the previous tasks
                        await Task.WhenAll(ongoingInserts);  // wait for all of the previous tasks to complete
                        expenseInserts.Add(InsertExpenseAsync(expensesToAdd));
                        expensesToAdd = new List<Expens>();
                        continue;
                    }

                    else
                    {
                        expenseInserts.Add(InsertExpenseAsync(expensesToAdd));
                        expensesToAdd = new List<Expens>();
                        continue;
                    }
                }
            }
            if (expensesToAdd.Count > 0)
            {
                expenseInserts.Add(InsertExpenseAsync(expensesToAdd));
                await Task.WhenAll(expenseInserts.ToArray());
                expensesToAdd = new List<Expens>();
            }

            var contextEndTime = DateTime.Now;
            var totalElapsedTime = ContextStartTime - contextEndTime;
            uploadResults.StartTime = ContextStartTime;
            uploadResults.EndTime = contextEndTime;
            uploadResults.Duration = totalElapsedTime;
            uploadResults.NumberOfTransactions = expenseInserts.Count * 3;
            return uploadResults;
        }

        public async Task<UploadResults> ImportFeesCsv(List<dynamic> csvRows, int rowsPerInsert)
        {
            UploadResults uploadResults = new UploadResults();
            uploadResults.OriginalRows = csvRows;
            var counter = 0;
            List<Task> feesInserts = new List<Task>();  // add task from InsertEventsAsync() to this list to await it before calling InsertEventsAsync again
            List<Models.FlatFee> feesToAdd = new List<Models.FlatFee>();

            foreach (var row in csvRows)
            {
                bool errorFlag = false;
                // for success/error files
                dynamic successFields = new ExpandoObject();
                var successDictionary = (IDictionary<string, object>)successFields;
                successDictionary.Add("FlatFeeGuid", null);  // update this field if a Time Entry is created
                foreach (var field in row)
                {
                    successDictionary.Add(field.Key, field.Value);
                }

                dynamic feeErrorFields = new ExpandoObject();
                var errorDictionary = (IDictionary<string, object>)feeErrorFields;
                errorDictionary.Add("Error", null);
                foreach (var field in row)
                {
                    errorDictionary.Add(field.Key, field.Value);
                }

                FlatFeeFields feeFields = null;
                var feesToInsert = new FlatFee()
                {
                    LastModifiedBy_Id = FirmTenant.PrimaryUserId,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    LastModifiedDate = DateTime.Now.ToUniversalTime(),
                    IsDeleted = false,
                    Guid = Guid.NewGuid(),
                    TenantId = FirmTenant.Id,
                };

                feeFields = new FlatFeeFields(row);

                if (!String.IsNullOrWhiteSpace(feeFields.Error))
                {
                    feeErrorFields.Error = feeFields.Error;
                    uploadResults.ErrorRows.Add(feeErrorFields);
                    errorFlag = true;
                }

                if (!(feeFields.MatterNumber is null))
                {
                    var projectNumberId = ProjectNumberIds.FirstOrDefault(a => a.Number == feeFields.MatterNumber);

                    if (projectNumberId is null)
                    {
                        feeErrorFields.Error = "No matter found with number " + feeFields.MatterNumber;
                        uploadResults.ErrorRows.Add(feeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        feesToInsert.ProjectId = projectNumberId.Id;
                        feesToInsert.AccountId = projectNumberId.AccountId;
                    }
                }

                try
                {
                    if (feeFields.Date.HasValue)
                    {
                        feesToInsert.Date = TimeZoneInfo.ConvertTimeToUtc(feeFields.Date.Value, TimeZoneInfo);
                    }
                    else
                    {
                        feeErrorFields.Error = "Date is required";
                        uploadResults.ErrorRows.Add(feeErrorFields);
                        errorFlag = true;
                    }
                }
                catch (Exception ex)
                {
                    feeErrorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(feeErrorFields);
                    errorFlag = true;
                }

                try
                {
                    if (feeFields.Qty <= 0)
                    {
                        feeErrorFields.Error = "Quantity must be greater than zero";
                        uploadResults.ErrorRows.Add(feeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        feesToInsert.Qty = feeFields.Qty;
                    }
                    if (feeFields.Price < 0)
                    {
                        feeErrorFields.Error = "Amount cannot be a negative number";
                        uploadResults.ErrorRows.Add(feeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        feesToInsert.Price = feeFields.Price;
                    }
                    if (!(String.IsNullOrWhiteSpace(feeFields.Description)))
                    {
                        feesToInsert.Description = feeFields.Description;
                    }
                    if (String.IsNullOrWhiteSpace(feeFields.User))
                    {
                        feeErrorFields.Error = "Must provide at least one user email address";
                        uploadResults.ErrorRows.Add(feeErrorFields);
                        errorFlag = true;
                    }
                    else if (!feeFields.User.Contains("@"))
                    {
                        feeErrorFields.Error = "Invalid email address.";
                        uploadResults.ErrorRows.Add(feeErrorFields);
                        errorFlag = true;
                    }
                    else
                    {
                        var userId = Users.FirstOrDefault(u => u.Email == feeFields.User).Id;
                        feesToInsert.UserId = userId;
                        feesToInsert.CreatedBy_Id = userId;
                    }
                }
                catch (Exception ex)
                {
                    feeErrorFields.Error = ex.InnerException;
                    uploadResults.ErrorRows.Add(feeErrorFields);
                    errorFlag = true;
                }

                switch (feeFields.Status)
                {
                    case "Billable":
                        feesToInsert.Status = 1;
                        feesToInsert.IsBillable = true;
                        feesToInsert.Total = feesToInsert.Price * feesToInsert.Qty;
                        break;
                    case "Billed":
                        feesToInsert.Status = 2;
                        feesToInsert.IsBillable = true;
                        feesToInsert.IsBilled = true;
                        feesToInsert.Total = feesToInsert.Price * feesToInsert.Qty;
                        break;
                    case "NotBillable":
                        feesToInsert.Status = 4;
                        feesToInsert.IsBillable = false;
                        feesToInsert.Price = 0;
                        break;
                    default:
                        feeErrorFields.Error = "Invalid status";
                        errorFlag = true;
                        break;
                }


                if (!String.IsNullOrWhiteSpace(feeFields.ItemCode))
                {
                    try
                    {
                        var codes = GetUtbmsCodes();
                        feesToInsert.Name = codes.FirstOrDefault(c => c.Code == feeFields.ItemCode).Name;
                    }
                    catch (Exception ex)
                    {
                        //TODO add method to create custom products.
                        feeErrorFields.Error = ex.InnerException;
                        uploadResults.ErrorRows.Add(feeErrorFields);
                        errorFlag = true;
                    }
                }
                else
                {
                    feesToInsert.Name = "Fee";
                }

                if (errorFlag == true)
                {
                    continue;
                }

                successFields.ExpenseGuid = feesToInsert.Guid;

                uploadResults.SuccessRows.Add(successFields);
                feesToAdd.Add(feesToInsert);
                counter++;

                if (counter % rowsPerInsert == 0)
                {
                    if (feesInserts.Count > 10)  // this prevents too many Tasks from running at once
                    {
                        var ongoingInserts = feesInserts.GetRange(0, feesInserts.Count - 10).ToArray();  // isolate the previous tasks
                        await Task.WhenAll(ongoingInserts);  // wait for all of the previous tasks to complete
                        feesInserts.Add(InsertFlatFeeAsync(feesToAdd));
                        feesToAdd = new List<FlatFee>();
                        continue;
                    }

                    else
                    {
                        feesInserts.Add(InsertFlatFeeAsync(feesToAdd));
                        feesToAdd = new List<FlatFee>();
                        continue;
                    }
                }
            }

            if (feesToAdd.Count > 0)
            {
                feesInserts.Add(InsertFlatFeeAsync(feesToAdd));
                await Task.WhenAll(feesInserts.ToArray());
                feesToAdd = new List<FlatFee>();
            }

            var contextEndTime = DateTime.Now;
            var totalElapsedTime = ContextStartTime - contextEndTime;
            uploadResults.StartTime = ContextStartTime;
            uploadResults.EndTime = contextEndTime;
            uploadResults.Duration = totalElapsedTime;
            uploadResults.NumberOfTransactions = feesInserts.Count * 3;
            return uploadResults;

        }

        /// <summary>
        /// Asynchronously inserts Activities to the database,
        /// and then queries the results based on the inserted Guids.
        /// Results are then used to add ActivityId to the activityGuidUserTags,
        /// and the assigned users and tags are updated with InsertAssignedUsersAndTagsAsync
        /// </summary>
        /// <param name="activitiesToAdd">List of activities to bulk insert </param>
        /// <param name="activityGuidUserTags"> List of activityGuidUserTags which 
        ///                                     correspond to activitiesToAdd</param>
        /// <returns></returns>
        async Task InsertActivitiesAsync(List<Activity> activitiesToAdd, List<ActivityGuidUserTag> activityGuidUserTags)
        {
            var sbCopyActivities = new SqlBulkCopy(ConnectionString);
            sbCopyActivities.DestinationTableName = "[dbo].[Activities]";
            var eventDataTable = ConvertListToDataTable(activitiesToAdd);
            Console.WriteLine("\nSaving changes for " + activitiesToAdd.Count.ToString() + " rows");
            await HelperFunctions.AutoMapColumnsAndBulkInsertAsync(sbCopyActivities, eventDataTable);
            Console.WriteLine("Events Inserted!");
            string queryString = "SELECT [Guid], [Id] FROM Activities";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                var queryWhereClause = new StringBuilder();
                queryWhereClause.Append(" WHERE TenantId = ");
                queryWhereClause.Append(Convert.ToString(FirmTenant.Id));
                queryWhereClause.Append(" AND [Guid] IN (");
                var guidCounter = 1;
                foreach (var activityGuidUserTag in activityGuidUserTags)
                {
                    queryWhereClause.Append("@Guid" + guidCounter.ToString() + ",");
                    command.Parameters.AddWithValue("@Guid" + guidCounter.ToString(), activityGuidUserTag.ActivityGuid);
                    guidCounter++;
                }
                queryWhereClause[queryWhereClause.Length - 1] = ')'; // replace the trailing , with ) to close the list of Guids in the query
                command.CommandText += queryWhereClause;

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    var tempcounter = 0;
                    while (reader.Read())
                    {
                        var temp = reader[0].ToString();
                        var activityGuidUserTag = activityGuidUserTags.FirstOrDefault(agt => agt.ActivityGuid == Guid.Parse(reader[0].ToString()));
                        if (activityGuidUserTag is null)
                        {
                            continue;
                        }
                        activityGuidUserTag.ActivityId = long.Parse(reader[1].ToString());
                        tempcounter++;
                    }
                }
                connection.Close();
            }
            await InsertAssignedUsersAndTagsAsync(activityGuidUserTags);
        }

        async Task InsertTimeAsync(List<TimeEntry> timeEntriesToAdd)
        {
            var sbCopyBilling = new SqlBulkCopy(ConnectionString);
            sbCopyBilling.DestinationTableName = "[dbo].[TimeEntries]";
            var timeDataTable = ConvertListToDataTable(timeEntriesToAdd);
            Console.WriteLine("\nSaving changes for " + timeEntriesToAdd.Count.ToString() + " rows");
            await HelperFunctions.AutoMapColumnsAndBulkInsertAsync(sbCopyBilling, timeDataTable);
            Console.WriteLine("Time Entries Inserted!");
            string queryString = "SELECT [Guid], [Id] FROM TimeEntries";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                var queryWhereClause = new StringBuilder();
                queryWhereClause.Append(" WHERE TenantId = ");
                queryWhereClause.Append(Convert.ToString(FirmTenant.Id));
                queryWhereClause.Append(" AND [Guid] IN (");
                
                queryWhereClause[queryWhereClause.Length - 1] = ')'; // replace the trailing , with ) to close the list of Guids in the query
                command.CommandText += queryWhereClause;
            }
        }

        async Task InsertExpenseAsync(List<Expens> expensesToAdd)
        {
            var sbCopyBilling = new SqlBulkCopy(ConnectionString);
            sbCopyBilling.DestinationTableName = "[dbo].[Expenses]";
            var expenseDataTable = ConvertListToDataTable(expensesToAdd);
            Console.WriteLine("\nSaving changes for " + expensesToAdd.Count.ToString() + " rows");
            await HelperFunctions.AutoMapColumnsAndBulkInsertAsync(sbCopyBilling, expenseDataTable);
            Console.WriteLine("Time Entries Inserted!");
            string queryString = "SELECT [Guid], [Id] FROM Expenses";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                var queryWhereClause = new StringBuilder();
                queryWhereClause.Append(" WHERE TenantId = ");
                queryWhereClause.Append(Convert.ToString(FirmTenant.Id));
                queryWhereClause.Append(" AND [Guid] IN (");

                queryWhereClause[queryWhereClause.Length - 1] = ')'; // replace the trailing , with ) to close the list of Guids in the query
                command.CommandText += queryWhereClause;
            }
        }

        async Task InsertFlatFeeAsync(List<FlatFee> feesToAdd)
        {
            var sbCopyBilling = new SqlBulkCopy(ConnectionString);
            sbCopyBilling.DestinationTableName = "[dbo].[FlatFees]";
            var feesDataTable = ConvertListToDataTable(feesToAdd);
            Console.WriteLine("\nSaving changes for " + feesToAdd.Count.ToString() + " rows");
            await HelperFunctions.AutoMapColumnsAndBulkInsertAsync(sbCopyBilling, feesDataTable);
            Console.WriteLine("Flat Fees Inserted!");
            string queryString = "SELECT [Guid], [Id] FROM FlatFees";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                var queryWhereClause = new StringBuilder();
                queryWhereClause.Append(" WHERE TenantId = ");
                queryWhereClause.Append(Convert.ToString(FirmTenant.Id));
                queryWhereClause.Append(" AND [Guid] IN (");

                queryWhereClause[queryWhereClause.Length - 1] = ')'; // replace the trailing , with ) to close the list of Guids in the query
                command.CommandText += queryWhereClause;
            }
        }

        async Task InsertRelationshipsAsync(List<AccountProjectLink> relsToAdd)
        {
            var sbCopyRelationships = new SqlBulkCopy(ConnectionString);
            sbCopyRelationships.DestinationTableName = "[dbo].[AccountProjectLinks]";
            var feesDataTable = ConvertListToDataTable(relsToAdd);
            Console.WriteLine("\nSaving changes for " + relsToAdd.Count.ToString() + " rows");
            await HelperFunctions.AutoMapColumnsAndBulkInsertAsync(sbCopyRelationships, feesDataTable);
            Console.WriteLine("Relationships Inserted!");
            //string queryString = "SELECT [Guid], [Id] FROM AccountProjectLinks";
            /*using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(queryString, connection);
                var queryWhereClause = new StringBuilder();
                queryWhereClause.Append(" WHERE TenantId = ");
                queryWhereClause.Append(Convert.ToString(FirmTenant.Id));
                queryWhereClause.Append(" AND [Guid] IN (");
                queryWhereClause[queryWhereClause.Length - 1] = ')'; // replace the trailing , with ) to close the list of Guids in the query
                command.CommandText += queryWhereClause;
            }*/
        }

        /// <summary>
        /// Inserts rows into ActivityUsers and TagActivities tables. Looks up the Id for the activity by Guid and
        /// then updates a DataTable with the same schema as the tables in the database. Once the DataTables are
        /// prepared, they are asynchronously bulk inserted to the database
        /// </summary>
        /// <param name="activityGuidUserTags">List of ActivityGuidUserTags, one per inserted activity</param>
        /// <returns></returns>
        async Task InsertAssignedUsersAndTagsAsync(List<ActivityGuidUserTag> activityGuidUserTags)
        {
            var insertTaskList = new List<Task>();

            var sbCopyActivityUsers = new SqlBulkCopy(ConnectionString);
            sbCopyActivityUsers.DestinationTableName = "[dbo].[ActivityUsers]";
            var activityUserDataTable = HelperFunctions.GetDataTable("ActivityUsers");

            var sbCopyTagActivities = new SqlBulkCopy(ConnectionString);
            sbCopyTagActivities.DestinationTableName = "[dbo].[TagActivities]";
            var activityTagDataTable = HelperFunctions.GetDataTable("TagActivities");

            foreach (var activityGuidUserTag in activityGuidUserTags.Where(agt => agt.ActivityId != null)) // go through each activity
            {
                // go through each assigned user and add it to the DataTable
                foreach (var userId in activityGuidUserTag.UserIds.Distinct())
                {
                    var dataRow = activityUserDataTable.NewRow();
                    dataRow["User_Id"] = userId;
                    dataRow["Activity_Id"] = activityGuidUserTag.ActivityId;
                    activityUserDataTable.Rows.Add(dataRow);
                }

                // go through each tag and add it to the DataTable
                foreach (var tagId in activityGuidUserTag.TagIds.Distinct())
                {
                    var dataRow = activityTagDataTable.NewRow();
                    dataRow["Tag_Id"] = tagId;
                    dataRow["Activity_Id"] = activityGuidUserTag.ActivityId;
                    activityTagDataTable.Rows.Add(dataRow);
                }
            }

            var insertUsersTask = HelperFunctions.AutoMapColumnsAndBulkInsertAsync(sbCopyActivityUsers, activityUserDataTable);
            var insertTagsTask = HelperFunctions.AutoMapColumnsAndBulkInsertAsync(sbCopyTagActivities, activityTagDataTable);
            //insertUsersTask.Start();
            //insertTagsTask.Start();
            insertTaskList.Add(insertUsersTask);
            insertTaskList.Add(insertTagsTask);
            await Task.WhenAll(insertTaskList.ToArray());
        }
    }
}