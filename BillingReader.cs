﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using CsvParser.Models;
using System.Data;

namespace CsvParser
{
    public class BillingReader
    {
        public BillingReader(string userEmail, string fileName, string firmDirectory)
        {
            FirmDirectory = firmDirectory;
            DbFunctions = new DatabaseFunctions(userEmail);
            FileName = fileName;

        }

        public static string FirmDirectory { get; set; }
        public static string FileName { get; set; }
        static DatabaseFunctions DbFunctions { get; set; }

        public async Task<int> UploadBillingCsv(string filepath, int rowsPerInsert, ImportType importType)
        {
            using (var reader = new StreamReader(filepath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Read();
                csv.ReadHeader();
                List<string> headers = csv.Context.HeaderRecord.ToList();
                var rows = csv.GetRecords<dynamic>();
                UploadResults uploadResults = null;
                switch (importType)
                {
                    case ImportType.TimeEntries:
                        List<TimeEntry> timeEntriesToInsert = new List<TimeEntry>();
                        HelperFunctions.ValidateHeaders(headers, importType);
                        uploadResults = await DbFunctions.ImportTimeCsv(rows.ToList(), rowsPerInsert);
                        break;
                    case ImportType.Expenses:
                        List<Expens> expensesToInsert = new List<Expens>();
                        HelperFunctions.ValidateHeaders(headers, importType);
                        uploadResults = await DbFunctions.ImportExpenseCsv(rows.ToList(), rowsPerInsert);
                        break;
                    case ImportType.FlatFee:
                        List<Models.FlatFee> feesToInsert = new List<Models.FlatFee>();
                        HelperFunctions.ValidateHeaders(headers, importType);
                        uploadResults = await DbFunctions.ImportFeesCsv(rows.ToList(), rowsPerInsert);
                        break;
                }

                var successPath = FileName.Replace(".csv", "") + " - Success File.csv";
                using (var successWriter = new StreamWriter(successPath))
                using (var successCsv = new CsvWriter(successWriter, CultureInfo.InvariantCulture))
                {
                    successCsv.WriteRecords(uploadResults.SuccessRows);
                }

                var errorPath = FileName.Replace(".csv", "") + " - Error File.csv";
                using (var errorWriter = new StreamWriter(errorPath))
                using (var errorCsv = new CsvWriter(errorWriter, CultureInfo.InvariantCulture))
                {
                    errorCsv.WriteRecords(uploadResults.ErrorRows);
                }
                var sqlLogOutputPath = Path.Combine(FirmDirectory, "Queries.txt");
                File.WriteAllLines(sqlLogOutputPath, DbFunctions.SqlLogFile);
                DbFunctions.CloseConnection();

                var uploadSummary = uploadResults.GetUploadSummary(rowsPerInsert);
                var splitDateTime = uploadResults.StartTime.ToString().Split(' ');
                var uploadSummaryFileName = "Upload Summary " + splitDateTime[0] + " " + splitDateTime[1] + ".txt";
                var uploadSummaryPath = Path.Combine(FirmDirectory, uploadSummaryFileName.Replace("/", "-").Replace(":", ""));
                File.WriteAllLines(uploadSummaryPath, uploadSummary);

                return uploadResults.SuccessRows.Count;

            }
        }

    }
}
