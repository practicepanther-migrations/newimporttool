﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvParser
{

    /// <summary>
    /// Store a collection of these to keep track of Account Numbers in PracticePanther
    /// and their respective Id.
    public class AccountNumberId
    {
        public int Number { get; set; }
        public long Id { get; set; }
        public long PrimaryContactId { get; set; }
    }
}
