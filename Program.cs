﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using CsvParser.Models;
using AutoMapper;
using System.IO;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace CsvParser
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            //Opens the file explorer to select the file you want to upload.
            var directory = string.Empty;
            var fileName = string.Empty;

            int importSelection;
            //var firmName = "Prya Murad";
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Title = "Select file to upload";
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "CSV files (*.csv)|*.csv";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;
                if(openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            using (CommonOpenFileDialog dialog = new CommonOpenFileDialog())
            {
                dialog.Title = "Select folder to save queries";
                dialog.InitialDirectory = "c:\\";
                dialog.IsFolderPicker = true;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    directory = dialog.FileName;
                    Console.WriteLine(directory);
                }
            }

            var tenantEmail = "atiffer+devpv@practicepanther.com";

            Console.WriteLine("Select the import type: 1 contacts and matters\n 2 events, 3 tasks, 4 notes, 5 time entries, 6 expenses, 7 flat fees, 8 relationships, 9 exit");
            importSelection = int.Parse(Console.ReadLine());

            switch (importSelection)
            {
                case 1:
                    var test = new ContactMatterUploader(tenantEmail, directory, fileName);
                    break;
                case 2:
                    var eventReader = new ActivityReader(tenantEmail, fileName, directory);
                    eventReader.UploadActivityCsv(fileName, 1000, ImportType.Events).Wait();
                    break;
                case 3:
                    var taskReader = new ActivityReader(tenantEmail, fileName, directory);
                    taskReader.UploadActivityCsv(fileName, 1000, ImportType.Tasks).Wait();
                    break;
                case 4:
                    var noteReader = new ActivityReader(tenantEmail, fileName, directory);
                    noteReader.UploadActivityCsv(fileName, 1000, ImportType.Notes).Wait();
                    break;
                case 5:
                    var timeReader = new BillingReader(tenantEmail, fileName, directory);
                    timeReader.UploadBillingCsv(fileName, 1000, ImportType.TimeEntries).Wait();
                    break;
                case 6:
                    var expenseReader = new BillingReader(tenantEmail, fileName, directory);
                    expenseReader.UploadBillingCsv(fileName, 1000, ImportType.Expenses).Wait();
                    break;
                case 7:
                    var feeReader = new BillingReader(tenantEmail, fileName, directory);
                    feeReader.UploadBillingCsv(fileName, 1000, ImportType.FlatFee).Wait();
                    break;
                case 8:
                    var relReader = new ActivityReader(tenantEmail, fileName, directory);
                    relReader.UploadActivityCsv(fileName, 1000, ImportType.Relationships).Wait();
                    break;
                case 9:
                    return;
            }

            //var eventFileName = "test events auto-random.csv";
            //var taskFileName = "test tasks.csv";
            // var noteFileName = "notes-matters-strategy.csv";
            //var timeFileName = "test time entries.csv";
            //var expenseFileName = "test expenses auto-random.csv";
            //var feeFileName = "test.csv";

            //var eventReader = new ActivityReader(tenantEmail, firmName, eventFileName);
            //var taskReader = new ActivityReader(tenantEmail, firmName, taskFileName);
            //var noteReader = new ActivityReader(tenantEmail, firmName, noteFileName);
            //var timeReader = new BillingReader(tenantEmail, firmName, timeFileName, firmDirectory);
            //var expenseReader = new BillingReader(tenantEmail, firmName, expenseFileName, firmDirectory);
            //var feeReader = new BillingReader(tenantEmail, firmName, feeFileName, firmDirectory);

            //var eventReadPath = Path.Combine(firmDirectory, eventFileName);
            //var taskReadPath = Path.Combine(firmDirectory, taskFileName);
            //var noteReadPath = Path.Combine(firmDirectory, noteFileName);
            //var timeReaderPath = Path.Combine(firmDirectory, timeFileName);
            //var expenseReaderPath = Path.Combine(firmDirectory, expenseFileName);
            //var feeReaderPath = Path.Combine(firmDirectory, feeFileName);

            //eventReader.UploadActivityCsv(eventReadPath, 1000, ImportType.Events).Wait();
            //taskReader.UploadActivityCsv(taskReadPath, 1000, ImportType.Tasks).Wait();
            //noteReader.UploadActivityCsv(noteReadPath, 1000, ImportType.Notes).Wait();
            //timeReader.UploadBillingCsv(timeReaderPath, 1000, ImportType.TimeEntries).Wait();
            //expenseReader.UploadBillingCsv(expenseReaderPath, 1000, ImportType.Expenses).Wait();
            //feeReader.UploadBillingCsv(feeReaderPath, 1000, ImportType.FlatFee).Wait();

            Console.WriteLine("Upload Complete!");
            Console.ReadKey();
        }
    }
}