namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class IntakeFormEntry
    {
        [Key]
        public Guid Guid { get; set; }

        public string Name { get; set; }

        public string Label { get; set; }

        public string Value { get; set; }

        public int Type { get; set; }

        public Guid? IntakeForm_Guid { get; set; }

        public int RowIndex { get; set; }

        public virtual IntakeForm IntakeForm { get; set; }
    }
}
