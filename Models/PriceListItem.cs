namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PriceListItem
    {
        public long Id { get; set; }

        public long PriceList_Id { get; set; }

        public decimal CustomPrice { get; set; }

        public long? Product_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public virtual PriceList PriceList { get; set; }

        public virtual Product Product { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
