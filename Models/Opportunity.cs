namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Opportunity
    {
        public long Id { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string Name { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public long? Account_Id { get; set; }

        public int Type { get; set; }

        public decimal? Amount { get; set; }

        public DateTime ClosingDate { get; set; }

        public decimal Probability { get; set; }

        public long? Campaign_Id { get; set; }

        public int Stage { get; set; }

        public Guid Guid { get; set; }

        public virtual Account Account { get; set; }

        public virtual Campaign Campaign { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
