namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            AccountingAccounts = new HashSet<AccountingAccount>();
            AccountingAccounts1 = new HashSet<AccountingAccount>();
            AccountProjectLinks = new HashSet<AccountProjectLink>();
            AccountProjectLinks1 = new HashSet<AccountProjectLink>();
            Accounts = new HashSet<Account>();
            Accounts1 = new HashSet<Account>();
            Activities = new HashSet<Activity>();
            Activities1 = new HashSet<Activity>();
            ActivityReminders = new HashSet<ActivityReminder>();
            ActivitySyncs = new HashSet<ActivitySync>();
            Addresses = new HashSet<Address>();
            Addresses1 = new HashSet<Address>();
            AspNetUsers = new HashSet<AspNetUser>();
            Attachments = new HashSet<Attachment>();
            Attachments1 = new HashSet<Attachment>();
            BankAccounts = new HashSet<BankAccount>();
            BankAccounts1 = new HashSet<BankAccount>();
            Blobs = new HashSet<Blob>();
            Blobs1 = new HashSet<Blob>();
            Blobs2 = new HashSet<Blob>();
            CalendarColors = new HashSet<CalendarColor>();
            CalendarColors1 = new HashSet<CalendarColor>();
            Campaigns = new HashSet<Campaign>();
            Campaigns1 = new HashSet<Campaign>();
            Campaigns2 = new HashSet<Campaign>();
            Categories = new HashSet<Category>();
            Categories1 = new HashSet<Category>();
            Contacts = new HashSet<Contact>();
            Contacts1 = new HashSet<Contact>();
            Contacts2 = new HashSet<Contact>();
            ContactSyncs = new HashSet<ContactSync>();
            ConversationMessages = new HashSet<ConversationMessage>();
            ConversationMessages1 = new HashSet<ConversationMessage>();
            ConversationParticipants = new HashSet<ConversationParticipant>();
            Conversations = new HashSet<Conversation>();
            Conversations1 = new HashSet<Conversation>();
            CustomFields = new HashSet<CustomField>();
            CustomFields1 = new HashSet<CustomField>();
            CustomFieldSets = new HashSet<CustomFieldSet>();
            CustomFieldSets1 = new HashSet<CustomFieldSet>();
            CustomFieldTabs = new HashSet<CustomFieldTab>();
            CustomFieldTabs1 = new HashSet<CustomFieldTab>();
            DefaultReminders = new HashSet<DefaultReminder>();
            DefaultReminders1 = new HashSet<DefaultReminder>();
            DocumentTemplates = new HashSet<DocumentTemplate>();
            DocumentTemplates1 = new HashSet<DocumentTemplate>();
            Expenses = new HashSet<Expens>();
            Expenses1 = new HashSet<Expens>();
            Expenses2 = new HashSet<Expens>();
            FeedNotifications = new HashSet<FeedNotification>();
            Feeds = new HashSet<Feed>();
            Feeds1 = new HashSet<Feed>();
            Feeds2 = new HashSet<Feed>();
            FlatFees = new HashSet<FlatFee>();
            FlatFees1 = new HashSet<FlatFee>();
            FlatFees2 = new HashSet<FlatFee>();
            IntakeForms = new HashSet<IntakeForm>();
            IntakeForms1 = new HashSet<IntakeForm>();
            IntakeFormTemplates = new HashSet<IntakeFormTemplate>();
            IntakeFormTemplates1 = new HashSet<IntakeFormTemplate>();
            IntakeFormTemplates2 = new HashSet<IntakeFormTemplate>();
            IntakeFormTemplates3 = new HashSet<IntakeFormTemplate>();
            IntakeFormTemplates4 = new HashSet<IntakeFormTemplate>();
            Inventories = new HashSet<Inventory>();
            Inventories1 = new HashSet<Inventory>();
            InventoryTransfers = new HashSet<InventoryTransfer>();
            InventoryTransfers1 = new HashSet<InventoryTransfer>();
            InventoryTransfers2 = new HashSet<InventoryTransfer>();
            InventoryTransfers3 = new HashSet<InventoryTransfer>();
            InventoryTransfers4 = new HashSet<InventoryTransfer>();
            Invitees = new HashSet<Invitee>();
            LawToolboxAccounts = new HashSet<LawToolboxAccount>();
            LawToolboxCourtRules = new HashSet<LawToolboxCourtRule>();
            LawToolboxCourtRules1 = new HashSet<LawToolboxCourtRule>();
            MerchantApplications = new HashSet<MerchantApplication>();
            OAuthClients = new HashSet<OAuthClient>();
            Opportunities = new HashSet<Opportunity>();
            Opportunities1 = new HashSet<Opportunity>();
            Partnerships = new HashSet<Partnership>();
            Partnerships1 = new HashSet<Partnership>();
            PaymentReminders = new HashSet<PaymentReminder>();
            PaymentReminders1 = new HashSet<PaymentReminder>();
            Payments = new HashSet<Payment>();
            Payments1 = new HashSet<Payment>();
            Payments2 = new HashSet<Payment>();
            PaymentSources = new HashSet<PaymentSource>();
            PendingOnlinePayments = new HashSet<PendingOnlinePayment>();
            Portals = new HashSet<Portal>();
            PresetProjectRates = new HashSet<PresetProjectRate>();
            PresetProjectRates1 = new HashSet<PresetProjectRate>();
            PriceLists = new HashSet<PriceList>();
            PriceLists1 = new HashSet<PriceList>();
            ProductLogs = new HashSet<ProductLog>();
            Products = new HashSet<Product>();
            Products1 = new HashSet<Product>();
            ProductVendors = new HashSet<ProductVendor>();
            ProductVendors1 = new HashSet<ProductVendor>();
            ProjectRates = new HashSet<ProjectRate>();
            Projects = new HashSet<Project>();
            Projects1 = new HashSet<Project>();
            Projects2 = new HashSet<Project>();
            RecurringPayments = new HashSet<RecurringPayment>();
            RecurringPayments1 = new HashSet<RecurringPayment>();
            RecurringPayments2 = new HashSet<RecurringPayment>();
            Roles = new HashSet<Role>();
            Roles1 = new HashSet<Role>();
            SaleDocumentItems = new HashSet<SaleDocumentItem>();
            SaleDocumentItems1 = new HashSet<SaleDocumentItem>();
            SaleDocuments = new HashSet<SaleDocument>();
            SaleDocuments1 = new HashSet<SaleDocument>();
            SaleDocuments2 = new HashSet<SaleDocument>();
            SaleDocumentTemplates = new HashSet<SaleDocumentTemplate>();
            SaleDocumentTemplates1 = new HashSet<SaleDocumentTemplate>();
            SalesTaxes = new HashSet<SalesTax>();
            SalesTaxes1 = new HashSet<SalesTax>();
            Shares = new HashSet<Share>();
            Shipments = new HashSet<Shipment>();
            Shipments1 = new HashSet<Shipment>();
            SyncLogs = new HashSet<SyncLog>();
            Tags = new HashSet<Tag>();
            Tags1 = new HashSet<Tag>();
            Tenants = new HashSet<Tenant>();
            Tenants1 = new HashSet<Tenant>();
            Tenants2 = new HashSet<Tenant>();
            TimeEntries = new HashSet<TimeEntry>();
            TimeEntries1 = new HashSet<TimeEntry>();
            TimeEntries2 = new HashSet<TimeEntry>();
            Timers = new HashSet<Timer>();
            UserGroups = new HashSet<UserGroup>();
            UserGroups1 = new HashSet<UserGroup>();
            UserImapSyncs = new HashSet<UserImapSync>();
            Users1 = new HashSet<User>();
            Users11 = new HashSet<User>();
            Workflows = new HashSet<Workflow>();
            Workflows1 = new HashSet<Workflow>();
            Accounts2 = new HashSet<Account>();
            Accounts3 = new HashSet<Account>();
            ActivityUsers = new HashSet<Activity>();
            Locations = new HashSet<Location>();
            Projects3 = new HashSet<Project>();
            Tenants3 = new HashSet<Tenant>();
            UserGroups2 = new HashSet<UserGroup>();
            Users12 = new HashSet<User>();
            Users = new HashSet<User>();
            WorkflowEvents = new HashSet<WorkflowEvent>();
            WorkflowTasks = new HashSet<WorkflowTask>();
            webpages_Roles = new HashSet<webpages_Roles>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public decimal HourlyRate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public long? Photo_Id { get; set; }

        public long? PhotoThumbnail_Id { get; set; }

        public long? RoleId { get; set; }

        [StringLength(50)]
        public string TimeZoneId { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [StringLength(50)]
        public string Home { get; set; }

        [StringLength(50)]
        public string Office { get; set; }

        [StringLength(10)]
        public string Ext { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(10)]
        public string Salutation { get; set; }

        [StringLength(50)]
        public string Position { get; set; }

        [StringLength(50)]
        public string SkypeId { get; set; }

        [StringLength(50)]
        public string Twitter { get; set; }

        public int Gender { get; set; }

        public DateTime? Birthday { get; set; }

        public long? Address_Id { get; set; }

        public bool IsEnabled { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public Guid Guid { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public string Notes { get; set; }

        public bool IsHideWelcomePage { get; set; }

        [StringLength(500)]
        public string GoogleUsername { get; set; }

        [StringLength(2000)]
        public string GoogleRefreshToken { get; set; }

        public bool IsEmailActivityAssigneeByDefault { get; set; }

        public Guid? ICalGuid { get; set; }

        public Guid? EmailGuid { get; set; }

        public bool IsReceiveDailyAgendaEmail { get; set; }

        public bool IsReceiveWeeklySummaryEmail { get; set; }

        public bool IsReceiveActivityEmails { get; set; }

        [StringLength(255)]
        public string MixpanelDistinctId { get; set; }

        public bool PopUpSettings_IsHideWelcomePopup { get; set; }

        public bool PopUpSettings_IsHideClientPopup { get; set; }

        public bool PopUpSettings_IsHideProjectPopup { get; set; }

        public bool PopUpSettings_IsHideTimeEntryExpensePopup { get; set; }

        public bool PopUpSettings_IsHideInvoicePopup { get; set; }

        public bool PopUpSettings_ISHideEmailClientPopup { get; set; }

        public bool PopUpSettings_IsHideTaskPopup { get; set; }

        public bool ImapServer_IsEnabled { get; set; }

        public string ImapServer_ServerAddress { get; set; }

        public int ImapServer_ServerPort { get; set; }

        public int ImapServer_EncryptionType { get; set; }

        public string ImapServer_Username { get; set; }

        public string ImapServer_password { get; set; }

        public bool ImapServer_IsRequiresAuthentication { get; set; }

        public bool SmtpServer_IsEnabled { get; set; }

        public string SmtpServer_ServerAddress { get; set; }

        public int SmtpServer_ServerPort { get; set; }

        public int SmtpServer_EncryptionType { get; set; }

        public string SmtpServer_Username { get; set; }

        public string SmtpServer_password { get; set; }

        public bool SmtpServer_IsRequiresAuthentication { get; set; }

        public int ImapServer_Provider { get; set; }

        public bool ImapServer_IsPasswordEncrypted { get; set; }

        public int SmtpServer_Provider { get; set; }

        public bool SmtpServer_IsPasswordEncrypted { get; set; }

        [StringLength(2000)]
        public string MicrosoftLiveConnectAuthToken { get; set; }

        public bool IsMicrosoftLiveConnectEnabled { get; set; }

        public bool IsExchangeIntegrationEnabled { get; set; }

        [StringLength(255)]
        public string ExchangeUsername { get; set; }

        [StringLength(255)]
        public string ExchangePassword { get; set; }

        public bool IsGoogleSyncEnabled { get; set; }

        public DateTime? LastGoogleSyncDate { get; set; }

        public DateTime? LastMicrosoftLiveSyncDate { get; set; }

        public DateTime? LastExchangeSyncDate { get; set; }

        public long? GoToWebinarWebinarKey { get; set; }

        public long? GoToWebinarRegistrantKey { get; set; }

        public bool IsEmailUserTaskCompletedByDefault { get; set; }

        public bool IsPasswordSet { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required]
        public string FullName { get; set; }

        public int ChatStatus { get; set; }

        public bool IsReceiveNotificationPopups { get; set; }

        public DateTime? SupportAccessExpiration { get; set; }

        public string SupportEncryptedAccessCode { get; set; }

        public bool TimeEntries_IsRoundingEnabled { get; set; }

        public int TimeEntries_RoundDirection { get; set; }

        public decimal TimeEntries_RoundToNearest { get; set; }

        public string DisplayName { get; set; }

        public string MiddleName { get; set; }

        public string DirectMailAdditionalEmailAddresses { get; set; }

        public bool NotificationsSettings_IsSendSaleDocumentViewedNotification { get; set; }

        public bool NotificationsSettings_IsSendSaleDocumentCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendSaleDocumentSentNotification { get; set; }

        public bool NotificationsSettings_IsSendPaymentRequestSentNotification { get; set; }

        public bool NotificationsSettings_IsSendAccountCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendFeedCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendProjectCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendExpenseCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendTimeEntryCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendPaymentCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendOnlinePaymentReceivedNotification { get; set; }

        public bool NotificationsSettings_IsSendIntakeFilledNotification { get; set; }

        public bool NotificationsSettings_IsSendActivityCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendTaskStatusChangedNotification { get; set; }

        public bool NotificationsSettings_IsSendPortalInvitationAcceptedNotification { get; set; }

        public bool NotificationsSettings_IsSendActivityReminderNotification { get; set; }

        public bool SyncSettings_IsSyncTasksInbound { get; set; }

        public bool SyncSettings_IsSyncTasksOutbound { get; set; }

        [StringLength(255)]
        public string SyncSettings_TaskListName { get; set; }

        public bool SyncSettings_IsSyncCalendarInbound { get; set; }

        public bool SyncSettings_IsSyncCalendarOutbound { get; set; }

        [StringLength(255)]
        public string SyncSettings_CalendarName { get; set; }

        public bool SyncSettings_IsSyncContactsOutbound { get; set; }

        public bool SyncSettings_IsSyncContactsMailSyncAddress { get; set; }

        public bool SyncSettings_IsSyncProjectsMailSyncAddress { get; set; }

        [Required]
        [StringLength(255)]
        public string SyncSettings_ContactsGroupName { get; set; }

        public bool SyncSettings_IsSyncEmailsInbound { get; set; }

        public string SyncSettings_EmailFolderName { get; set; }

        [StringLength(255)]
        public string ExchangeEmailAddress { get; set; }

        public bool IsSupportRecordingEnabled { get; set; }

        public DateTime? SupportRecordingExpiration { get; set; }

        [StringLength(1500)]
        public string ExchangeURL { get; set; }

        public decimal HourlyCost { get; set; }

        public bool NotificationsSettings_IsSendFlatFeeCreatedNotification { get; set; }

        public bool NotificationsSettings_IsSendInvoiceSubmittedForApprovalNotification { get; set; }

        public bool NotificationsSettings_IsSendInvoiceApprovedNotification { get; set; }

        [StringLength(20)]
        public string LedesTimekeeperId { get; set; }

        public DateTime? LastDateOfPaymentPlanMigrationSnooze { get; set; }

        public DateTime? LastDateUpdateTenantInfoPopUpSnooze { get; set; }

        public DateTime? LastDatePantherPaymentMigrationPopUpSnooze { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountingAccount> AccountingAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountingAccount> AccountingAccounts1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountProjectLink> AccountProjectLinks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountProjectLink> AccountProjectLinks1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActivityReminder> ActivityReminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActivitySync> ActivitySyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses1 { get; set; }

        public virtual Address Address { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attachment> Attachments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attachment> Attachments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BankAccount> BankAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BankAccount> BankAccounts1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs2 { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Blob Blob1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CalendarColor> CalendarColors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CalendarColor> CalendarColors1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Campaign> Campaigns { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Campaign> Campaigns1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Campaign> Campaigns2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Category> Categories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Category> Categories1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contact> Contacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contact> Contacts1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contact> Contacts2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContactSync> ContactSyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversationMessage> ConversationMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversationMessage> ConversationMessages1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversationParticipant> ConversationParticipants { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversation> Conversations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversation> Conversations1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomField> CustomFields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomField> CustomFields1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldSet> CustomFieldSets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldSet> CustomFieldSets1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldTab> CustomFieldTabs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldTab> CustomFieldTabs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DefaultReminder> DefaultReminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DefaultReminder> DefaultReminders1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DocumentTemplate> DocumentTemplates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DocumentTemplate> DocumentTemplates1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedNotification> FeedNotifications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeForm> IntakeForms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeForm> IntakeForms1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormTemplate> IntakeFormTemplates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormTemplate> IntakeFormTemplates1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormTemplate> IntakeFormTemplates2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormTemplate> IntakeFormTemplates3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormTemplate> IntakeFormTemplates4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inventory> Inventories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inventory> Inventories1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransfer> InventoryTransfers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransfer> InventoryTransfers1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransfer> InventoryTransfers2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransfer> InventoryTransfers3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransfer> InventoryTransfers4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invitee> Invitees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxAccount> LawToolboxAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxCourtRule> LawToolboxCourtRules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxCourtRule> LawToolboxCourtRules1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantApplication> MerchantApplications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OAuthClient> OAuthClients { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Partnership> Partnerships { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Partnership> Partnerships1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentReminder> PaymentReminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentReminder> PaymentReminders1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentSource> PaymentSources { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingOnlinePayment> PendingOnlinePayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Portal> Portals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PresetProjectRate> PresetProjectRates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PresetProjectRate> PresetProjectRates1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PriceList> PriceLists { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PriceList> PriceLists1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductLog> ProductLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductVendor> ProductVendors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductVendor> ProductVendors1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectRate> ProjectRates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Role> Roles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Role> Roles1 { get; set; }

        public virtual Role Role { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentItem> SaleDocumentItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentItem> SaleDocumentItems1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentTemplate> SaleDocumentTemplates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentTemplate> SaleDocumentTemplates1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesTax> SalesTaxes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesTax> SalesTaxes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Share> Shares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SyncLog> SyncLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> Tags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> Tags1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Timer> Timers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserGroup> UserGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserGroup> UserGroups1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserImapSync> UserImapSyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users1 { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users11 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Workflow> Workflows { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Workflow> Workflows1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> ActivityUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Location> Locations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserGroup> UserGroups2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users12 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkflowEvent> WorkflowEvents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkflowTask> WorkflowTasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<webpages_Roles> webpages_Roles { get; set; }
    }
}
