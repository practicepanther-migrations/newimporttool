namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Expenses")]
    public partial class Expens
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Expens()
        {
            Blobs = new HashSet<Blob>();
            Blobs1 = new HashSet<Blob>();
            Feeds = new HashSet<Feed>();
        }

        public long Id { get; set; }

        public long? ProjectId { get; set; }

        public long? TenantId { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public long? AccountId { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsEnabled { get; set; }

        public DateTime Date { get; set; }

        public long? ProductId { get; set; }

        public decimal Amount { get; set; }

        public long? SaleDocumentItemId { get; set; }

        public bool IsDoNotCharge { get; set; }

        public int Status { get; set; }

        public Guid Guid { get; set; }

        public bool IsBillable { get; set; }

        public long? Category_Id { get; set; }

        public long? Tax1Id { get; set; }

        public long? Tax2Id { get; set; }

        public long? AccountingAccountId { get; set; }

        public long? PrimaryReceipt_Id { get; set; }

        public bool IsBilled { get; set; }

        public string Notes { get; set; }

        public int? UserId { get; set; }

        public int CostType { get; set; }

        public long? HardCostPayment_Id { get; set; }

        public decimal Qty { get; set; }

        public decimal TotalAmount { get; set; }

        public virtual AccountingAccount AccountingAccount { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs1 { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Category Category { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual Product Product { get; set; }

        public virtual Project Project { get; set; }

        public virtual SaleDocumentItem SaleDocumentItem { get; set; }

        public virtual SalesTax SalesTax { get; set; }

        public virtual SalesTax SalesTax1 { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }
    }
}
