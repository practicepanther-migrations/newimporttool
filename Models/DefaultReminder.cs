namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DefaultReminder
    {
        [Key]
        public Guid Guid { get; set; }

        public int TimeType { get; set; }

        public int Time { get; set; }

        public int NotificationType { get; set; }

        public int UserId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public int? CreatedBy_Id { get; set; }

        public long? WorkFlowEventId { get; set; }

        public long? WorkFlowTaskId { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual WorkflowEvent WorkflowEvent { get; set; }

        public virtual WorkflowTask WorkflowTask { get; set; }
    }
}
