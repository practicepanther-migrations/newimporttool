namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CalendarColor
    {
        [Key]
        public Guid Guid { get; set; }

        [StringLength(7)]
        public string Color { get; set; }

        public int? CalendarUserId { get; set; }

        public int? UserId { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
