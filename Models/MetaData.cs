namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MetaData
    {
        public Guid Id { get; set; }

        public int Type { get; set; }

        public string Value { get; set; }
    }
}
