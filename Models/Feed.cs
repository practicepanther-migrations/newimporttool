namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Feed
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Feed()
        {
            FeedNotifications = new HashSet<FeedNotification>();
        }

        public long Id { get; set; }

        [StringLength(1000)]
        public string Text { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public long TenantId { get; set; }

        public long? ContactId { get; set; }

        public long? AccountId { get; set; }

        public int? SaleDocumentId { get; set; }

        public long? ProductId { get; set; }

        public long? ProjectId { get; set; }

        public long? ActivityId { get; set; }

        public int? UserId { get; set; }

        public int? CreatedById { get; set; }

        public long? CampaignId { get; set; }

        public long? BlobId { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? LastModifiedById { get; set; }

        public Guid Guid { get; set; }

        public int Type { get; set; }

        [StringLength(1000)]
        public string Title { get; set; }

        public long? TimeEntryId { get; set; }

        public long? ExpenseId { get; set; }

        public long? PaymentId { get; set; }

        public long? ActivityReminderId { get; set; }

        public long? FlatFeeId { get; set; }

        public virtual Account Account { get; set; }

        public virtual Activity Activity { get; set; }

        public virtual ActivityReminder ActivityReminder { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Campaign Campaign { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Expens Expens { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedNotification> FeedNotifications { get; set; }

        public virtual FlatFee FlatFee { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual Product Product { get; set; }

        public virtual Project Project { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual TimeEntry TimeEntry { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }
    }
}
