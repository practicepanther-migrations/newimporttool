namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Payment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Payment()
        {
            Blobs = new HashSet<Blob>();
            Expenses = new HashSet<Expens>();
            Feeds = new HashSet<Feed>();
            PaymentWebhookNotifications = new HashSet<PaymentWebhookNotification>();
            PendingOnlinePayments = new HashSet<PendingOnlinePayment>();
            RecurringPaymentOccurrences = new HashSet<RecurringPaymentOccurrence>();
            SaleDocumentPayments = new HashSet<SaleDocumentPayment>();
            Shares = new HashSet<Share>();
        }

        public long Id { get; set; }

        public decimal Amount { get; set; }

        public int Method { get; set; }

        [StringLength(100)]
        public string Details { get; set; }

        public int Type { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public string AuthNumber { get; set; }

        public string TransactionId { get; set; }

        public string BankAcctName { get; set; }

        public string BankName { get; set; }

        public string BankRoutingNumber { get; set; }

        public int BankAcctType { get; set; }

        public string CheckAcctNumber { get; set; }

        public int? CheckNumber { get; set; }

        public string CreditCardNameOnCard { get; set; }

        [StringLength(4)]
        public string CreditCardLast4Digits { get; set; }

        public int CreditCardType { get; set; }

        public int Number { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public DateTime Date { get; set; }

        public int? CreditCardExpMonth { get; set; }

        public int? CreditCardExpYear { get; set; }

        public Guid Guid { get; set; }

        [StringLength(1000)]
        public string Notes { get; set; }

        [StringLength(3)]
        public string CurrencyCode { get; set; }

        public long AccountId { get; set; }

        public long? ProjectId { get; set; }

        public long TenantId { get; set; }

        [StringLength(100)]
        public string QuickbooksId { get; set; }

        public DateTime? LastQuickbooksSyncDate { get; set; }

        public Guid? XeroId { get; set; }

        public DateTime? LastXeroSyncDate { get; set; }

        public decimal AmountApplied { get; set; }

        public decimal AmountCredited { get; set; }

        public bool IsPaymentCleared { get; set; }

        public Guid? BankAccountGuid { get; set; }

        public bool IsReconciled { get; set; }

        public int? ReconciledBy_Id { get; set; }

        public DateTime? ReconciledDate { get; set; }

        public long? CheckPayeeId { get; set; }

        public bool? CheckIsPrinted { get; set; }

        [StringLength(100)]
        public string QuickbooksPurchaseId { get; set; }

        [StringLength(100)]
        public string QuickbooksDepositId { get; set; }

        public bool IsDepositSlipPrinted { get; set; }

        public decimal FeeAmountInCents { get; set; }

        public int Status { get; set; }

        public Guid? RefundParentPaymentGuid { get; set; }

        public virtual Account Account { get; set; }

        public virtual Account Account1 { get; set; }

        public virtual BankAccount BankAccount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        public virtual Project Project { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentWebhookNotification> PaymentWebhookNotifications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingOnlinePayment> PendingOnlinePayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPaymentOccurrence> RecurringPaymentOccurrences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentPayment> SaleDocumentPayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Share> Shares { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }
    }
}
