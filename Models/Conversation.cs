namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Conversation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Conversation()
        {
            ConversationMessages = new HashSet<ConversationMessage>();
            ConversationParticipants = new HashSet<ConversationParticipant>();
        }

        [Key]
        public Guid Guid { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsDeleted { get; set; }

        [StringLength(1000)]
        public string Subject { get; set; }

        public long TenantId { get; set; }

        public long? Account_Id { get; set; }

        public long? Project_Id { get; set; }

        public DateTime? DeletedDate { get; set; }

        public long? CreatedByContact_Id { get; set; }

        public int? CreatedByUser_Id { get; set; }

        public int? DeletedBy_Id { get; set; }

        public virtual Account Account { get; set; }

        public virtual Contact Contact { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversationMessage> ConversationMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversationParticipant> ConversationParticipants { get; set; }

        public virtual Project Project { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
