namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SaleDocumentPayment
    {
        public long Id { get; set; }

        public decimal Amount { get; set; }

        public int? SaleDocumentId { get; set; }

        public long? PaymentId { get; set; }

        public long? CreditId { get; set; }

        public Guid Guid { get; set; }

        public virtual Credit Credit { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }
    }
}
