namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MerchantApplication
    {
        [Key]
        public Guid Guid { get; set; }

        public int Status { get; set; }

        public string AffiniPayId { get; set; }

        public DateTime? StartedDate { get; set; }

        public DateTime? SubmittedDate { get; set; }

        public DateTime? ProvisionedDate { get; set; }

        public DateTime? DeclinedDate { get; set; }

        public string auth_code { get; set; }

        public string reference { get; set; }

        public string plan { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        public string email { get; set; }

        public string business_name { get; set; }

        public string business_address1 { get; set; }

        public string business_address2 { get; set; }

        public string business_city { get; set; }

        public string business_state { get; set; }

        public string business_zip_code { get; set; }

        public string business_phone { get; set; }

        public string business_country { get; set; }

        public int industry_code { get; set; }

        public string goods_and_services { get; set; }

        public bool has_dba { get; set; }

        public string dba_name { get; set; }

        public string dba_address1 { get; set; }

        public string dba_address2 { get; set; }

        public string dba_city { get; set; }

        public string dba_state { get; set; }

        public string dba_zip_code { get; set; }

        public string dba_phone { get; set; }

        public string structure { get; set; }

        public string business_type { get; set; }

        public string federal_tax_id { get; set; }

        public string number_of_locations { get; set; }

        public string years_in_business { get; set; }

        public string business_website { get; set; }

        public bool past_bankruptcy { get; set; }

        public DateTime? bankruptcy_date { get; set; }

        public string owner_first_name { get; set; }

        public string owner_last_name { get; set; }

        public string owner_title { get; set; }

        public DateTime? owner_date_of_birth { get; set; }

        public string owner_social_security_number { get; set; }

        public int owner_percentage_of_ownership { get; set; }

        public string owner_address1 { get; set; }

        public string owner_address2 { get; set; }

        public string owner_city { get; set; }

        public string owner_state { get; set; }

        public string owner_zip_code { get; set; }

        public string owner_phone_number { get; set; }

        public string owner_email { get; set; }

        public string owner_years_in_residence { get; set; }

        public string owner_drivers_license_number { get; set; }

        public string owner_drivers_license_state { get; set; }

        public DateTime? owner_drivers_license_expiration { get; set; }

        public string owner_country_of_citizenship { get; set; }

        public bool currently_accepting_cards { get; set; }

        public string current_card_processor { get; set; }

        public bool wants_to_accept_amex { get; set; }

        public string current_amex_merchant_number { get; set; }

        public string operating_account_name { get; set; }

        public string operating_account_routing_number { get; set; }

        public string operating_account_bank_name { get; set; }

        public string operating_account_number { get; set; }

        public string trust_account_name { get; set; }

        public string trust_account_routing_number { get; set; }

        public string trust_account_bank_name { get; set; }

        public string trust_account_number { get; set; }

        public string signed_by { get; set; }

        public string signature { get; set; }

        public int? SubmittedBy_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public bool IsAuthorizedSignOn { get; set; }

        public DateTime? business_incorporated_date { get; set; }

        public string business_incorporated_state { get; set; }

        public string business_sub_type { get; set; }

        public string HeadNoteFirmId { get; set; }

        public string Owner_Bar_Number { get; set; }

        public string Owner_Bar_State { get; set; }

        public decimal? Owner_percentage_Ownership { get; set; }

        public string owner_office_phone_number { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }
    }
}
