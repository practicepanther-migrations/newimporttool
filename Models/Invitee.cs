namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Invitee
    {
        public long Id { get; set; }

        public long? ProjectId { get; set; }

        public long? ActivityId { get; set; }

        public long? UserId { get; set; }

        public long? ContactId { get; set; }

        public int? User_Id { get; set; }

        public int InviteeType { get; set; }

        public virtual Activity Activity { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Project Project { get; set; }

        public virtual User User { get; set; }
    }
}
