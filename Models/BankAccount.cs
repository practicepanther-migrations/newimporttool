namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BankAccount
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BankAccount()
        {
            Payments = new HashSet<Payment>();
            PendingOnlinePayments = new HashSet<PendingOnlinePayment>();
            RecurringPayments = new HashSet<RecurringPayment>();
        }

        [Key]
        public Guid Guid { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        public int Type { get; set; }

        public bool IsDefault { get; set; }

        public long TenantId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public decimal OpeningBalance { get; set; }

        [StringLength(500)]
        public string LawPayAchAccountId { get; set; }

        [StringLength(500)]
        public string LawPayMerchantAccountId { get; set; }

        [StringLength(3)]
        public string CurrencyCode { get; set; }

        [StringLength(500)]
        public string AccountNumber { get; set; }

        [StringLength(500)]
        public string RoutingNumber { get; set; }

        [StringLength(500)]
        public string SwiftCode { get; set; }

        [StringLength(500)]
        public string AccountHolder { get; set; }

        [StringLength(500)]
        public string Institution { get; set; }

        [StringLength(500)]
        public string DomicileBranch { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsEnabled { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        [StringLength(500)]
        public string LawPayAchAccountName { get; set; }

        [StringLength(500)]
        public string LawPayMerchantAccountName { get; set; }

        public string Description { get; set; }

        [StringLength(100)]
        public string QuickbooksId { get; set; }

        public Guid? XeroId { get; set; }

        public bool QuickbooksIsDepositToUndepositedFunds { get; set; }

        public bool IsPrintDepositSlips { get; set; }

        [StringLength(500)]
        public string HeadNoteId { get; set; }

        public int Status { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingOnlinePayment> PendingOnlinePayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments { get; set; }
    }
}
