namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WorkflowTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WorkflowTask()
        {
            DefaultReminders = new HashSet<DefaultReminder>();
            Tags = new HashSet<Tag>();
            Users = new HashSet<User>();
        }

        public long Id { get; set; }

        public long WorkflowId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsDueDate { get; set; }

        public int? NumOfDays { get; set; }

        public int? DaysType { get; set; }

        public int? BeforeOrAfter { get; set; }

        public long? WorkflowEventId { get; set; }

        public int? Priority { get; set; }

        public string EventColor { get; set; }

        public bool IsSendNotificationEmail { get; set; }

        public int DueOrCompletionDate { get; set; }

        public long? WorkFlowParentTaskId { get; set; }

        public bool IsTaskConditional { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DefaultReminder> DefaultReminders { get; set; }

        public virtual WorkflowEvent WorkflowEvent { get; set; }

        public virtual Workflow Workflow { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> Tags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }
    }
}
