namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserImapSync
    {
        public long Id { get; set; }

        public long? ProjectId { get; set; }

        public long? AccountId { get; set; }

        public int UserId { get; set; }

        [Required]
        public string ImapFolderName { get; set; }

        public long NextUid { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public DateTime? LastSyncDate { get; set; }

        public bool IsLastSyncSuccessful { get; set; }

        public string LastSyncStatusMessage { get; set; }

        public virtual Account Account { get; set; }

        public virtual Project Project { get; set; }

        public virtual User User { get; set; }
    }
}
