namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LawToolboxAccount
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LawToolboxAccount()
        {
            LawToolboxToolsets = new HashSet<LawToolboxToolset>();
        }

        public Guid Id { get; set; }

        public int FirmId { get; set; }

        public int AdminUserId { get; set; }

        public int SubscriptionId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string StateId { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhone { get; set; }

        public string ContactName { get; set; }

        public bool IsActive { get; set; }

        public long TenantId { get; set; }

        public int? CreatedBy_Id { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxToolset> LawToolboxToolsets { get; set; }
    }
}
