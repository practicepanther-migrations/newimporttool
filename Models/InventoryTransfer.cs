namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class InventoryTransfer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InventoryTransfer()
        {
            InventoryTransferItems = new HashSet<InventoryTransferItem>();
            ProductLogs = new HashSet<ProductLog>();
            Shipments = new HashSet<Shipment>();
        }

        public long Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? TenantId { get; set; }

        public DateTime? SentDate { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public bool IsReviewed { get; set; }

        public DateTime? ReviewedDate { get; set; }

        public int? SentBy_Id { get; set; }

        public int? ReceivedBy_Id { get; set; }

        public int? ReviewedBy_Id { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool IsAutoSend { get; set; }

        public bool IsAutoReceive { get; set; }

        public int Type { get; set; }

        public string ReferenceNumber { get; set; }

        public long? VendorId { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransferItem> InventoryTransferItems { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        public virtual User User3 { get; set; }

        public virtual User User4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductLog> ProductLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments { get; set; }
    }
}
