namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SaleDocumentItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SaleDocumentItem()
        {
            Expenses = new HashSet<Expens>();
            FlatFees = new HashSet<FlatFee>();
            TimeEntries = new HashSet<TimeEntry>();
        }

        public long Id { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string Description { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public int? SaleDocumentId { get; set; }

        public decimal Qty { get; set; }

        public long? TenantId { get; set; }

        public bool Tax1Data_IsCompound { get; set; }

        public bool Tax2Data_IsCompound { get; set; }

        public decimal Price { get; set; }

        [StringLength(500)]
        public string Tax1Data_Name { get; set; }

        public decimal Tax1Data_Rate { get; set; }

        [StringLength(500)]
        public string Tax2Data_Name { get; set; }

        public decimal Tax2Data_Rate { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        public long? Tax1Data_Id { get; set; }

        public long? Tax2Data_Id { get; set; }

        public decimal? Tax1Data_Amount { get; set; }

        public decimal? Tax2Data_Amount { get; set; }

        public long? Tax2Ref_Id { get; set; }

        public int RowIndex { get; set; }

        public long? FlatRateProjectId { get; set; }

        public int Type { get; set; }

        public DateTime? Date { get; set; }

        public string UserName { get; set; }

        public decimal Tax1 { get; set; }

        public decimal Discount { get; set; }

        public decimal Sub { get; set; }

        public decimal Tax2 { get; set; }

        public Guid Guid { get; set; }

        [StringLength(20)]
        public string LedesUtbmsCode { get; set; }

        [StringLength(20)]
        public string LedesItemCode { get; set; }

        [StringLength(20)]
        public string LedesExpenseCode { get; set; }

        [StringLength(10)]
        public string LedesTimekeeperClass { get; set; }

        public long? Project_Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees { get; set; }

        public virtual Project Project { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }

        public virtual SalesTax SalesTax { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries { get; set; }
    }
}
