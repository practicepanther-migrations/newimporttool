namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Project
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Project()
        {
            AccountProjectLinks = new HashSet<AccountProjectLink>();
            Activities = new HashSet<Activity>();
            Attachments = new HashSet<Attachment>();
            Blobs = new HashSet<Blob>();
            ContactSyncs = new HashSet<ContactSync>();
            Conversations = new HashSet<Conversation>();
            CustomFieldValues = new HashSet<CustomFieldValue>();
            Expenses = new HashSet<Expens>();
            Feeds = new HashSet<Feed>();
            FlatFees = new HashSet<FlatFee>();
            IntakeForms = new HashSet<IntakeForm>();
            Invitees = new HashSet<Invitee>();
            LawToolboxCourtRules = new HashSet<LawToolboxCourtRule>();
            LawToolboxMatters = new HashSet<LawToolboxMatter>();
            Payments = new HashSet<Payment>();
            PaymentSources = new HashSet<PaymentSource>();
            PendingOnlinePayments = new HashSet<PendingOnlinePayment>();
            ProjectRates = new HashSet<ProjectRate>();
            RecurringPayments = new HashSet<RecurringPayment>();
            SaleDocumentItems = new HashSet<SaleDocumentItem>();
            SaleDocuments = new HashSet<SaleDocument>();
            TimeEntries = new HashSet<TimeEntry>();
            Timers = new HashSet<Timer>();
            UserImapSyncs = new HashSet<UserImapSync>();
            Users = new HashSet<User>();
            Tags = new HashSet<Tag>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(800)]
        public string Name { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public long? AccountId { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? TenantId { get; set; }

        public DateTime? DueDate { get; set; }

        public int Status { get; set; }

        public int HourlyRateType { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsEnabled { get; set; }

        public decimal HourlyRate { get; set; }

        public decimal FlatRate { get; set; }

        public Guid Guid { get; set; }

        public string Notes { get; set; }

        public bool IsFlatRateBilled { get; set; }

        [StringLength(50)]
        public string BoxFolderId { get; set; }

        public int? Number { get; set; }

        [StringLength(850)]
        public string NameAndNumber { get; set; }

        public decimal BillableFlatRate { get; set; }

        public bool EvergreenIsOn { get; set; }

        public decimal EvergreenAmount { get; set; }

        [StringLength(24)]
        public string LedesClientMatterId { get; set; }

        public decimal ContingencyRatePercent { get; set; }

        public bool IsEmailSync { get; set; }

        public bool IsFilesSync { get; set; }

        [StringLength(50)]
        public string DropboxFolderId { get; set; }

        public DateTime? OpenDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? OriginatedBy_Id { get; set; }

        public bool IsContactSync { get; set; }

        public Guid? DefaultPaymentSourceGuid { get; set; }

        public bool IsAddEvergreenToInvoice { get; set; }

        [StringLength(1000)]
        public string BoxSharedFolderUrl { get; set; }

        [StringLength(100)]
        public string GoogleDriveFolderId { get; set; }

        [StringLength(100)]
        public string OneDriveFolderId { get; set; }

        public Guid? PresetProjectRate_Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountProjectLink> AccountProjectLinks { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attachment> Attachments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContactSync> ContactSyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversation> Conversations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldValue> CustomFieldValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeForm> IntakeForms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invitee> Invitees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxCourtRule> LawToolboxCourtRules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxMatter> LawToolboxMatters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentSource> PaymentSources { get; set; }

        public virtual PaymentSource PaymentSource { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingOnlinePayment> PendingOnlinePayments { get; set; }

        public virtual PresetProjectRate PresetProjectRate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectRate> ProjectRates { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentItem> SaleDocumentItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Timer> Timers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserImapSync> UserImapSyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
