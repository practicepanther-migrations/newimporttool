namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Share
    {
        [Key]
        public Guid Guid { get; set; }

        public DateTime SharedDate { get; set; }

        public long SharedByUserId { get; set; }

        public long SharedByContactId { get; set; }

        public long? Activity_Id { get; set; }

        public long? Blob_Id { get; set; }

        public long? Payment_Id { get; set; }

        public int? SaleDocument_Id { get; set; }

        public int? SharedByUser_Id { get; set; }

        public virtual Activity Activity { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }

        public virtual User User { get; set; }
    }
}
