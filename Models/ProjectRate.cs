namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProjectRate
    {
        [Key]
        public Guid Guid { get; set; }

        public decimal HourlyRate { get; set; }

        public int RateType { get; set; }

        public long? Project_Id { get; set; }

        public long? Role_Id { get; set; }

        public int? User_Id { get; set; }

        public Guid? PresetProjectRate_Id { get; set; }

        public virtual PresetProjectRate PresetProjectRate { get; set; }

        public virtual Project Project { get; set; }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}
