namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PaymentReminder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PaymentReminder()
        {
            SaleDocumentPaymentReminders = new HashSet<SaleDocumentPaymentReminder>();
        }

        [Key]
        public Guid Guid { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsEnabled { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public int DueOverdue { get; set; }

        public int NumOfDays { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public decimal ReminderMinAmountDue { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long Tenant_Id { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentPaymentReminder> SaleDocumentPaymentReminders { get; set; }
    }
}
