namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AccountProjectLink
    {
        public long Id { get; set; }

        public Guid Guid { get; set; }

        public long AccountId { get; set; }

        public long ProjectId { get; set; }

        public string RelationshipName { get; set; }

        public string Notes { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? ContactId { get; set; }

        public virtual Account Account { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Project Project { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
