namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ConversationParticipant
    {
        [Key]
        public Guid Guid { get; set; }

        public int Status { get; set; }

        public long? Contact_Id { get; set; }

        public Guid? Conversation_Guid { get; set; }

        public int? User_Id { get; set; }

        public DateTime? LastReadDate { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsArchived { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Conversation Conversation { get; set; }

        public virtual User User { get; set; }
    }
}
