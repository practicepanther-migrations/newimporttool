namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomField
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CustomField()
        {
            CustomFieldValues = new HashSet<CustomFieldValue>();
            CustomFieldSets = new HashSet<CustomFieldSet>();
        }

        public long Id { get; set; }

        public Guid Guid { get; set; }

        public long TenantId { get; set; }

        [Required]
        public string Name { get; set; }

        public int ObjectType { get; set; }

        public int ValueType { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public string DropDownListValues { get; set; }

        public int RowIndex { get; set; }

        public bool IsRequired { get; set; }

        public bool IsDefault { get; set; }

        public bool IsIncludeInList { get; set; }

        [StringLength(255)]
        public string HelpText { get; set; }

        public string Tooltip { get; set; }

        public Guid? CustomFieldTabGuid { get; set; }

        public virtual CustomFieldTab CustomFieldTab { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldValue> CustomFieldValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldSet> CustomFieldSets { get; set; }
    }
}
