namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ActivityReminder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ActivityReminder()
        {
            Feeds = new HashSet<Feed>();
        }

        public long Id { get; set; }

        public Guid Guid { get; set; }

        public int TimeType { get; set; }

        public int Time { get; set; }

        public bool IsSent { get; set; }

        public DateTime? SentDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public int NotificationType { get; set; }

        public long? Activity_Id { get; set; }

        public bool IsSnoozed { get; set; }

        public int SnoozeMinutes { get; set; }

        public virtual Activity Activity { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }
    }
}
