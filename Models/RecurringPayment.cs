namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RecurringPayment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RecurringPayment()
        {
            RecurringPaymentOccurrences = new HashSet<RecurringPaymentOccurrence>();
        }

        [Key]
        public Guid Guid { get; set; }

        [StringLength(25)]
        public string LawPayTokenId { get; set; }

        [StringLength(1000)]
        public string Notes { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public string Currency { get; set; }

        public string Reference { get; set; }

        public decimal Amount { get; set; }

        public DateTime Schedule_Start { get; set; }

        public DateTime? Schedule_End { get; set; }

        public int Schedule_IntervalDelay { get; set; }

        public int Schedule_IntervalUnit { get; set; }

        public decimal? MaxOccurrences { get; set; }

        public decimal? MaxAmount { get; set; }

        public bool IsCreateInvoiceWithPayment { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public long Account_Id { get; set; }

        public Guid BankAccount_Guid { get; set; }

        public int? CreatedBy_Id { get; set; }

        public long? Item_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public Guid PaymentSource_Guid { get; set; }

        public long? Project_Id { get; set; }

        public long? Tax1_Id { get; set; }

        public long? Tax2_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public int Status { get; set; }

        public int? StatusReason { get; set; }

        public int? BilledBy_Id { get; set; }

        public string HeadNoteId { get; set; }

        public DateTime? LastDateOfMigrationRequest { get; set; }

        public int? MigrationRequestCount { get; set; }

        public int? Installments { get; set; }

        public virtual Account Account { get; set; }

        public virtual BankAccount BankAccount { get; set; }

        public virtual PaymentSource PaymentSource { get; set; }

        public virtual Product Product { get; set; }

        public virtual Project Project { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPaymentOccurrence> RecurringPaymentOccurrences { get; set; }

        public virtual SalesTax SalesTax { get; set; }

        public virtual SalesTax SalesTax1 { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }
    }
}
