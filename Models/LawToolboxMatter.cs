namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LawToolboxMatter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LawToolboxMatter()
        {
            LawToolboxCourtRules = new HashSet<LawToolboxCourtRule>();
        }

        public Guid Id { get; set; }

        public long ProjectId { get; set; }

        [Required]
        public string MatterNumberLTB { get; set; }

        public string ToolsetId { get; set; }

        public string StateId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxCourtRule> LawToolboxCourtRules { get; set; }

        public virtual Project Project { get; set; }
    }
}
