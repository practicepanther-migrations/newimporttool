namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProductLog
    {
        public long Id { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public decimal? Qty { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public long? Inventory_Id { get; set; }

        public long? Product_Id { get; set; }

        public long? InventoryTransferId { get; set; }

        public long? InvoiceId { get; set; }

        public int? Invoice_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public virtual Inventory Inventory { get; set; }

        public virtual InventoryTransfer InventoryTransfer { get; set; }

        public virtual Product Product { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }
    }
}
