namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Attachment
    {
        public long Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public bool IsImage { get; set; }

        public byte[] File { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? Project_Id { get; set; }

        public virtual Project Project { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
