namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Activity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Activity()
        {
            ActivityConditionalTasks = new HashSet<ActivityConditionalTask>();
            ActivityReminders = new HashSet<ActivityReminder>();
            ActivitySyncs = new HashSet<ActivitySync>();
            Blobs = new HashSet<Blob>();
            Feeds = new HashSet<Feed>();
            FlatFees = new HashSet<FlatFee>();
            IntakeForms = new HashSet<IntakeForm>();
            Invitees = new HashSet<Invitee>();
            SaleDocumentPaymentReminders = new HashSet<SaleDocumentPaymentReminder>();
            Shares = new HashSet<Share>();
            TimeEntries = new HashSet<TimeEntry>();
            Contacts = new HashSet<Contact>();
            Users = new HashSet<User>();
            Tags = new HashSet<Tag>();
        }

        public long Id { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime? StartDateTime { get; set; }

        public DateTime? EndDateTime { get; set; }

        public bool IsAllDayActivity { get; set; }

        public int? Status { get; set; }

        public int? Priority { get; set; }

        public bool IsSendNotificationEmail { get; set; }

        public bool IsRecurringActivity { get; set; }

        public DateTime? RecurringActivityStartDate { get; set; }

        public DateTime? RecurringActivityEndDate { get; set; }

        public int? RecurringRepeatType { get; set; }

        public string Description { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? TenantId { get; set; }

        public long? AccountId { get; set; }

        public long? ContactId { get; set; }

        public int Type { get; set; }

        public int? CallType { get; set; }

        public int? SaleDocument_Id { get; set; }

        public long? ProjectId { get; set; }

        [Required]
        [StringLength(1000)]
        public string Name { get; set; }

        public long? SaleDocumentId { get; set; }

        [StringLength(7)]
        public string EventColor { get; set; }

        [StringLength(7)]
        public string EventBackgroundColor { get; set; }

        public Guid Guid { get; set; }

        [StringLength(100)]
        public string Location { get; set; }

        public string GoogleId { get; set; }

        public DateTime? RecurrenceStopDate { get; set; }

        public long? RecurrenceId { get; set; }

        [StringLength(50)]
        public string MandrillId { get; set; }

        public DateTime? EmailProperties_Date { get; set; }

        public string EmailProperties_From { get; set; }

        public string EmailProperties_To { get; set; }

        public string EmailProperties_Cc { get; set; }

        public string EmailProperties_Bcc { get; set; }

        public decimal? CallProperties_CallDuration { get; set; }

        public DateTime? CallProperties_CallDateTime { get; set; }

        public int? CallProperties_CallType { get; set; }

        public long? WorkflowId { get; set; }

        public bool IsSendTaskCompleteEmail { get; set; }

        public DateTime? Date { get; set; }

        public bool IsHoursLoggedManually { get; set; }

        public bool IsSendCompletedEmail { get; set; }

        public DateTime? NoteProperties_NoteDateTime { get; set; }

        public string LawToolboxDeadlineId { get; set; }

        public string LawToolboxDeadlineNumber { get; set; }

        public Guid? LawToolboxCourtRule_Id { get; set; }

        [StringLength(150)]
        public string EmailProperties_ExternalMessageId { get; set; }

        [StringLength(150)]
        public string EmailProperties_ExternalThreadId { get; set; }

        public long? HtmlBody_Id { get; set; }

        public bool IsPrivate { get; set; }

        public virtual Account Account { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual LawToolboxCourtRule LawToolboxCourtRule { get; set; }

        public virtual Project Project { get; set; }

        public virtual Recurrence Recurrence { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual Workflow Workflow { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActivityConditionalTask> ActivityConditionalTasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActivityReminder> ActivityReminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActivitySync> ActivitySyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeForm> IntakeForms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invitee> Invitees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentPaymentReminder> SaleDocumentPaymentReminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Share> Shares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contact> Contacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
