namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Credit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Credit()
        {
            SaleDocumentPayments = new HashSet<SaleDocumentPayment>();
        }

        public long Id { get; set; }

        public int CreditForSaleDocument_Id { get; set; }

        public decimal CreditAmount { get; set; }

        public decimal CreditApplied { get; set; }

        public decimal CreditAvailable { get; set; }

        public Guid Guid { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentPayment> SaleDocumentPayments { get; set; }
    }
}
