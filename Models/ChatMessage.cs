namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ChatMessage
    {
        [Key]
        public Guid Guid { get; set; }

        public Guid? UserFromId { get; set; }

        public Guid? UserToId { get; set; }

        public Guid? ConversationId { get; set; }

        public Guid? RoomId { get; set; }

        public DateTime Date { get; set; }

        public string Message { get; set; }

        public Guid? ClientGuid { get; set; }

        public bool IsDeleted { get; set; }

        public long Tenant_Id { get; set; }

        public bool IsOfflineMessage { get; set; }

        public bool IsDelivered { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
