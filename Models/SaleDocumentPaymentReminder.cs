namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SaleDocumentPaymentReminder
    {
        [Key]
        public Guid Guid { get; set; }

        public DateTime SentDate { get; set; }

        public long? ActivityEmail_Id { get; set; }

        public Guid PaymentReminder_Guid { get; set; }

        public int SaleDocument_Id { get; set; }

        public virtual Activity Activity { get; set; }

        public virtual PaymentReminder PaymentReminder { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }
    }
}
