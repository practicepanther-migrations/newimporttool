namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TimeEntry
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TimeEntry()
        {
            Feeds = new HashSet<Feed>();
            Timers = new HashSet<Timer>();
        }

        public long Id { get; set; }

        public string Description { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public decimal Hours { get; set; }

        public long? AccountId { get; set; }

        public long? ProjectId { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public long? ProductId { get; set; }

        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsEnabled { get; set; }

        public DateTime Date { get; set; }

        public int? UserId { get; set; }

        public long? SaleDocumentItemId { get; set; }

        public bool IsNoCharge { get; set; }

        public decimal Total { get; set; }

        public decimal HourlyRate { get; set; }

        public int Status { get; set; }

        public Guid Guid { get; set; }

        public bool IsBillable { get; set; }

        public bool IsBilled { get; set; }

        public string Notes { get; set; }

        public long? ActivityId { get; set; }

        public long? UtbmsCodeId { get; set; }

        public virtual Account Account { get; set; }

        public virtual Activity Activity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        public virtual Product Product { get; set; }

        public virtual Project Project { get; set; }

        public virtual SaleDocumentItem SaleDocumentItem { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        public virtual UtbmsCode UtbmsCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Timer> Timers { get; set; }
    }
}
