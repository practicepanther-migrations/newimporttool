namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PaymentSource
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PaymentSource()
        {
            Accounts = new HashSet<Account>();
            Projects = new HashSet<Project>();
            RecurringPayments = new HashSet<RecurringPayment>();
        }

        [Key]
        public Guid Guid { get; set; }

        public long AccountId { get; set; }

        public long? ProjectId { get; set; }

        public int Type { get; set; }

        [StringLength(1000)]
        public string Notes { get; set; }

        public long TenantId { get; set; }

        [Required]
        [StringLength(25)]
        public string LawPayTokenId { get; set; }

        [Required]
        [StringLength(4)]
        public string AccountLastDigits { get; set; }

        [StringLength(3)]
        public string RoutingLastDigits { get; set; }

        public int? ExpirationMonth { get; set; }

        public int? ExpirationYear { get; set; }

        [StringLength(100)]
        public string DisplayName { get; set; }

        public bool IsDeleted { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string HeadNoteId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts { get; set; }

        public virtual Account Account { get; set; }

        public virtual Project Project { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments { get; set; }
    }
}
