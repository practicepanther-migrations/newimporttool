namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class IntakeFormTemplateMapping
    {
        [Key]
        public Guid Guid { get; set; }

        public string FormFieldName { get; set; }

        public string TargetFieldValue { get; set; }

        public Guid? IntakeFormTemplate_Guid { get; set; }

        public string FormFieldLabel { get; set; }

        public int? FormFieldType { get; set; }

        public int RowIndex { get; set; }

        public virtual IntakeFormTemplate IntakeFormTemplate { get; set; }
    }
}
