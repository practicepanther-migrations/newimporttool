namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PendingOnlinePaymentSaleDocument
    {
        public long Id { get; set; }

        public Guid PendingOnlinePaymentGuid { get; set; }

        public int? SaleDocumentId { get; set; }

        public decimal Amount { get; set; }

        public virtual PendingOnlinePayment PendingOnlinePayment { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }
    }
}
