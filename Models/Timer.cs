namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Timer
    {
        [Key]
        public Guid Guid { get; set; }

        public double PreviousDuration { get; set; }

        public DateTime? StartDateTime { get; set; }

        public int State { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        public long? Account_Id { get; set; }

        public long? Project_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public int? User_Id { get; set; }

        public DateTime? LastUserTime { get; set; }

        public long? TimeEntryId { get; set; }

        public string Notes { get; set; }

        public virtual Account Account { get; set; }

        public virtual Project Project { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual TimeEntry TimeEntry { get; set; }

        public virtual User User { get; set; }
    }
}
