namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PendingOnlinePayment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PendingOnlinePayment()
        {
            PendingOnlinePaymentSaleDocuments = new HashSet<PendingOnlinePaymentSaleDocument>();
        }

        [Key]
        public Guid Guid { get; set; }

        public long AccountId { get; set; }

        public long? ProjectId { get; set; }

        public int? CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsCompleted { get; set; }

        public DateTime? CompletedDate { get; set; }

        public string CompletedResponse { get; set; }

        public string TransactionId { get; set; }

        public decimal Amount { get; set; }

        public int Method { get; set; }

        public string Notes { get; set; }

        public int BankAccountType { get; set; }

        public string CurrencyCode { get; set; }

        public long? PaymentId { get; set; }

        public string retUrl { get; set; }

        public Guid? BankAccountGuid { get; set; }

        public virtual Account Account { get; set; }

        public virtual BankAccount BankAccount { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual Project Project { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingOnlinePaymentSaleDocument> PendingOnlinePaymentSaleDocuments { get; set; }
    }
}
