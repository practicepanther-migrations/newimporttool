namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Account
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Account()
        {
            AccountProjectLinks = new HashSet<AccountProjectLink>();
            Activities = new HashSet<Activity>();
            Addresses = new HashSet<Address>();
            Blobs = new HashSet<Blob>();
            Contacts = new HashSet<Contact>();
            Conversations = new HashSet<Conversation>();
            CustomFieldValues = new HashSet<CustomFieldValue>();
            Expenses = new HashSet<Expens>();
            Feeds = new HashSet<Feed>();
            FlatFees = new HashSet<FlatFee>();
            IntakeForms = new HashSet<IntakeForm>();
            InventoryTransfers = new HashSet<InventoryTransfer>();
            LawToolboxCourtRules = new HashSet<LawToolboxCourtRule>();
            Opportunities = new HashSet<Opportunity>();
            Partnerships = new HashSet<Partnership>();
            Payments = new HashSet<Payment>();
            Payments1 = new HashSet<Payment>();
            PaymentSources = new HashSet<PaymentSource>();
            PendingOnlinePayments = new HashSet<PendingOnlinePayment>();
            ProductVendors = new HashSet<ProductVendor>();
            Projects = new HashSet<Project>();
            RecurringPayments = new HashSet<RecurringPayment>();
            SaleDocuments = new HashSet<SaleDocument>();
            TimeEntries = new HashSet<TimeEntry>();
            Timers = new HashSet<Timer>();
            UserImapSyncs = new HashSet<UserImapSync>();
            Users = new HashSet<User>();
            AccountUsers = new HashSet<User>();
            Tags = new HashSet<Tag>();
        }

        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public bool IsCustomPriceList { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? TenantId { get; set; }

        public long? PrimaryContactId { get; set; }

        public long? PriceListId { get; set; }

        public long? BillingAddressId { get; set; }

        public long? ShippingAddressId { get; set; }

        [StringLength(200)]
        public string Website { get; set; }

        public long? CampaignId { get; set; }

        public long? Logo_Id { get; set; }

        public Guid Guid { get; set; }

        public string Notes { get; set; }

        [StringLength(5)]
        public string CultureName { get; set; }

        [StringLength(3)]
        public string CurrencyCode { get; set; }

        public bool IsAllowClientToViewAllOutstandingInvoices { get; set; }

        public bool IsAllowClientToViewAllPayments { get; set; }

        public bool IsAllowClientToViewAccountBalances { get; set; }

        public bool IsSendInvoicePaymentReminders { get; set; }

        public bool IsSendQuoteApprovalReminders { get; set; }

        public bool IsAllowClientToViewAllPaidInvoices { get; set; }

        [StringLength(50)]
        public string BoxFolderId { get; set; }

        public int? Number { get; set; }

        [StringLength(100)]
        public string QuickbooksId { get; set; }

        public DateTime? LastQuickbooksSyncDate { get; set; }

        public Guid? XeroId { get; set; }

        public DateTime? LastXeroSyncDate { get; set; }

        [StringLength(255)]
        public string NameAndNumber { get; set; }

        public bool IsEmailSync { get; set; }

        public bool IsFilesSync { get; set; }

        [StringLength(50)]
        public string DropboxFolderId { get; set; }

        public bool IsContactSync { get; set; }

        public Guid? DefaultPaymentSourceGuid { get; set; }

        [StringLength(20)]
        public string LedesClientId { get; set; }

        [StringLength(1000)]
        public string BoxSharedFolderUrl { get; set; }

        [StringLength(100)]
        public string GoogleDriveFolderId { get; set; }

        [StringLength(100)]
        public string OneDriveFolderId { get; set; }

        public bool UtbmsIsEnabled { get; set; }

        [StringLength(50)]
        public string HeadNoteClientId { get; set; }

        public bool IsDisableEcheck { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountProjectLink> AccountProjectLinks { get; set; }

        public virtual Address Address { get; set; }

        public virtual Address Address1 { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Campaign Campaign { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual PaymentSource PaymentSource { get; set; }

        public virtual PriceList PriceList { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contact> Contacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversation> Conversations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldValue> CustomFieldValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeForm> IntakeForms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransfer> InventoryTransfers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxCourtRule> LawToolboxCourtRules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Partnership> Partnerships { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentSource> PaymentSources { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingOnlinePayment> PendingOnlinePayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductVendor> ProductVendors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Timer> Timers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserImapSync> UserImapSyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> AccountUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
