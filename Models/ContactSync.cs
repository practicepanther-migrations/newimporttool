namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ContactSync
    {
        public long Id { get; set; }

        public long ActivityId { get; set; }

        public int UserId { get; set; }

        public string ExchangeId { get; set; }

        public string GoogleId { get; set; }

        public string OutlookId { get; set; }

        public DateTime? LastGoogleSyncDate { get; set; }

        public DateTime? LastExchangeSyncDate { get; set; }

        public DateTime? LastOutlookSyncDate { get; set; }

        public long? Contact_Id { get; set; }

        public long? Project_Id { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Project Project { get; set; }

        public virtual User User { get; set; }
    }
}
