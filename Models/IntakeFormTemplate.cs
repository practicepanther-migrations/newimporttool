namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class IntakeFormTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public IntakeFormTemplate()
        {
            IntakeForms = new HashSet<IntakeForm>();
            IntakeFormTemplateMappings = new HashSet<IntakeFormTemplateMapping>();
        }

        [Key]
        public Guid Guid { get; set; }

        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        public bool IsRequirePortalSignin { get; set; }

        public bool IsSendEmailOnSubmission { get; set; }

        public bool IsCreateTaskOnSubmission { get; set; }

        public string Description { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public long TenantId { get; set; }

        public bool IsAllowCreatingNewAccountOnSubmission { get; set; }

        public bool IsAllowCreatingNewProjectOnSubmission { get; set; }

        public bool IsAllowUpdatingAccountOnSubmission { get; set; }

        public bool IsAllowUpdatingProjectOnSubmission { get; set; }

        [Required]
        public string Template { get; set; }

        public int? AssignNewRecordsTo_Id { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? CreateTaskOnSubmissionAssignedTo_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public int? SendEmailOnSubmissionTo_Id { get; set; }

        public bool IsAutoFillContactAndMatterInfo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeForm> IntakeForms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormTemplateMapping> IntakeFormTemplateMappings { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        public virtual User User3 { get; set; }

        public virtual User User4 { get; set; }
    }
}
