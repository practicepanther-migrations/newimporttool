namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Partnership
    {
        public long Id { get; set; }

        public decimal Percent { get; set; }

        public long? Product_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public long? Account_Id { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public virtual Account Account { get; set; }

        public virtual Product Product { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
