namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OAuthClient
    {
        public string Id { get; set; }

        [Required]
        public string Secret { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public int ApplicationType { get; set; }

        public bool Active { get; set; }

        public int RefreshTokenLifeTime { get; set; }

        [StringLength(100)]
        public string AllowedOrigin { get; set; }

        [StringLength(1000)]
        public string RedirectUrl { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }
    }
}
