namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GoogleDataStore
    {
        public long Id { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}
