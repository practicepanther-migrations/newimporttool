namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FeedNotification
    {
        [Key]
        public Guid Guid { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsRead { get; set; }

        public DateTime? ReadDate { get; set; }

        public long? Feed_Id { get; set; }

        public int? User_Id { get; set; }

        public bool IsSticky { get; set; }

        public virtual Feed Feed { get; set; }

        public virtual User User { get; set; }
    }
}
