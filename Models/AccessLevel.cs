namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AccessLevel
    {
        public long Id { get; set; }

        public long RoleId { get; set; }

        public int Controller { get; set; }

        public int Action { get; set; }

        public virtual Role Role { get; set; }
    }
}
