namespace CsvParser.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PPContext : DbContext
    {
        public PPContext()
            : base("name=PPContext")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AccessLevel> AccessLevels { get; set; }
        public virtual DbSet<AccountingAccount> AccountingAccounts { get; set; }
        public virtual DbSet<AccountProjectLink> AccountProjectLinks { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<ActivityConditionalTask> ActivityConditionalTasks { get; set; }
        public virtual DbSet<ActivityReminder> ActivityReminders { get; set; }
        public virtual DbSet<ActivitySync> ActivitySyncs { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<BankAccount> BankAccounts { get; set; }
        public virtual DbSet<Blob> Blobs { get; set; }
        public virtual DbSet<CalendarColor> CalendarColors { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<ChatMessage> ChatMessages { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContactSync> ContactSyncs { get; set; }
        public virtual DbSet<ConversationMessage> ConversationMessages { get; set; }
        public virtual DbSet<ConversationParticipant> ConversationParticipants { get; set; }
        public virtual DbSet<Conversation> Conversations { get; set; }
        public virtual DbSet<Credit> Credits { get; set; }
        public virtual DbSet<CustomField> CustomFields { get; set; }
        public virtual DbSet<CustomFieldSet> CustomFieldSets { get; set; }
        public virtual DbSet<CustomFieldTab> CustomFieldTabs { get; set; }
        public virtual DbSet<CustomFieldValue> CustomFieldValues { get; set; }
        public virtual DbSet<DefaultReminder> DefaultReminders { get; set; }
        public virtual DbSet<DocumentTemplate> DocumentTemplates { get; set; }
        public virtual DbSet<ELMAH_Error> ELMAH_Error { get; set; }
        public virtual DbSet<Expens> Expenses { get; set; }
        public virtual DbSet<Family> Families { get; set; }
        public virtual DbSet<FeedNotification> FeedNotifications { get; set; }
        public virtual DbSet<Feed> Feeds { get; set; }
        public virtual DbSet<FlatFee> FlatFees { get; set; }
        public virtual DbSet<GoogleCalendarWatchEvent> GoogleCalendarWatchEvents { get; set; }
        public virtual DbSet<GoogleDataStore> GoogleDataStores { get; set; }
        public virtual DbSet<IntakeFormEntry> IntakeFormEntries { get; set; }
        public virtual DbSet<IntakeForm> IntakeForms { get; set; }
        public virtual DbSet<IntakeFormTemplateMapping> IntakeFormTemplateMappings { get; set; }
        public virtual DbSet<IntakeFormTemplate> IntakeFormTemplates { get; set; }
        public virtual DbSet<Inventory> Inventories { get; set; }
        public virtual DbSet<InventoryTransferItem> InventoryTransferItems { get; set; }
        public virtual DbSet<InventoryTransfer> InventoryTransfers { get; set; }
        public virtual DbSet<Invitee> Invitees { get; set; }
        public virtual DbSet<LawToolboxAccount> LawToolboxAccounts { get; set; }
        public virtual DbSet<LawToolboxCourtRule> LawToolboxCourtRules { get; set; }
        public virtual DbSet<LawToolboxMatter> LawToolboxMatters { get; set; }
        public virtual DbSet<LawToolboxToolset> LawToolboxToolsets { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<MandrillEvent> MandrillEvents { get; set; }
        public virtual DbSet<MerchantApplication> MerchantApplications { get; set; }
        public virtual DbSet<MetaData> MetaDatas { get; set; }
        public virtual DbSet<OAuthClient> OAuthClients { get; set; }
        public virtual DbSet<OAuthRefreshToken> OAuthRefreshTokens { get; set; }
        public virtual DbSet<Opportunity> Opportunities { get; set; }
        public virtual DbSet<Partnership> Partnerships { get; set; }
        public virtual DbSet<PaymentReminder> PaymentReminders { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentSource> PaymentSources { get; set; }
        public virtual DbSet<PaymentWebhookNotification> PaymentWebhookNotifications { get; set; }
        public virtual DbSet<PayPal_IPN> PayPal_IPN { get; set; }
        public virtual DbSet<PendingOnlinePayment> PendingOnlinePayments { get; set; }
        public virtual DbSet<PendingOnlinePaymentSaleDocument> PendingOnlinePaymentSaleDocuments { get; set; }
        public virtual DbSet<Portal> Portals { get; set; }
        public virtual DbSet<PresetProjectRate> PresetProjectRates { get; set; }
        public virtual DbSet<PriceListItem> PriceListItems { get; set; }
        public virtual DbSet<PriceList> PriceLists { get; set; }
        public virtual DbSet<ProductLog> ProductLogs { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductVendor> ProductVendors { get; set; }
        public virtual DbSet<ProjectRate> ProjectRates { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectUser> ProjectUsers { get; set; }
        public virtual DbSet<Recurrence> Recurrences { get; set; }
        public virtual DbSet<RecurringPaymentOccurrence> RecurringPaymentOccurrences { get; set; }
        public virtual DbSet<RecurringPayment> RecurringPayments { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SaleDocumentItem> SaleDocumentItems { get; set; }
        public virtual DbSet<SaleDocumentPaymentReminder> SaleDocumentPaymentReminders { get; set; }
        public virtual DbSet<SaleDocumentPayment> SaleDocumentPayments { get; set; }
        public virtual DbSet<SaleDocument> SaleDocuments { get; set; }
        public virtual DbSet<SaleDocumentTemplate> SaleDocumentTemplates { get; set; }
        public virtual DbSet<SalesTax> SalesTaxes { get; set; }
        public virtual DbSet<Share> Shares { get; set; }
        public virtual DbSet<Shipment> Shipments { get; set; }
        public virtual DbSet<SyncLog> SyncLogs { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<Tenant> Tenants { get; set; }
        public virtual DbSet<TimeEntry> TimeEntries { get; set; }
        public virtual DbSet<Timer> Timers { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }
        public virtual DbSet<UserImapSync> UserImapSyncs { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UtbmsCode> UtbmsCodes { get; set; }
        public virtual DbSet<webpages_Membership> webpages_Membership { get; set; }
        public virtual DbSet<webpages_OAuthMembership> webpages_OAuthMembership { get; set; }
        public virtual DbSet<webpages_Roles> webpages_Roles { get; set; }
        public virtual DbSet<Website> Websites { get; set; }
        public virtual DbSet<WorkflowEvent> WorkflowEvents { get; set; }
        public virtual DbSet<Workflow> Workflows { get; set; }
        public virtual DbSet<WorkflowTask> WorkflowTasks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.AccountId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Blobs)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.AccountId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Contacts)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.AccountId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Conversations)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.InventoryTransfers)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.VendorId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Opportunities)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Partnerships)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Payments)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.AccountId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Payments1)
                .WithOptional(e => e.Account1)
                .HasForeignKey(e => e.CheckPayeeId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.PaymentSources)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.AccountId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.ProductVendors)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.RecurringPayments)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Timers)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Accounts2)
                .Map(m => m.ToTable("AccountsUsersFollowing"));

            modelBuilder.Entity<Account>()
                .HasMany(e => e.AccountUsers)
                .WithMany(e => e.Accounts3)
                .Map(m => m.ToTable("AccountUsers"));

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Tags)
                .WithMany(e => e.Accounts)
                .Map(m => m.ToTable("TagAccounts"));

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.ActivityConditionalTasks)
                .WithOptional(e => e.Activity)
                .HasForeignKey(e => e.Activity_Id);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.ActivityReminders)
                .WithOptional(e => e.Activity)
                .HasForeignKey(e => e.Activity_Id);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Blobs)
                .WithOptional(e => e.Activity)
                .HasForeignKey(e => e.ActivityId);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.SaleDocumentPaymentReminders)
                .WithOptional(e => e.Activity)
                .HasForeignKey(e => e.ActivityEmail_Id);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Shares)
                .WithOptional(e => e.Activity)
                .HasForeignKey(e => e.Activity_Id);

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Contacts)
                .WithMany(e => e.Activities1)
                .Map(m => m.ToTable("ActivityContacts"));

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Users)
                .WithMany(e => e.ActivityUsers)
                .Map(m => m.ToTable("ActivityUsers"));

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Tags)
                .WithMany(e => e.Activities)
                .Map(m => m.ToTable("TagActivities"));

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.BillingAddressId);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Accounts1)
                .WithOptional(e => e.Address1)
                .HasForeignKey(e => e.ShippingAddressId);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Contacts)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.MailingAddressId);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Contacts1)
                .WithOptional(e => e.Address1)
                .HasForeignKey(e => e.OtherAddressId);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Locations)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.Address_Id);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.SaleDocuments)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.ToBillingAddress_Id);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.SaleDocuments1)
                .WithOptional(e => e.Address1)
                .HasForeignKey(e => e.ToShippingAddress_Id);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Shipments)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.Address_Id);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Tenants)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.BillingAddress_Id);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.Address_Id);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<BankAccount>()
                .HasMany(e => e.RecurringPayments)
                .WithRequired(e => e.BankAccount)
                .HasForeignKey(e => e.BankAccount_Guid)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.Logo_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Activities)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.HtmlBody_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Contacts)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.Photo_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.DocumentTemplates)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.Template_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Expenses)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.PrimaryReceipt_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.SaleDocuments)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.PDF_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Shares)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.Blob_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Tenants)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.HeaderLogo_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Tenants1)
                .WithOptional(e => e.Blob1)
                .HasForeignKey(e => e.Logo_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Tenants2)
                .WithOptional(e => e.Blob2)
                .HasForeignKey(e => e.LogoThumbnail_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Blob)
                .HasForeignKey(e => e.Photo_Id);

            modelBuilder.Entity<Blob>()
                .HasMany(e => e.Users1)
                .WithOptional(e => e.Blob1)
                .HasForeignKey(e => e.PhotoThumbnail_Id);

            modelBuilder.Entity<Campaign>()
                .HasMany(e => e.Opportunities)
                .WithOptional(e => e.Campaign)
                .HasForeignKey(e => e.Campaign_Id);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.Categories1)
                .WithOptional(e => e.Category1)
                .HasForeignKey(e => e.ParentCategoryId);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.Expenses)
                .WithOptional(e => e.Category)
                .HasForeignKey(e => e.Category_Id);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.PrimaryContactId);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Activities)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.ContactId);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.Contact_Id);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Addresses1)
                .WithOptional(e => e.Contact1)
                .HasForeignKey(e => e.ContactId);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Blobs)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.ContactId);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.ContactSyncs)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.Contact_Id);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.ConversationMessages)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.FromContact_Id);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.ConversationParticipants)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.Contact_Id);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Conversations)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.CreatedByContact_Id);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.CustomFieldValues)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.ContactId);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.CustomFieldValues1)
                .WithOptional(e => e.Contact1)
                .HasForeignKey(e => e.ValueContactId);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.SaleDocuments)
                .WithOptional(e => e.Contact)
                .HasForeignKey(e => e.Contact_Id);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Shares)
                .WithRequired(e => e.Contact)
                .HasForeignKey(e => e.SharedByContactId);

            modelBuilder.Entity<Conversation>()
                .HasMany(e => e.ConversationMessages)
                .WithOptional(e => e.Conversation)
                .HasForeignKey(e => e.Conversation_Guid);

            modelBuilder.Entity<Conversation>()
                .HasMany(e => e.ConversationParticipants)
                .WithOptional(e => e.Conversation)
                .HasForeignKey(e => e.Conversation_Guid);

            modelBuilder.Entity<CustomField>()
                .HasMany(e => e.CustomFieldValues)
                .WithOptional(e => e.CustomField)
                .WillCascadeOnDelete();

            modelBuilder.Entity<CustomField>()
                .HasMany(e => e.CustomFieldSets)
                .WithMany(e => e.CustomFields)
                .Map(m => m.ToTable("CustomFieldsCustomFieldSets"));

            modelBuilder.Entity<Expens>()
                .HasMany(e => e.Blobs)
                .WithOptional(e => e.Expens)
                .HasForeignKey(e => e.Expense_Id);

            modelBuilder.Entity<Expens>()
                .HasMany(e => e.Blobs1)
                .WithOptional(e => e.Expens1)
                .HasForeignKey(e => e.ExpenseId);

            modelBuilder.Entity<Expens>()
                .HasMany(e => e.Feeds)
                .WithOptional(e => e.Expens)
                .HasForeignKey(e => e.ExpenseId);

            modelBuilder.Entity<Family>()
                .HasMany(e => e.Products)
                .WithOptional(e => e.Family)
                .HasForeignKey(e => e.Family_Id);

            modelBuilder.Entity<Feed>()
                .HasMany(e => e.FeedNotifications)
                .WithOptional(e => e.Feed)
                .HasForeignKey(e => e.Feed_Id);

            modelBuilder.Entity<IntakeForm>()
                .HasMany(e => e.IntakeFormEntries)
                .WithOptional(e => e.IntakeForm)
                .HasForeignKey(e => e.IntakeForm_Guid);

            modelBuilder.Entity<IntakeFormTemplate>()
                .HasMany(e => e.IntakeForms)
                .WithOptional(e => e.IntakeFormTemplate)
                .HasForeignKey(e => e.Template_Guid);

            modelBuilder.Entity<IntakeFormTemplate>()
                .HasMany(e => e.IntakeFormTemplateMappings)
                .WithOptional(e => e.IntakeFormTemplate)
                .HasForeignKey(e => e.IntakeFormTemplate_Guid);

            modelBuilder.Entity<Inventory>()
                .HasMany(e => e.ProductLogs)
                .WithOptional(e => e.Inventory)
                .HasForeignKey(e => e.Inventory_Id);

            modelBuilder.Entity<InventoryTransfer>()
                .HasMany(e => e.InventoryTransferItems)
                .WithOptional(e => e.InventoryTransfer)
                .HasForeignKey(e => e.InventoryTransfer_Id);

            modelBuilder.Entity<InventoryTransfer>()
                .HasMany(e => e.Shipments)
                .WithOptional(e => e.InventoryTransfer)
                .HasForeignKey(e => e.InventoryTransfer_Id);

            modelBuilder.Entity<LawToolboxAccount>()
                .HasMany(e => e.LawToolboxToolsets)
                .WithOptional(e => e.LawToolboxAccount)
                .HasForeignKey(e => e.LawToolboxAccount_Id);

            modelBuilder.Entity<LawToolboxCourtRule>()
                .HasMany(e => e.Activities)
                .WithOptional(e => e.LawToolboxCourtRule)
                .HasForeignKey(e => e.LawToolboxCourtRule_Id);

            modelBuilder.Entity<LawToolboxMatter>()
                .HasMany(e => e.LawToolboxCourtRules)
                .WithOptional(e => e.LawToolboxMatter)
                .HasForeignKey(e => e.LawToolboxMatter_Id);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Locations)
                .Map(m => m.ToTable("LocationUsers"));

            modelBuilder.Entity<PaymentReminder>()
                .HasMany(e => e.SaleDocumentPaymentReminders)
                .WithRequired(e => e.PaymentReminder)
                .HasForeignKey(e => e.PaymentReminder_Guid);

            modelBuilder.Entity<Payment>()
                .HasMany(e => e.Expenses)
                .WithOptional(e => e.Payment)
                .HasForeignKey(e => e.HardCostPayment_Id);

            modelBuilder.Entity<Payment>()
                .HasMany(e => e.PaymentWebhookNotifications)
                .WithOptional(e => e.Payment)
                .HasForeignKey(e => e.Payment_Id);

            modelBuilder.Entity<Payment>()
                .HasMany(e => e.RecurringPaymentOccurrences)
                .WithOptional(e => e.Payment)
                .HasForeignKey(e => e.Payment_Id);

            modelBuilder.Entity<Payment>()
                .HasMany(e => e.Shares)
                .WithOptional(e => e.Payment)
                .HasForeignKey(e => e.Payment_Id);

            modelBuilder.Entity<PaymentSource>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.PaymentSource)
                .HasForeignKey(e => e.DefaultPaymentSourceGuid);

            modelBuilder.Entity<PaymentSource>()
                .HasMany(e => e.Projects)
                .WithOptional(e => e.PaymentSource)
                .HasForeignKey(e => e.DefaultPaymentSourceGuid);

            modelBuilder.Entity<PaymentSource>()
                .HasMany(e => e.RecurringPayments)
                .WithRequired(e => e.PaymentSource)
                .HasForeignKey(e => e.PaymentSource_Guid)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Portal>()
                .HasMany(e => e.Contacts)
                .WithOptional(e => e.Portal)
                .HasForeignKey(e => e.Portal_Id);

            modelBuilder.Entity<PresetProjectRate>()
                .HasMany(e => e.ProjectRates)
                .WithOptional(e => e.PresetProjectRate)
                .HasForeignKey(e => e.PresetProjectRate_Id);

            modelBuilder.Entity<PresetProjectRate>()
                .HasMany(e => e.Projects)
                .WithOptional(e => e.PresetProjectRate)
                .HasForeignKey(e => e.PresetProjectRate_Id);

            modelBuilder.Entity<PriceList>()
                .HasMany(e => e.Locations)
                .WithOptional(e => e.PriceList)
                .HasForeignKey(e => e.PriceList_Id);

            modelBuilder.Entity<PriceList>()
                .HasMany(e => e.PriceListItems)
                .WithRequired(e => e.PriceList)
                .HasForeignKey(e => e.PriceList_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PriceList>()
                .HasMany(e => e.Websites)
                .WithOptional(e => e.PriceList)
                .HasForeignKey(e => e.PriceList_Id);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.Expenses)
                .WithOptional(e => e.Product)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Product>()
                .HasMany(e => e.InventoryTransferItems)
                .WithOptional(e => e.Product)
                .HasForeignKey(e => e.Product_Id);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.Partnerships)
                .WithOptional(e => e.Product)
                .HasForeignKey(e => e.Product_Id);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.PriceListItems)
                .WithOptional(e => e.Product)
                .HasForeignKey(e => e.Product_Id);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductLogs)
                .WithOptional(e => e.Product)
                .HasForeignKey(e => e.Product_Id);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductVendors)
                .WithOptional(e => e.Product)
                .HasForeignKey(e => e.Product_Id);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.RecurringPayments)
                .WithOptional(e => e.Product)
                .HasForeignKey(e => e.Item_Id);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Attachments)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.Project_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.ContactSyncs)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.Project_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Conversations)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.Project_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.PaymentSources)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.ProjectId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.ProjectRates)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.Project_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.RecurringPayments)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.Project_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.SaleDocumentItems)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.Project_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Timers)
                .WithOptional(e => e.Project)
                .HasForeignKey(e => e.Project_Id)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Projects3)
                .Map(m => m.ToTable("ProjectsUsersFollowing"));

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Tags)
                .WithMany(e => e.Projects)
                .Map(m => m.ToTable("TagProjects"));

            modelBuilder.Entity<RecurringPayment>()
                .HasMany(e => e.RecurringPaymentOccurrences)
                .WithRequired(e => e.RecurringPayment)
                .HasForeignKey(e => e.RecurringPayment_Guid);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.ProjectRates)
                .WithOptional(e => e.Role)
                .HasForeignKey(e => e.Role_Id);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Role)
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<SaleDocumentItem>()
                .Property(e => e.Tax1Data_Rate)
                .HasPrecision(16, 3);

            modelBuilder.Entity<SaleDocumentItem>()
                .Property(e => e.Tax2Data_Rate)
                .HasPrecision(16, 3);

            modelBuilder.Entity<SaleDocumentItem>()
                .Property(e => e.Tax1)
                .HasPrecision(18, 8);

            modelBuilder.Entity<SaleDocumentItem>()
                .Property(e => e.Tax2)
                .HasPrecision(18, 8);

            modelBuilder.Entity<SaleDocument>()
                .HasMany(e => e.Activities)
                .WithOptional(e => e.SaleDocument)
                .HasForeignKey(e => e.SaleDocument_Id);

            modelBuilder.Entity<SaleDocument>()
                .HasMany(e => e.Credits)
                .WithRequired(e => e.SaleDocument)
                .HasForeignKey(e => e.CreditForSaleDocument_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SaleDocument>()
                .HasMany(e => e.ProductLogs)
                .WithOptional(e => e.SaleDocument)
                .HasForeignKey(e => e.Invoice_Id);

            modelBuilder.Entity<SaleDocument>()
                .HasMany(e => e.RecurringPaymentOccurrences)
                .WithOptional(e => e.SaleDocument)
                .HasForeignKey(e => e.Invoice_Id);

            modelBuilder.Entity<SaleDocument>()
                .HasMany(e => e.SaleDocumentPaymentReminders)
                .WithRequired(e => e.SaleDocument)
                .HasForeignKey(e => e.SaleDocument_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SaleDocument>()
                .HasMany(e => e.Shares)
                .WithOptional(e => e.SaleDocument)
                .HasForeignKey(e => e.SaleDocument_Id);

            modelBuilder.Entity<SaleDocument>()
                .HasMany(e => e.Shipments)
                .WithOptional(e => e.SaleDocument)
                .HasForeignKey(e => e.Invoice_Id);

            modelBuilder.Entity<SaleDocumentTemplate>()
                .HasMany(e => e.Tenants)
                .WithOptional(e => e.SaleDocumentTemplate)
                .HasForeignKey(e => e.SaleDocumentTemplate_Id);

            modelBuilder.Entity<SalesTax>()
                .Property(e => e.Rate)
                .HasPrecision(16, 3);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.Expenses)
                .WithOptional(e => e.SalesTax)
                .HasForeignKey(e => e.Tax1Id);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.Expenses1)
                .WithOptional(e => e.SalesTax1)
                .HasForeignKey(e => e.Tax2Id);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.Products)
                .WithOptional(e => e.SalesTax)
                .HasForeignKey(e => e.Tax1Id);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.Products1)
                .WithOptional(e => e.SalesTax1)
                .HasForeignKey(e => e.Tax2Id);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.RecurringPayments)
                .WithOptional(e => e.SalesTax)
                .HasForeignKey(e => e.Tax1_Id);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.RecurringPayments1)
                .WithOptional(e => e.SalesTax1)
                .HasForeignKey(e => e.Tax2_Id);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.SaleDocumentItems)
                .WithOptional(e => e.SalesTax)
                .HasForeignKey(e => e.Tax2Ref_Id);

            modelBuilder.Entity<SalesTax>()
                .HasMany(e => e.SalesTaxes1)
                .WithOptional(e => e.SalesTax1)
                .HasForeignKey(e => e.TaxGroupId);

            modelBuilder.Entity<Tag>()
                .HasMany(e => e.WorkflowEvents)
                .WithMany(e => e.Tags)
                .Map(m => m.ToTable("TagWorkflowEvents"));

            modelBuilder.Entity<Tag>()
                .HasMany(e => e.WorkflowTasks)
                .WithMany(e => e.Tags)
                .Map(m => m.ToTable("TagWorkflowTasks"));

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.Tenant)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Activities)
                .WithOptional(e => e.Tenant)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Blobs)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Campaigns)
                .WithOptional(e => e.Tenant)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.ChatMessages)
                .WithRequired(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Contacts)
                .WithOptional(e => e.Tenant)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.CustomFieldSets)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.CustomFieldTabs)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Families)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.FlatFees)
                .WithRequired(e => e.Tenant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Inventories)
                .WithOptional(e => e.Tenant)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.InventoryTransferItems)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.InventoryTransfers)
                .WithOptional(e => e.Tenant)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Locations)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.MerchantApplications)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.OAuthClients)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Opportunities)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Partnerships)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.PaymentReminders)
                .WithRequired(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Payments)
                .WithRequired(e => e.Tenant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.PaymentSources)
                .WithRequired(e => e.Tenant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.PresetProjectRates)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.PriceListItems)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.PriceLists)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.ProductLogs)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Products)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.ProductVendors)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.RecurringPayments)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.SaleDocuments)
                .WithOptional(e => e.Tenant)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.SaleDocumentTemplates)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.SalesTaxes)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Shipments)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.SyncLogs)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Tags)
                .WithRequired(e => e.Tenant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.TimeEntries)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Timers)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.UserGroups)
                .WithOptional(e => e.Tenant)
                .HasForeignKey(e => e.Tenant_Id);

            modelBuilder.Entity<Tenant>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Tenants3)
                .Map(m => m.ToTable("TenantUsers"));

            modelBuilder.Entity<UserGroup>()
                .HasMany(e => e.Users)
                .WithMany(e => e.UserGroups2)
                .Map(m => m.ToTable("UserGroupUsers"));

            modelBuilder.Entity<User>()
                .HasMany(e => e.AccountingAccounts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AccountingAccounts1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AccountProjectLinks)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AccountProjectLinks1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Accounts1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Activities)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Activities1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ActivityReminders)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Addresses1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AspNetUsers)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Attachments)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Attachments1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BankAccounts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BankAccounts1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Blobs)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Blobs1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Blobs2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CalendarColors)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CalendarUserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CalendarColors1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Campaigns)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Campaigns1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Campaigns2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.Owner_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Categories)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Categories1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Contacts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Contacts1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Contacts2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.OwnerId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ConversationMessages)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.DeletedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ConversationMessages1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.FromUser_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ConversationParticipants)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Conversations)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedByUser_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Conversations1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.DeletedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CustomFields)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CustomFields1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CustomFieldSets)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CustomFieldSets1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CustomFieldTabs)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CustomFieldTabs1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DefaultReminders)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DefaultReminders1)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DocumentTemplates)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DocumentTemplates1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Expenses)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Expenses1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Expenses2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.FeedNotifications)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Feeds)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Feeds1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Feeds2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.FlatFees)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.FlatFees1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.FlatFees2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.IntakeForms)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.IntakeForms1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.IntakeFormTemplates)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.AssignNewRecordsTo_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.IntakeFormTemplates1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.IntakeFormTemplates2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.CreateTaskOnSubmissionAssignedTo_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.IntakeFormTemplates3)
                .WithOptional(e => e.User3)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.IntakeFormTemplates4)
                .WithOptional(e => e.User4)
                .HasForeignKey(e => e.SendEmailOnSubmissionTo_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Inventories)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Inventories1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.InventoryTransfers)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.InventoryTransfers1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.InventoryTransfers2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.ReceivedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.InventoryTransfers3)
                .WithOptional(e => e.User3)
                .HasForeignKey(e => e.ReviewedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.InventoryTransfers4)
                .WithOptional(e => e.User4)
                .HasForeignKey(e => e.SentBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Invitees)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.LawToolboxAccounts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.LawToolboxCourtRules)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.LawToolboxCourtRules1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.MerchantApplications)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.SubmittedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.OAuthClients)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Opportunities)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Opportunities1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Partnerships)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Partnerships1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PaymentReminders)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PaymentReminders1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Payments)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Payments1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Payments2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.ReconciledBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PaymentSources)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedById)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PendingOnlinePayments)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Portals)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PresetProjectRates)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PresetProjectRates1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PriceLists)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PriceLists1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ProductLogs)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Products)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Products1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ProductVendors)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ProductVendors1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ProjectRates)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Projects)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Projects1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Projects2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.OriginatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.RecurringPayments)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.BilledBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.RecurringPayments1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.RecurringPayments2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Roles)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Roles1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SaleDocumentItems)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SaleDocumentItems1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SaleDocuments)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.ApprovedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SaleDocuments1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SaleDocuments2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SaleDocumentTemplates)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SaleDocumentTemplates1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SalesTaxes)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SalesTaxes1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Shares)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.SharedByUser_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Shipments)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Shipments1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SyncLogs)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tags)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tags1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tenants)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tenants1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tenants2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.PrimaryUserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TimeEntries)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TimeEntries1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TimeEntries2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<User>()
                .HasMany(e => e.Timers)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserGroups)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserGroups1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Users1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Users11)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Workflows)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Workflows1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.LastModifiedBy_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Users12)
                .WithMany(e => e.Users)
                .Map(m => m.ToTable("UsersUsersFollowing").MapRightKey("User_Id1"));

            modelBuilder.Entity<User>()
                .HasMany(e => e.WorkflowEvents)
                .WithMany(e => e.Users)
                .Map(m => m.ToTable("UserWorkflowEvents"));

            modelBuilder.Entity<User>()
                .HasMany(e => e.WorkflowTasks)
                .WithMany(e => e.Users)
                .Map(m => m.ToTable("UserWorkflowTasks"));

            modelBuilder.Entity<User>()
                .HasMany(e => e.webpages_Roles)
                .WithMany(e => e.Users)
                .Map(m => m.ToTable("webpages_RolesUser"));
        }
    }
}
