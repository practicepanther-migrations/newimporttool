namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ActivityConditionalTask
    {
        public long Id { get; set; }

        public Guid Guid { get; set; }

        public Guid ActivityTaskGuid { get; set; }

        public bool IsConditionalTask { get; set; }

        public long ParentId { get; set; }

        public long? WorkflowId { get; set; }

        public string Description { get; set; }

        public int NumOfDays { get; set; }

        public string Subject { get; set; }

        public int DueDateType { get; set; }

        public int BeforeOrAfter { get; set; }

        public long? Activity_Id { get; set; }

        public virtual Activity Activity { get; set; }
    }
}
