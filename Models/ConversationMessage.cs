namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ConversationMessage
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ConversationMessage()
        {
            Blobs = new HashSet<Blob>();
        }

        [Key]
        public Guid Guid { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Body { get; set; }

        public Guid? Conversation_Guid { get; set; }

        public long? FromContact_Id { get; set; }

        public int? FromUser_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedDate { get; set; }

        public int? DeletedBy_Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Conversation Conversation { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
