namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LawToolboxToolset
    {
        public Guid Id { get; set; }

        public bool IsDefault { get; set; }

        public string LawToolboxToolsetId { get; set; }

        public string LawToolboxToolsetName { get; set; }

        public string StateId { get; set; }

        public long? TenantId { get; set; }

        public Guid? LawToolboxAccount_Id { get; set; }

        public virtual LawToolboxAccount LawToolboxAccount { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
