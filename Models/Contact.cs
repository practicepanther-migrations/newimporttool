namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Contact
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Contact()
        {
            AccountProjectLinks = new HashSet<AccountProjectLink>();
            Accounts = new HashSet<Account>();
            Activities = new HashSet<Activity>();
            Addresses = new HashSet<Address>();
            Addresses1 = new HashSet<Address>();
            Blobs = new HashSet<Blob>();
            ContactSyncs = new HashSet<ContactSync>();
            ConversationMessages = new HashSet<ConversationMessage>();
            ConversationParticipants = new HashSet<ConversationParticipant>();
            Conversations = new HashSet<Conversation>();
            CustomFieldValues = new HashSet<CustomFieldValue>();
            CustomFieldValues1 = new HashSet<CustomFieldValue>();
            Feeds = new HashSet<Feed>();
            Invitees = new HashSet<Invitee>();
            SaleDocuments = new HashSet<SaleDocument>();
            Shares = new HashSet<Share>();
            Activities1 = new HashSet<Activity>();
        }

        public long Id { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [StringLength(50)]
        public string Home { get; set; }

        [StringLength(50)]
        public string Office { get; set; }

        [StringLength(10)]
        public string Ext { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(10)]
        public string Salutation { get; set; }

        [StringLength(50)]
        public string Position { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? TenantId { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public long? MailingAddressId { get; set; }

        public long? OtherAddressId { get; set; }

        public int Gender { get; set; }

        public DateTime? Anniversary { get; set; }

        public long? AccountId { get; set; }

        public int Type { get; set; }

        public DateTime? ConversionDate { get; set; }

        [StringLength(50)]
        public string SkypeId { get; set; }

        [StringLength(50)]
        public string Twitter { get; set; }

        public int LeadStatus { get; set; }

        [StringLength(50)]
        public string LeadCompanyName { get; set; }

        public int? OwnerId { get; set; }

        public long? CampaignId { get; set; }

        public long? Photo_Id { get; set; }

        public Guid Guid { get; set; }

        public string Notes { get; set; }

        [StringLength(50)]
        public string JobTitle { get; set; }

        public bool IsEmailOptOut { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(10)]
        public string Prefix { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        //[Required]
        [StringLength(101)]
        public string FullName { get; set; }

        public long? Portal_Id { get; set; }

        [StringLength(255)]
        public string DisplayName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountProjectLink> AccountProjectLinks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses1 { get; set; }

        public virtual Address Address { get; set; }

        public virtual Address Address1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Campaign Campaign { get; set; }

        public virtual Portal Portal { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContactSync> ContactSyncs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversationMessage> ConversationMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversationParticipant> ConversationParticipants { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversation> Conversations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldValue> CustomFieldValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldValue> CustomFieldValues1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invitee> Invitees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Share> Shares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities1 { get; set; }
    }
}
