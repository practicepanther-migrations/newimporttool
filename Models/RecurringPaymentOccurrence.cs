namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RecurringPaymentOccurrence
    {
        [Key]
        public Guid Guid { get; set; }

        [StringLength(25)]
        public string LawPayTokenId { get; set; }

        public decimal Amount { get; set; }

        public int Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public DateTime DueDate { get; set; }

        public int Attempts { get; set; }

        public DateTime? LastAttempt { get; set; }

        public int? Invoice_Id { get; set; }

        public long? Payment_Id { get; set; }

        public Guid RecurringPayment_Guid { get; set; }

        public string HeadNoteId { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual RecurringPayment RecurringPayment { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }
    }
}
