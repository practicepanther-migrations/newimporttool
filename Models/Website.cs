namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Website
    {
        public long Id { get; set; }

        public long TenantId { get; set; }

        public string URI { get; set; }

        public long? PriceList_Id { get; set; }

        public virtual PriceList PriceList { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
