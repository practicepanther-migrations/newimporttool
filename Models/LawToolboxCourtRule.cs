namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LawToolboxCourtRule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LawToolboxCourtRule()
        {
            Activities = new HashSet<Activity>();
        }

        public Guid Id { get; set; }

        public long ProjectId { get; set; }

        public long AccountId { get; set; }

        public long? TenantId { get; set; }

        public string Name { get; set; }

        public string ToolsetName { get; set; }

        public string ToolsetId { get; set; }

        public string TriggerName { get; set; }

        public string TriggerId { get; set; }

        public DateTime? TriggerDate { get; set; }

        public string ToggleOptionId { get; set; }

        public string ToggleId { get; set; }

        public string ToggleDescription { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public Guid? LawToolboxMatter_Id { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        public virtual LawToolboxMatter LawToolboxMatter { get; set; }

        public virtual Project Project { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
