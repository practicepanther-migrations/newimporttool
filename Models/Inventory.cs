namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Inventory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inventory()
        {
            ProductLogs = new HashSet<ProductLog>();
        }

        public long Id { get; set; }

        public decimal QtyAvailable { get; set; }

        public decimal QtyOnHold { get; set; }

        public decimal QtyOnPurchaseOrder { get; set; }

        public long? ProductId { get; set; }

        public long? LocationId { get; set; }

        public long? TenantId { get; set; }

        public decimal ReorderPoint { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public virtual Location Location { get; set; }

        public virtual Product Product { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductLog> ProductLogs { get; set; }
    }
}
