namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tenant
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tenant()
        {
            AccountingAccounts = new HashSet<AccountingAccount>();
            Accounts = new HashSet<Account>();
            Activities = new HashSet<Activity>();
            Addresses = new HashSet<Address>();
            BankAccounts = new HashSet<BankAccount>();
            Blobs = new HashSet<Blob>();
            Campaigns = new HashSet<Campaign>();
            Categories = new HashSet<Category>();
            ChatMessages = new HashSet<ChatMessage>();
            Contacts = new HashSet<Contact>();
            Conversations = new HashSet<Conversation>();
            CustomFields = new HashSet<CustomField>();
            CustomFieldSets = new HashSet<CustomFieldSet>();
            CustomFieldTabs = new HashSet<CustomFieldTab>();
            DocumentTemplates = new HashSet<DocumentTemplate>();
            Expenses = new HashSet<Expens>();
            Families = new HashSet<Family>();
            Feeds = new HashSet<Feed>();
            FlatFees = new HashSet<FlatFee>();
            IntakeForms = new HashSet<IntakeForm>();
            IntakeFormTemplates = new HashSet<IntakeFormTemplate>();
            Inventories = new HashSet<Inventory>();
            InventoryTransferItems = new HashSet<InventoryTransferItem>();
            InventoryTransfers = new HashSet<InventoryTransfer>();
            LawToolboxAccounts = new HashSet<LawToolboxAccount>();
            LawToolboxCourtRules = new HashSet<LawToolboxCourtRule>();
            LawToolboxToolsets = new HashSet<LawToolboxToolset>();
            Locations = new HashSet<Location>();
            MerchantApplications = new HashSet<MerchantApplication>();
            OAuthClients = new HashSet<OAuthClient>();
            Opportunities = new HashSet<Opportunity>();
            Partnerships = new HashSet<Partnership>();
            PaymentReminders = new HashSet<PaymentReminder>();
            Payments = new HashSet<Payment>();
            PaymentSources = new HashSet<PaymentSource>();
            PresetProjectRates = new HashSet<PresetProjectRate>();
            PriceListItems = new HashSet<PriceListItem>();
            PriceLists = new HashSet<PriceList>();
            ProductLogs = new HashSet<ProductLog>();
            Products = new HashSet<Product>();
            ProductVendors = new HashSet<ProductVendor>();
            Projects = new HashSet<Project>();
            Recurrences = new HashSet<Recurrence>();
            RecurringPayments = new HashSet<RecurringPayment>();
            Roles = new HashSet<Role>();
            SaleDocumentItems = new HashSet<SaleDocumentItem>();
            SaleDocuments = new HashSet<SaleDocument>();
            SaleDocumentTemplates = new HashSet<SaleDocumentTemplate>();
            SalesTaxes = new HashSet<SalesTax>();
            Shipments = new HashSet<Shipment>();
            SyncLogs = new HashSet<SyncLog>();
            Tags = new HashSet<Tag>();
            TimeEntries = new HashSet<TimeEntry>();
            Timers = new HashSet<Timer>();
            UserGroups = new HashSet<UserGroup>();
            Websites = new HashSet<Website>();
            Workflows = new HashSet<Workflow>();
            Users = new HashSet<User>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public long StorageMaxContainerSize { get; set; }

        public long StorageCurrentContainerSize { get; set; }

        public int MaxActiveUsers { get; set; }

        public int MaxInvoicesPerMonth { get; set; }

        public int MaxAccounts { get; set; }

        public bool IsInventoryManagementEnabled { get; set; }

        public bool IsMarketingEnabled { get; set; }

        public int MaxMarketingEmailsPerMonth { get; set; }

        public string ChargifySubscriptionId { get; set; }

        public string ChargifyCustomerId { get; set; }

        public string ChargifyProductId { get; set; }

        public string ChargifyProductHandle { get; set; }

        [StringLength(200)]
        public string BillingEmailAddress { get; set; }

        public long? BillingAddress_Id { get; set; }

        public int ChargifySubscriptionStatus { get; set; }

        public string AuthorizeNetLoginId { get; set; }

        public string AuthorizeNetTransactionKey { get; set; }

        public bool IsPayPalEnabled { get; set; }

        public string PayPalEmailAddress { get; set; }

        public long? Logo_Id { get; set; }

        [StringLength(30)]
        public string TimeZoneId { get; set; }

        [StringLength(10)]
        public string CultureName { get; set; }

        public int? Language { get; set; }

        public int? PrimaryUserId { get; set; }

        public long? LogoThumbnail_Id { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [StringLength(50)]
        public string Home { get; set; }

        [StringLength(50)]
        public string Office { get; set; }

        [StringLength(10)]
        public string Ext { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        public string DefaultInvoiceTermsAndConditions { get; set; }

        public string DefaultQuoteTermsAndConditions { get; set; }

        public string EmailInvoiceTemplate { get; set; }

        public string EmailQuoteTemplate { get; set; }

        public string EmailPaymentTemplate { get; set; }

        public string EmailInvoiceReminderTemplate { get; set; }

        public int EmailInvoiceReminderFrequency { get; set; }

        public int EmailInvoiceReminderMaxTimes { get; set; }

        public string EmailQuoteReminderTemplate { get; set; }

        public int EmailQuoteReminderFrequency { get; set; }

        public int EmailQuoteReminderMaxTimes { get; set; }

        public Guid Guid { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        [StringLength(200)]
        public string Website { get; set; }

        [StringLength(200)]
        public string EmailInvoiceSubjectTemplate { get; set; }

        [StringLength(200)]
        public string EmailQuoteSubjectTemplate { get; set; }

        [StringLength(200)]
        public string EmailPaymentSubjectTemplate { get; set; }

        public string PayPalFirstName { get; set; }

        public string PayPalLastName { get; set; }

        public bool IsWizardComplete { get; set; }

        public bool IsAuthorizeNetEnabled { get; set; }

        public bool IsAuthorizeNetTestMode { get; set; }

        public bool IsStripeEnabled { get; set; }

        public string StripeSecretKey { get; set; }

        public string StripePublishableKey { get; set; }

        [StringLength(3)]
        public string CurrencyCode { get; set; }

        [StringLength(7)]
        public string HeaderColor { get; set; }

        public long? HeaderLogo_Id { get; set; }

        public int Type { get; set; }

        [StringLength(200)]
        public string EmailInvoiceReminderSubjectTemplate { get; set; }

        [StringLength(200)]
        public string EmailQuoteReminderSubjectTemplate { get; set; }

        public bool IsAllowClientsToViewAllOutstandingInvoices { get; set; }

        public bool IsAllowClientsToViewAllPayments { get; set; }

        public bool IsAllowClientsToViewAccountBalances { get; set; }

        public bool IsAllowClientToViewAllPaidInvoices { get; set; }

        [StringLength(255)]
        public string GA_CampaignContent { get; set; }

        [StringLength(255)]
        public string GA_CampaignMedium { get; set; }

        [StringLength(255)]
        public string GA_CampaignName { get; set; }

        [StringLength(255)]
        public string GA_CampaignSource { get; set; }

        [StringLength(255)]
        public string GA_CampaignTerm { get; set; }

        public DateTime? FirstContactDateTime { get; set; }

        [StringLength(100)]
        public string AmbassadorReferringShortCode { get; set; }

        public string UrlReferrer { get; set; }

        public int? AmbassadorCampaignId { get; set; }

        public string AmbassadorDiscountValue { get; set; }

        public bool IsAmbassadorDiscountValueUsed { get; set; }

        [StringLength(50)]
        public string BoxFolderId { get; set; }

        public string BoxAuthToken { get; set; }

        public bool IsBoxIntegrationEnabled { get; set; }

        public bool IsQuickbooksIntegrationEnabled { get; set; }

        public string QuickbooksAuthToken { get; set; }

        public bool IsXeroIntegrationEnabled { get; set; }

        public string XeroAuthToken { get; set; }

        public string QuickbooksBankOperatingAccountId { get; set; }

        public Guid? XeroBankOperatingAccountId { get; set; }

        public long? SaleDocumentTemplate_Id { get; set; }

        public bool EvergreenIsOnDefault { get; set; }

        public decimal EvergreenAmountDefault { get; set; }

        public bool LedesIsEnabled { get; set; }

        [StringLength(20)]
        public string TaxpayerId { get; set; }

        public bool UtbmsIsEnabled { get; set; }

        public bool UtbmsIsABA_Bankruptcy { get; set; }

        public bool UtbmsIsABA_Counselling { get; set; }

        public bool UtbmsIsABA_Litigation { get; set; }

        public bool UtbmsIsABA_Project { get; set; }

        public bool UtbmsIsABA_WorkersComp { get; set; }

        public bool UtbmsIsLOC_Ediscovery { get; set; }

        public bool UtbmsIsLOC_Grc { get; set; }

        public bool UtbmsIsLOC_Patent { get; set; }

        public bool UtbmsIsLOC_Project { get; set; }

        public bool UtbmsIsLOC_Trademark { get; set; }

        public bool UtbmsIsEW_Civil_Litigation { get; set; }

        public bool IsChatEnabled { get; set; }

        [StringLength(20)]
        public string SfAccountOwnerId { get; set; }

        public bool IsPortalEnabled { get; set; }

        [StringLength(200)]
        public string EmailPaymentRequestSubjectTemplate { get; set; }

        public string EmailPaymentRequestTemplate { get; set; }

        public bool IsLawPayEnabled { get; set; }

        public string LawPaySecretKey { get; set; }

        public string LawPayPublicKey { get; set; }

        public bool IsLawPayOperatingMerchantEnabled { get; set; }

        public string LawPayOperatingMerchantAccountId { get; set; }

        public bool IsLawPayOperatingAchEnabled { get; set; }

        public string LawPayOperatingAchAccountId { get; set; }

        public bool IsLawPayTrustMerchantEnabled { get; set; }

        public string LawPayTrustMerchantAccountId { get; set; }

        public bool IsLawPayTrustAchEnabled { get; set; }

        public string LawPayTrustAchAccountId { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public string ConfirmEmailUrl { get; set; }

        [StringLength(1000)]
        public string GA_LandingPage { get; set; }

        public bool SaleDocument_Default_IsSendInvoicePaymentReminders { get; set; }

        public bool SaleDocument_Default_IsSendQuoteApprovalReminders { get; set; }

        public bool SaleDocument_Default_IsUseCreditToPayBalance { get; set; }

        public bool SaleDocument_Default_IsUseTrustToPayBalance { get; set; }

        public bool SaleDocument_Default_IsAllowOnlinePayment { get; set; }

        public bool SaleDocument_Default_IsAllowPartialPayment { get; set; }

        public bool SaleDocument_Default_IsCarryForwardBalance { get; set; }

        public Guid? SaleDocument_Default_Tax1Guid { get; set; }

        public Guid? SaleDocument_Default_Tax2Guid { get; set; }

        public int SaleDocument_Default_DueDate_Days { get; set; }

        public int SaleDocument_Default_DueDate_Option { get; set; }

        [StringLength(50)]
        public string DropboxFolderId { get; set; }

        public string DropboxAuthToken { get; set; }

        public bool IsDropboxIntegrationEnabled { get; set; }

        public string PortalHeaderText { get; set; }

        public bool IsRequirePortalLoginToViewInvoice { get; set; }

        public DateTime? LastRecycleBinEmptyDate { get; set; }

        public int? LastRecycleBinEmptyById { get; set; }

        public int ProjectDisplayNameFormat { get; set; }

        public int AccountDisplayNameFormat { get; set; }

        public int UserDisplayNameFormat { get; set; }

        public bool IsAutoNumberingEnabledForAccounts { get; set; }

        public bool IsAutoNumberingEnabledForProjects { get; set; }

        public bool DefaultSyncEmailsForAccount { get; set; }

        public bool DefaultSyncFilesForAccount { get; set; }

        public bool DefaultSyncEmailsForProject { get; set; }

        public bool DefaultSyncFilesForProject { get; set; }

        public bool DefaultSyncContactsForAccount { get; set; }

        public bool DefaultSyncContactsForProject { get; set; }

        public bool UtbmsIsRequired { get; set; }

        public string LawPayAccessToken { get; set; }

        public DateTime? ConversionDate { get; set; }

        [StringLength(255)]
        public string AuthorizeNetPublicKey { get; set; }

        public bool DefaultInterest_IsChargeInterest { get; set; }

        public decimal DefaultInterest_AnnualPercent { get; set; }

        public int DefaultInterest_InterestType { get; set; }

        public int DefaultInterest_ChargeEachNumOfDays { get; set; }

        public bool IsCalendarRulesEnabled { get; set; }

        public string CalendarRulesLoginToken { get; set; }

        public int SaleDocument_Default_CarryForwardBalanceType { get; set; }

        public bool IsMailchimpIntegrationEnabled { get; set; }

        [StringLength(255)]
        public string MailchimpApiKey { get; set; }

        [StringLength(255)]
        public string MailchimpListId { get; set; }

        public bool IsAppComfirmedSignup { get; set; }

        public bool IsCalendarRulesServiceStarted { get; set; }

        public bool IsDeveloperAccount { get; set; }

        [StringLength(1000)]
        public string BoxSharedFolderUrl { get; set; }

        [StringLength(100)]
        public string GoogleDriveFolderId { get; set; }

        public bool IsGoogleDriveIntegrationEnabled { get; set; }

        public string OneDriveAuthToken { get; set; }

        [StringLength(100)]
        public string OneDriveFolderId { get; set; }

        public bool IsOneDriveIntegrationEnabled { get; set; }

        [StringLength(255)]
        public string GA_ClientId { get; set; }

        [StringLength(255)]
        public string GA_CampaignClickId { get; set; }

        [StringLength(255)]
        public string Retargeting_CampaignName { get; set; }

        [StringLength(255)]
        public string Retargeting_CampaignSource { get; set; }

        [StringLength(255)]
        public string Retargeting_CampaignContent { get; set; }

        [StringLength(255)]
        public string Retargeting_CampaignMedium { get; set; }

        public bool IsDefaultSubmitInvoicesForApproval { get; set; }

        public bool IsUtbmsDefaultForNewAccounts { get; set; }

        public Guid? BoxOwnerUserGuid { get; set; }

        public string QuickbooksRealmId { get; set; }

        public string QuickbooksRefreshToken { get; set; }

        [StringLength(255)]
        public string GA_Gclid { get; set; }

        public bool IsDuplicateContactCheckOn { get; set; }

        public bool IsHeadNoteEnabled { get; set; }

        public int? HeadNoteStatus { get; set; }

        public bool IsAuthorizedSignOn { get; set; }

        public string federal_tax_id { get; set; }

        public string structure { get; set; }

        public DateTime? business_incorporated_date { get; set; }

        public string business_incorporated_state { get; set; }

        public string owner_email { get; set; }

        public string owner_first_name { get; set; }

        public string owner_last_name { get; set; }

        public string owner_social_security_number { get; set; }

        public string owner_address1 { get; set; }

        public string owner_address2 { get; set; }

        public string owner_city { get; set; }

        public string owner_state { get; set; }

        public string owner_zip_code { get; set; }

        public string owner_phone_number { get; set; }

        public string owner_office_phone_number { get; set; }

        public string Owner_Bar_Number { get; set; }

        public string Owner_Bar_State { get; set; }

        public decimal? Owner_percentage_Ownership { get; set; }

        public bool IgnoredUpdateYourInfo { get; set; }

        public DateTime? HeadNoteVerficationDate { get; set; }

        public DateTime? LastDateRecurringPaymentMigrationNotice { get; set; }

        public string NumberOfAttorneys { get; set; }

        public string PracticeAreas { get; set; }

        public string Country { get; set; }

        public bool IsDisableEcheck { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountingAccount> AccountingAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses { get; set; }

        public virtual Address Address { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BankAccount> BankAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blob> Blobs { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Blob Blob1 { get; set; }

        public virtual Blob Blob2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Campaign> Campaigns { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Category> Categories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChatMessage> ChatMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contact> Contacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversation> Conversations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomField> CustomFields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldSet> CustomFieldSets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomFieldTab> CustomFieldTabs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DocumentTemplate> DocumentTemplates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expens> Expenses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Family> Families { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlatFee> FlatFees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeForm> IntakeForms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormTemplate> IntakeFormTemplates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inventory> Inventories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransferItem> InventoryTransferItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryTransfer> InventoryTransfers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxAccount> LawToolboxAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxCourtRule> LawToolboxCourtRules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LawToolboxToolset> LawToolboxToolsets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Location> Locations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantApplication> MerchantApplications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OAuthClient> OAuthClients { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Partnership> Partnerships { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentReminder> PaymentReminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentSource> PaymentSources { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PresetProjectRate> PresetProjectRates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PriceListItem> PriceListItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PriceList> PriceLists { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductLog> ProductLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductVendor> ProductVendors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Recurrence> Recurrences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPayment> RecurringPayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Role> Roles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentItem> SaleDocumentItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentTemplate> SaleDocumentTemplates { get; set; }

        public virtual SaleDocumentTemplate SaleDocumentTemplate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesTax> SalesTaxes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SyncLog> SyncLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> Tags { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeEntry> TimeEntries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Timer> Timers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserGroup> UserGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Website> Websites { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Workflow> Workflows { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }
    }
}
