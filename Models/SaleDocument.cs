namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SaleDocument
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SaleDocument()
        {
            Activities = new HashSet<Activity>();
            Credits = new HashSet<Credit>();
            Feeds = new HashSet<Feed>();
            PendingOnlinePaymentSaleDocuments = new HashSet<PendingOnlinePaymentSaleDocument>();
            ProductLogs = new HashSet<ProductLog>();
            RecurringPaymentOccurrences = new HashSet<RecurringPaymentOccurrence>();
            SaleDocumentItems = new HashSet<SaleDocumentItem>();
            SaleDocumentPaymentReminders = new HashSet<SaleDocumentPaymentReminder>();
            SaleDocumentPayments = new HashSet<SaleDocumentPayment>();
            Shares = new HashSet<Share>();
            Shipments = new HashSet<Shipment>();
            Payments = new HashSet<Payment>();
        }

        public int Id { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public DateTime? DueDate { get; set; }

        public string TermsAndConditions { get; set; }

        public string CustomerNotes { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public long? AccountId { get; set; }

        public long? Contact_Id { get; set; }

        public long? TenantId { get; set; }

        public long? ToShippingAddress_Id { get; set; }

        public long? ToBillingAddress_Id { get; set; }

        public int Type { get; set; }

        public DateTime Date { get; set; }

        public long? ProjectId { get; set; }

        public bool IsAllowOnlinePayment { get; set; }

        public decimal DiscountPercent { get; set; }

        public Guid Guid { get; set; }

        public int Number { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public bool IsSentToCustomer { get; set; }

        public DateTime? SentToCustomerDate { get; set; }

        public bool IsApprovedByCustomer { get; set; }

        public DateTime? ApprovedByCustomerDate { get; set; }

        public DateTime? ExpDate { get; set; }

        public bool IsViewedByCustomer { get; set; }

        public DateTime? ViewedByCustomerDate { get; set; }

        public int ViewedByCustomerNumOfTimes { get; set; }

        public bool IsInvoicedToCustomer { get; set; }

        public DateTime? InvoicedToCustomerDate { get; set; }

        [StringLength(10)]
        public string CurrencyCode { get; set; }

        public bool IsNotifyWhenViewedByClient { get; set; }

        public bool IsNotifyWhenPaidByClient { get; set; }

        public bool IsNotifyWhenApprovedByClient { get; set; }

        public bool IsAllowPartialPayment { get; set; }

        [StringLength(100)]
        public string PoNumber { get; set; }

        public bool IsSendInvoicePaymentReminders { get; set; }

        public bool IsSendQuoteApprovalReminders { get; set; }

        [StringLength(100)]
        public string QuickbooksId { get; set; }

        public DateTime? LastQuickbooksSyncDate { get; set; }

        public Guid? XeroId { get; set; }

        public DateTime? LastXeroSyncDate { get; set; }

        public int InvoiceType { get; set; }

        public bool IsCarryForwardBalance { get; set; }

        public decimal AmountPaid { get; set; }

        public decimal AmountDue { get; set; }

        public decimal Sub { get; set; }

        public decimal Tax { get; set; }

        public decimal Discount { get; set; }

        public decimal Total { get; set; }

        public int Status { get; set; }

        public long? PDF_Id { get; set; }

        public bool InterestOptions_IsChargeInterest { get; set; }

        public decimal InterestOptions_AnnualPercent { get; set; }

        public int InterestOptions_InterestType { get; set; }

        public int InterestOptions_ChargeEachNumOfDays { get; set; }

        public int NumOfAutomaticInterestChargesAdded { get; set; }

        public long? SaleDocumentTemplateId { get; set; }

        public bool IsMultiProjectInvoice { get; set; }

        public bool IsApproved { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public int? ApprovedBy_Id { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        public virtual Address Address { get; set; }

        public virtual Address Address1 { get; set; }

        public virtual Blob Blob { get; set; }

        public virtual Contact Contact { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Credit> Credits { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PendingOnlinePaymentSaleDocument> PendingOnlinePaymentSaleDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductLog> ProductLogs { get; set; }

        public virtual Project Project { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecurringPaymentOccurrence> RecurringPaymentOccurrences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentItem> SaleDocumentItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentPaymentReminder> SaleDocumentPaymentReminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocumentPayment> SaleDocumentPayments { get; set; }

        public virtual SaleDocumentTemplate SaleDocumentTemplate { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Share> Shares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shipment> Shipments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
