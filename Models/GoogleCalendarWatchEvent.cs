namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GoogleCalendarWatchEvent
    {
        public long Id { get; set; }

        public string X_Goog_Channel_ID { get; set; }

        public string X_Goog_Resource_ID { get; set; }

        public string X_Goog_Resource_URI { get; set; }

        public string X_Goog_Resource_State { get; set; }

        public string X_Goog_Message_Number { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
