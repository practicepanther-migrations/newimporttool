namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SaleDocumentTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SaleDocumentTemplate()
        {
            SaleDocuments = new HashSet<SaleDocument>();
            Tenants = new HashSet<Tenant>();
        }

        public long Id { get; set; }

        public Guid Guid { get; set; }

        public int TemplateName { get; set; }

        public string CustomTemplateCss { get; set; }

        public bool SaleDocument_IsShowInvoiceTitle { get; set; }

        public string SaleDocument_InvoiceTitle { get; set; }

        public bool SaleDocument_IsShowQuoteTitle { get; set; }

        public string SaleDocument_QuoteTitle { get; set; }

        public bool SaleDocument_IsShowSaleDocumentNumber { get; set; }

        public bool SaleDocument_IsShowSaleDocumentIssueDate { get; set; }

        public string SaleDocument_SaleDocumentIssueDateText { get; set; }

        public bool SaleDocument_IsShowInvoiceDueDate { get; set; }

        public string SaleDocument_InvoiceDueDateText { get; set; }

        public bool SaleDocument_IsShowQuoteExpDate { get; set; }

        public string SaleDocument_QuoteExpDateText { get; set; }

        public bool SaleDocument_IsShowSaleDocumentReferenceNumber { get; set; }

        public string SaleDocument_SaleDocumentReferenceNumberText { get; set; }

        public bool Time_IsShowTimeItemColumn { get; set; }

        public string Time_TimeItemColumnHeaderText { get; set; }

        public bool Time_IsShowTimeDescriptionColumn { get; set; }

        public string Time_TimeDescriptionColumnHeaderText { get; set; }

        public bool Time_IsShowTimeUserColumn { get; set; }

        public string Time_TimeUserColumnHeaderText { get; set; }

        public bool Time_IsShowTimeUserInitialsInsteadOfFullName { get; set; }

        public bool Time_IsShowTimeDateColumn { get; set; }

        public string Time_TimeDateColumnHeaderText { get; set; }

        public bool Time_IsShowTimeHoursColumn { get; set; }

        public string Time_TimeHoursColumnHeaderText { get; set; }

        public bool Time_IsShowTimeRateColumn { get; set; }

        public string Time_TimeRateColumnHeaderText { get; set; }

        public bool Time_IsShowTimeSubtotalColumn { get; set; }

        public string Time_TimeSubtotalColumnHeaderText { get; set; }

        public bool Items_IsShowItemsItemColumn { get; set; }

        public string Items_ItemsItemColumnHeaderText { get; set; }

        public bool Items_IsShowItemsDescriptionColumn { get; set; }

        public string Items_ItemsDescriptionColumnHeaderText { get; set; }

        public bool Items_IsShowItemsQtyColumn { get; set; }

        public string Items_ItemsQtyColumnHeaderText { get; set; }

        public bool Items_IsShowItemsPriceColumn { get; set; }

        public string Items_ItemsPriceColumnHeaderText { get; set; }

        public bool Items_IsShowItemsSubtotalColumn { get; set; }

        public string Items_ItemsSubtotalColumnHeaderText { get; set; }

        public bool Company_IsShowCompanyLogo { get; set; }

        public bool Company_IsShowCompanyAddress { get; set; }

        public bool Company_IsShowCompanyWorkNumber { get; set; }

        public bool Company_IsShowCompanyMobileNumber { get; set; }

        public bool Company_IsShowcompanyFaxNumber { get; set; }

        public bool Company_IsShowCompanyWebsite { get; set; }

        public bool Company_IsShowCompanyEmail { get; set; }

        public bool Account_IsShowContactAddress { get; set; }

        public bool Account_IsShowContactHomeNumber { get; set; }

        public bool Account_IsshowContactOfficeNumber { get; set; }

        public bool Account_IsShowContactFaxNumber { get; set; }

        public bool Account_IsShowContactEmail { get; set; }

        public bool Project_IsShowProjectName { get; set; }

        public string Project_ProjectName { get; set; }

        public bool Summary_IsShowOnlinePaymentButton { get; set; }

        public string Summary_OnlinePaymentButtonText { get; set; }

        public bool Company_IsShowAssignedToEmail { get; set; }

        public bool Summary_IsShowTermsAndConditions { get; set; }

        public string Summary_TermsAndConditionsHeaderText { get; set; }

        public string SaleDocument_SaleDocumentNumberText { get; set; }

        public bool Time_IsShowSummary { get; set; }

        public bool Items_IsShowSummary { get; set; }

        public bool Expense_IsShowItemColumn { get; set; }

        public string Expense_ItemColumnHeaderText { get; set; }

        public bool Expense_IsShowDescriptionColumn { get; set; }

        public string Expense_DescriptionColumnHeaderText { get; set; }

        public bool Expense_IsShowUserColumn { get; set; }

        public string Expense_UserColumnHeaderText { get; set; }

        public bool Expense_IsShowUserInitialsInsteadOfFullName { get; set; }

        public bool Expense_IsShowDateColumn { get; set; }

        public string Expense_DateColumnHeaderText { get; set; }

        public bool Expense_IsShowQuantityColumn { get; set; }

        public string Expense_QuantityColumnHeaderText { get; set; }

        public bool Expense_IsShowPriceColumn { get; set; }

        public string Expense_PriceColumnHeaderText { get; set; }

        public bool Expense_IsShowSubtotalColumn { get; set; }

        public string Expense_SubtotalColumnHeaderText { get; set; }

        public bool Expense_IsShowSummary { get; set; }

        public bool Time_IsShowSummaryByUser { get; set; }

        public bool Payments_IsShowPaymentNotes { get; set; }

        public bool Payments_IsShowPaymentDate { get; set; }

        public bool Payments_IsShowFullPaymentAmount { get; set; }

        public bool Summary_IsShowTrustAccountBalance { get; set; }

        public bool Summary_IsShowTrustAccountLedger { get; set; }

        public bool Summary_IsShowOperatingAccountBalance { get; set; }

        public bool Summary_IsShowOperatingAccountLedger { get; set; }

        public int Summary_ShowAccountSummaryAndLedgerPerContactOrMatter { get; set; }

        public string Summary_TrustAccountBalanceText { get; set; }

        public string Summary_OperatingAccountBalanceText { get; set; }

        public int Summary_TrustAccountLedgerType { get; set; }

        public int Summary_OperatingAccountLedgerType { get; set; }

        public bool Project_IsShowLedesId { get; set; }

        public string Project_LedesIdName { get; set; }

        public bool Time_IsShowItemCode { get; set; }

        public bool Time_IsShowUtbmsCode { get; set; }

        public bool Expense_IsShowItemCode { get; set; }

        public bool IsDefault { get; set; }

        public string DefaultInvoiceTermsAndConditions { get; set; }

        public string DefaultQuoteTermsAndConditions { get; set; }

        public string DefaultInvoiceNotes { get; set; }

        public int DefaultInvoiceNotesPosition { get; set; }

        public int SaleDocument_Default_DueDate_Days { get; set; }

        public int SaleDocument_Default_DueDate_Option { get; set; }

        public bool SaleDocument_Default_IsSendInvoicePaymentReminders { get; set; }

        public bool SaleDocument_Default_IsSendQuoteApprovalReminders { get; set; }

        public bool SaleDocument_Default_IsUseCreditToPayBalance { get; set; }

        public bool SaleDocument_Default_IsUseTrustToPayBalance { get; set; }

        public bool SaleDocument_Default_IsAllowOnlinePayment { get; set; }

        public bool SaleDocument_Default_IsAllowPartialPayment { get; set; }

        public bool SaleDocument_Default_IsCarryForwardBalance { get; set; }

        public int SaleDocument_Default_CarryForwardBalanceType { get; set; }

        public Guid? SaleDocument_Default_Tax1Guid { get; set; }

        public Guid? SaleDocument_Default_Tax2Guid { get; set; }

        public bool DefaultInterest_IsChargeInterest { get; set; }

        public decimal DefaultInterest_AnnualPercent { get; set; }

        public int DefaultInterest_InterestType { get; set; }

        public int DefaultInterest_ChargeEachNumOfDays { get; set; }

        public long? TenantId { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        [StringLength(1000)]
        public string Name { get; set; }

        public bool Items_IsShowItemsUserColumn { get; set; }

        public bool Items_IsShowDateColumn { get; set; }

        public string Items_DateColumnHeaderText { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleDocument> SaleDocuments { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants { get; set; }
    }
}
