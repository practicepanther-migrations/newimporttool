namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Shipment
    {
        public long Id { get; set; }

        public string TrackingNumber { get; set; }

        public DateTime? ArrivalDate { get; set; }

        public long? Address_Id { get; set; }

        public long? Tenant_Id { get; set; }

        public int ShippingCarrier { get; set; }

        public decimal ShippingCost { get; set; }

        public decimal InsuranceAmount { get; set; }

        public decimal InsuranceCost { get; set; }

        public DateTime? ShippedDate { get; set; }

        public DateTime? EstimatedArrivalDate { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public long? InventoryTransfer_Id { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public int? Invoice_Id { get; set; }

        public virtual Address Address { get; set; }

        public virtual InventoryTransfer InventoryTransfer { get; set; }

        public virtual SaleDocument SaleDocument { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
