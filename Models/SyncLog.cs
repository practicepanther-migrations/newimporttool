namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SyncLog
    {
        public int SyncType { get; set; }

        public DateTime DateTime { get; set; }

        public bool Status { get; set; }

        public int? User_Id { get; set; }

        [Key]
        public Guid Guid { get; set; }

        public Guid InvocationId { get; set; }

        public long? Tenant_Id { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }
    }
}
