namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Recurrence
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Recurrence()
        {
            Activities = new HashSet<Activity>();
        }

        public long Id { get; set; }

        public int Frequency { get; set; }

        public int DayOfWeek { get; set; }

        public int DayOfMonth { get; set; }

        public int MonthOfYear { get; set; }

        public DateTime? StopDate { get; set; }

        public int? MaxNumOfOccurences { get; set; }

        public int? RepeatEvery { get; set; }

        public long TenantId { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public Guid Guid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
