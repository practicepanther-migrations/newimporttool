namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ActivitySync
    {
        public long Id { get; set; }

        public long ActivityId { get; set; }

        public int UserId { get; set; }

        [StringLength(250)]
        public string ExchangeId { get; set; }

        [StringLength(250)]
        public string GoogleId { get; set; }

        public string OutlookId { get; set; }

        public DateTime? LastGoogleSyncDate { get; set; }

        public DateTime? LastExchangeSyncDate { get; set; }

        public DateTime? LastOutlookSyncDate { get; set; }

        public virtual Activity Activity { get; set; }

        public virtual User User { get; set; }
    }
}
