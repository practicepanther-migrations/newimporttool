namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PayPal_IPN
    {
        public long Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string RequestURL { get; set; }

        public string Response { get; set; }

        public int Method { get; set; }
    }
}
