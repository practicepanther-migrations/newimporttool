namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MandrillEvent
    {
        public long Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Json { get; set; }
    }
}
