namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PaymentWebhookNotification
    {
        [Key]
        public Guid Guid { get; set; }

        public DateTime FirstReceivedDate { get; set; }

        public DateTime LastSentDate { get; set; }

        public Guid HeadnoteTransactionId { get; set; }

        [StringLength(100)]
        public string HeadnoteWebhookEvent { get; set; }

        [StringLength(100)]
        public string NotificationId { get; set; }

        public long? Payment_Id { get; set; }

        public virtual Payment Payment { get; set; }
    }
}
