namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class InventoryTransferItem
    {
        public long Id { get; set; }

        public long? InventoryTransfer_Id { get; set; }

        public decimal QtyReceived { get; set; }

        public long? Product_Id { get; set; }

        public decimal QtySent { get; set; }

        public decimal QtyLost { get; set; }

        public long? Tenant_Id { get; set; }

        public decimal QtyToTransfer { get; set; }

        public virtual InventoryTransfer InventoryTransfer { get; set; }

        public virtual Product Product { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
