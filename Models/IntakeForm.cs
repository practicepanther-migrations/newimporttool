namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class IntakeForm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public IntakeForm()
        {
            IntakeFormEntries = new HashSet<IntakeFormEntry>();
        }

        [Key]
        public Guid Guid { get; set; }

        public DateTime CreatedDate { get; set; }

        public long TenantId { get; set; }

        public long? ProjectId { get; set; }

        public long? AccountId { get; set; }

        public string CreatedByIpAddress { get; set; }

        public long? ActivityId { get; set; }

        public Guid? Template_Guid { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsDeleted { get; set; }

        public int? CreatedBy_Id { get; set; }

        public int? LastModifiedBy_Id { get; set; }

        public virtual Account Account { get; set; }

        public virtual Activity Activity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntakeFormEntry> IntakeFormEntries { get; set; }

        public virtual IntakeFormTemplate IntakeFormTemplate { get; set; }

        public virtual Project Project { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
