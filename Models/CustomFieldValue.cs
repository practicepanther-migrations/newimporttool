namespace CsvParser.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomFieldValue
    {
        public long Id { get; set; }

        public Guid Guid { get; set; }

        public long? CustomFieldId { get; set; }

        public long? AccountId { get; set; }

        public long? ProjectId { get; set; }

        public DateTime? ValueDateTime { get; set; }

        public string ValueString { get; set; }

        public decimal? ValueNumber { get; set; }

        public long? ContactId { get; set; }

        public bool ValueBoolean { get; set; }

        public long? ValueContactId { get; set; }

        public virtual Account Account { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Contact Contact1 { get; set; }

        public virtual CustomField CustomField { get; set; }

        public virtual Project Project { get; set; }
    }
}
