﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvParser.Models;
using System.Windows.Forms;

namespace CsvParser
{
    public class HelperFunctions
    {
        /// <summary>
        /// Checks the tenant setting for display name and prepares accordingly
        /// </summary>
        /// <param name="account">Account model with respective contact model already assigned.</param>
        /// <returns>display name properly formatted</returns>
        public static string GetDisplayName(Account account, int accountDisplayNameFormat)
        {
            if (account.Name == null | account.Name == "")
            {
                switch (accountDisplayNameFormat)
                {
                    // NumberFirstLast
                    case 0:
                        var numberFirstLast = account.Number.ToString() + " - " + account.Contact.FirstName + " " + account.Contact.MiddleName + " " + account.Contact.LastName;
                        account.NameAndNumber = numberFirstLast.Trim();
                        break;
                    // NumberLastFirst
                    case 1:
                        var numberLastFirst = account.Number.ToString() + " - " + account.Contact.LastName + ", " + account.Contact.FirstName + " " + account.Contact.MiddleName;
                        account.NameAndNumber = numberLastFirst.Trim();
                        break;
                    // FirstLast
                    case 2:
                        var firstLast = account.Contact.FirstName + " " + account.Contact.MiddleName + " " + account.Contact.LastName;
                        account.NameAndNumber = firstLast.Trim();
                        break;
                    // LastFirst
                    case 3:
                        var lastFirst = account.Contact.LastName + ", " + account.Contact.FirstName + " " + account.Contact.MiddleName;
                        account.NameAndNumber = lastFirst.Trim();
                        break;
                }
            }
            else
            {
                string nameAndNumber;
                switch (accountDisplayNameFormat)
                {
                    // includes number
                    case 0:
                        nameAndNumber = account.Number.ToString() + " - " + account.Name;
                        account.NameAndNumber = nameAndNumber.Trim();
                        break;
                    case 1:
                        nameAndNumber = account.Number.ToString() + " - " + account.Name;
                        account.NameAndNumber = nameAndNumber.Trim();
                        break;
                    case 2:
                        account.NameAndNumber = account.Name;
                        break;
                    case 3:
                        account.NameAndNumber = account.Name;
                        break;
                }
            }
            return account.NameAndNumber;
        }

        public static string GetDisplayName(Contact contact, int accountDisplayNameFormat)
        {
            string lastFirst;
            string firstLast;
            switch (accountDisplayNameFormat)
            {
                // First Middle Last
                case 0:
                    firstLast = contact.FirstName + " " + contact.MiddleName + " " + contact.LastName;
                    contact.DisplayName = firstLast.Trim();
                    break;
                case 2:
                    firstLast = contact.FirstName + " " + contact.MiddleName + " " + contact.LastName;
                    contact.DisplayName = firstLast.Trim();
                    break;

                // Last, First Middle
                case 1:
                    lastFirst = contact.LastName + ", " + contact.FirstName + " " + contact.MiddleName;
                    contact.DisplayName = lastFirst.Trim();
                    break;
                case 3:
                    lastFirst = contact.LastName + ", " + contact.FirstName + " " + contact.MiddleName;
                    contact.DisplayName = lastFirst.Trim();
                    break;
            }

            return contact.DisplayName;
        }

        /// <summary>
        /// Checks if provided field is a default field
        /// </summary>
        /// <param name="fieldName">Column header from CSV</param>
        /// <returns>bool</returns>
        public static bool IsDefault(string fieldName)
        {
            List<string> defaultFields = new List<string>()
            {
                "Contact: Number","Company: Name","Contact: FirstName","Contact: MiddleName","Contact: LastName","Contact: Email","Contact: HomeNumber","Contact: MobileNumber","Contact: OfficeNumber","Contact: FaxNumber","Contact: Street1","Contact: Street2","Contact: City","Contact: ProvinceState","Contact: Country","Contact: ZipPostalCode","Contact: ContactNotes","Company: CompanyNotes","Contact: Website","Contact: Tags",
                "Matter: Name","Matter: Number","Matter: Notes","Matter: Status","Matter: RateType","Matter: FlatRateAmount","Matter: ContingencyRatePercent","Matter: HourlyRateAmount","Matter: Tags","Matter: DueDate","Matter: CloseDate","Matter: OpenDate","Matter: OriginatedBy","Matter: OperatingBalance","Matter: TrustBalance"

            };

            if (defaultFields.Contains(fieldName))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// Identifies custom fields in CSV row and puts them into a collection which can be inserted into the CustomFieldValues table.
        /// </summary>
        /// <param name="row">Row of CSV file</param>
        /// <param name="customFields">CustomFields for tenant</param>
        /// <param name="accountId">Related row in Accounts table</param>
        /// <param name="contactId">Related row in Contacts table</param>
        /// <param name="projectId">Related row in Projects table</param>
        /// <returns></returns>
        public static List<CustomFieldValueField> GetCustomFieldValues(dynamic row, List<CustomField> customFields)
        {
            var rowCustomFieldValues = new List<CustomFieldValueField>();

            // get all of the fields for this row, filter out any defaults
            // to leave only the custom fields
            foreach (var field in row) // thanks Vlad
            {
                if (!IsDefault(field.Key))
                {
                    // continue to next field if there is no value
                    if (field.Value == "")
                    {
                        continue;
                    }
                    else
                    {
                        var fieldSplit = field.Key.Split(':');
                        var objectType = fieldSplit[0].ToLower().Trim();
                        string fieldName = null;
                        try
                        {
                            fieldName = fieldSplit[1].Trim();
                        }

                        // skip if there is no : in the name to split by
                        catch (IndexOutOfRangeException)
                        {
                            continue;
                        }

                        int ObjectType = 0;
                        switch (objectType)
                        {
                            case "company":
                                ObjectType = 1;
                                break;

                            case "matter":
                                ObjectType = 2;
                                break;

                            case "contact":
                                ObjectType = 3;
                                break;
                        }

                        var cf = customFields.FirstOrDefault(c => c.ObjectType == ObjectType && c.Name == fieldName);
                        if (cf != null)
                        {
                            var cfv = new CustomFieldValueField(cf, field.Value);
                            rowCustomFieldValues.Add(cfv);
                        }
                        //try
                        //{
                        //    var cf = DbFunctions.customFields.FirstOrDefault(c => c.ObjectType == ObjectType && c.Name == fieldName);
                        //    if (cf != null)
                        //    {
                        //        var cfv = new CustomFieldValueField(cf, field.Value);
                        //        rowCustomFieldValues.Add(cfv);
                        //    }

                        //}
                        //catch (Exception)
                        //{
                        //    continue;
                        //}
                    }
                }
                else
                {
                    continue;
                }
            }
            return rowCustomFieldValues;
        }

        /// <summary>
        /// Convert all empty strings to null
        /// </summary>
        /// <param name="csvFields">Fields which need cleaning</param>
        /// <returns></returns>
        public static Object Clean(Object csvFields)
        {
            var isAllNull = true;
            Type contactFieldType = csvFields.GetType();
            var contactFieldsProperties = contactFieldType.GetProperties();

            // go through each property and check if the property actually has a value.
            foreach (var prop in contactFieldsProperties)
            {
                var propValue = prop.GetValue(csvFields);
                // just continue if value is null
                if (propValue is null)
                {
                    continue;
                }
                else
                {
                    // change isAllNull to false if value exists.
                    if (!String.IsNullOrWhiteSpace(propValue.ToString()))
                    {
                        isAllNull = false;
                        continue;
                    }
                    else
                    {
                        // remove "" values since "" technically is not null
                        prop.SetValue(csvFields, null);
                    }
                }
            }

            if (isAllNull)
            {
                return null;
            }

            else
            {
                return csvFields;
            }
        }

        /// <summary>
        /// Ensures that all headers are present in the CSV
        /// </summary>
        /// <param name="headers">Column names from CSV file</param>
        /// <param name="importType">Can pass Events, Tasks, Notes</param>
        public static void ValidateHeaders(List<string> headers, ImportType importType)
        {
            List<string> missingHeader = new List<string>();

            switch (importType)
            {
                case ImportType.Events:
                    List<string> eventHeaders = new List<string>()
                    { "ContactNumber", "MatterNumber", "StartDateTime", "EndDateTime",
                        "IsAllDay (Yes/No)", "Subject", "Description",
                        "Assigned To (Email Address)", "Tags", "Location" };
                    foreach (var eventHeaderRow in eventHeaders)
                    {
                        if (!headers.Exists(h => h == eventHeaderRow))
                        {
                            missingHeader.Add(eventHeaderRow);
                        }
                    }
                    break;
                case ImportType.Tasks:
                    List<string> taskHeaders = new List<string>()
                    { "ContactNumber", "MatterNumber", "DueDate", "Subject", "Description", "Status (Completed/NotCompleted/InProgress)",
                        "Assigned To (Email Address)", "Tags", "Location" };
                    foreach (var taskHeaderRow in taskHeaders)
                    {
                        if (!headers.Exists(h => h == taskHeaderRow))
                        {
                            missingHeader.Add(taskHeaderRow);
                        }
                    }
                    break;
                case ImportType.Notes:
                    List<string> noteHeaders = new List<string>()
                    { "ContactNumber", "MatterNumber", "Date", "Subject", "Description", "User (Email Address)", "Tags"};
                    foreach (var noteHeaderRow in noteHeaders)
                    {
                        if (!headers.Exists(h => h == noteHeaderRow))
                        {
                            missingHeader.Add(noteHeaderRow);
                        }
                    }
                    break;
                case ImportType.TimeEntries:
                    List<string> timeHeaders = new List<string>()
                    { "MatterNumber", "Date", "Hours", "Rate", "Description", "Status (Billable/Billed/NotBillable)", 
                      "Billed By (Email Address)", "ItemCode"};
                    foreach (var timeHeaderRow in timeHeaders)
                    {
                        if (!headers.Exists(h => h == timeHeaderRow))
                        {
                            missingHeader.Add(timeHeaderRow);
                        }
                    }
                    break;
                case ImportType.Expenses:
                    List<string> expenseHeaders = new List<string>()
                    { "MatterNumber", "Quantity", "Price", "Date", "Description", "Status (Billable/Billed/NotBillable)", "Billed By (Email Address)"};
                    foreach (var expenseHeaderRow in expenseHeaders)
                    {
                        if (!headers.Exists(h => h == expenseHeaderRow))
                        {
                            missingHeader.Add(expenseHeaderRow);
                        }
                    }
                    break;
                case ImportType.FlatFee:
                    List<string> feeHeaders = new List<string>()
                    { "MatterNumber", "Quantity", "Price", "Date", "Description", "Status (Billable/Billed/NotBillable)", "Billed By (Email Address)", "ItemCode"};
                    foreach (var feeHeaderRow in feeHeaders)
                    {
                        if (!headers.Exists(h => h == feeHeaderRow))
                        {
                            missingHeader.Add(feeHeaderRow);
                        }
                    }
                    break;
                case ImportType.Relationships:
                    List<string> relHeaders = new List<string>()
                    { "ContactNumber", "MatterNumber", "RelationshipName", "RelationshipNotes"};
                    foreach (var feeHeaderRow in relHeaders)
                    {
                        if (!headers.Exists(h => h == feeHeaderRow))
                        {
                            missingHeader.Add(feeHeaderRow);
                        }
                    }
                    break;
            }

            if(missingHeader.Any())
            {
                var message = string.Join(", ", missingHeader);
                MessageBox.Show(text:"The following headers are missing or mistyped: " + message + ".",caption:"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// Maps columns of DataTable to columns of the database table targeted by the SqlBulkCopy,
        /// and then asynchronoulsy writes the DataTable to the targeted table
        /// </summary>
        /// <param name="sbc">Must already target a table in the database</param>
        /// <param name="dt">Column names must match database table column names exactly</param>
        /// <returns></returns>
        public static async Task AutoMapColumnsAndBulkInsertAsync(SqlBulkCopy sbc, DataTable dt)
        {
            var taskGuid = Guid.NewGuid();
            Console.WriteLine("\nInserting rows into table " + sbc.DestinationTableName + "\t\tTask ID: " + taskGuid);

            foreach (DataColumn column in dt.Columns)
            {
                sbc.ColumnMappings.Add(column.ColumnName, column.ColumnName);
            }
            await sbc.WriteToServerAsync(dt);
            Console.WriteLine("\nFinished inserting rows into table " + sbc.DestinationTableName + "\t\tTask ID: " + taskGuid);
        }

        public static DataTable GetDataTable(string tableName)
        {
            var activityIdColumn = new DataColumn();
            switch (tableName)
            {
                case "ActivityUsers":
                    var activityUserDataTable = new DataTable();
                    //var activityIdColumn = new DataColumn();
                    activityIdColumn.ColumnName = "Activity_Id";
                    activityIdColumn.DataType = System.Type.GetType("System.Int64");
                    activityUserDataTable.Columns.Add(activityIdColumn);
                    var userIdColumn = new DataColumn();
                    userIdColumn.ColumnName = "User_Id";
                    userIdColumn.DataType = System.Type.GetType("System.Int64");
                    activityUserDataTable.Columns.Add(userIdColumn);
                    return activityUserDataTable;
                case "TagActivities":
                    var activityTagDataTable = new DataTable();
                    //var activityIdColumn = new DataColumn();
                    activityIdColumn.ColumnName = "Activity_Id";
                    activityIdColumn.DataType = System.Type.GetType("System.Int64");
                    activityTagDataTable.Columns.Add(activityIdColumn);
                    var tagIdColumn = new DataColumn();
                    tagIdColumn.ColumnName = "Tag_Id";
                    tagIdColumn.DataType = System.Type.GetType("System.Int64");
                    activityTagDataTable.Columns.Add(tagIdColumn);
                    return activityTagDataTable;
                default:
                    throw new Exception();
            }
        }

        public static Tag ParseTags(string tags, int tagType)
        {
            var newTag = new Tag();
            ActivityGuidUserTag Tags = new ActivityGuidUserTag();
            if (!String.IsNullOrWhiteSpace(tags))
            {
                var tagsList = tags.Split(',');

                foreach (var tagName in tagsList)
                {
                    var ppTag = DatabaseFunctions.PpTags.FirstOrDefault(t => t.Name.ToLower() == tagName.ToLower() && t.TagFor == tagType);
                    if (ppTag is null)
                    {
                        newTag.Name = tagName;
                        newTag.Guid = Guid.NewGuid();
                        newTag.TagFor = tagType;
                        newTag.TenantId = DatabaseFunctions.FirmTenant.Id;
                        newTag.CreatedDate = DateTime.Now;
                        newTag.CreatedById = DatabaseFunctions.FirmTenant.PrimaryUserId;
                        newTag.LastModifiedById = DatabaseFunctions.FirmTenant.PrimaryUserId;
                        newTag.LastModifiedDate = DateTime.Now.ToUniversalTime();
                        newTag.IsEnabled = true;
                        newTag.IsDeleted = false;

                        // update in memory tags
                        DatabaseFunctions.PpTags.Add(newTag);
                    }
                    else
                    {
                        // store the Id to update the AccountTags table once the context is open
                        Tags.TagIds.Add(ppTag.Id);
                        newTag = null;
                    }
                }
            }
            else
            {
                newTag = null;
            }
            return newTag;
        }

    }
}
