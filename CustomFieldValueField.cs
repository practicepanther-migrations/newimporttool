﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvParser.Models;

namespace CsvParser
{
    public class CustomFieldValueField
    {
        /// <summary>
        /// Roughly represents the CustomFieldValues table
        /// </summary>
        /// <param name="customField">Id for related row in CustomField table</param>
        /// <param name="value">Actual value for this CustomField</param>
        /// <param name="accountId">Id for linked row in Accounts table, pass 0 if field is for Projects or Contacts</param>
        /// <param name="projectId">Id for linked row in Projects table, pass 0 if field is for Accounts or Contacts</param>
        /// <param name="contactId">Id for linked row in Contacts table, pass 0 if field is for Accounts or Projects</param>
        public CustomFieldValueField(CustomField customField, string value)
        {
            CustomField = customField;
            /*
            Copied from the CustomField model in PP.

            Only one value per row, cannot have both a ValueString and ValueDate

            public enum CustomFieldValueType
                {
                    TextBox = 1,
                    Date = 2,
                    DateTime = 3,
                    Number = 4,
                    Currency = 5,
                    TextEditor = 6,
                    DropDownList = 7,
                    Checkbox = 8,
                    Contact = 9
                }
             */

            // assign value to the proper field
            switch (customField.ValueType)
            {
                case 1:
                    ValueString = value;
                    break;
                case 6:
                    ValueString = value;
                    break;
                case 7:
                    ValueString = value;
                    break;
                case 2:
                    ValueDateTime = Convert.ToDateTime(value);
                    break;
                case 3:
                    ValueDateTime = Convert.ToDateTime(value);
                    break;
                case 4:
                    ValueNumber = decimal.Parse(value);
                    break;
                case 5:
                    ValueNumber = decimal.Parse(value);
                    break;
                case 8:
                    ValueBoolean = bool.Parse(value.ToLower());
                    break;
                case 9:
                    ValueContactId = DatabaseFunctions.GetContactId(value);
                    break;
            }

            // assign linked Account, Project, or Contact
            /*switch (customField.ObjectType)
            {
                case (1):
                    AccountId = accountId;
                    break;
                case (2):
                    ProjectId = projectId;
                    break;
                case (3):
                    ContactId = contactId;
                    break;
            }*/
        }

        public CustomField CustomField { get; set; }
        public long? AccountId { get; set; }
        public long? ContactId { get; set; }
        public long? ProjectId { get; set; }
        public long? ValueContactId { get; set; }
        public DateTime? ValueDateTime { get; set; }
        public string ValueString { get; set; }
        public decimal? ValueNumber { get; set; }
        public bool? ValueBoolean { get; set; }
    }
}
